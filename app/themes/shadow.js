import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#d4d4d4',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 3,
    shadowOpacity: 0.2
  }
})

export default styles
