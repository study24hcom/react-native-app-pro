import { SHAPE, TEXT } from 'constants/color'
import { isIos } from '../utils'
import { Platform } from 'react-native'

export const tabBarStyle = {
  activeTintColor: TEXT.PRIMARYBOLD,
  inactiveTintColor: SHAPE.GRAYBOLD,
  style: {
    backgroundColor: '#ffffff',
    paddingBottom: isIos() ? 5 : 0,
    borderTopColor: '#eee',
    paddingTop: 5,
    paddingLeft: 0,
    paddingRight: 0,
    height: 55
  },
  ...Platform.select({
    ios: {},
    android: {
      iconStyle: {
        top: -15,
        width: 45,
        height: 45
      }
    }
  }),
  labelStyle: {
    fontFamily: 'System',
    fontSize: isIos() ? 12 : 8,
    position: 'relative',
    top: isIos() ? 0 : -25,
    fontWeight: 'bold'
  },
  showIcon: true,
  indicatorStyle: {
    backgroundColor: SHAPE.PRIMARY
  }
}

export default {
  tabBarStyle
}
