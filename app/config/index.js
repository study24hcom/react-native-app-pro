import {Text} from 'react-native'
import DebugConfig from './DebugConfig'
import AppConfig from './AppConfig'

// Allow/disallow font-scaling in app
Text.defaultProps.allowFontScaling = AppConfig.allowTextFontScaling

if (__DEV__) {
  // If ReactNative's yellow box warnings are too much, it is possible to turn
  // it off, but the healthier approach is to fix the warnings.  =)
  console.disableYellowBox = !DebugConfig.yellowBox
}

export const API_RES_AUTH = 'https://auth-api.tungtung.vn'
export const API_QUIZ = 'https:quiz-api.tungtung.vn'
export const API_MEDIA = 'https://media.tungtung.vn'
export const API_QUIZ_LATEX = 'https://quiz-latex.tungtung.vn'
export const API_STATISTIC = 'https://statistics-api.tungtung.vn'
export const API_PUSH = 'https://push-api.tungtung.vn'
export const TUNGTUNG_BROWSER_PUBLIC = 'https://tungtung.vn'
export const MOBILE_API_QUIZ = 'https://quiz-mobile-api.tungtung.vn'
export const SEARCH_API = 'https://search-api.tungtung.vn'
export const MANAGER_API = 'https://manager-api.tungtung.vn'
export const FACEBOOK_APP_ID = '609154359261757'
export const GOOGLE_ANALYTIC_ID = 'UA-57884672-22'

export const FIREBASE = {
  apiKey: 'AIzaSyAVz0RiwUe6dnZE9tvnzgr0R7NkOUKyG4o',
  authDomain: 'tungtung-141004.firebaseapp.com',
  databaseURL: 'https://tungtung-141004.firebaseio.com',
  projectId: 'tungtung-141004',
  storageBucket: 'tungtung-141004.appspot.com',
  messagingSenderId: '124852232304'
}

export const SITE_INFO = {
  PUBLIC_URL: 'http://tungtung.vn',
  AUTHOR: 'tungtung.vn',
  DEFAULT_TITLE: 'Ứng dụng tạo và chia sẻ đề thi trắc nghiệm trực tuyến',
  AFTER_TITLE: 'tungtung.vn',
  DESCRIPTION: 'Tạo và chia sẻ đề thi trắc nghiệm trực tuyến, ôn luyện tiếng anh, ôn thi THPT quốc gia, interview, TOEIC',
  KEYWORDS: 'trắc nghiệm trực tuyến, đề thi trắc nghiệm, trắc nghiệm tiếng anh, ôn thi THPT quốc gia, ôn luyện đại học, interview, test'
}
