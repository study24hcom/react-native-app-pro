import React, {PureComponent} from 'react'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'
import {Dimensions} from 'react-native'

const window = Dimensions.get('window')

const Featured = styled.Image`
  flex: 1;
  height: 150;
  border-radius: 3px;
  resizeMode: cover;
  `
const View = styled.View`flex: 1`
export default class FeaturedImage extends PureComponent {
  static propTypes = {
    image: PropTypes.string
  }

  render () {
    const {image} = this.props
    return (
      <View>
        {image !== '' && <Featured source={{uri: image}} style={{
          width: window.width}} />}
      </View>
    )
  }
}
