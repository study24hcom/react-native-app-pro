import React, {PureComponent} from 'react'
import {TextInput} from 'react-native'
import {autobind} from 'core-decorators'
import styled from 'styled-components/native'
import {INPUT, SHAPE, TEXT} from 'constants/color'
import {SIZE} from 'constants/font'

const Input = styled(TextInput)`
  padding-horizontal: 12px;
  padding-vertical: 12px;
  font-family: OpenSans-Regular;
  font-weight: 500;
  font-size: ${SIZE.NORMAL};
  color: ${TEXT.NORMAL}
  border-bottom-width: 1px;
  border-color: ${props =>
  props.isFocus ? `${INPUT.FOCUS}` : `${INPUT.BORDER}`};
  background-color: #ffffff;
  border-radius: 4px;
`

@autobind()
export default class InputProfile extends PureComponent {
  state = {
    isFocus: false
  }

  _handleFocus (e) {
    this.setState({isFocus: true})
    if (this.props.onFocus) this.props.onFocus(e)
  }

  _handleBlur (e) {
    this.setState({isFocus: false})
    if (this.props.onBlur) this.props.onBlur(e)
  }

  render () {
    return (
      <Input
        autoCapitalize='none'
        {...this.props}
        isFocus={this.state.isFocus}
        underlineColorAndroid={'transparent'}
        autoCorrect={false}
        onFocus={this._handleFocus}
        onBlur={this._handleBlur}
        placeholderTextColor={SHAPE.GRAYTEXT}
      />
    )
  }
}
