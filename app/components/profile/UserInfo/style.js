import React from 'react'
import {StyleSheet} from 'react-native'
import styled from 'styled-components/native'
import {SIZE} from 'constants/font'
import {SHAPE} from 'constants/color'
import Text from 'components/elements/text'

const HeaderContainer = styled.View`
  background-color: #ffffff;
  flex: 1;
  padding-bottom: 16px;
`
const UserWrapper = styled.View`
  flex-direction: row;
  padding-vertical: 8px;
  justify-content: center;
  `

const UserAvatarWrapper = styled.View`
  padding-horizontal: 16px;
  flex-direction: row;
  align-items: center;
  `
const QuizListWrapper = styled.View`
  flex-direction: row;
  flex: 1;
  `
const InfoQuizList = styled.View`
  flex: 1;
  padding-horizontal: 8px;
  `
const FacebookButton = styled.TouchableOpacity`
  background-color: ${SHAPE.FACEBOOK};
  padding: 8px;
  border-radius: 2px;
  margin-top: 8px;
`
const Username = styled(Text)`
  font-size: ${SIZE.BIG};
  padding-vertical: 4px;
  color: ${SHAPE.PRIMARYBOLD}
`

const Description = styled(Text)`
  padding: 4px 16px;
  color: ${SHAPE.GRAYTEXT};
  font-size: ${SIZE.HEADING6}
  `
const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#E8E8E8',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  }
})

const DetailContainerTouch = styled.TouchableOpacity`
  align-items: center;
  justify-content: center;
  ${props => props.flex && `flex: ${props.flex}`}
`

const DetailContainer = styled.View`
  align-items: center;
  justify-content: center;
  ${props => props.flex && `flex: ${props.flex}`}
  `
const Number = styled(Text)`
  padding-bottom: 4px;
  font-weight: 700;
  color: ${props => props.color};
  `
const TextAd = styled.Text`
  color: #fff;
  font-size: 15px;
  `

const TextView = styled.View`
  border-radius: 4px;
  background-color: ${SHAPE.RED};
  padding-horizontal: 5px;
  margin-left: 4px;
`
export const TextAdView = ({children}) => (
  <TextView>
    <TextAd>{children}</TextAd>
  </TextView>
)
export {
  HeaderContainer,
  UserWrapper,
  styles,
  Username,
  Description,
  QuizListWrapper,
  UserAvatarWrapper,
  DetailContainerTouch,
  DetailContainer,
  Number,
  InfoQuizList,
  FacebookButton
}
