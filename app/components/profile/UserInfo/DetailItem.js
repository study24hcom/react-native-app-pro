import React from 'react'
import PropTypes from 'prop-types'
import Text from '../../elements/text/index'
import {TEXT} from 'constants/color'
import {SIZE} from 'constants/font'
import {
  DetailContainerTouch,
  DetailContainer,
  Number
} from './style'

export default class DetailItem extends React.Component {
  static propTypes = {
    onPressItem: PropTypes.func,
    total: PropTypes.number,
    title: PropTypes.string,
    isPress: PropTypes.bool,
    flex: PropTypes.number,
    color: PropTypes.string
  }

  render () {
    if (this.props.isPress) {
      return (
        <DetailContainerTouch flex={this.props.flex} activeOpacity={0.5} onPress={this.props.onPressItem}>
          <Number color={this.props.color} fontSize={SIZE.HEADING1}>{this.props.total}</Number>
          <Text numberOfLines={1} fontSize={SIZE.HEADING7} padding={'0 4px'} color={TEXT.GRAY}>{this.props.title}</Text>
        </DetailContainerTouch>
      )
    }
    return (
      <DetailContainer flex={this.props.flex}>
        <Number fontSize={SIZE.HEADING1} color={this.props.color}>{this.props.total}</Number>
        <Text numberOfLines={1} fontSize={SIZE.HEADING7} padding={'0 4px'} color={TEXT.GRAY}>{this.props.title}</Text>
      </DetailContainer>
    )
  }
}
