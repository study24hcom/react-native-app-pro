import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Linking} from 'react-native'
import UserAvatar from 'components/elements/user-avatar/avatar'
import PlaceholderPlaylist from '../../placeholder'
import DetailItem from './DetailItem'
import {
  HeaderContainer, UserWrapper, styles, Username, Description, QuizListWrapper,
  UserAvatarWrapper, InfoQuizList, FacebookButton, TextAdView
} from './style'
import TextIcon from 'components/elements/text-icon/index'
import {SIZE} from 'constants/font'
import {autobind} from 'core-decorators'
import {SHAPE} from '../../../constants/color'

@autobind
export default class UserInfo extends Component {
  static propTypes = {
    isMe: PropTypes.bool,
    avatar: PropTypes.string,
    username: PropTypes.string,
    fullname: PropTypes.string,
    facebookID: PropTypes.string,
    facebook: PropTypes.string,
    totalQuizlist: PropTypes.number,
    totalPlaylist: PropTypes.number,
    totalFollower: PropTypes.number,
    description: PropTypes.string,
    onPressSetting: PropTypes.func,
    onPressQuizList: PropTypes.func,
    onPressPlaylist: PropTypes.func,
    onPressUserFollow: PropTypes.func,
    isLoading: PropTypes.bool,
    isPartner: PropTypes.bool,
    admin: PropTypes.bool
  }

  onPressFacebookUser () {
    if (this.props.facebookID) {
      const link = `https://facebook.com/${this.props.facebookID}`
      Linking.canOpenURL(link).then(supported => {
        supported && Linking.openURL(link)
      }, (err) => console.log(err))
    } else if (this.props.facebook) {
      const link = `${this.props.facebook}`
      Linking.canOpenURL(link).then(supported => {
        supported && Linking.openURL(link)
      }, (err) => console.log(err))
    }
  }

  render () {
    if (this.props.isLoading) {
      return <PlaceholderPlaylist height={180} />
    }
    return (
      <HeaderContainer style={styles.shadow}>
        <UserWrapper>
          <UserAvatarWrapper>
            <UserAvatar avatar={this.props.avatar} username={this.props.username} avatarSize={80} />
          </UserAvatarWrapper>
          <InfoQuizList>
            <QuizListWrapper>
              <DetailItem color={SHAPE.RED} flex={1} isPress={/* this.props.totalQuizlist > 0 */ false} title={'đề thi'}
                onPressItem={this.props.onPressQuizList} total={this.props.totalQuizlist} />
              <DetailItem color={SHAPE.PINK} flex={1.6} isPress={/* this.props.totalPlaylist > 0 */ false} title={'bộ sưu tập'}
                onPressItem={this.props.onPressPlaylist} total={this.props.totalPlaylist} />
              <DetailItem color={SHAPE.ORANGE} flex={1.9} isPress={/* this.props.totalFollower > 0 */ false} title={'người theo dõi'}
                onPressItem={this.props.onPressPlaylist} total={this.props.totalFollower} />
            </QuizListWrapper>
            {(this.props.facebookID || this.props.facebook)
              ? <FacebookButton onPress={this.onPressFacebookUser}>
                <TextIcon color={'#fff'} size={SIZE.BIG} center icon={'facebook'}>{this.props.fullname}</TextIcon>
              </FacebookButton> : null}
          </InfoQuizList>
        </UserWrapper>
        <UserAvatarWrapper>
          <Username>{`@${this.props.username}`}</Username>
          {this.props.isPartner && !this.props.admin && <TextAdView>P</TextAdView>}
          {this.props.admin && <TextAdView>Ad</TextAdView>}
          {/* {(this.props.isPartner || this.props.admin) && */}
          {/* <Icon name={'check-circle'} size={16} color={SHAPE.PRIMARYBOLD} />} */}
        </UserAvatarWrapper>
        {this.props.description && <Description>{this.props.description}</Description>}
      </HeaderContainer>
    )
  }
}
