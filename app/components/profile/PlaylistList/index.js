import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {FlatList} from 'react-native'
import {autobind} from 'core-decorators'
import PlaylistItem from 'components/playlist/playlist-item'
import HeadingLine from 'components/elements/heading-line'
import BoxAlert from 'components/elements/box-alert/index'
import PlaceholderPlaylist from 'components/placeholder'
import {playlist} from 'constants/description'

const PlaylistContainer = styled.View`
  flex: 1;
`

@autobind
export class PlaylistList extends PureComponent {
  static propTypes = {
    data: PropTypes.array
  }

  renderItem ({item}) {
    return <PlaylistItem horizontal onPressItem={this.props.onPressItem} playlistItem={item} {...item} />
  }

  render () {
    return (
      <FlatList
        data={this.props.data}
        horizontal
        inverted={false}
        keyExtractor={item => item.id}
        renderItem={this.renderItem}
        pagingEnabled
        showsHorizontalScrollIndicator={false}
      />
    )
  }
}

export default class PlaylistBox extends PureComponent {
  static propTypes = {
    data: PropTypes.array,
    onPressViewAll: PropTypes.func,
    pagination: PropTypes.shape({
      totalItem: PropTypes.number
    }),
    onPressItem: PropTypes.func,
    isMe: PropTypes.bool
  }

  isShow () {
    return this.props.data.length > 0 && !this.props.isLoadingFirst
  }

  render () {
    return (
      <PlaylistContainer>
        <HeadingLine description={this.props.isMe ? playlist.byMe : playlist.byUser}
          hideTotalItem totalItem={this.props.pagination.totalItem} title='Bộ sưu tập đã tạo'
          isShow={this.isShow()} onPress={this.props.onPressViewAll} />
        {this.props.isLoadingFirst && <PlaceholderPlaylist margin={10} height={180} />}
        {this.isShow() &&
        <PlaylistList onPressItem={this.props.onPressItem} data={this.props.data.slice(0, 5)} />}
        {!this.props.isLoadingFirst && this.props.data.length === 0 && !this.props.isMe &&
        <BoxAlert content='User này chưa có bộ sưu tập nào' />}
        {!this.props.isLoadingFirst && this.props.data.length === 0 && this.props.isMe &&
        <BoxAlert content='Bạn chưa có bộ sưu tập nào' />}
      </PlaylistContainer>
    )
  }
}
