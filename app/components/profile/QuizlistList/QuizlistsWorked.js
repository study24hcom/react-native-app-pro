import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {FlatList} from 'react-native'
import {autobind} from 'core-decorators'
import QuizListItem from 'components/quizlist/quizlist-item'
import HeadingLine from 'components/elements/heading-line'
import BoxAlert from 'components/elements/box-alert'
import PlaceholderPlaylist from 'components/placeholder'
import {quizList} from '../../../constants/description'

const QuizlistListContainer = styled.View`
  flex: 1
`

@autobind
export class QuizlistList extends PureComponent {
  static propTypes = {
    data: PropTypes.array
  }

  renderItem ({item}) {
    return <QuizListItem horizontal isPress onPress={this.props.onPressItem} {...item} />
  }

  render () {
    return (
      <FlatList
        data={this.props.data}
        horizontal
        inverted={false}
        keyExtractor={item => item.id}
        renderItem={this.renderItem}
        pagingEnabled
        showsHorizontalScrollIndicator={false}
      />
    )
  }
}

export default class QuizlistsWorked extends PureComponent {
  static propTypes = {
    data: PropTypes.array,
    isMe: PropTypes.bool,
    onPressViewAll: PropTypes.func,
    pagination: PropTypes.shape({
      totalItem: PropTypes.number
    }),
    onPressItem: PropTypes.func
  }

  isShow () {
    return this.props.data.length > 0 && !this.props.isLoadingFirst
  }

  render () {
    return (
      <QuizlistListContainer>
        <HeadingLine
          description={this.props.isMe ? quizList.worked.byMe : quizList.worked.byUser}
          totalItem={this.props.pagination.totalItem} isShow={this.isShow()}
          hideTotalItem title='Đề thi đã làm' onPress={this.props.onPressViewAll} />
        {this.props.isLoadingFirst &&
        <PlaceholderPlaylist margin={10} height={180} />}
        {this.isShow() &&
        <QuizlistList onPressItem={this.props.onPressItem}
          data={this.props.data.slice(0, 5)} />}
        {!this.props.isLoadingFirst && this.props.data.length === 0 && this.props.isMe &&
        <BoxAlert content='Bạn chưa làm đề thi nào' />}
        {!this.props.isLoadingFirst && this.props.data.length === 0 && !this.props.isMe &&
        <BoxAlert content='User này chưa làm đề thi nào' />}
      </QuizlistListContainer>
    )
  }
}
