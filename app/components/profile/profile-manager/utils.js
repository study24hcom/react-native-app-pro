export function convertStateToInitialValuesProfile ({ auth: { user } }) {
  console.log(user)
  return {
    fullname: user.fullname,
    gender: user.gender,
    birthday: user.birthday,
    description: user.description,
    facebook: user.facebook
  }
}

export function convertFormValuesToApi (values) {
  return {
    fullname: values.fullname,
    gender: typeof values.gender === 'object'
      ? values.gender.value
      : values.gender,
    biography: values.description,
    birthday: values.birthday,
    facebook: values.facebook
  }
}
