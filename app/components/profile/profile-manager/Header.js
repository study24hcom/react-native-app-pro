import React from 'react'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'
import {ActivityIndicator} from 'react-native'
import {autobind} from 'core-decorators'
import NavigationBar from 'react-native-navbar'
import Text from 'components/elements/text/index'
import {SHAPE} from 'constants/color'

const Button = styled.TouchableOpacity`
  height: 45px;
  justify-content: center;
  align-items: center;
  border-radius: 22.5px;
  padding-horizontal: 16px;
  `
@autobind
export default class HeaderNav extends React.PureComponent {
  static propTypes = {
    onPressSubmit: PropTypes.func,
    onPressGoBack: PropTypes.func,
    submitting: PropTypes.bool
  }

  renderRight () {
    return <Button activeOpacity={0.7} onPress={this.props.onPressSubmit}>
      {this.props.submitting && <ActivityIndicator color='#fff' />}
      {!this.props.submitting && <Text fontWeight={600} color='#ffffff'>Xong</Text>}
    </Button>
  }

  renderLeft () {
    return (<Button activeOpacity={0.7} onPress={this.props.onPressGoBack}>
      <Text fontWeight={400} color='#ffffff'>Hủy</Text>
    </Button>
    )
  }

  render () {
    return (
      <NavigationBar
        statusBar={{style: 'light-content'}}
        containerStyle={{backgroundColor: SHAPE.PRIMARY}}
        style={{backgroundColor: SHAPE.PRIMARY}}
        leftButton={this.renderLeft()}
        rightButton={this.renderRight()}
        title={{title: `Cập nhật thông tin`, style: {color: '#ffffff'}}}
      />
    )
  }
}
