import React from 'react'
import styled from 'styled-components/native'
import {Dimensions} from 'react-native'
import {SHAPE} from 'constants/color'

const {width: windowWidth} = Dimensions.get('window')

export const ScreenImageContainer = styled.ImageBackground`
  flex: 1;
`

export const RegisterImageContainer = styled.ImageBackground`
  flex: 1;
  `

export const RegisterImage = ({children}) => (
  <RegisterImageContainer
    source={require('../../../../assets/home/background.png')}
    resizeMode='cover'>
    {children}
  </RegisterImageContainer>
)

export const ScreenImage = ({children}) => (
  <ScreenImageContainer
    source={require('../../../../assets/home/background.png')}
    resizeMode='cover'
  >
    {children}
  </ScreenImageContainer>
)

export const KeyboardWrapper = styled.KeyboardAvoidingView`
  flex: 1;
`
export const LogoWrapper = styled.View`
  align-items: center;
  justify-content: center;
  padding-top: 24px;
  flex: 1;
  background-color: ${SHAPE.PRIMARY};
`

export const FormWrapper = styled.View`
  padding-horizontal: 8px;
  padding-vertical: 20px;
  flex: 3;
`

export const FormContainer = styled.View`
  overflow: hidden;
`

export const Button = styled.TouchableOpacity`
  padding: 16px 16px;
  justify-content: center;
  background-color: ${SHAPE.PRIMARYBOLD};
  align-items: center;
  border-radius: 8px;
`

export const AbsoluteBottom = styled.TouchableOpacity`
  position: absolute;
  bottom: 0px;
  left: 0px;
  right: 0px;
  padding: 16px 16px;
  justify-content: center;
  align-items: center;
`

const LOGO_WIDTH = windowWidth - 64

export const LogoStyle = styled.Image`
  width: ${LOGO_WIDTH}px;
  height: ${123 * LOGO_WIDTH / 779}px;
`

export const LogoWhite = () => (
  <LogoStyle source={require('../../../../assets/home/logo-white.png')} />
)
