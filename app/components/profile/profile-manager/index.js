import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {createValidateComponent} from 'components/elements/redux-form-validate'
import {reduxForm, Field} from 'redux-form'
import InputDateTime from 'components/elements/input-date-time'
import InputLabel from 'components/elements/input-label'
import InputSelect from 'components/elements/input-select'
import Clearfix from '../../elements/clearfix/index'
import {View, ScrollView} from 'react-native'
import HeaderNav from './Header'
import {withNavigation} from 'react-navigation'
import {autobind} from 'core-decorators'

const FInputLabel = createValidateComponent(InputLabel)
const FInputIcon = createValidateComponent(InputLabel)
const FInputSelect = createValidateComponent(InputSelect)
const FInputDateTime = createValidateComponent(InputDateTime)

const GENDER_OPTIONS = [
  {
    value: 'male',
    label: 'Nam'
  },
  {
    value: 'female',
    label: 'Nữ'
  }
]
@withNavigation
@reduxForm({
  form: 'profileInfo'
})
@autobind
export default class ProfileInformationForm extends PureComponent {
  static propTypes = {
    isAuthenticated: PropTypes.bool
  }

  onPressGoBack () {
    this.props.navigation.goBack(null)
  }

  render () {
    const {handleSubmit, submitting} = this.props
    return (
      <View style={{flex: 1}}>
        <HeaderNav onPressGoBack={this.onPressGoBack} submitting={submitting}
          onPressSubmit={handleSubmit(this.props.onSubmit)} />
        <ScrollView style={{backgroundColor: '#fff', flex: 1, padding: 16}}>
          <Field
            name='fullname'
            label='Tên'
            placeholder='Họ và tên'
            component={FInputIcon}
          />
          <Clearfix height={16} />
          <Field
            name='birthday'
            dateFormat='DD/MM/YYYY'
            label='Sinh nhật'
            component={FInputDateTime}
          />
          <Clearfix height={16} />
          <Field
            name='gender'
            label='Giới tính'
            options={GENDER_OPTIONS}
            component={FInputSelect}
          />
          <Clearfix height={16} />
          <Field
            name='description'
            label='Tiểu sử'
            multiline
            placeholder='Mô tả và bản thân của bạn'
            component={FInputLabel}
          />
          <Clearfix height={16} />
          <Field
            name='facebook'
            label='Facebook'
            placeholder='https://facebook.com/nguyentrong.dev'
            numberOfLines={5}
            component={FInputLabel}
          />
          <Clearfix height={16} />
        </ScrollView>
      </View>
    )
  }
}
