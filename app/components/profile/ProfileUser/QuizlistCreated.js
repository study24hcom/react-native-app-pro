import React, { Component } from 'react'
import {View} from 'react-native'
import PropTypes from 'prop-types'
import { getQuizListsByUser } from '@redux/actions/quizListsAction'
import { QUIZ_LISTS_BY_USER } from 'constants/quizLists'
import connectQuizLists from 'hoc/quizlists-wrapper'
import QuizlistsCreated from '../QuizlistList/QuizlistCreated'

@connectQuizLists({
  action: getQuizListsByUser,
  key: QUIZ_LISTS_BY_USER.key,
  payload: QUIZ_LISTS_BY_USER.payload,
  customPassAction: props => {
    return {
      username: props.username
    }
  }
})
export default class QuizlistBoxCreated extends Component {
  static propTypes = {
    propsForFlatlist: PropTypes.object,
    onPressItem: PropTypes.func,
    username: PropTypes.string,
    onPressViewAll: PropTypes.func
  }

  render () {
    return (
      <View style={{flex: 1}}>
        <QuizlistsCreated onPressViewAll={this.props.onPressViewAll}
          onPressItem={this.props.onPressItem}
          {...this.props.propsForFlatlist} />
      </View>
    )
  }
}
