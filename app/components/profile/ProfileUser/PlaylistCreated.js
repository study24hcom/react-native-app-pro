import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {View} from 'react-native'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {getFeaturedPlaylistsByUser} from '@redux/actions/playlistAction'
import PlaylistBox from '../PlaylistList'
import {awaitCheckPending} from 'utils/await'

@connectAwaitAutoDispatch(
  state => ({
    playlist: state.playlist.featuredListsByUser
  }), {getFeaturedPlaylistsByUser})
export default class PlaylistBoxCreated extends PureComponent {
  static propTypes = {
    username: PropTypes.string,
    onPressItem: PropTypes.func,
    onPressViewAll: PropTypes.func
  }

  componentDidMount () {
    this.props.getFeaturedPlaylistsByUser({username: this.props.username})
  }

  isLoadingFirst () {
    return awaitCheckPending(this.props, 'getFeaturedPlaylistsByUser')
  }

  render () {
    return (
      <View style={{flex: 1}}>
        <PlaylistBox isLoadingFirst={this.isLoadingFirst()} onPressViewAll={this.props.onPressViewAll}
          onPressItem={this.props.onPressItem} {...this.props.playlist} />
      </View>
    )
  }
}
