import React from 'react'
import PropTypes from 'prop-types'
import Button from '../../elements/button'
import hocFollowUser from 'hoc/follow-user'
import Text from '../../elements/text'
import styled from 'styled-components/native'
import Icon from '../../elements/Icon'
import {SIZE} from '../../../constants/font'

const FollowText = styled(Text)`
  color: #fff;
  font-size: ${SIZE.TITLE};
  font-weight: 400
`

const ButtonFollowUser = ({isFollowed, onClick}) => (
  <Button
    onClick={onClick}
    customPadding={16}
    customColor={isFollowed ? 'primary' : 'green'}
  >
    <Icon name='bookmark' color='#fff' size={SIZE.TITLE} />
    <FollowText>{'  '}{isFollowed ? 'Đang theo dõi' : 'Theo dõi'}</FollowText>
  </Button>
)
ButtonFollowUser.propTypes = {
  userId: PropTypes.string
}

export default hocFollowUser(ButtonFollowUser)
