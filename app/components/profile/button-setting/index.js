import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import Button from '../../elements/button'
import styled from 'styled-components/native'

const Text = styled.Text`
  color: #000;
  font-size: 16px;
`

export default class ButtonSetting extends PureComponent {
  static PropTypes = {
    onClick: PropTypes.func
  }

  render () {
    const {onClick} = this.props
    return (
      <Button
        onClick={onClick}
        customPadding={8}
        customBorder={'#CCCCCC'}>
        <Text>{'Cài đặt thông tin cá nhân'}</Text>
      </Button>
    )
  }
}
