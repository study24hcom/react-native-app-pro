import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {getQuizListsByHistory} from '@redux/actions/quizListsAction'
import {QUIZ_LISTS_BY_HISTORY} from 'constants/quizLists'
import connectQuizLists from 'hoc/quizlists-wrapper'
import QuizlistsWorked from '../QuizlistList/QuizlistsWorked'
import {View} from 'react-native'

@connectQuizLists({
  action: getQuizListsByHistory,
  key: QUIZ_LISTS_BY_HISTORY.key,
  payload: QUIZ_LISTS_BY_HISTORY.payload,
  customPassAction: props => {
    return {
      username: props.username
    }
  }
})
export default class QuizlistBoxWorked extends Component {
  static propTypes = {
    propsForFlatlist: PropTypes.object,
    onPressItem: PropTypes.func,
    username: PropTypes.string,
    onPressViewAll: PropTypes.func,
    isMe: PropTypes.bool
  }

  render () {
    return (
      <View style={{flex: 1}}>
        <QuizlistsWorked isMe={this.props.isMe} onPressViewAll={this.props.onPressViewAll}
          onPressItem={this.props.onPressItem} {...this.props.propsForFlatlist} />
      </View>
    )
  }
}
