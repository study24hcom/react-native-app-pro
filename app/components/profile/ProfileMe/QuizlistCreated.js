import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {getQuizListsByMe} from '@redux/actions/quizListsAction'
import {QUIZ_LISTS_BY_ME} from 'constants/quizLists'
import connectQuizLists from 'hoc/quizlists-wrapper'
import QuizlistsCreated from '../QuizlistList/QuizlistCreated'
import {View} from 'react-native'

@connectQuizLists({
  action: getQuizListsByMe,
  key: QUIZ_LISTS_BY_ME.key,
  payload: QUIZ_LISTS_BY_ME.payload
})
export default class QuizlistBoxCreated extends Component {
  static propTypes = {
    propsForFlatlist: PropTypes.object,
    onPressItem: PropTypes.func,
    onPressViewAll: PropTypes.func,
    isMe: PropTypes.bool
  }

  render () {
    return (
      <View style={{flex: 1}}>
        <QuizlistsCreated isMe={this.props.isMe} onPressViewAll={this.props.onPressViewAll}
          onPressItem={this.props.onPressItem} {...this.props.propsForFlatlist} />
      </View>
    )
  }
}
