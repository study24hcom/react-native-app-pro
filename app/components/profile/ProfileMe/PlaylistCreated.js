import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {getPlaylistsMe} from '@redux/actions/playlistAction'
import PlaylistBox from '../PlaylistList'
import {autobind} from 'core-decorators'
import {View} from 'react-native'
import {awaitCheckPending} from 'utils/await'

@connectAwaitAutoDispatch(
  state => ({
    playlist: state.playlist.listsMe
  }), {getPlaylistsMe})
@autobind
export default class PlaylistBoxCreated extends PureComponent {
  static propTypes = {
    username: PropTypes.string,
    onPressItem: PropTypes.func,
    onPressViewAll: PropTypes.func,
    isMe: PropTypes.bool
  }

  componentDidMount () {
    this.props.getPlaylistsMe()
  }

  isLoadingFirst () {
    return awaitCheckPending(this.props, 'getPlaylistsMe')
  }

  render () {
    return (
      <View style={{flex: 1}}>
        <PlaylistBox isMe={this.props.isMe} onPressViewAll={this.props.onPressViewAll}
          onPressItem={this.props.onPressItem}
          isLoadingFirst={this.isLoadingFirst()} {...this.props.playlist} />
      </View>
    )
  }
}
