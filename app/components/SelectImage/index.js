import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Keyboard } from 'react-native'
import styled from 'styled-components/native'
import MediaApi from 'api/MediaApi'
import Loading from 'components/elements/loading'
import Clearfix from 'components/elements/clearfix'
import { autobind } from 'core-decorators'
import ImagePicker from 'react-native-image-picker'
import Text from '../elements/text/index'
import { SIZE, WEIGHT } from '../../constants/font'
import { SHAPE as TEXT } from '../../constants/color'
import TextIcon from '../elements/text-icon/index'
import Icon from '../elements/Icon'

const SelectImageContainer = styled.View``
const ImageDisplay = styled.Image`
  width: 200px;
  height: 105px;
  resize-mode: cover;
`
const WrapperLabel = styled.View`
  flex-direction: row;
  align-items: center;
  `
const TouchableOpacity = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  display: flex;
`
const View = styled.View``
const Label = styled(Text)`
  color: rgba(0, 0, 0, .5);
  font-weight: ${WEIGHT.NORMAL};
  font-size: ${SIZE.NORMAL};
`
const Input = styled.TextInput``
var options = {
  title: 'Lựa chọn ảnh đại diện',
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
}

@autobind
export default class SelectImage extends PureComponent {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func
  }

  state = {
    isUploading: false,
    avatarSource: null,
    url: '',
    data: null
  }

  handleClick (e) {
    e.preventDefault()
    if (!this.state.isUploading) {
      this.file.focus()
    }
  }

  _handleFocus (e) {
    Keyboard.dismiss()
    this.onClick()
    if (this.props.onFocus) this.props.onFocus(e)
  }

  onClick () {
    ImagePicker.showImagePicker(options, (response) => {
      // You can also display the image using data:
      // let source = { uri: 'data:image/jpeg;base64,' + response.data };

      this.setState({
        avatarSource: response.data,
        url: response.uri
      })
    })
  }

  getFormData () {
    if (this.state.url) {
      let data = new FormData()
      data.append('picture', {uri: this.state.url, name: 'selfie.jpg', type: 'image/jpg'})
      return data
    }
  }

  async handleChangeFile (e) {
    this.setState({isUploading: true})
    let fileRes = await MediaApi.uploadPhoto(this.getFormData())
    this.props.onChange(fileRes.url)
    this.setState({isUploading: false})
  }

  handleClear () {
    this.props.onChange('')
  }

  render () {
    return (
      <SelectImageContainer>
        <WrapperLabel>
          <Label>
            {this.props.label}
          </Label>
          <Clearfix width={8} />
          {this.state.avatarSource && this.state.url && !this.state.isUploading &&
          <Icon onPress={this.handleChangeFile} size={SIZE.HEADING3} color={TEXT.PRIMARY} name='cloud-upload'
            simpleLineIcon />}
          {this.state.isUploading &&
          <View>
            <Loading padding={0} />
          </View>}
        </WrapperLabel>
        <Input
          value={this.props.value}
          innerRef={comp => (this.file = comp)}
          onFocus={this._handleFocus}
          style={{display: 'none'}}
          onChange={this.handleChangeFile}
        />
        {this.state.avatarSource &&
        <View>
          <Clearfix height={16} />
          <ImageDisplay source={{uri: this.state.url}} />
        </View>}
        <Clearfix height={8} />
        <TouchableOpacity onPress={this.handleClick}>
          <TextIcon color={TEXT.PRIMARY} icon='picture' simpleLineIcon>
            {this.state.avatarSource ? 'Thay thế hình ảnh' : 'Lựa chọn hình ảnh'}
          </TextIcon>
        </TouchableOpacity>
        {/* {this.props.value && */}
        {/* <div> */}
        {/* <ImageDisplay onClick={this.handleClick} src={this.props.value}/> */}
        {/* <Clearfix height={8}/> */}
        {/* </div>} */}
        {/* <div> */}
        {/* <LinkA onClick={this.handleClick}> */}
        {/* <i className='icon-picture'/> */}
        {/* {' '} */}
        {/* {this.props.value ? 'Thay thế hình ảnh' : 'Lựa chọn hình ảnh'} */}
        {/* /!*</LinkA>*!/ */}
        {/* /!*&nbsp;*!/ */}
        {/* /!*{this.props.value &&*!/ */}
        {/* /!*<LinkA colorType='red' onClick={this.handleClear}>*!/ */}
        {/* /!*Xóa*!/ */}
        {/* /!*</LinkA>}*!/ */}
        {/* /!*</div>*!/ */}
      </SelectImageContainer>
    )
  }
}
