import styled from 'styled-components/native/index'
import {SHAPE} from 'constants/color'
import Text from 'components/elements/text'
import {SIZE} from 'constants/font'

export const LoaddingWrapper = styled.View`
  position: absolute;
  top: 0px;
  left: 0px;
  right: 0px;
  bottom: 0px;
  justify-content: center;
  align-items: center;
  background-color: #ffffff;
`

export const TextCenter = styled(Text)`
  text-align: center;
  font-family: OpenSans-Regular;
  color: ${SHAPE.GRAYTEXT};
  font-size: ${props => props.fontSize ? props.fontSize : SIZE.NORMAL}px;
`
