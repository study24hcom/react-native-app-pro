import React from 'react'
import styled from 'styled-components/native'
import LottieView from 'lottie-react-native'
import { autobind } from 'core-decorators'
import { LoaddingWrapper, TextCenter } from './style'

const LoadingDiv = styled.View`
  width: 300px;
  height: 300px;
  padding-left: 8px;
`

@autobind
export default class RattingSuccess extends React.PureComponent {
  componentDidMount () {
    this.animation.reset()
    this.animation.play()
  }

  render () {
    return (
      <LoaddingWrapper>
        <LoadingDiv>
          <LottieView
            ref={animation => {
              this.animation = animation
            }}
            style={{
              width: 270,
              height: 270
            }}
            loop
            source={require('../../../assets/lotties/ic_fav.json')}
          />
          <TextCenter>
            Cảm ơn bạn, bạn đã đánh giá thành công.
          </TextCenter>
        </LoadingDiv>
      </LoaddingWrapper>
    )
  }
}
