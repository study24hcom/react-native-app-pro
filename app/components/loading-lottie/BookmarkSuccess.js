import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import LottieView from 'lottie-react-native'
import {autobind} from 'core-decorators'
import {TextCenter} from './style'
import {Modal} from 'react-native'

const WaitingContainer = styled.View`
  align-items: center;
  padding: 8px;
  justify-content: center;
  flex: 1;
`
const LoadingDiv = styled.View`
  align-items: center;
  justify-content: center;
`
@autobind
export default class BookmarkSuccess extends React.PureComponent {
  static propTypes = {
    modalVisible: PropTypes.bool
  }

  componentDidMount () {
    this.animation.reset()
    this.animation.play()
  }

  render () {
    return (
      <Modal animationType={'fade'} transparent={false} visible={this.props.modalVisible}
        presentationStyle={'fullScreen'}>
        <WaitingContainer>
          <LoadingDiv>
            <LottieView
              ref={animation => {
                this.animation = animation
              }}
              style={{width: 250, height: 250}}
              loop
              source={require('../../../assets/lotties/like.json')}
            />
          </LoadingDiv>
          <TextCenter>Chúc mừng bạn, bạn đã lưu trữ thành công.</TextCenter>
        </WaitingContainer>
      </Modal>
    )
  }
}
