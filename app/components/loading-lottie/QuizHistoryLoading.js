import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import LottieView from 'lottie-react-native'
import { autobind } from 'core-decorators'
import Clearfix from 'components/elements/clearfix'
import * as Progress from 'react-native-progress'
import { LoaddingWrapper, TextCenter } from './style'
import createDotHoc from './dotHoc'

const LoadingDiv = styled.View`
  width: 300px;
  height: 250px;
  padding-left: 10px;
`

const WrapLoadingBarCenter = styled.View`
  width: 300px;
  align-items: center;
`

@createDotHoc
@autobind
export default class QuizListPlayLoading extends React.PureComponent {
  state = {
    dots: []
  }

  static propTypes = {
    progress: PropTypes.number,
    textLoading: PropTypes.string
  }

  componentDidMount () {
    this.animation.reset()
    this.animation.play()
  }

  render () {
    return (
      <LoaddingWrapper>
        <LoadingDiv>
          <LottieView
            ref={animation => {
              this.animation = animation
            }}
            style={{
              width: 250,
              height: 240
            }}
            loop
            source={require('../../../assets/lotties/stopwatch.json')}
          />
        </LoadingDiv>
        <WrapLoadingBarCenter>
          <TextCenter>
            Chờ tí, tớ đang nạp dữ liệu {'\n'} cho bạn đây{' '}
            {this.props.textLoading}
          </TextCenter>
          <Clearfix height={16} />
          <Progress.Bar progress={this.props.progress} width={200} />
        </WrapLoadingBarCenter>
      </LoaddingWrapper>
    )
  }
}
