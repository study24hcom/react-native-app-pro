import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import LottieView from 'lottie-react-native'
import { autobind } from 'core-decorators'
import { LoaddingWrapper, TextCenter } from './style'

const LoadingDiv = styled.View`
  width: 300px;
  height: 250px;
  padding-left: 10px;
`

const WrapLoadingBarCenter = styled.View`
  width: 300px;
  align-items: center;
`

@autobind
export default class LoadingSuccess extends React.PureComponent {
  static propTypes = {
    progress: PropTypes.number
  }

  componentDidMount () {
    this.animation.reset()
    this.animation.play()
  }

  render () {
    return (
      <LoaddingWrapper>
        <LoadingDiv>
          <LottieView
            ref={animation => {
              this.animation = animation
            }}
            style={{
              width: 250,
              height: 250
            }}
            loop
            source={require('../../../assets/lotties/success.json')}
          />
        </LoadingDiv>
        <WrapLoadingBarCenter>
          <TextCenter>Chào mừng bạn quay trở lại với tungtung.vn</TextCenter>
        </WrapLoadingBarCenter>
      </LoaddingWrapper>
    )
  }
}
