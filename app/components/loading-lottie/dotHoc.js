import React from 'react'
import { autobind } from 'core-decorators'

export default function createDotHoc (Component) {
  @autobind
  class DotHoc extends React.PureComponent {
    state = {
      dots: []
    }

    autoDots () {
      if (this.state.dots.length === 3) {
        this.setState({
          dots: []
        })
      } else {
        this.setState({
          dots: [...this.state.dots, '.']
        })
      }
    }

    componentDidMount () {
      this.interval = setInterval(this.autoDots, 400)
    }

    componentWillUnmount () {
      clearInterval(this.interval)
    }

    render () {
      return (
        <Component textLoading={this.state.dots.join('')} {...this.props} />
      )
    }
  }
  return DotHoc
}
