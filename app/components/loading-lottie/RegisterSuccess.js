import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import LottieView from 'lottie-react-native'
import {autobind} from 'core-decorators'
import {LoaddingWrapper, TextCenter} from './style'

const LoadingDiv = styled.View``

const WrapLoadingBarCenter = styled.View`
  align-items: center;
`
@autobind
export default class RegisterSuccess extends React.PureComponent {
  static propTypes = {
    progress: PropTypes.number
  }

  componentDidMount () {
    this.animation.reset()
    this.animation.play()
  }

  render () {
    return (
      <LoaddingWrapper>
        <LoadingDiv>
          <LottieView
            ref={animation => {
              this.animation = animation
            }}
            style={{
              width: 250,
              height: 250
            }}
            loop
            source={require('../../../assets/lotties/emoji_wink.json')}
          />
        </LoadingDiv>
        <WrapLoadingBarCenter>
          <TextCenter>Chúc mừng bạn, bạn đã đăng kí thành công.</TextCenter>
        </WrapLoadingBarCenter>
      </LoaddingWrapper>
    )
  }
}
