import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import LottieView from 'lottie-react-native'
import {autobind} from 'core-decorators'
import {LoaddingWrapper} from './style'

const WaitingContainer = styled.View`
  align-items: center;
  padding: 8px;
  justify-content: center;
  flex: 1;
`
const LoadingDiv = styled.View`
  align-items: center;
  justify-content: center;
`
@autobind
export default class Success extends React.PureComponent {
  static propTypes = {
    modalVisible: PropTypes.bool
  }

  componentDidMount () {
    this.animation.reset()
    this.animation.play()
  }

  render () {
    return (
      <LoaddingWrapper>
        <WaitingContainer>
          <LoadingDiv>
            <LottieView
              ref={animation => {
                this.animation = animation
              }}
              style={{width: 250, height: 250}}
              loop
              source={require('../../../assets/lotties/like.json')}
            />
          </LoadingDiv>
        </WaitingContainer>
      </LoaddingWrapper>
    )
  }
}
