import PropTypes from 'prop-types'
import {requireNativeComponent, ViewPropTypes} from 'react-native'

var iface = {
  name: 'ImageView',
  propTypes: {
    text: PropTypes.string,
    ...ViewPropTypes // include the default view properties
  }
}

export default requireNativeComponent('RCTFlexibleRichTextView', iface)
