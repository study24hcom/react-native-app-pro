import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import Header from './Header'

const SearchLayoutContainer = styled.View`
  flex: 1;
  `
@autobind
export default class SearchHeaderLayout extends PureComponent {
  static propTypes = {
    children: PropTypes.any,
    onSubmit: PropTypes.func
  }

  render () {
    return <SearchLayoutContainer>
      <Header />
      {this.props.children}
    </SearchLayoutContainer>
  }
}
