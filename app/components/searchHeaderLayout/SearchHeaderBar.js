import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {View} from 'react-native'
import {withNavigation} from 'react-navigation'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import {setKeywordsSearch} from '@redux/actions/optionAction'
import {searchUser, searchQuizlist} from '@redux/actions/searchAction'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {TEXT, SHAPE} from 'constants/color'
import {SIZE} from 'constants/font'
import Icon from 'components/elements/Icon'
import Text from 'components/elements/text'

const SearchContainer = styled.TouchableOpacity`
  flex: 1;
  background-color: ${TEXT.PRIMARYBOLD};
  flex-direction: row;
  height: 30px;
  margin-right: 25px;
  align-items: center;
  padding-horizontal: 16px;
  justify-content: center;
  border-radius: 15px;
`
@withNavigation
@connectAwaitAutoDispatch(state => ({
  keywords: state.option.keywords
}), {searchUser, searchQuizlist, setKeywordsSearch})
@autobind
export default class SearchHeaderBar extends Component {
  static propTypes = {
    keywords: PropTypes.string,
    searchUser: PropTypes.func,
    searchQuizlist: PropTypes.func,
    setKeywordsSearch: PropTypes.func
  }

  onPressSearch () {
    this.props.navigation.navigate('SearchLayoutScreen')
  }

  render () {
    return (
      <SearchContainer activeOpacity={1} onPress={this.onPressSearch}>
        <Icon name='search' size={SIZE.NORMAL} color={SHAPE.GRAYMORELIGHT} />
        <View style={{alignItems: 'center'}}>
          <Text numberOfLines={1} fontSize={SIZE.NORMAL} color={SHAPE.GRAYMORELIGHT}>{`  ${this.props.keywords}`}</Text>
        </View>
      </SearchContainer>
    )
  }
}
