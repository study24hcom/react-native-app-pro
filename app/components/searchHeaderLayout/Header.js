import React, {PureComponent} from 'react'
import {Platform} from 'react-native'
import {HeaderBackButton, withNavigation} from 'react-navigation'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {getNewNotifications} from '@redux/actions/notificationAction'
import {TEXT} from 'constants/color'
import SearchHeaderBar from './SearchHeaderBar'

const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : 0
const StatusBar = styled.View`
  height: ${STATUSBAR_HEIGHT}px;
`
const HeaderBar = styled.View`
  height: ${APPBAR_HEIGHT};
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
`
const HeaderLargeContainer = styled.View`
  background-color: ${TEXT.PRIMARY};
`
@connectAwaitAutoDispatch(
  state => ({
    newNotifications: state.notification.newNotifications
  }),
  {getNewNotifications}
)
@withNavigation
@autobind
export default class Header extends PureComponent {
  onPressBack () {
    this.props.navigation.goBack()
  }

  render () {
    return <HeaderLargeContainer>
      <StatusBar />
      <HeaderBar>
        <HeaderBackButton width={20} tintColor={'#fff'} onPress={this.onPressBack} />
        <SearchHeaderBar />
      </HeaderBar>
    </HeaderLargeContainer>
  }
}
