import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {StyleSheet} from 'react-native'
import styled from 'styled-components/native'
import Placeholder from 'rn-placeholder'

const PaddingPlaceholder = styled.View`
  padding: 8px;
  border-radius: 4px;
  margin-bottom: ${props => props.margin ? `${props.margin}` : 0};
  ${props => props.height ? `height: ${props.height}px` : null}
`

export default class PlaceholderView extends PureComponent {
  static propTypes = {
    height: PropTypes.number,
    margin: PropTypes.number
  }

  render () {
    return <PaddingPlaceholder height={this.props.height} margin={this.props.margin}
      style={styles.shadow}>
      <Placeholder.Paragraph
        lineNumber={6}
        textSize={16}
        lineSpacing={6}
        color='#eeeeee'
        animate='shine'
        width='100%'
        lastLineWidth='70%'
        firstLineWidth='50%'
      />
    </PaddingPlaceholder>
  }
}

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#DDDDDD',
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  }
})
