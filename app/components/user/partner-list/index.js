import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import TagItem from '../partner-item'

const PartnerListContainer = styled.View`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  padding: 0px 8px
`

const Wrap = styled.View`
  ${props => (!props.isLast ? 'margin-right: 8px;' : '')}
  padding-bottom: 8px;
`

export default class PartnerList extends PureComponent {
  static propTypes = {
    onPress: PropTypes.func,
    partner: PropTypes.arrayOf(PropTypes.object),
    size: PropTypes.number
  }
  render () {
    return (
      <PartnerListContainer>
        {this.props.partner.map((partner, index) => {
          return (
            <Wrap isLast={index === this.props.partner.length - 1} key={partner.id}>
              <TagItem onPress={() => this.props.onPress(partner)} size={this.props.size} name={partner.username} />
            </Wrap>
          )
        })}
      </PartnerListContainer>
    )
  }
}
