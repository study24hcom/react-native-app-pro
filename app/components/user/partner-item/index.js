import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'

const PartnerItemContainer = styled.TouchableOpacity`
  padding: 4px 16px;
  border-radius: 2px;
  background-color: rgba(140, 197, 255, 0.08);
  border: 1px solid rgba(0, 126, 255, 0.24);
`
const Text = styled.Text`
  color: #007eff !important;
  ${props => props.color ? `font-size: ${props.size}` : 12}px
`

export default class PartnerItem extends PureComponent {
  static propTypes = {
    name: PropTypes.string,
    size: PropTypes.number,
    onPress: PropTypes.func
  }

  render () {
    return (
      <PartnerItemContainer
        activeOpacity={1}
        onPress={this.props.onPress}
        size={this.props.size}>
        <Text>{this.props.name}</Text>
      </PartnerItemContainer>
    )
  }
}
