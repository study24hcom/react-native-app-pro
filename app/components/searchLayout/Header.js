import React, {PureComponent} from 'react'
import {Platform} from 'react-native'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {getNewNotifications} from '@redux/actions/notificationAction'
import {TEXT} from 'constants/color'
import SearchBar from './SearchBar'

const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : 0
const StatusBar = styled.View`
  height: ${STATUSBAR_HEIGHT}px;
`
const HeaderBar = styled.View`
  height: ${APPBAR_HEIGHT};
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
`
const HeaderLargeContainer = styled.View`
  background-color: ${TEXT.PRIMARY};
`
@connectAwaitAutoDispatch(
  state => ({
    newNotifications: state.notification.newNotifications
  }),
  {getNewNotifications}
)
@autobind
export default class Header extends PureComponent {
  render () {
    return <HeaderLargeContainer>
      <StatusBar />
      <HeaderBar>
        <SearchBar />
      </HeaderBar>
    </HeaderLargeContainer>
  }
}
