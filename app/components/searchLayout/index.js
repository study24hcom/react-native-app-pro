import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {getNewNotifications} from '@redux/actions/notificationAction'
import Header from './Header'

const SearchLayoutContainer = styled.View`
  flex: 1;
  `
@connectAwaitAutoDispatch(
  state => ({
    newNotifications: state.notification.newNotifications
  }),
  {getNewNotifications}
)
@autobind
export default class SearchLayout extends PureComponent {
  static propTypes = {
    children: PropTypes.any,
    onSubmit: PropTypes.func
  }

  render () {
    return <SearchLayoutContainer>
      <Header />
      {this.props.children}
    </SearchLayoutContainer>
  }
}
