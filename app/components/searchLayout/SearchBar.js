import React, {Component} from 'react'
import {ActivityIndicator} from 'react-native'
import PropTypes from 'prop-types'
import {withNavigation} from 'react-navigation'
import * as _ from 'lodash'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import {setKeywordsSearch} from '@redux/actions/optionAction'
import {searchKeyWord} from '@redux/actions/searchAction'
import {getHistories, saveHistories} from '@redux/actions/historiesAction'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {TEXT, SHAPE} from 'constants/color'
import {SIZE} from 'constants/font'
import Icon from 'components/elements/Icon'
import Text from 'components/elements/text'
import {Answers} from 'react-native-fabric'
import {awaitCheckPending} from 'utils/await'

const Input = styled.TextInput`
  font-size: ${SIZE.NORMAL};
  flex: 1;
  height: 30px;
  padding: 4px;
  padding-left: 8px;
  color: ${SHAPE.GRAYMORELIGHT}
`
const View = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-horizontal: 8px;
`
const GoBack = styled.TouchableOpacity`
  margin-left: 8px;
  height: 30px;
  justify-content: center;
`
const SearchWrapper = styled.View`
  flex: 1;
  border-radius: 20px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding-horizontal: 8px;
  background-color: ${TEXT.PRIMARYBOLD};
  `
@withNavigation
@connectAwaitAutoDispatch(state => ({
  keywords: state.option.keywords
}), {setKeywordsSearch, searchKeyWord, saveHistories, getHistories})
@autobind
export default class SearchBar extends Component {
  static propTypes = {
    keywords: PropTypes.string,
    searchUser: PropTypes.func,
    searchQuizlist: PropTypes.func,
    setKeywordsSearch: PropTypes.func
  }

  async componentDidMount () {
    this.props.getHistories()
  }

  constructor (props) {
    super(props)
    this.onRequestDelay = _.debounce(this.onRequest, 350)
    this.state = {
      keywords: this.props.keywords,
      isFocus: false,
      isLoading: false
    }
  }

  onRequest () {
    this.props.searchKeyWord({keywords: this.state.keywords.toLowerCase().trim()})
    this.setState({isLoading: false})
  }

  onChangeText (keywords) {
    this.setState({isLoading: true})
    this.props.setKeywordsSearch({keywords})
    this.setState({keywords})
    this.onRequestDelay()
  }

  onSubmit () {
    if (this.state.keywords) {
      Answers.logSearch(this.state.keywords)
      this.props.navigation.navigate('SearchScreen')
      this.props.saveHistories(this.state.keywords)
    }
  }

  componentWillMount () {
    this.setState({isFocus: true})
  }

  onPressGoBack () {
    this.props.navigation.goBack(null)
    this.props.setKeywordsSearch({keywords: ''})
  }

  isLoading () {
    return (awaitCheckPending(this.props, 'searchKeyWord') || this.state.isLoading)
  }

  render () {
    return (
      <View>
        <SearchWrapper>
          <Icon name='search' color={SHAPE.GRAYMORELIGHT} size={SIZE.NORMAL} />
          <Input
            innerRef={comp => (this.input = comp)}
            onChangeText={this.onChangeText}
            value={this.props.keywords}
            clearButtonMode={this.isLoading() ? 'never' : 'while-editing'}
            autoCapitalize='none'
            underlineColorAndroid={'transparent'}
            autoCorrect={false}
            autoFocus
            selectionColor='#fff'
            onSubmitEditing={this.onSubmit}
            placeholder='Tìm đề thi, tag, chuyên mục...'
            placeholderTextColor='#f1f1f1'
            returnKeyType='search' />
          {this.isLoading() && <ActivityIndicator color='white' />}
        </SearchWrapper>
        <GoBack onPress={this.onPressGoBack}>
          <Text color={SHAPE.GRAYMORELIGHT}>Hủy</Text>
        </GoBack>
      </View>
    )
  }
}
