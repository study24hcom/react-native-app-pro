import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {SHAPE} from 'constants/color'
import Icon from 'components/elements/Icon'
import Text from 'components/elements/text'
import {SIZE} from 'constants/font'
import Clearfix from 'components/elements/clearfix'
import {View} from 'react-native'

const ListItemContainer = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  background-color: #fff;
  `
const ListContainer = styled.View``
const IconView = styled.View`
  width: 30px;
  height: 30px;
  justify-content: center;
  align-items: center;
  background-color: ${props => props.color ? `${props.color}` : SHAPE.PRIMARYBOLD};
  margin-vertical: 7.5px;
  margin-horizontal: 10px;
  margin-right: 15px;
  border-radius: 5px;
`
const TitleView = styled.View`
  flex-direction: row;
  border-bottom-width: ${props => props.isLast ? `0` : `0.5px`};
  border-bottom-color: ${SHAPE.GRAYBOLD};
  align-items: center;
  justify-content: space-between;
  flex: 1;
  height: 45px;
  padding-right: 10px;
  `

const List = styled.View`
  border-top-width: 0.5px;
  border-bottom-width: 0.5px;
  border-color: ${SHAPE.GRAYBOLD};
  background-color: #fff;
  `

export class ListItem extends PureComponent {
  static propTypes = {
    icon: PropTypes.string,
    title: PropTypes.string,
    onPress: PropTypes.func,
    isLast: PropTypes.bool,
    color: PropTypes.string,
    simpleLineIcon: PropTypes.bool
  }

  render () {
    const {icon, title, onPress, isLast, color, simpleLineIcon} = this.props
    return <ListItemContainer activeOpacity={0.6} onPress={onPress}>
      <IconView color={color}>
        <Icon simpleLineIcon={simpleLineIcon} color={'#fff'} size={20} name={icon} />
      </IconView>
      <TitleView isLast={isLast}>
        <Text fontSize={SIZE.TITLE} color={'#000'}>{title}</Text>
        <Icon simpleLineIcon name={'arrow-right'} />
      </TitleView>
    </ListItemContainer>
  }
}

export default class ListMenu extends PureComponent {
  static propTypes = {
    lists: PropTypes.array,
    title: PropTypes.string
  }

  render () {
    return <ListContainer>
      {this.props.title &&
      <View>
        <Text padding={`8px`} fontSize={SIZE.NORMAL} color={SHAPE.BLACK}>{this.props.title}</Text>
        <Clearfix height={4} />
      </View>}
      < List>
        {this.props.lists.map((item, index) => <ListItem isLast={index === this.props.lists.length - 1}
          key={index} {...item} />)
        }
      </List>
    </ListContainer>
  }
}
