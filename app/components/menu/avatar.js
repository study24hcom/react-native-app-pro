import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {SIZE} from 'constants/font'
import {SHAPE} from 'constants/color'
import {autobind} from 'core-decorators'
import Text from 'components/elements/text'
import Avatar from 'components/elements/user-avatar/avatar'

const Username = styled(Text)`
  font-size: ${SIZE.BIG};
  padding-top: 8px;
  padding-bottom: 4px;
  color: ${SHAPE.PRIMARYBOLD}
`

const FullName = styled(Text)`
  font-size: ${SIZE.TITLE};
  color: ${SHAPE.GRAYTEXT};
`
const UserAvatarWrapper = styled.View`
  padding: 16px;
  background-color: #fff;
  align-items: center;
  `
@autobind
export default class AvatarMenu extends PureComponent {
  static propTypes = {
    auth: PropTypes.shape({
      avatar: PropTypes.string,
      username: PropTypes.string,
      fullname: PropTypes.string
    })
  }

  render () {
    const {auth: {avatar, username, fullname}} = this.props
    return <UserAvatarWrapper>
      <Avatar avatar={avatar} username={username} avatarSize={100} />
      <Username>{`@${username}`}</Username>
      <FullName>{fullname}</FullName>
    </UserAvatarWrapper>
  }
}
