import React from 'react'
import PropTypes from 'prop-types'
import Button from '../../elements/button'
import hocFollowTag from 'hoc/follow-tag'
import styled from 'styled-components/native'
import Icon from '../../elements/Icon'

const Text = styled.Text`
  color: #fff;
  font-size: 16px;
`

const ButtonFollowTag = ({isFollowed, onClick, size, block, isSuccess}) => (
  <Button
    onClick={onClick}
    customPadding={16}
    customColor={isFollowed ? 'primarybold' : 'primary'}>
    <Icon name='bookmark' color='#fff' size={16} />
    <Text>{'  '}{isFollowed ? 'Đang theo dõi' : 'Theo dõi tag'}</Text>
  </Button>
)
ButtonFollowTag.propTypes = {
  tagName: PropTypes.string
}

export default hocFollowTag({autoCheck: true})(ButtonFollowTag)
