import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import TagItem from '../tag-item'

const TagListContainer = styled.View`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  padding: 0px 0px
`

const Wrap = styled.View`
  ${props => (!props.isLast ? 'margin-right: 8px;' : '')}
  padding-vertical: 4px;
`
export default class TagList extends PureComponent {
  static propTypes = {
    onPress: PropTypes.func,
    tags: PropTypes.arrayOf(PropTypes.object),
    size: PropTypes.number
  }

  render () {
    return (
      <TagListContainer>
        {this.props.tags.map((tag, index) => {
          return (
            tag.name &&
            <Wrap isLast={index === this.props.tags.length - 1} key={tag.id}>
              <TagItem onPress={this.props.onPress} tag={tag} size={this.props.size} name={tag.name} />
            </Wrap>
          )
        })}
      </TagListContainer>
    )
  }
}
