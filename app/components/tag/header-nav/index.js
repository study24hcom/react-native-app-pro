import React from 'react'
import {autobind} from 'core-decorators'
import {HeaderBackButton} from 'react-navigation'
import {StyleSheet, TouchableOpacity} from 'react-native'
import NavigationBar from 'react-native-navbar'
import Text from 'components/elements/text/index'
import {SHAPE} from 'constants/color'
import {connectAutoDispatch} from '@redux/connect'
import {getFeaturedTags} from '@redux/actions/tagAction'

const styles = StyleSheet.create({
  buttonNav: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

@connectAutoDispatch(state => ({}), {getFeaturedTags})
@autobind
export default class HeaderNav extends React.PureComponent {
  onPressBack () {
    this.props.navigation.goBack(null)
  }

  onPressEdit () {
    this.props.getFeaturedTags()
    this.props.navigation.navigate('FollowScreen')
  }

  renderRight () {
    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={this.onPressEdit}
        style={[styles.buttonNav, {paddingRight: 8, paddingLeft: 8}]}
        underlayColor='transparent'
      >
        <Text fontWeight={600} color='#ffffff'>
          Sửa
        </Text>
      </TouchableOpacity>
    )
  }

  renderLeft () {
    return (
      <HeaderBackButton width={20} tintColor={'#fff'} onPress={this.onPressBack} />
    )
  }

  render () {
    return (
      <NavigationBar
        statusBar={{style: 'light-content'}}
        containerStyle={{backgroundColor: SHAPE.PRIMARY}}
        style={{backgroundColor: SHAPE.PRIMARY}}
        leftButton={this.renderLeft()}
        rightButton={this.renderRight()}
        title={{title: 'Tag nổi bật', style: {color: '#ffffff', fontWeight: '700'}}}
      />
    )
  }
}
