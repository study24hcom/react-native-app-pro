import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import styled from 'styled-components/native'
import Text from '../../elements/text'
import {TEXT} from 'constants/color'
import {SIZE, WEIGHT} from 'constants/font'
import {Answers} from 'react-native-fabric'

const TagItemContainer = styled.TouchableOpacity`
  padding: 6px 16px;
  border-radius: 2px;
  background-color: rgba(140, 197, 255, 0.08);
  border: 1px solid rgba(0, 126, 255, 0.24);
`
@autobind
export default class TagItem extends PureComponent {
  static propTypes = {
    name: PropTypes.string,
    size: PropTypes.number,
    onPress: PropTypes.func,
    tag: PropTypes.object
  }

  onPressTag () {
    this.props.onPress(this.props.tag)
    Answers.logContentView(this.props.name, 'TAG', this.props.tag.slug)
  }

  render () {
    return (
      <TagItemContainer
        activeOpacity={1}
        onPress={this.onPressTag}
        size={this.props.size}>
        <Text fontWeight={WEIGHT.NORMAL} fontSize={SIZE.SMALL} color={TEXT.PRIMARY}>{this.props.name}</Text>
      </TagItemContainer>
    )
  }
}
