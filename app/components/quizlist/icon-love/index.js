import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import createBookmarkQuizlist from 'hoc/bookmark-quizlist'
import Icon from '../../elements/Icon'
import {SHAPE} from 'constants/color'
import {ActivityIndicator} from 'react-native'
import {SIZE} from 'constants/font'
import {connectAwaitAutoDispatch} from '@redux/connect'

const Button = styled.TouchableOpacity`
  width: 45px;
  height: 45px;
  justify-content: center;
  align-items: center;
  border-radius: 22.5px;
  `
@connectAwaitAutoDispatch(state => ({
  quizListId: state.quiz.info._id
}), {})
@createBookmarkQuizlist({autoCheck: true})
@autobind
export default class IconLove extends React.Component {
  static PropTypes = {
    isChecking: PropTypes.bool,
    isBookmarked: PropTypes.bool,
    onClick: PropTypes.func,
    quizListId: PropTypes.string
  }

  render () {
    const {isBookmarked, onClick} = this.props
    return <Button activeOpacity={0.5} onPress={onClick}>
      {this.props.isChecking && <ActivityIndicator color='#fff' />}
      {!this.props.isChecking &&
      <Icon name={'heart'} color={isBookmarked ? `${SHAPE.RED}` : '#fff'} size={SIZE.HEADING1} />}
    </Button>
  }
}
