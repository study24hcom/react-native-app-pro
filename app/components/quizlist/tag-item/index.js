import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'

const TagItemContainer = styled.TouchableOpacity`
  padding: 4px 16px;
  border-radius: 2px;
  background-color: rgba(140, 197, 255, 0.08);
  border: 1px solid rgba(0, 126, 255, 0.24);
`
const Text = styled.Text`
  color: #007eff !important;
  font-size: ${props => (props.size === 'lg' ? 18 : 15)}px;
`

export default class TagItem extends PureComponent {
  static propTypes = {
    name: PropTypes.string,
    size: PropTypes.string,
    onPress: PropTypes.func
  }

  render () {
    return (
      <TagItemContainer
        activeOpacity={1}
        onPress={this.props.onPress}
        size={this.props.size}>
        <Text>{this.props.name}</Text>
      </TagItemContainer>
    )
  }
}
