import React, {Component} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {Modal} from 'react-native'
import LottieView from 'lottie-react-native'
import {autobind} from 'core-decorators'
import Text from 'components/elements/text'
import TextIcon from 'components/elements/text-icon/index'
import HeadingLine from 'components/elements/heading-line/index'
import CountdownTime from 'components/elements/countdown-time/index'
import {SHAPE} from 'constants/color'

const WaitingContainer = styled.View`
  align-items: center;
  padding: 8px;
  justify-content: center;
  flex: 1;
`
const LoadingDiv = styled.View`
  align-items: center;
  justify-content: center;
`
const AlertWaitingResult = styled.View`
  align-items: center;
`
const Button = styled.TouchableOpacity`
  border: 1px;
  border-color: #010101;
  justify-content: center;
  align-items: center;
  height: 50px;
  margin: 8px 16px;
  margin-bottom: 25px;
`
@autobind
export default class WaitingScreen extends Component {
  static propTypes = {
    handleBack: PropTypes.func,
    endTime: PropTypes.any,
    isShowWaiting: PropTypes.bool,
    onFinishWaiting: PropTypes.func
  }

  componentDidMount() {
    this.animation.reset()
    this.animation.play()
  }

  renderCountdownTimeShowResult() {
    return (
      <LoadingDiv>
        <HeadingLine noLine title={'Thời gian mở đáp án còn lại'}/>
        <AlertWaitingResult>
          <CountdownTime color={SHAPE.GREEN} deadline={this.props.endTime}
                         onFinish={this.props.onFinishWaiting}/>
        </AlertWaitingResult>
      </LoadingDiv>
    )
  }

  open() {
    this.setState({modalVisible: true})
  }

  close() {
    this.setState({modalVisible: false})
  }

  render() {
    return (
      <Modal animationType={'fade'} transparent={false} visible={this.props.isShowWaiting}
             presentationStyle={'fullScreen'}>
        <WaitingContainer>
          <LoadingDiv>
            <LottieView
              ref={animation => {
                this.animation = animation
              }}
              style={{width: 200, height: 200}}
              loop
              source={require('../../../../assets/lotties/like.json')}
            />
          </LoadingDiv>
          <Text align={'center'} color={'red'} fontSize={16}>Chúc mừng bạn đã hoàn thành bài làm</Text>
          {this.renderCountdownTimeShowResult()}
        </WaitingContainer>
        <Button onPress={this.props.handleBack}>
          <TextIcon color={'#000'} icon={'refresh'} size={20}>Quay lại</TextIcon>
        </Button>
      </Modal>
    )
  }
}
