import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import {
  ShortInfo,
  MetaWrapper,
  Meta,
  TitleOptionWrapper,
  TitleViewWrapper,
  AvatarWrapper,
  AvatarWrapperTouch,
  SpanUsername,
  TextAdView
} from './style'
import {SHAPE} from 'constants/color'
import BoxShadow from 'components/elements/WhiteBoxShadow'
import Avatar from 'components/elements/user-avatar/avatar'
import Title from 'components/elements/title'
import {Answers} from 'react-native-fabric'
import TotalRating from 'components/quizlist/total-rating/index'
import TextIcon from 'components/elements/text-icon/index'
import {SIZE, WEIGHT} from 'constants/font'

@autobind
export default class QuizListBox extends PureComponent {
  static propTypes = {
    name: PropTypes.string,
    slug: PropTypes.string,
    totalRatings: PropTypes.number,
    ratingAvg: PropTypes.number,
    totalAccess: PropTypes.number,
    timeLength: PropTypes.number,
    totalQuestion: PropTypes.number,
    user: PropTypes.shape({
      username: PropTypes.string,
      avatar: PropTypes.string,
      isPartner: PropTypes.bool,
      admin: PropTypes.bool
    }),
    onPress: PropTypes.func,
    isPress: PropTypes.bool,
    horizontal: PropTypes.bool,
    onPressUser: PropTypes.func,
    isPressUser: PropTypes.bool
  }

  onPressItem () {
    this.props.onPress(this.props)
    Answers.logContentView(this.props.name, 'QUIZLIST', this.props.slug)
  }

  onPressUser () {
    this.props.onPressUser(this.props.user)
    Answers.logContentView(this.props.user.username, 'USER', this.props.user.username)
  }

  renderInfoQuizList () {
    const {timeLength, totalAccess, totalQuestion} = this.props
    return <MetaWrapper>
      <Meta flex={1.8}>
        <TextIcon fontWeight={WEIGHT.BOLD} color={SHAPE.GREEN} size={SIZE.HEADING6}
          icon={`clock-o`}>{`${timeLength} phút`}</TextIcon>
      </Meta>
      <Meta flex={2.5}>
        <TextIcon fontWeight={WEIGHT.BOLD} color={SHAPE.RED} size={SIZE.HEADING6}
          icon={`bar-chart-o`}>{`${totalAccess} lượt thi`}</TextIcon>
      </Meta>
      <Meta flex={1.7}>
        <TextIcon fontWeight={WEIGHT.BOLD} color={SHAPE.ORANGE} size={SIZE.HEADING6}
          icon={`question-circle-o`}>{`${totalQuestion} câu`}</TextIcon>
      </Meta>
    </MetaWrapper>
  }

  renderUserInfo () {
    const {user, isPressUser, ratingAvg, totalRatings} = this.props
    return isPressUser
      ? <MetaWrapper>
        <Meta flex={4.3}>
          {user.username &&
          <AvatarWrapperTouch activeOpacity={0.9} onPress={this.onPressUser}>
            <Avatar avatar={user.avatar} avatarSize={SIZE.HEADING6} username={user.username} />
            <SpanUsername>{`${user.username}`}</SpanUsername>
            {user.isPartner && !user.admin && <TextAdView>P</TextAdView>}
            {user.admin && <TextAdView>Ad</TextAdView>}
          </AvatarWrapperTouch>}
        </Meta>
        <Meta flex={1.7}>
          {this.props.ratingAvg > 0 && <TotalRating size={SIZE.SMALLER} value={ratingAvg} total={totalRatings} />}
        </Meta>
      </MetaWrapper>
      : <MetaWrapper>
        <Meta flex={4.3}>
          {user.username &&
          <AvatarWrapper>
            <Avatar avatar={user.avatar} avatarSize={14} username={user.username} />
            <SpanUsername>{`${user.username}`}</SpanUsername>
            {user.isPartner && !user.admin && <TextAdView>P</TextAdView>}
            {user.admin && <TextAdView>Ad</TextAdView>}
          </AvatarWrapper>}
        </Meta>
        <Meta flex={1.7}>
          {this.props.ratingAvg > 0 && <TotalRating size={SIZE.SMALLER} value={ratingAvg} total={totalRatings} />}
        </Meta>
      </MetaWrapper>
  }

  renderTitleQuizList () {
    const {name, isPress} = this.props
    if (isPress) {
      return <TitleOptionWrapper onPress={this.onPressItem} activeOpacity={0.7}>
        <Title numberOfLines={3} isPress>{name}</Title>
      </TitleOptionWrapper>
    }
    return <TitleViewWrapper>
      <Title>{name}</Title>
    </TitleViewWrapper>
  }

  render () {
    return (
      <BoxShadow horizontal={this.props.horizontal}>
        {this.renderTitleQuizList()}
        <ShortInfo>
          {this.renderInfoQuizList()}
          {this.renderUserInfo()}
        </ShortInfo>
        {this.props.children}
      </BoxShadow>
    )
  }
}
