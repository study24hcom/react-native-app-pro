import React from 'react'
import styled from 'styled-components/native'
import {SIZE, WEIGHT} from 'constants/font'
import {SHAPE} from 'constants/color'
import Text from 'components/elements/text'

export const TitleOptionWrapper = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  justify-content: space-between
`
export const TitleViewWrapper = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between

`
export const ShortInfo = styled.View``

export const MetaWrapper = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: 8px;
  align-items: center;
`
export const AvatarWrapperTouch = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  display: flex;
`
export const AvatarWrapper = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`
export const SpanUsername = styled(Text)`
  font-weight: ${WEIGHT.BOLD};
  font-size: ${SIZE.HEADING6};
  color: ${SHAPE.BLACK}
  padding-left: 8px;
  display: flex;
  `
export const Meta = styled.View`
  flex: ${props => props.flex ? props.flex : 1}
`

const TextAd = styled(Text)`
  color: #fff;
  font-size: ${SIZE.HEADING6};
  display: flex;
  `

const TextView = styled.View`
  border-radius: 4px;
  background-color: ${SHAPE.RED};
  padding-horizontal: 8px;
  margin-left: 4px;
`
export const TextAdView = ({children}) => (
  <TextView>
    <TextAd>{children}</TextAd>
  </TextView>
)
export default {
  ShortInfo,
  MetaWrapper,
  Meta,
  TitleOptionWrapper,
  TitleViewWrapper,
  AvatarWrapper,
  AvatarWrapperTouch,
  SpanUsername,
  TextAdView
}
