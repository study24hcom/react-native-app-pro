import React, {Component} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {Modal, TextInput, Keyboard} from 'react-native'
import {autobind} from 'core-decorators'
import QuizApi from 'api/QuizApi'
import HeaderNav from './HeaderNav'
import StarView from '../../elements/star-view'
import Text from 'components/elements/text'
import {INPUT, TEXT} from 'constants/color'
import {SIZE} from 'constants/font'
import RattingSuccess from '../../loading-lottie/RattingSuccess'

const ModalContainer = styled.View`
  margin-horizontal: 16px;
`
const StarWrapper = styled.View`
  margin-vertical: 8px;
  justify-content: center;
  align-items: center;
 `
const InputWrapper = styled.View`
  border-top-width: 1px;
  border-top-color: ${INPUT.BORDER}
  `
const TextStar = styled(Text)`
  padding-top: 4px;
  font-size: 12px;
`
@autobind
export default class ModalRating extends Component {
  state = {
    modalVisible: false,
    rating: this.props.rating ? this.props.rating : 0,
    content: '',
    isSuccess: false
  }

  static propTypes = {
    quizListId: PropTypes.string,
    rating: PropTypes.number,
    onPressExit: PropTypes.func,
    onChangeStar: PropTypes.func,
    setAuthRating: PropTypes.func,
    addRating: PropTypes.func,
    navigation: PropTypes.any
  }

  open () {
    this.setState({rating: this.props.rating})
    this.setState({modalVisible: true})
  }

  close () {
    this.setState({rating: this.props.rating})
    this.setState({modalVisible: false})
  }

  onPressExit () {
    if (this.props.onPressExit) {
      this.props.onPressExit()
    } else {
      this.props.navigation.goBack(null)
    }
  }

  onChangeStar (rating) {
    if (this.props.onChangeStar) {
      this.props.onChangeStar(rating)
      this.setState({rating})
    } else {
      this.setState({rating})
    }
  }

  onPressSubmit () {
    if (!this.state.content) {
      alert('Vui lòng nhập nội dung đánh giá.')
      return
    }
    if (this.state.rating === 0) {
      alert('Vui lòng chạm vào Ngôi sao để đánh giá.')
      return
    }
    Keyboard.dismiss()
    QuizApi.addQuizListRating({
      quizListKey: this.props.quizListId,
      rating: this.state.rating,
      content: this.state.content
    }).then(result => {
      if (result._id) {
        this.setState({isSuccess: true})
        this.props.setAuthRating(true)
        setTimeout(() => {
          this.onPressExit()
        }, 1000)
        this.props.addRating(result)
      }
    })
  }

  render () {
    return (
      <Modal animationType={'slide'} transparent={false} visible={this.state.modalVisible}
        presentationStyle={'fullScreen'}>
        <HeaderNav onPressSend={this.onPressSubmit} onPressExit={this.onPressExit} />
        <ModalContainer>
          <StarWrapper>
            <StarView size={25} value={this.state.rating}
              onChange={this.onChangeStar} />
            <TextStar color={TEXT.GRAY}>Chạm vào một Ngôi sao để đánh giá.</TextStar>
          </StarWrapper>
          <InputWrapper>
            <TextInput
              placeholder='Nhận xét'
              autoCorrect={false}
              style={{fontSize: SIZE.NORMAL, paddingTop: 16, height: 300}}
              multiline
              numberOfLines={5}
              onChangeText={(content) => this.setState({content})}
              value={this.state.content}
            />
          </InputWrapper>
        </ModalContainer>
        {this.state.isSuccess && <RattingSuccess />}
      </Modal>
    )
  }
}
