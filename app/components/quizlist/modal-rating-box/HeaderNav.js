import React from 'react'
import { autobind } from 'core-decorators'
import PropTypes from 'prop-types'
import { StyleSheet, TouchableOpacity } from 'react-native'
import NavigationBar from 'react-native-navbar'
import Text from 'components/elements/text/index'
import { SHAPE } from 'constants/color'

const styles = StyleSheet.create({
  buttonNav: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

@autobind
export default class HeaderNav extends React.PureComponent {
  static propTypes = {
    onPressExit: PropTypes.func,
    onPressSend: PropTypes.func,
    title: PropTypes.string
  }

  renderRight () {
    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={this.props.onPressSend}
        style={[styles.buttonNav, {paddingRight: 8, paddingLeft: 8}]}
        underlayColor='transparent'
      >
        <Text fontWeight={600} color='#ffffff'>Gửi</Text>
      </TouchableOpacity>
    )
  }

  renderLeft () {
    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={this.props.onPressExit}
        style={[styles.buttonNav, {paddingLeft: 8, paddingRight: 8}]}
        underlayColor='transparent'>
        <Text fontWeight={600} color='#ffffff'>Hủy</Text>
      </TouchableOpacity>
    )
  }

  render () {
    return (
      <NavigationBar
        statusBar={{style: 'light-content'}}
        containerStyle={{backgroundColor: SHAPE.GREEN}}
        style={{backgroundColor: SHAPE.GREEN}}
        leftButton={this.renderLeft()}
        rightButton={this.renderRight()}
        title={{title: 'Đánh giá đề thi', style: {color: '#ffffff'}}}
      />
    )
  }
}
