import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {FlatList} from 'react-native'
import {autobind} from 'core-decorators'
import ScoreItem from '../score-lists/ScoreItem'
import HeadingLine from '../../elements/heading-line'
import BoxAlert from '../../elements/box-alert/index'

const Scores = styled.View``

@autobind
export class ScoreList extends PureComponent {
  static propTypes = {
    ratings: PropTypes.array,
    onPressUser: PropTypes.func
  }

  renderItem ({item, index}) {
    return <ScoreItem onPressUser={this.props.onPressUser} stt={index + 1} {...item} />
  }

  render () {
    return (
      <FlatList
        data={this.props.scores}
        keyExtractor={(item, index) => item.id}
        renderItem={this.renderItem}
      />
    )
  }
}

export default class BoxScores extends PureComponent {
  static propTypes = {
    scores: PropTypes.array,
    pagination: PropTypes.shape({
      totalItem: PropTypes.number
    }),
    onPressViewAll: PropTypes.func,
    onPressUser: PropTypes.func
  }

  render () {
    return (
      <Scores>
        <HeadingLine
          noLine
          isShow
          title='Bảng điểm'
          totalItem={this.props.pagination.totalItem}
          onPress={this.props.onPressViewAll}
        />
        {this.props.scores.length > 0
          ? <ScoreList onPressUser={this.props.onPressUser} scores={this.props.scores.slice(0, 10)} />
          : <BoxAlert simpleLineIcon icon='people' content=' Chưa có thành viên nào làm bài.'
          />}
      </Scores>
    )
  }
}
