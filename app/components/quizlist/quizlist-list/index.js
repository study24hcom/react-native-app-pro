import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {FlatList, View} from 'react-native'
import {autobind} from 'core-decorators'
import QuizListItem from '../quizlist-item'
import Placeholder from 'rn-placeholder'
import BoxShadow from 'components/elements/WhiteBoxShadow'
import Clearfix from 'components/elements/clearfix'
import Loading from 'components/elements/loading'

@autobind
export default class QuizlistList extends PureComponent {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape(QuizListItem.propTypes)),
    onScrollToBottom: PropTypes.func,
    onRefresh: PropTypes.func,
    isLoadingMore: PropTypes.bool,
    isLoadingFirst: PropTypes.bool,
    onPressItem: PropTypes.func,
    onPressUser: PropTypes.func,
    isPressUser: PropTypes.bool,
    header: PropTypes.any
  }

  renderItem ({item}) {
    return <QuizListItem isPressUser={this.props.isPressUser} isPress onPressUser={this.props.onPressUser}
      onPress={this.props.onPressItem} {...item} />
  }

  renderLoading () {
    if (!this.props.isLoadingMore) return
    return (
      <Loading margin={0} padding={0} />
    )
  }

  renderPlaceHolder () {
    return <BoxShadow style={{marginBottom: 16}}>
      <Placeholder.Paragraph
        lineNumber={3}
        textSize={16}
        lineSpacing={5}
        color='#eeeeee'
        animate='shine'
        width='100%'
        lastLineWidth='70%'
        firstLineWidth='50%' />
    </BoxShadow>
  }

  render () {
    if (this.props.isLoadingFirst) {
      return <View>
        {this.props.header}
        {this.renderPlaceHolder()}
        {this.renderPlaceHolder()}
        {this.renderPlaceHolder()}
        {this.renderPlaceHolder()}
        {this.renderPlaceHolder()}
        {this.renderPlaceHolder()}
        {this.renderPlaceHolder()}
      </View>
    }
    return (
      <FlatList
        data={this.props.data}
        keyExtractor={item => item.id}
        renderItem={this.renderItem}
        ListHeaderComponent={this.props.header}
        ItemSeparatorComponent={() => <Clearfix height={8} />}
        ListFooterComponent={this.renderLoading()}
        onEndReached={this.props.onScrollToBottom}
        onEndReachedThreshold={0.3}
        onRefresh={this.props.onRefresh}
        refreshing={this.props.isResfreshing}
      />
    )
  }
}
