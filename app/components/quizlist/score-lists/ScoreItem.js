import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import UserAvatar from '../../elements/user-avatar'
import {getColorByNumber} from 'utils/color'
import Text from '../../elements/text/index'
import {SIZE, WEIGHT} from '../../../constants/font'
import {autobind} from 'core-decorators'

const ScoreWrapper = styled.View`
  display: flex;
  align-items: center;
  flex-direction: row;
  padding: 8px 16px;
  background-color: #fff;
`
const IndexColumn = styled.View`
  flex: 1;
`
const ColorSpan = styled(Text)`
  ${props =>
  props.color
    ? `color: ${props.color};`
    : `color: ${getColorByNumber(props.index)}`};
  font-size: 18px;
  font-weight: ${WEIGHT.BOLD};
`

const UserColumn = styled.TouchableOpacity`
  flex: 5;
`

const UserColumnView = styled.View`
  flex: 5;
`

const ScoreColumn = styled.View`
  flex: 1;
  justify-content: flex-end;
  flex-direction: row;
`
@autobind
export default class ScoreItem extends PureComponent {
  static propTypes = {
    stt: PropTypes.number,
    user: PropTypes.shape(UserAvatar.propTypes),
    score: PropTypes.number,
    totalQuestion: PropTypes.number,
    avatarSize: PropTypes.number,
    usernameSize: PropTypes.number,
    showTime: PropTypes.bool,
    onPressUser: PropTypes.func,
    noPress: PropTypes.bool
  }

  static defaultProps = {
    avatarSize: 40,
    usernameSize: SIZE.NORMAL,
    totalQuestion: 40
  }

  onPressUser () {
    this.props.onPressUser(this.props.user)
  }

  render () {
    const {stt, user, score, totalQuestion, noPress} = this.props
    return (
      <ScoreWrapper>
        <IndexColumn>
          <ColorSpan index={stt}>{stt}</ColorSpan>
        </IndexColumn>
        {noPress
          ? <UserColumnView>
            <UserAvatar usernameColor={'#333333'} {...user} avatarSize={this.props.avatarSize}
              usernameSize={this.props.usernameSize} />
          </UserColumnView>
          : <UserColumn activeOpacity={0.9} onPress={this.onPressUser}>
            <UserAvatar usernameColor={'#333333'} {...user} avatarSize={this.props.avatarSize}
              usernameSize={this.props.usernameSize} />
          </UserColumn>}
        <ScoreColumn>
          <ColorSpan index={stt}>{score}</ColorSpan>
          <ColorSpan color='#333333'>{` / ${totalQuestion}`}</ColorSpan>
        </ScoreColumn>
      </ScoreWrapper>
    )
  }
}
