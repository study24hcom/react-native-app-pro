import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import ScoreItem from '../score-lists/ScoreItem'
import HeadingLine from '../../elements/heading-line'
import shadowStyle from 'themes/shadow'

const Scores = styled.View``
const ScoreShadowWrapper = styled.TouchableOpacity``

export default class BoxAuthScore extends PureComponent {
  static propTypes = {
    score: PropTypes.object,
    onPress: PropTypes.func
  }

  render () {
    return (
      <Scores>
        <HeadingLine
          onPress={this.props.onPress}
          noLine
          hideTotalItem
          totalItem={1}
          viewAllText='Xem bài làm'
          title='Điểm của bạn'
        />
        <ScoreShadowWrapper
          onPress={this.props.onPress}
          style={[shadowStyle.shadow]}>
          <ScoreItem noPress stt={this.props.score.index} {...this.props.score} />
        </ScoreShadowWrapper>
      </Scores>
    )
  }
}
