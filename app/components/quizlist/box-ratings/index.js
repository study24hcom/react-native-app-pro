import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {FlatList} from 'react-native'
import {autobind} from 'core-decorators'
import RatingItem from '../rating-lists/RatingItem'
import HeadingLine from '../../elements/heading-line'
import BoxAlert from '../../elements/box-alert/index'

const Ratting = styled.View`
  flex: 1;
`

@autobind
export class RatingList extends PureComponent {
  static propTypes = {
    ratings: PropTypes.array,
    onPressUser: PropTypes.func
  }

  renderItem ({item}) {
    return <RatingItem onPressUser={this.props.onPressUser} horizontal {...item} />
  }

  render () {
    return (
      <FlatList
        data={this.props.ratings}
        horizontal
        inverted={false}
        keyExtractor={item => item.id}
        renderItem={this.renderItem}
        pagingEnabled
        showsHorizontalScrollIndicator={false}
      />
    )
  }
}

export default class BoxRatings extends PureComponent {
  static propTypes = {
    ratings: PropTypes.array,
    onPressUser: PropTypes.func,
    onPressViewAll: PropTypes.func,
    pagination: PropTypes.shape({
      totalItem: PropTypes.number
    })
  }

  render () {
    return (
      <Ratting>
        <HeadingLine
          noLine
          title='Đánh giá'
          isShow
          onPress={this.props.onPressViewAll}
          totalItem={this.props.pagination.totalItem}
        />
        {this.props.ratings.length > 0
          ? <RatingList onPressUser={this.props.onPressUser} ratings={this.props.ratings.slice(0, 5)} />
          : <BoxAlert
            icon='comments-o'
            content=' Hãy đánh giá bài làm sau khi làm bài nhé.'
          />}
      </Ratting>
    )
  }
}
