import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import TagItem from '../tag-item'

const TagListContainer = styled.View`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row
`
const Wrap = styled.View`
  ${props => (!props.isLast ? 'margin-right: 16px;' : '')}
  padding-bottom: 8px;
`
export default class TagList extends PureComponent {
  static propTypes = {
    onPress: PropTypes.func,
    tags: PropTypes.arrayOf(PropTypes.object),
    size: PropTypes.oneOf(['lg'])
  }

  render () {
    return (
      <TagListContainer>
        {this.props.tags.map((tag, index) => {
          return (
            tag.name &&
            <Wrap isLast={index === this.props.tags.length - 1} key={tag.id}>
              <TagItem onPress={() => this.props.onPress(tag.name)} size={this.props.size} name={tag.name} />
            </Wrap>
          )
        })}
      </TagListContainer>
    )
  }
}
