import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform } from 'react-native'
import styled from 'styled-components/native'
import { autobind } from 'core-decorators'
import { TEXT } from 'constants/color'
import Icon from '../../elements/Icon'
import { SIZE } from 'constants/font'
import HeaderTitle from '../../../../node_modules/react-navigation/lib/views/Header/HeaderTitle'
import HeaderBackButton from '../../../../node_modules/react-navigation/lib/views/Header/HeaderBackButton'

const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56
const TITLE_OFFSET = Platform.OS === 'ios' ? 70 : 56
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : 0

const StatusBar = styled.View`
  height: ${STATUSBAR_HEIGHT};
  `
const HeaderBar = styled.View`
  height: ${APPBAR_HEIGHT};
  flex-direction: row;
  background-color: ${TEXT.PRIMARY};
  align-items: center;
`
const HeaderLargeContainer = styled.View`
  background-color: ${TEXT.PRIMARY};
`
const HeaderBack = styled(HeaderBackButton)`
  position: absolute;
  left: 0;
`
const Title = styled(HeaderTitle)`
  color: #fff;
  left: ${TITLE_OFFSET};
  right: ${TITLE_OFFSET};
  position: absolute;
  `
const Right = styled.View`
  position: absolute;
  right: 0;
  border-radius: 40px;
`
@autobind
export default class Header extends Component {
  static propTypes = {
    navigation: PropTypes.any,
    onPress: PropTypes.func,
    children: PropTypes.string
  }

  goBack () {
    this.props.navigation.goBack(null)
  }

  render () {
    return <HeaderLargeContainer>
      <StatusBar />
      <HeaderBar>
        <HeaderBack tintColor='#fff' onPress={this.goBack} />
        <Title>{this.props.children}</Title>
        <Right>
          <Icon onPress={this.props.onPress} size={SIZE.HEADING7} color='#fff'
            paddingRight={16} simpleLineIcon
            name='options-vertical' />
        </Right>
      </HeaderBar>
    </HeaderLargeContainer>
  }
}
