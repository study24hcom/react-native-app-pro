import React from 'react'
import {View} from 'react-native'
import {autobind} from 'core-decorators'
import PopupDialog, {FadeAnimation} from 'react-native-popup-dialog'
import PropTypes from 'prop-types'
import ButtonBookmark from '../botton-bookmark'
import ButtonModal from '../../elements/botton-modal'

const fadeAnimation = new FadeAnimation({
  animationDuration: 300,
  toValue: 0
})

@autobind
export default class ModalBox extends React.Component {
  static propTypes = {
    onPressShareBox: PropTypes.func,
    onPressAddPlaylist: PropTypes.func,
    isAuthQuiz: PropTypes.bool,
    quizListId: PropTypes.string,
    onPressBookmark: PropTypes.bool
  }

  state = {
    height: 0
  }

  open () {
    this.popupDialog.show()
  }

  close () {
    this.popupDialog.dismiss()
  }

  resizeModal (ev) {
    this.setState({height: ev.nativeEvent.layout.height + 2})
  }

  render () {
    return <PopupDialog
      dialogStyle={{marginTop: -150}}
      dialogAnimation={fadeAnimation}
      height={this.state.height}
      ref={(popupDialog) => {
        this.popupDialog = popupDialog
      }}>
      <View onLayout={(ev) => this.resizeModal(ev)}>
        <ButtonModal onPress={this.props.onPressShareBox} icon='share-alt' title='Chia sẻ Facebook'
          messenger='Chia sẻ đề thi đến bạn bè Facebook' />
        {/* <ButtonModal onPress={this.props.onPressAddPlaylist} icon='plus' title='Thêm vào bộ sưu tập' */}
        {/* messenger='Tạo bộ sưu tập' /> */}
        <ButtonBookmark quizListId={this.props.quizListId} onClose={this.close} />
        {/* {this.props.isAuthQuiz && */}
        {/* <ButtonModal onPress={this.props.onPressDeleteQuizList} icon='trash' title='Xóa đề thi' */}
        {/* messenger='Xóa đề thi này khỏi hệ thống' />} */}
      </View>
    </PopupDialog>
  }
}
