import React from 'react'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'
import StarView from '../../elements/star-view/index'
import TextIcon from '../../elements/text-icon/index'
import ModalRating from '../modal-rating/index'
import {autobind} from 'core-decorators'
import {SIZE} from 'constants/font'
import {connectAutoDispatch} from '@redux/connect'
import {setAuthRating, addRating} from '@redux/actions/quizAddonAction'

const View = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 16px 8px;
  align-items: center;
  `
const Touch = styled.TouchableOpacity``
@connectAutoDispatch(null, {setAuthRating, addRating})
@autobind
export default class BoxWriteRating extends React.Component {
  static propTypes = {
    quizListId: PropTypes.string
  }
  state = {
    rating: 0
  }

  onPress () {
    this.modal.open()
  }

  onPressExit () {
    this.modal.close()
  }

  onChangeStar (rating) {
    this.setState({rating})
  }

  render () {
    return (<View>
      <StarView size={20} value={this.state.rating} onChange={this.onChangeStar} />
      <Touch onPress={this.onPress}>
        <TextIcon size={SIZE.NORMAL} icon='pencil-square-o'>Viết nhận xét</TextIcon>
      </Touch>
      <ModalRating ref={modal => {
        this.modal = modal
      }}
        addRating={this.props.addRating}
        setAuthRating={this.props.setAuthRating}
        onPressExit={this.onPressExit}
        rating={this.state.rating}
        onChangeStar={this.onChangeStar}
        quizListId={this.props.quizListId} />
    </View>
    )
  }
}
