import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Dimensions, StyleSheet} from 'react-native'
import styled from 'styled-components/native'
import Markdown from 'react-native-simple-markdown'
import toShortString from 'to-short-string'
import {SIZE} from 'constants/font'
import Avatar from '../../elements/user-avatar/avatar'
import StarView from '../../elements/star-view'
import Clearfix from 'components/elements/clearfix'
import Text from '../../elements/text'
import {autobind} from 'core-decorators'

const window = Dimensions.get('window')
const AvartarWrapper = styled.TouchableOpacity``
const View = styled.View``
const RatingItemContainer = styled.View`
  min-height: 150px;
  padding: 16px;
  width: ${window.width - 16}px;
  background-color: #fff;
  border-radius: 4px;
`
const Wrapper = styled.View`
  width: ${window.width}px;
  background-color: transparent
  padding-horizontal: 8px;
  padding-vertical: 4px;
`
const HeaderContainer = styled.View`
  display: flex;
  flex-direction: row;
`
const HeadInfo = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: 4px;
`
const HeaderRight = styled.View`
  flex-direction: column;
  margin-left: 8px;
  margin-top: -4px;
  flex: 1;
  `
const Username = styled(Text)`
  font-size: ${SIZE.NORMAL}
  `
const BodyContainer = styled.View`
  margin-left: ${props => props.size ? `${props.size}px` : `0`};
  `
const MarkdownDisplay = styled(Markdown)``
const markdownStyles = {
  text: {
    color: '#8f949a',
    fontSize: SIZE.HEADING6,
    fontWeight: '300',
    fontFamily: 'OpenSans-Bold'
  }
}

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 1.5
    },
    shadowRadius: 2,
    shadowOpacity: 0.1
  }
})
@autobind
export default class RatingItem extends Component {
  static propTypes = {
    user: PropTypes.shape({
      username: PropTypes.string,
      avatar: PropTypes.string
    }),
    rating: PropTypes.number,
    content: PropTypes.string,
    comment: PropTypes.shape({
      user: PropTypes.shape({
        username: PropTypes.string,
        avatar: PropTypes.string
      }),
      content: PropTypes.string
    }),
    isDetails: PropTypes.bool,
    onPressUser: PropTypes.func
  }

  onPressUser () {
    this.props.onPressUser(this.props.user)
  }

  renderForComment () {
    const {comment: {user: {username}, content, user}} = this.props
    if (!username) return null
    return (
      <View>
        <Clearfix height={24} />
        <HeaderContainer>
          <AvartarWrapper activeOpacity={0.9} onPress={this.onPressUser}>
            <Avatar {...user} avatarSize={35} />
          </AvartarWrapper>
          <HeaderRight>
            <HeadInfo>
              <Username style={{flex: 1}} color={'#333'} fontWeight={500}>{user.username}</Username>
            </HeadInfo>
            {this.props.isDetails
              ? <MarkdownDisplay styles={markdownStyles}>{content}</MarkdownDisplay>
              : <MarkdownDisplay styles={markdownStyles}>{toShortString(content, 100, 1)}</MarkdownDisplay>}
          </HeaderRight>
        </HeaderContainer>
      </View>
    )
  }

  shouldComponentUpdate () {
    return false
  }

  render () {
    const {user, rating, content, comment} = this.props
    return (
      <Wrapper>
        <RatingItemContainer style={styles.shadow} horizontal={this.props.horizontal}>
          <HeaderContainer>
            <AvartarWrapper activeOpacity={0.9} onPress={this.onPressUser}>
              <Avatar {...user} avatarSize={40} />
            </AvartarWrapper>
            <HeaderRight>
              <HeadInfo>
                <Username style={{flex: 1}} color={'#333'} fontWeight={500}>{user.username}</Username>
                <StarView size={SIZE.SMALL} value={rating} readOnly />
              </HeadInfo>
              {this.props.isDetails
                ? <MarkdownDisplay styles={markdownStyles}>{content}</MarkdownDisplay>
                : <MarkdownDisplay styles={markdownStyles}>{toShortString(content, 100, 1)}</MarkdownDisplay>}
            </HeaderRight>
          </HeaderContainer>
          <BodyContainer size={40}>
            {comment.isComment && this.renderForComment()}
          </BodyContainer>
        </RatingItemContainer>
      </Wrapper>
    )
  }
}
