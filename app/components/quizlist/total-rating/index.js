import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {SHAPE, TEXT} from 'constants/color'
import StarRating from 'react-native-star-rating'
import {SIZE} from '../../../constants/font'

const SpanTotal = styled.Text`
  color: ${TEXT.GRAY};
  font-weight: 600;
  font-size: ${props => props.size ? `${props.size}` : `${SIZE.NORMAL}`}
  `
const StarRatingWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  flex: 1;
  `

export default function TotalRating ({total, value, size}) {
  return (
    <StarRatingWrapper>
      <StarRating
        disabled
        emptyStar={'ios-star'}
        emptyStarColor={SHAPE.GRAYBOLDER}
        fullStar={'ios-star'}
        fullStarColor={SHAPE.YELLOW}
        halfStar={'ios-star-half'}
        halfStarColor={SHAPE.YELLOW}
        iconSet={'Ionicons'}
        maxStars={5}
        starSize={size}
        rating={value}
      />
      <SpanTotal size={size}>{`  (${total})`}</SpanTotal>
    </StarRatingWrapper>
  )
}
TotalRating.propTypes = {
  total: PropTypes.number,
  ratingAvg: PropTypes.number,
  size: PropTypes.number
}
TotalRating.defaultProps = {
  size: 20
}
