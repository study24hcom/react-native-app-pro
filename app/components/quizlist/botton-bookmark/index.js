import React from 'react'
import PropTypes from 'prop-types'
import { autobind } from 'core-decorators'
import createBookmarkQuizlist from '../../../hoc/bookmark-quizlist'
import ButtonModal from '../../elements/botton-modal'

@createBookmarkQuizlist({autoCheck: true})
@autobind
export default class ButtonBookmark extends React.Component {
  static PropTypes = {
    isChecking: PropTypes.bool,
    isBookmarked: PropTypes.bool,
    onClick: PropTypes.func,
    quizListId: PropTypes.string,
    onClose: PropTypes.func
  }

  onClick () {
    this.props.onClick()
    this.props.onClose()
  }

  render () {
    return <ButtonModal onPress={this.onClick} icon='bookmark-o'
      title={this.props.isBookmarked ? 'Bỏ lưu' : 'Lưu trữ'}
      messenger={this.props.isBookmarked ? 'Xóa đề thi khỏi mục đã lưu' : 'Đề thi sẽ được lưu tại mục đã lưu'}
      isChecking={this.props.isChecking}
    />
  }
}
