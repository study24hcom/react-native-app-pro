import React, {PureComponent} from 'react'
import {withNavigation} from 'react-navigation'
import styled from 'styled-components/native'
import {TEXT, SHAPE} from 'constants/color'
import {SIZE} from 'constants/font'
import {autobind} from 'core-decorators'
import Icon from 'components/elements/Icon'
import Text from 'components/elements/text'

const SearchContainer = styled.TouchableOpacity`
  flex: 1;
  background-color: ${TEXT.PRIMARYBOLD};
  flex-direction: row;
  height: 30px;
  margin-horizontal: 4px;
  align-items: center;
  justify-content: center;
  border-radius: 15px;
  padding-horizontal: 16px;
`
@withNavigation
@autobind
export default class Search extends PureComponent {
  onPressSearch () {
    this.props.navigation.navigate('SearchLayoutScreen')
  }

  render () {
    return (
      <SearchContainer activeOpacity={1} onPress={this.onPressSearch}>
        <Icon name='search' size={SIZE.NORMAL} color={SHAPE.GRAYMORELIGHT} />
        <Text numberOfLines={1} fontSize={SIZE.NORMAL} color={SHAPE.GRAYMORELIGHT}> Tìm đề thi, tag, chuyên
          mục...</Text>
      </SearchContainer>
    )
  }
}
