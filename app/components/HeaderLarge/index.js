import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Platform, Alert} from 'react-native'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {getNewNotifications} from '@redux/actions/notificationAction'
import {TEXT} from 'constants/color'
import Search from './Search'
import Menu from './Left/Menu'
import Logo from './Left/Logo'
import NotificationIcon from './Right/Notification'
import SettingIcon from './Right/Setting'
import Create from './Right/Create'

const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : 0

const StatusBar = styled.View`
  height: ${STATUSBAR_HEIGHT}px;
`
const HeaderBar = styled.View`
  height: ${APPBAR_HEIGHT};
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
`
const HeaderLargeContainer = styled.View`
  background-color: ${TEXT.PRIMARY};
`
@connectAwaitAutoDispatch(
  state => ({
    newNotifications: state.notification.newNotifications
  }),
  {getNewNotifications}
)
@autobind
export default class HeaderLarge extends Component {
  static propTypes = {
    navigation: PropTypes.any
  }

  componentDidMount () {
    this.props.getNewNotifications({})
  }

  autoDelay (callback) {
    if (!this.delay) {
      callback()
      this.delay = true
    }
    setTimeout(() => {
      this.delay = false
    }, 600)
  }

  onPressNotification () {
    this.autoDelay(() => {
      this.props.navigation.navigate('NotificationScreen')
    })
  }

  onPressMenu () {
    this.autoDelay(() => {
      this.props.navigation.navigate('DrawerOpen')
    })
  }

  onPressLogo () {
    this.autoDelay(() => {
    })
  }

  onPressCreatePlaylist () {
    Alert.alert('Tạo playlist', 'Tính năng này đang được phát triển. Vui lòng quay trở lại sau.')
    // this.autoDelay(() => {
    //   this.props.navigation.navigate('CreatePlaylistScreen')
    // })
  }

  onPressSetting () {
    this.autoDelay(() => {
      this.props.navigation.navigate('SettingScreen')
    })
  }

  render () {
    let screen = this.props.navigation.state.routeName
    return <HeaderLargeContainer>
      <StatusBar />
      <HeaderBar>
        <Menu onPress={this.onPressMenu} />
        <Search />
        {screen === 'tabInfo' && <SettingIcon onPressItem={this.onPressSetting} />}
        {screen === 'tabPlaylist' && <Create onPressItem={this.onPressCreatePlaylist} />}
        {(screen !== 'tabInfo' && screen !== 'tabPlaylist') &&
        <NotificationIcon badge={this.props.newNotifications} onPressItem={this.onPressNotification} />}
      </HeaderBar>
    </HeaderLargeContainer>
  }
}
