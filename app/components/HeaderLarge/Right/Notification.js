import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Badge from '../../elements/badge/index'

const TouchableOpacity = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`
const IconWrapper = styled.View`
  width: 45px;
  height: 45px;
  justify-content: center;
  align-items: center
`
export default class NotificationIcon extends PureComponent {
  static propTypes = {
    badge: PropTypes.number,
    onPressItem: PropTypes.func
  }

  render () {
    return (
      <IconWrapper>
        <TouchableOpacity onPress={this.props.onPressItem}>
          <Badge badge={this.props.badge} icon='bell' />
        </TouchableOpacity>
      </IconWrapper>
    )
  }
}
