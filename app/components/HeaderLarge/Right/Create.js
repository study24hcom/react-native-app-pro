import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Icon from 'components/elements/Icon'

const TouchableOpacity = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`
const IconWrapper = styled.View`
  width: 45px;
  height: 45px;
  justify-content: center;
  align-items: center
`
export default class CreateIcon extends PureComponent {
  static propTypes = {
    onPressItem: PropTypes.func
  }

  render () {
    return (
      <IconWrapper>
        <TouchableOpacity onPress={this.props.onPressItem}>
          <Icon name='plus' color='#fff' size={21} />
        </TouchableOpacity>
      </IconWrapper>
    )
  }
}
