import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Icon from '../../elements/Icon'

const TouchableOpacity = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`
const IconWrapper = styled.View`
  width: 45px;
  height: 45px;
  justify-content: center;
  align-items: center
`
export default class SettingIcon extends PureComponent {
  static propTypes = {
    onPressItem: PropTypes.func
  }

  render () {
    return (
      <IconWrapper>
        <TouchableOpacity onPress={this.props.onPressItem}>
          <Icon simpleLineIcon name='settings' color='#fff' size={21} />
        </TouchableOpacity>
      </IconWrapper>
    )
  }
}
