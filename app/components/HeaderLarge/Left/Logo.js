import React, {PureComponent} from 'react'
import styled from 'styled-components/native'

const LogoContainer = styled.TouchableOpacity`
  width: 45px;
  height: 45px;
  justify-content: center;
  align-items: center;
  flex-direction: row
`
const LogoImage = styled.Image`
  width: 35px;
  height: 35px;
  resize-mode: cover;
  `
export default class Logo extends PureComponent {
  render () {
    return (
      <LogoContainer onPress={this.props.onPress}>
        <LogoImage source={require('../../../images/Logo_icon.png')} />
      </LogoContainer>
    )
  }
}
