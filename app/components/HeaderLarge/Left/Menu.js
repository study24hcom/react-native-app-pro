import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Icon from 'components/elements/Icon'
import {SIZE} from 'constants/font'

const MenuContainer = styled.TouchableOpacity`
  width: 45px;
  height: 45px;
  justify-content: center;
  align-items: center;
  flex-direction: row
`
export default class Menu extends PureComponent {
  static propTypes = {
    onPress: PropTypes.func
  }

  render () {
    return (
      <MenuContainer onPress={this.props.onPress}>
        <Icon simpleLineIcon color={'#fff'} size={SIZE.HEADING1} name={'menu'} />
      </MenuContainer>
    )
  }
}
