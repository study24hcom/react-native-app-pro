import React, {PureComponent} from 'react'
import {StyleSheet, Dimensions} from 'react-native'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import BoxImage from '../elements/box-image/index'
import PlaylistBox from './playlist-box'
import {autobind} from 'core-decorators'
import {Answers} from 'react-native-fabric'

const {width} = Dimensions.get('window')
const WrapperContainer = styled.View`
    background-color: #ffffff;
    ${props => (props.horizontal ? `width: ${width}` : '')};
    ${props => (props.horizontal ? `border-top-width: 0.5px;` : '')};
    ${props => (props.horizontal ? `border-top-color: #E8E8E8` : '')};
    ${props => (props.horizontal ? `flex: 1` : '')};
`
const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#DDDDDD',
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowRadius: 4,
    shadowOpacity: 2,
    elevation: 0.4
  }
})
@autobind
export default class PlaylistItem extends PureComponent {
  static propTypes = {
    onPressItem: PropTypes.func,
    info: PropTypes.shape({
      image: PropTypes.string,
      name: PropTypes.string,
      user: PropTypes.shape(BoxImage.propTypes)
    }),
    quizLists: PropTypes.array,
    horizontal: PropTypes.bool,
    onPressUser: PropTypes.func,
    isPressUser: PropTypes.bool,
    playlistItem: PropTypes.shape({
      info: PropTypes.shape({
        image: PropTypes.string,
        name: PropTypes.string,
        user: PropTypes.shape(BoxImage.propTypes)
      }),
      quizLists: PropTypes.array
    })
  }

  onPressItem () {
    this.props.onPressItem(this.props.playlistItem)
    Answers.logContentView(this.props.playlistItem.info.name, 'PLAYLIST', this.props.playlistItem.info.name)
  }

  onPressUser () {
    this.props.onPressUser(this.props.info.user)
    Answers.logContentView(this.props.info.user.username, 'USER', this.props.info.user.username)
  }

  render () {
    const {info, quizLists} = this.props
    return (
      <WrapperContainer horizontal={this.props.horizontal} style={styles.shadow}>
        <BoxImage src={info.image} alt={info.name} onPress={this.onPressItem}>
          <PlaylistBox isPressUser={this.props.isPressUser} onPressUser={this.onPressUser} isPress
            onPress={this.onPressItem} info={info}
            quizListsLength={quizLists.length} />
        </BoxImage>
      </WrapperContainer>
    )
  }
}
