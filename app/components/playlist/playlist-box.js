import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {SHAPE} from 'constants/color'
import Title from '../elements/title'
import Avatar from '../elements/user-avatar/avatar'
import {autobind} from 'core-decorators'
import TextIcon from '../elements/text-icon/index'
import {
  WrapperContainer,
  TitleOptionWrapper,
  TitleViewWrapper,
  ShortInfo,
  AvatarWrapper,
  AvatarWrapperTouch,
  SpanUsername,
  TextAdView,
  styles
} from './style'
import {SIZE, WEIGHT} from '../../constants/font'

@autobind
export default class PlaylistItem extends PureComponent {
  static propTypes = {
    onPress: PropTypes.func,
    quizListsLength: PropTypes.number,
    info: PropTypes.shape({
      image: PropTypes.string,
      name: PropTypes.string,
      user: PropTypes.shape(Avatar.PropTypes)
    }),
    isPress: PropTypes.bool,
    onPressUser: PropTypes.func
  }

  render () {
    const {info, quizListsLength, onPress, isPress, isPressUser} = this.props
    return (
      <WrapperContainer elevation={0.18} style={styles.shadow}>
        {isPress
          ? <TitleOptionWrapper onPress={onPress} activeOpacity={0.9}>
            <Title isPress>{info.name}</Title>
          </TitleOptionWrapper>
          : <TitleViewWrapper>
            <Title>{info.name}</Title>
          </TitleViewWrapper>}
        <ShortInfo>
          {quizListsLength
            ? <TextIcon fontWeight={WEIGHT.BOLD} size={SIZE.NORMAL} icon={`question-circle-o`}
              color={SHAPE.GREEN}>{`${quizListsLength} đề`}</TextIcon> : null}
          {(isPressUser && info.user)
            ? <AvatarWrapperTouch activeOpacity={0.9} onPress={this.props.onPressUser}>
              <Avatar avatar={info.user.avatar} avatarSize={SIZE.NORMAL} username={info.user.username} />
              <SpanUsername>{info.user.username}</SpanUsername>
              {info.user.isPartner && !info.user.admin && <TextAdView>P</TextAdView>}
              {info.user.admin && <TextAdView>Ad</TextAdView>}
            </AvatarWrapperTouch> : null}
          {(!isPressUser && info.user)
            ? <AvatarWrapper>
              <Avatar avatar={info.user.avatar} avatarSize={SIZE.NORMAL} username={info.user.username} />
              <SpanUsername>{info.user.username}</SpanUsername>
              {info.user.isPartner && !info.user.admin && <TextAdView>P</TextAdView>}
              {info.user.admin && <TextAdView>Ad</TextAdView>}
            </AvatarWrapper> : null}
        </ShortInfo>
        {this.props.children}
      </WrapperContainer>
    )
  }
}
