import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {FlatList, View} from 'react-native'
import {autobind} from 'core-decorators'
import PlaylistItem from './playlist-item'
import PlaceholderPlaylist from '../placeholder'
import Clearfix from 'components/elements/clearfix/index'
import LoadingCircle from 'components/elements/loading/LoadingCircle'

@autobind
export default class PlaylistList extends Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape(PlaylistItem.propTypes)),
    onScrollToBottom: PropTypes.func,
    onRefresh: PropTypes.func,
    isLoadingMore: PropTypes.bool,
    isLoadingFirst: PropTypes.bool,
    isRefreshing: PropTypes.bool,
    onPressItem: PropTypes.func,
    onPressUser: PropTypes.func,
    isPressUser: PropTypes.bool,
    header: PropTypes.any
  }

  renderItem ({item}) {
    return <PlaylistItem isPressUser={this.props.isPressUser} onPressUser={this.props.onPressUser}
      {...item} playlistItem={item} onPressItem={this.props.onPressItem} />
  }

  renderLoading () {
    if (!this.props.isLoadingMore) return
    return <LoadingCircle />
  }

  renderLoadFirst () {
    return <View>
      {this.props.header}
      <PlaceholderPlaylist margin={10} height={180} />
      <PlaceholderPlaylist margin={10} height={180} />
      <PlaceholderPlaylist margin={10} height={180} />
    </View>
  }

  render () {
    if (this.props.data.length === 0 && this.props.isLoadingFirst) {
      return this.renderLoadFirst()
    }
    return (
      <FlatList
        data={this.props.data}
        keyExtractor={(item) => item.id}
        renderItem={this.renderItem}
        ListHeaderComponent={this.props.header}
        ItemSeparatorComponent={() => <Clearfix height={8} />}
        ListFooterComponent={this.renderLoading()}
        onEndReached={this.props.onScrollToBottom}
        onEndReachedThreshold={0}
        onRefresh={this.props.onRefresh}
        refreshing={this.props.isRefreshing}
      />
    )
  }
}
