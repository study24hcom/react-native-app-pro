import React, { Component } from 'react'
import styled from 'styled-components/native'
import { createValidateComponent } from 'components/elements/redux-form-validate'
import { reduxForm, Field } from 'redux-form'
import InputLabel from 'components/elements/input-label'
import InputSelect from 'components/elements/input-select'
import categories from 'constants/categories'
import { validateObject } from 'utils'
import Text from 'components/elements/text'
// import Icon from 'components/elements/Icon'
import Toggle from 'components/elements/toggle'
import ButtonCustom from '../elements/button/index'
import trim from 'trim'

const CreateContainerView = styled.View`
  flex: 1;
  margin: 8px;
  `

// const View = styled.View``
const FInputLabel = createValidateComponent(InputLabel)
const FMarkdown = createValidateComponent(InputLabel)
const FInputSelect = createValidateComponent(InputSelect)
// const FSelectImage = createValidateComponent(SelectImage)
const FToggle = createValidateComponent(Toggle)

const categoryOptions = categories.map(category => ({
  ...category,
  value: category.id,
  label: category.name
}))

// const OptionCategory = option => (
//   <View>
//     <Icon className={option.icon} /> {option.label}
//   </View>
// )

const constraints = {
  name: {
    presence: {
      message: '^Tên của playlist không được để trống'
    },
    length: {
      minimum: 3,
      tooShort: '^Tên playlist có ít nhất %{count} từ',
      tokenizer: function (value) {
        return trim(value).split(/\s+/g)
      }
    }
  },
  description: {
    presence: {
      message: '^Mô tả không được để trống'
    },
    length: {
      minimum: 3,
      tooShort: '^Mô tả cần có ít nhất %{count} từ',
      tokenizer: function (value) {
        return trim(value).split(/\s+/g)
      }
    }
  }
}

const validate = values => {
  const errors = validateObject(values, constraints)
  if (!values.category) {
    errors.category = 'Vui lòng chọn chuyên mục'
  }
  return errors
}

@reduxForm({
  form: 'PlaylistInformationForm',
  validate,
  destroyOnUnmount: false
})
export default class CreatePlaylist extends Component {
  submitCheck (callback) {
    this.props.handleSubmit(callback)()
  }

  render () {
    return <CreateContainerView>
      <Field
        name='name'
        label='Tên bộ sưu tập'
        placeholder='Tên bộ sưu tập của bạn'
        component={FInputLabel}
      />
      <Field
        name='description'
        label='Mô tả'
        placeholder='Mô tả bài thi của bạn'
        component={FMarkdown}
      />
      {/* <Field */}
      {/* name='image' */}
      {/* label='Hình ảnh đại diện' */}
      {/* component={FSelectImage} */}
      {/* /> */}
      <Field
        name='category'
        label='Chuyên mục'
        placeholder='Chọn chuyên mục'
        options={categoryOptions}
        component={FInputSelect}
      />
      <Field
        name='isDisplayInProfile'
        label='Hiển thị ở trang profile của bạn'
        component={FToggle}
      />
      <ButtonCustom onClick={this.submitCheck}><Text>Tạo</Text></ButtonCustom>
    </CreateContainerView>
  }
}
