import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {FlatList} from 'react-native'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import {connectAutoDispatch} from '@redux/connect'
import {setInfo} from '@redux/actions/quizAction'
import HeadingLine from '../elements/heading-line'
import QuizListItem from '../quizlist/quizlist-item'

const PlaylistQuizlistScreenContainer = styled.View`
  flex: 1;
`
const Clearfix = styled.View`
  height: 16px;
`

@connectAutoDispatch(null, {setInfo})
@autobind
export class Quizlists extends PureComponent {
  static propTypes = {
    quizLists: PropTypes.arrayOf(PropTypes.shape(QuizListItem.propTypes))
  }

  renderItem ({item}) {
    return <QuizListItem isPressUser onPressUser={this.props.onPressUser} isPress
      onPress={() => this.onPressItem(item)} {...item} />
  }

  onPressItem (quizListItem) {
    if (!this.delay) {
      this.props.setInfo(quizListItem)
      this.props.navigation.navigate('QuizListDetailScreen', {
        title: quizListItem.name
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  render () {
    return (
      <FlatList
        data={this.props.quizLists}
        ItemSeparatorComponent={() => <Clearfix />}
        contentContainerStyle={{padding: 8, paddingBottom: 0}}
        keyExtractor={item => item.id}
        renderItem={this.renderItem}
      />
    )
  }
}

export default class PlaylistQuizlist extends PureComponent {
  static propTypes = {
    quizLists: PropTypes.arrayOf(PropTypes.shape(Quizlists.propTypes)),
    quizListsLength: PropTypes.number,
    onPress: PropTypes.func,
    navigation: PropTypes.any,
    onPressUser: PropTypes.func,
    isPressUser: PropTypes.bool
  }

  render () {
    return (
      <PlaylistQuizlistScreenContainer>
        <HeadingLine isShow isPlaylist title='Đề thi' onPress={this.props.onPress}
          totalItem={this.props.quizListsLength} />
        <Quizlists isPressUser={this.props.isPressUser} onPressUser={this.props.onPressUser}
          navigation={this.props.navigation} quizLists={this.props.quizLists.slice(0, 10)} />
      </PlaylistQuizlistScreenContainer>
    )
  }
}
