import React from 'react'
import {StyleSheet} from 'react-native'
import styled from 'styled-components/native'
import Text from '../../components/elements/text'
import {SHAPE} from 'constants/color'
import {SIZE, WEIGHT} from 'constants/font'

export const WrapperContainer = styled.View`
  flex: 1;
  padding: 16px;
  padding-bottom: 24px;
`
export const TitleOptionWrapper = styled.TouchableOpacity`
  flex: 1;
  `
export const TitleViewWrapper = styled.View`
  flex: 1;
  `

export const ShortInfo = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-top: 16px;
`

export const AvatarWrapperTouch = styled.TouchableOpacity`
  flex: 1;
  flex-direction: row;
  align-items: center;
`

export const AvatarWrapper = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
`

export const SpanUsername = styled(Text)`
  padding-left: 8px;
  font-weight: ${WEIGHT.NORMAL};
  font-size: ${SIZE.NORMAL};
  `
export const TextAd = styled(Text)`
  color: #fff;
  font-size: 12px;
  `

export const TextView = styled.View`
  border-radius: 4px;
  background-color: ${SHAPE.RED};
  padding-horizontal: 8px;
  margin-left: 4px;
`
export const TextAdView = ({children}) => (
  <TextView>
    <TextAd>{children}</TextAd>
  </TextView>
)
export const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#D4D4D4',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 3,
    shadowOpacity: 0.25
  }
})
export default {
  WrapperContainer,
  TitleOptionWrapper,
  TitleViewWrapper,
  ShortInfo,
  AvatarWrapper,
  AvatarWrapperTouch,
  SpanUsername,
  styles
}
