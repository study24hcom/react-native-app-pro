import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { autobind } from 'core-decorators'
import WKWebView from 'react-native-wkwebview-reborn'
import Cookie from 'react-native-cookie'
import { TUNGTUNG_BROWSER_PUBLIC } from 'config'

function getUrl (url) {
  return TUNGTUNG_BROWSER_PUBLIC + url
}

@connect(
  state => ({
    authToken: state.auth.token
  }),
  null,
  null,
  { withRef: true }
)
@autobind
export default class TungtungWkWebView extends React.Component {
  static propTypes = {
    url: PropTypes.string,
    authToken: PropTypes.string,
    onMessage: PropTypes.func,
    onProgress: PropTypes.func
  }

  static defaultProps = {
    onProgress: () => {}
  }

  sendPostMessage (data = {}) {
    var stringParse = ''
    Object.keys(data).map(key => {
      stringParse += `${key}:${data[key]}`
    })
    this.webview.evaluateJavaScript(
      'receivedMessageFromReactNative("' + stringParse + '")'
    )
  }

  handleMessageBrowser (e) {
    const { type, data } = e.body.data
    this.props.onMessage(type, data)
  }

  componentWillMount () {
    Cookie.set(getUrl(this.props.url), 'authToken', this.props.authToken).then(
      () => {
        console.log(`set cookie '${this.props.url} for authToken`)
      }
    )
  }

  render () {
    return (
      <WKWebView
        ref={ref => {
          this.webview = ref
        }}
        sendCookies
        onProgress={this.props.onProgress}
        style={{ flex: 1 }}
        injectedJavaScript={`window.setAuthToken("${this.props.authToken}");`}
        onMessage={this.handleMessageBrowser}
        source={{
          uri: getUrl(this.props.url)
        }}
      />
    )
  }
}
