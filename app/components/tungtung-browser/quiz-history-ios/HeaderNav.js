import React from 'react'
import { autobind } from 'core-decorators'
import Icon from 'components/elements/Icon'
import PropTypes from 'prop-types'
import { StyleSheet, TouchableOpacity } from 'react-native'
import NavigationBar from 'react-native-navbar'
import Text from 'components/elements/text/index'
import { SHAPE } from 'constants/color'

const styles = StyleSheet.create({
  buttonNav: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

@autobind
export default class HeaderNav extends React.PureComponent {
  static propTypes = {
    onPressMenu: PropTypes.func,
    onPressDone: PropTypes.func,
    isMenuOpen: PropTypes.bool,
    title: PropTypes.string
  }

  renderRight () {
    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={this.props.onPressDone}
        style={[styles.buttonNav, { paddingRight: 8, paddingLeft: 8 }]}
        underlayColor='transparent'
      >
        <Text fontWeight={600} color='#ffffff'>
          Xong
        </Text>
      </TouchableOpacity>
    )
  }

  renderLeft () {
    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={this.props.onPressMenu}
        style={[styles.buttonNav, { paddingLeft: 8, paddingRight: 8 }]}
        underlayColor='transparent'
      >
        <Text>
          <Icon
            size={22}
            color='#ffffff'
            name={this.props.isMenuOpen ? 'arrow-left' : 'list'}
            simpleLineIcon
          />
        </Text>
      </TouchableOpacity>
    )
  }

  render () {
    return (
      <NavigationBar
        statusBar={{ style: 'light-content' }}
        containerStyle={{ backgroundColor: SHAPE.GREEN }}
        style={{ backgroundColor: SHAPE.GREEN }}
        leftButton={this.renderLeft()}
        rightButton={this.renderRight()}
        title={{ title: 'Xem bài làm', style: { color: '#ffffff' } }}
      />
    )
  }
}
