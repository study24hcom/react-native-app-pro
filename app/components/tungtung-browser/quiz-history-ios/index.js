import React, {Component} from 'react'
import PropTypes from 'prop-types'
import SideMenuDrawer from 'react-native-side-menu'
import {autobind} from 'core-decorators'
import Loading from 'components/loading-lottie/QuizHistoryLoading'
import styled from 'styled-components/native'
import TungtungWkWebView from '../wkwebview'
import SideMenu from '../quiz-player-ios/side-menu'
import HeaderNav from './HeaderNav'

const PlayerWrapper = styled.View`
  flex: 1;
  position: relative;
`

function getUrlHistory (slug, historyId) {
  return '/history-mobile/' + slug + '/' + historyId
}

@autobind
export default class QuizHistoryIos extends Component<{}> {
  static propTypes = {
    slug: PropTypes.string,
    historyId: PropTypes.string
  }

  state = {
    isOpen: false,
    isLoaded: false,
    progress: 0,
    quizzes: []
  }

  toggleMenu (isOpen) {
    this.setState({isOpen: isOpen})
  }

  toggleMenuFromNavbar () {
    this.setState({isOpen: !this.state.isOpen})
  }

  componentDidMount () {
    setTimeout(() => {
      this.setState({progress: 0.2})
    }, 200)
  }

  handleProgress (progress) {
    this.setState({progress})
  }

  renderMenuQuestion () {
    return (
      <SideMenu
        onPressQuestion={this.handlePressQuestionIndex}
        onClose={this.toggleMenu}
        showResult
        quizzes={this.state.quizzes}
      />
    )
  }

  handlePressQuestionIndex (index) {
    this.webview.sendPostMessage({goToQuiz: index})
    this.toggleMenuFromNavbar()
  }

  handleMessageBrowser (type, data) {
    switch (type) {
      case 'changeQuizzes':
        this.setState({quizzes: data})
        break
      case 'loaded':
        this.setState({isLoaded: true})
        break
      default:
    }
  }

  render () {
    return (
      <SideMenuDrawer
        isOpen={this.state.isOpen}
        onChange={this.toggleMenu}
        menu={this.renderMenuQuestion()}
      >
        <HeaderNav
          isMenuOpen={this.state.isOpen}
          onPressMenu={this.toggleMenuFromNavbar}
          onPressDone={this.props.onPressDone}
        />
        <PlayerWrapper>
          <TungtungWkWebView
            ref={ref => {
              if (ref) {
                this.webview = ref.getWrappedInstance()
              }
            }}
            onProgress={this.handleProgress}
            url={getUrlHistory(this.props.slug, this.props.historyId)}
            onMessage={this.handleMessageBrowser}
          />
          {!this.state.isLoaded && <Loading progress={this.state.progress} />}
        </PlayerWrapper>
      </SideMenuDrawer>
    )
  }
}
