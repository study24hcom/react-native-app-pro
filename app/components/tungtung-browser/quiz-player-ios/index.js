import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {withNavigation} from 'react-navigation'
import {Alert} from 'react-native'
import SideMenuDrawer from 'react-native-side-menu'
import {autobind} from 'core-decorators'
import {SHAPE} from 'constants/color'
import SideMenu from './side-menu/index'
import HeaderNavPlay from './HeaderNav'
import HeaderNavHistory from '../quiz-history-ios/HeaderNav'
import styled from 'styled-components/native'
import TungtungWkWebView from '../wkwebview'
import Loading from 'components/loading-lottie/QuizPlayerLoading'
// import Success from 'components/loading-lottie/QuizPlayerSuccess'
import Text from 'components/elements/text'
import WaitingScreen from '../../quizlist/waiting-screen/index'
import moment from 'moment'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {updateTimestamp} from '@redux/actions/quizAction'
import {getTimestamp} from 'api/QuizApi'

const PlayerWrapper = styled.View`
  flex: 1;
  position: relative;
`

const ButtonExit = styled.TouchableOpacity`
  padding: 12px 16px;
  background-color: ${SHAPE.RED};
  align-items: center;
`

function getUrlPlay (slug) {
  return '/play-mobile/' + slug
}

@withNavigation
@connectAwaitAutoDispatch(
  state => ({
    customField: state.quiz.info.customField,
    isCustomTime: state.quiz.info.customField.isCustomTime,
    endTime: state.quiz.info.customField.endTime,
    currentTimeServer: state.quiz.property.timestamp
  }),
  {updateTimestamp}
)
@autobind
export default class QuizPlayerIos extends Component<{}> {
  static propTypes = {
    slug: PropTypes.string,
    onPressExit: PropTypes.func,
    onQuizListSubmitted: PropTypes.func,
    onPressDone: PropTypes.func
  }

  static defaultProps = {
    onQuizListSubmitted: () => {
    }
  }

  state = {
    isOpen: false,
    isOnline: false,
    isLoaded: false,
    isShowWaiting: false,
    isShowResult: false,
    isSubmitting: false,
    progress: 0,
    quizzes: []
  }

  totalIsLoaded = 0

  componentDidMount () {
    setTimeout(() => {
      this.setState({progress: 0.2})
    }, 200)
  }

  toggleMenu (isOpen) {
    this.setState({isOpen: isOpen})
  }

  toggleMenuFromNavbar () {
    this.setState({isOpen: !this.state.isOpen})
  }

  handleProgress (progress) {
    this.setState({progress})
  }

  renderMenuQuestion () {
    return (
      <SideMenu
        onPressQuestion={this.handlePressQuestionIndex}
        onClose={this.toggleMenu}
        showResult={this.state.isShowResult}
        quizzes={this.state.quizzes}>
        <ButtonExit
          onPress={() => this.props.onPressExit(!this.state.isShowResult)}>
          <Text fontWeight={600} color='#fff'>
            Thoát
          </Text>
        </ButtonExit>
      </SideMenu>
    )
  }

  handlePressQuestionIndex (index) {
    this.webview.sendPostMessage({goToQuiz: index})
    this.toggleMenuFromNavbar()
  }

  async handlePressSubmit () {
    if (this.state.isLoaded) {
      this.webview.sendPostMessage({submitQuizList: 'play'})
      let time = await getTimestamp()
      let currentTimeServer = new Date(time.timestamp)
      this.props.updateTimestamp(currentTimeServer)
    } else {
      Alert.alert('Bạn không thể nạp bài lúc này.')
    }
  }

  handleSubmitted (history) {
    if (history.success === false) {

    } else {
      this.props.onQuizListSubmitted(history)
      if (this.state.isOnline) {
        this.setState({isShowWaiting: true})
      } else {
        this.setState({isShowResult: true})
      }
    }
  }

  isCustomTimeShowResult () {
    if (!this.props.isCustomTime) {
      return true
    } else {
      let momentCurrentTime = moment(this.props.currentTimeServer)
      let momentEndTime = moment(this.props.endTime)
      return momentCurrentTime.isAfter(momentEndTime)
    }
  }

  onFinishWaiting () {
    if (this.state.isOnline && this.state.isShowWaiting) {
      this.setState({isShowWaiting: false})
      this.setState({isShowResult: true})
    }
  }

  handleSubmitting () {
    this.setState({isSubmitting: true})
    if (!this.isCustomTimeShowResult()) {
      this.setState({isOnline: true})
    }
  }

  handleBack () {
    this.props.navigation.goBack(null)
  }

  handleMessageBrowser (type, data) {
    switch (type) {
      case 'changeQuizzes':
        this.setState({quizzes: data})
        break
      case 'quizListSubmitting':
        this.handleSubmitting()
        break
      case 'quizListSubmitted':
        this.handleSubmitted(data)
        break
      case 'loaded':
        if (this.totalIsLoaded === 0) {
          this.setState({isLoaded: true})
          this.totalIsLoaded = this.totalIsLoaded + 1
        } else if (this.totalIsLoaded === 1) {
          setTimeout(() => {
            this.setState({isSubmitting: false})
          }, 1500)
        }
        break
      default:
    }
  }

  renderHeaderNav () {
    if (!this.state.isShowResult) {
      return (
        <HeaderNavPlay
          isMenuOpen={this.state.isOpen}
          onPressMenu={this.toggleMenuFromNavbar}
          onPressSubmit={this.handlePressSubmit}
        />
      )
    } else {
      return (
        <HeaderNavHistory
          isMenuOpen={this.state.isOpen}
          onPressMenu={this.toggleMenuFromNavbar}
          onPressDone={this.props.onPressDone}
        />
      )
    }
  }

  render () {
    return (
      <SideMenuDrawer
        isOpen={this.state.isOpen}
        onChange={this.toggleMenu}
        menu={this.renderMenuQuestion()}>
        {this.renderHeaderNav()}
        <PlayerWrapper>
          <TungtungWkWebView
            ref={ref => {
              if (ref) {
                this.webview = ref.getWrappedInstance()
              }
            }}
            onProgress={this.handleProgress}
            url={getUrlPlay(this.props.slug)}
            onMessage={this.handleMessageBrowser}
          />
          {!this.state.isLoaded && <Loading progress={this.state.progress} />}
          {/* {this.state.isSubmitting && <Success/>} */}
        </PlayerWrapper>
        {this.state.isShowWaiting && <WaitingScreen
          handleBack={this.handleBack}
          onFinishWaiting={this.onFinishWaiting}
          isShowWaiting={this.state.isShowWaiting}
          endTime={this.props.endTime} />}
      </SideMenuDrawer>
    )
  }
}
