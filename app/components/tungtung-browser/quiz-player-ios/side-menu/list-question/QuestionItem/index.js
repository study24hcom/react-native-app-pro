import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import TextFont from 'components/elements/text/index'
import TextType from './TextType'

const Question = styled.TouchableOpacity`
  padding: 20px 15px;
  border-bottom-width: 1px;
  border-color: #eee;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

export default class QuizItemNumber extends PureComponent {
  static propTypes = {
    index: PropTypes.number,
    quiz: PropTypes.shape({
      correct_check: PropTypes.any,
      chooseAnswerIndex: PropTypes.number
    }),
    showResult: PropTypes.bool,
    onPress: PropTypes.func
  }

  static defaultProps = {
    quiz: {}
  }

  componentDidUpdate () {
    console.log(
      'question ' +
        this.props.index +
        ' check: ' +
        this.props.quiz.correct_check +
        ' vs ' +
        this.props.showResult
    )
  }

  render () {
    return (
      <Question onPress={this.props.onPress}>
        <TextFont>Câu {(this.props.index + 1).toString()}</TextFont>
        <TextType
          showResult={this.props.showResult}
          correctCheck={this.props.quiz.correct_check}
          chooseAnswerIndex={this.props.quiz.chooseAnswerIndex}
        />
      </Question>
    )
  }
}

/*

 */
