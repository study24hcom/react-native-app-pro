import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { SHAPE } from 'constants/color'
import TextFont from 'components/elements/text/index'
import { convertNumberToAlpha } from 'utils/index'
import QUIZ_CHECK_TYPE from 'constants/quizCheckType'

const { CHECKED, EMPTY, CORRECT, INCORRECT } = QUIZ_CHECK_TYPE

function getColorAnswerIndex (chooseAnswerIndex) {
  if (typeof chooseAnswerIndex === 'undefined' || chooseAnswerIndex === null) {
    return SHAPE.GRAYTEXT
  }
  switch (chooseAnswerIndex) {
    case 0:
      return SHAPE.PRIMARY
    case 1:
      return SHAPE.GREEN
    case 2:
      return SHAPE.PINK
    case 3:
      return SHAPE.ORANGE
    default:
      return SHAPE.YELLOW
  }
}

export default class TextType extends PureComponent {
  static propTypes = {
    chooseAnswerIndex: PropTypes.number,
    correctCheck: PropTypes.any,
    showResult: PropTypes.bool
  }

  getTypeQuestion () {
    if (!this.props.showResult && this.props.chooseAnswerIndex >= 0) { return CHECKED }
    if (this.props.showResult) return this.props.correctCheck
    return ''
  }

  getAlphaChoose () {
    const { chooseAnswerIndex } = this.props
    if (chooseAnswerIndex < 0) return ''
    return convertNumberToAlpha(chooseAnswerIndex)
  }

  getOption () {
    let option = {
      color: SHAPE.PRIMARY,
      text: ''
    }
    switch (this.getTypeQuestion()) {
      case CHECKED:
        option = {
          color: getColorAnswerIndex(this.props.chooseAnswerIndex),
          text:
            typeof this.props.chooseAnswerIndex !== 'number'
              ? 'Chưa chọn'
              : `Chọn ${this.getAlphaChoose()}`
        }
        break
      case EMPTY:
        option = { color: SHAPE.ORANGE, text: 'Chưa chọn' }
        break
      case CORRECT:
        option = { color: SHAPE.GREEN, text: 'Đúng' }
        break
      case INCORRECT:
        option = { color: SHAPE.RED, text: 'Sai' }
        break
      default:
    }
    return option
  }

  render () {
    let option = this.getOption()
    return (
      <TextFont fontSize={14} fontWeight={600} color={option.color}>
        {option.text}
      </TextFont>
    )
  }
}
