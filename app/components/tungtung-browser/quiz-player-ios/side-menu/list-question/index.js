import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { FlatList } from 'react-native'
import QuizQuestionItem from './QuestionItem/index'

/*
<QuizQuestionItem
            onPress={() => this.props.onPressQuestion(index)}
            index={index}
            quiz={item}
            showResult={this.props.showResult}
          />
 */
export default class ListQuestion extends PureComponent {
  static propTypes = {
    quizzes: PropTypes.arrayOf(
      PropTypes.shape({
        correctCheck: PropTypes.any,
        chooseAnswerIndex: PropTypes.number
      })
    ),
    showResult: PropTypes.bool,
    onPressQuestion: PropTypes.func
  }

  render () {
    console.log('Show result' + this.props.showResult)
    return (
      <FlatList
        data={this.props.quizzes}
        renderItem={({ item, index }) => (
          <QuizQuestionItem
            onPress={() => this.props.onPressQuestion(index)}
            index={index}
            quiz={item}
            showResult={this.props.showResult}
          />
        )}
        shouldItemUpdate={(prev, next) => {
          let check =
            prev.item.chooseAnswerIndex !== next.item.chooseAnswerIndex ||
            this.props.showResult ||
            prev.item.correctCheck !== next.item.correctCheck
          return check
        }}
        keyExtractor={(item, index) => (item ? item.id : index)}
      />
    )
  }
}

/*

 shouldItemUpdate={(prev, next) => {
 if (prev && next) {
 if (prev.item && next.item) {
 return (
 prev.item.chooseAnswerIndex !== next.item.chooseAnswerIndex
 );
 }
 }
 return false;
 }}
 */

/*
  render() {
    const { quizzes, onPressQuestion } = this.props;
    return (
      <ScrollView>
        {quizzes.map((quiz, index) => (
          <QuizQuestionItem
            onPress={() => onPressQuestion(index)}
            key={quiz.id}
            quiz={quiz}
            index={index}
            showResult={this.props.showResult}
          />
        ))}
      </ScrollView>
    );
  }
 */
