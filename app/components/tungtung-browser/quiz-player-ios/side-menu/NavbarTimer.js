import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import TextFont from 'components/elements/text/index'
import Timer from 'components/elements/timer/index'
import { SHAPE } from 'constants/color'

const View = styled.View`
  flex-direction: row;
  border-bottom-width: 1px;
  border-color: #eee;
  padding: 25px 15px 10px;
  justify-content: space-between;
`

const CloseWrapper = styled.TouchableOpacity`
  width: 60px;
  flex-direction: row;
`

export default class TimerContainer extends PureComponent {
  static propTypes = {
    timeSecondLength: PropTypes.number,
    onClose: PropTypes.func
  }

  static defaultProps = {
    timeSecondLength: 5000
  }

  render () {
    return (
      <View>
        <CloseWrapper onPress={this.props.onClose}>
          <TextFont color={SHAPE.RED}>Thoát</TextFont>
        </CloseWrapper>
        <Timer {...this.props} seconds={this.props.timeSecondLength} />
      </View>
    )
  }
}
