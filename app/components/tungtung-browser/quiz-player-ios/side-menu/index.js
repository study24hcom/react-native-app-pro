import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import ListQuestion from './list-question/index'
// import NavbarTimer from './NavbarTimer'

const View = styled.View`
  flex: 1;
  background-color: #fafbfb;
  border-right-width: 1px;
  border-color: #eee;
`

export default class MenuQuestion extends PureComponent {
  static propTypes = {
    onPressQuestion: PropTypes.func,
    showResult: PropTypes.bool
  }

  render () {
    return (
      <View>
        <ListQuestion
          showResult={this.props.showResult}
          onPressQuestion={this.props.onPressQuestion}
          quizzes={this.props.quizzes}
        />
        {this.props.children}
      </View>
    )
  }
}

/*
 <NavbarTimer
          isStart
          onClose={this.props.onClose}
          onFinish={this.props.onFinishTime}
        />
 */
