import React from 'react'
import styled from 'styled-components/native'
import {Dimensions} from 'react-native'

const {width: windowWidth} = Dimensions.get('window')

const ScreenImageContainer = styled.ImageBackground`
  flex: 1;
`

export const ScreenImage = ({children}) => (
  <ScreenImageContainer
    source={require('../../../assets/home/background.png')}
    resizeMode='cover'
  >
    {children}
  </ScreenImageContainer>
)

export const WelcomeWrapper = styled.View`
  align-items: center;
  justify-content: center;
  background-color: transparent;
  margin-top: 100px;
`
export const ButtonWrapper = styled.View`
  justify-content: space-between;
  flex-direction: row;
  `

export const ButtonContainer = styled.View`
  margin: 8px;
  `
export const ButtonRegister = styled.TouchableOpacity`
  border-radius: 4px;
  height: 50px;
  border-width: 1px;
  border-color: white;
  width: ${(windowWidth) / 2 - 16};
  justify-content: center;
  align-items: center;
  background-color: white;
`
export const ButtonLogin = styled.TouchableOpacity`
  border-radius: 4px;
  height: 50px; 
  border-width: 1px;
  border-color: white;
  justify-content: center;
  align-items: center;
  background-color: transparent;
  width: ${(windowWidth) / 2 - 16};
  `

const LOGO_WIDTH = windowWidth - 68

export const LogoStyle = styled.Image`
  width: ${LOGO_WIDTH}px;
  height: ${123 * LOGO_WIDTH / 779}px;
`

export const LogoWhite = () => (
  <LogoStyle source={require('../../../assets/home/logo-white.png')} />
)
