import React from 'react'
import {Dimensions, View} from 'react-native'
import welcome from 'constants/welcome'
import styled from 'styled-components/native'
import Carousel, {Pagination} from 'react-native-snap-carousel'
import Text from 'components/elements/text/index'
import {SIZE} from 'constants/font'
import {LogoWhite, WelcomeWrapper} from '../style'
import Clearfix from 'components/elements/clearfix/index'

const {width: windowWidth} = Dimensions.get('window')
const marginHorizontal = 12

const Title = styled(Text)`
  font-weight: 500;
  font-size: ${SIZE.TITLE};
  color: #fff;
  margin-bottom: 16px;
  text-align: center;
  `
const Detail = styled(Text)`
  font-weight: 200;
  font-size: ${SIZE.NORMAL};
  color: #fff;
  line-height: 24px;
`
export default class Intro extends React.Component {
  state = {
    activeSlide: 0,
    slider1Ref: null
  }

  _renderItem ({item, index}) {
    return (
      <View style={{width: windowWidth, paddingHorizontal: marginHorizontal, alignItems: 'center'}}>
        <Title numberOfLines={2}>{item.title}</Title>
        <Clearfix height={24} />
        <Detail>{item.detail}</Detail>
      </View>
    )
  }

  get pagination () {
    const {activeSlide, slider1Ref} = this.state
    return (
      <Pagination
        dotsLength={welcome.length}
        activeDotIndex={activeSlide}
        dotStyle={{
          width: 12,
          height: 3,
          marginHorizontal: 4,
          backgroundColor: 'rgba(255, 255, 255, 0.92)'
        }}
        carouselRef={slider1Ref}
        tappableDots={!!slider1Ref}
        inactiveDotStyle={{}}
        // Define styles for inactive dots here}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    )
  }

  get carousel () {
    return <Carousel
      ref={(c) => {
        if (!this.state.slider1Ref) {
          this.setState({slider1Ref: c})
        }
      }}
      onSnapToItem={(index) => this.setState({activeSlide: index})}
      data={welcome}
      renderItem={this._renderItem}
      sliderWidth={windowWidth}
      itemWidth={windowWidth}
      hasParallaxImages
      inactiveSlideOpacity={1}
      inactiveSlideScale={1}
    />
  }

  render () {
    return (
      <View style={{flex: 1, backgroundColor: 'transparent'}}>
        <WelcomeWrapper>
          <LogoWhite />
        </WelcomeWrapper>
        {this.pagination}
        {this.carousel}
      </View>
    )
  }
}
