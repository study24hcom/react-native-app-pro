import React, {Component} from 'react'
import {withNavigation} from 'react-navigation'
import {
  ButtonWrapper,
  ButtonLogin,
  ButtonRegister,
  ButtonContainer,
  ScreenImage
} from './style'
import Text from 'components/elements/text'
import {SIZE, WEIGHT} from 'constants/font'
import {SHAPE} from 'constants/color'
import {autobind} from 'core-decorators'
import Intro from './intro'
import AnimatedLinearGradient from 'components/amimated-linear-gradient'
import LoginByFb from 'components/auth/LoginByFb'
import {View} from 'react-native'

@withNavigation
@autobind
export default class Welcome extends Component {
  goToLogin () {
    this.props.navigation.navigate('tabLogin')
  }

  goToRegister () {
    this.props.navigation.navigate('tabRegister')
  }

  render () {
    return (
      <AnimatedLinearGradient>
        <ScreenImage>
          <Intro />
          <ButtonContainer>
            <ButtonWrapper>
              <ButtonLogin activeOpacity={0.5} onPress={this.goToLogin}>
                <Text color='#fff' fontSize={SIZE.BIG} fontWeight={WEIGHT.NORMAL}>Đăng nhập</Text>
              </ButtonLogin>
              <ButtonRegister activeOpacity={0.5} onPress={this.goToRegister}>
                <Text color={SHAPE.PRIMARYBOLD} fontSize={SIZE.BIG} fontWeight={WEIGHT.NORMAL}>Tạo tài khoản</Text>
              </ButtonRegister>
            </ButtonWrapper>
            <View style={{marginTop: 12}}>
              <LoginByFb />
            </View>
          </ButtonContainer>
        </ScreenImage>
      </AnimatedLinearGradient>
    )
  }
}
