import React from 'react'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'
import {HeaderBackButton} from 'react-navigation'
import {ActivityIndicator} from 'react-native'
import {autobind} from 'core-decorators'
import NavigationBar from 'react-native-navbar'
import Text from 'components/elements/text/index'
import {SHAPE} from 'constants/color'

const Button = styled.TouchableOpacity`
  width: 45px;
  height: 45px;
  justify-content: center;
  align-items: center;
  border-radius: 22.5px;
  `
@autobind
export default class HeaderNav extends React.PureComponent {
  static propTypes = {
    onPressSubmit: PropTypes.func,
    onPressGoBack: PropTypes.func,
    submitting: PropTypes.bool
  }

  static defaultProps = {
    title: 'Đăng kí'
  }

  renderRight () {
    return <Button activeOpacity={0.7} onPress={this.props.onPressSubmit}>
      {this.props.submitting && <ActivityIndicator color='#fff' />}
      {!this.props.submitting && <Text fontWeight={600} color='#ffffff'>Gửi</Text>}
    </Button>
  }

  renderLeft () {
    return (
      <HeaderBackButton width={20} tintColor={'#fff'} onPress={this.props.onPressGoBack} />
    )
  }

  render () {
    return (
      <NavigationBar
        statusBar={{style: 'light-content'}}
        containerStyle={{backgroundColor: SHAPE.PRIMARY}}
        style={{backgroundColor: SHAPE.PRIMARY}}
        leftButton={this.renderLeft()}
        rightButton={this.renderRight()}
        title={{title: `${this.props.title}`, style: {color: '#ffffff', fontWeight: '600'}}}
      />
    )
  }
}
