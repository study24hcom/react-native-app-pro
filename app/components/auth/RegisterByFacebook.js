import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {LoginManager} from 'react-native-fbsdk'
import {autobind} from 'core-decorators'
import {Platform, Keyboard, TouchableWithoutFeedback, View} from 'react-native'
import {createValidateComponent} from 'components/elements/redux-form-validate'
import {validateUsername} from 'utils/validate'
import validator from 'validator'
import {reduxForm, Field} from 'redux-form'
import {FormWrapper, KeyboardWrapper, ViewText} from './style'
import HeaderNav from './HeaderNav/index'
import RegisterSuccess from 'components/loading-lottie/RegisterSuccess'
import Text from 'components/elements/text/index'
import {SIZE} from 'constants/font'
import Clearfix from 'components/elements/clearfix/index'
import {SHAPE} from 'constants/color'
import InputWithIcon from './InputWithIcon'

const FGroup = createValidateComponent(InputWithIcon)

const validate = values => {
  let errors = {}
  if (!values.email) {
    errors.email = 'Vui lòng nhập email'
  } else if (!validator.isEmail(values.email)) {
    errors.email = 'Định dạng email không đúng'
  }
  if (validateUsername(values.username) !== true) {
    errors.username = validateUsername(values.username)
  }
  if (!values.password) {
    errors.password = 'Vui lòng nhập mật khẩu'
  } else if (values.password.length < 6) {
    errors.password = 'Mật khẩu phải lớn hơn 6 ký tự'
  }
  if (!values.fullname) errors.fullname = 'Vui lòng nhập họ tên'
  return errors
}

const offset = (Platform.OS === 'android') ? -200 : 0

@reduxForm({
  form: 'RegisterForm',
  validate
})
@autobind
export default class RegisterFB extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    title: PropTypes.string,
    onPressCancel: PropTypes.func,
    email: PropTypes.string,
    fullname: PropTypes.string,
    isRegisterSuccess: PropTypes.bool,
    avatar_url: PropTypes.string
  }

  onPressGoBack () {
    Keyboard.dismiss()
    LoginManager.logOut()
    this.props.onPressCancel()
  }

  render () {
    const {handleSubmit, submitting} = this.props
    return (
      <View style={{flex: 1}}>
        <KeyboardWrapper keyboardVerticalOffset={offset} behavior='padding'>
          <HeaderNav title={`Xin chào ${this.props.title}`} onPressGoBack={this.onPressGoBack} submitting={submitting}
            onPressSubmit={handleSubmit(this.props.onSubmit)} />
          {this.props.initialValues.email
            ? <ViewText>
              <Text style={{textAlign: 'center'}} color={SHAPE.BLACK} fontSize={SIZE.HEADING3} fontWeight={200}>Nhập tên
                tài khoản</Text>
              <Clearfix height={16} />
              <Text style={{textAlign: 'center'}} color={SHAPE.GRAYTEXT} fontSize={SIZE.SMALL} fontWeight={200}>Tên tài
                khoản dùng để hiển thị và phân biệt với những user khác</Text>
            </ViewText> : <ViewText>
              <Text style={{textAlign: 'center'}} color={SHAPE.BLACK} fontSize={SIZE.HEADING4} fontWeight={200}>Nhập tên
                tài khoản và email của bạn</Text>
              <Clearfix height={16} />
              <Text style={{textAlign: 'center'}} color={SHAPE.GRAYTEXT} fontSize={SIZE.SMALL} fontWeight={200}>Tên tài
                khoản và email dùng để hiển thị, lưu trữ và phân biệt với những user khác</Text>
            </ViewText>}
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <FormWrapper>
              <Field
                auto
                label='Tên tài khoản'
                placeholder='Tên tài khoản'
                name='username'
                icon={'user-circle'}
                component={FGroup}
                onSubmitEditing={this.props.initialValues.email ? handleSubmit(this.props.onSubmit) : null}
                returnKeyType={this.props.initialValues.email ? 'go' : null}
              />
              <Clearfix height={16} />
              {!this.props.initialValues.email &&
              <Field
                label='Email'
                placeholder='Email đăng ký'
                name='email'
                component={FGroup}
              />}
            </FormWrapper>
          </TouchableWithoutFeedback>
        </KeyboardWrapper>
        {this.props.isRegisterSuccess && <RegisterSuccess />}
      </View>
    )
  }
}
