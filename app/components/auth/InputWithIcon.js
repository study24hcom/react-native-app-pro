import React, {PureComponent} from 'react'
import {TextInput} from 'react-native'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {INPUT, SHAPE} from 'constants/color'
import {autobind} from 'core-decorators'
import Icon from 'components/elements/Icon'

const Input = styled(TextInput)`
  padding: 8px;
  padding-right: 16px;
  height: 50px;
  flex: 1;
  background-color: #fff;
  font-family: OpenSans-Regular;
  ${props => props.noRadius && `border-radius: 0`};
  ${props => props.isRadius && `border-radius: 8px`};
  ${props => props.isTopRadius && `
  border-top-left-radius: 8px;
  border-top-right-radius: 8px;
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0`}
  ${props => props.isBottomRadius && `
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-bottom-left-radius: 8px;
  border-bottom-right-radius: 8px;`}
  `
const InputWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  border-color: ${props =>
  props.isFocus ? `${INPUT.FOCUS}` : `#d4d4d4`};
  background-color: #ffffff;
  ${props => props.isRadius && `border-radius: 8px`};
  ${props => props.noRadius && `border-radius: 0`};
  ${props => props.isTopRadius && `
  border-top-left-radius: 8px;
  border-top-right-radius: 8px;
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0`}
  ${props => props.isBottomRadius && `
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-bottom-left-radius: 8px;
  border-bottom-right-radius: 8px;`}
  height: 50px;
`
const IconWrapper = styled.View`
  height: 50px;
  width: 50px;
  align-items: center;
  background-color: #fff;
  justify-content: center;
  ${props => props.noRadius && `border-radius: 0`};
  ${props => props.isRadius && `border-radius: 8px`};
  ${props => props.isTopRadius && `
  border-top-left-radius: 8px;
  border-top-right-radius: 8px;
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0`}
  ${props => props.isBottomRadius && `
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-bottom-left-radius: 8px;
  border-bottom-right-radius: 8px;`}
  `
@autobind
export default class InputWithIcon extends PureComponent {
  static propTypes = {
    auto: PropTypes.bool
  }
  state = {
    isFocus: false
  }

  _handleFocus (e) {
    this.setState({isFocus: true})
    if (this.props.onFocus) this.props.onFocus(e)
  }

  _handleBlur (e) {
    this.setState({isFocus: false})
    if (this.props.onBlur) this.props.onBlur(e)
  }

  componentDidMount () {
    if (this.props.auto) {
      setTimeout(() => {
        this.input.focus()
      }, 500)
    }
  }

  render () {
    return (
      <InputWrapper {...this.props}>
        <IconWrapper {...this.props}>
          <Icon color={SHAPE.PRIMARY} size={20} name={this.props.icon} />
        </IconWrapper>
        <Input
          isFocus={this.state.isFocus}
          autoCapitalize='none'
          {...this.props}
          innerRef={comp => {
            this.input = comp
          }}
          underlineColorAndroid={'transparent'}
          autoCorrect={false}
          onFocus={this._handleFocus}
          onBlur={this._handleBlur}
          placeholderTextColor={SHAPE.GRAYTEXT}
        />
        {this.props.isIconRight &&
        <IconWrapper {...this.props}>
          <Icon simpleLineIcon={this.props.simpleLineIcon}
            color={this.props.secureTextEntry ? SHAPE.GRAYTEXT : SHAPE.PRIMARY}
            size={20} name={this.props.iconRight} onPress={this.props.onPressIcon} />
        </IconWrapper>}
      </InputWrapper>
    )
  }
}
