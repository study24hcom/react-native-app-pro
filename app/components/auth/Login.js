import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Platform, TouchableWithoutFeedback, Keyboard} from 'react-native'
import {withNavigation} from 'react-navigation'
import {createValidateComponent} from 'components/elements/redux-form-validate'
import {reduxForm, Field} from 'redux-form'
import Clearfix from 'components/elements/clearfix'
import Text from 'components/elements/text'
import {
  ScreenImage, KeyboardWrapper,
  LogoWrapper, FormWrapper,
  FormContainer, Button, LogoWhite,
  ForgotPassword, HeaderNav, AbsoluteBottom
} from './style'
import InputPassword from './InputWithIcon'
import Icon from 'components/elements/Icon'
import {SHAPE} from '../../constants/color'

const offset = (Platform.OS === 'android') ? -200 : 0

const FGroup = createValidateComponent(InputPassword)
@withNavigation
@reduxForm({
  form: 'LoginForm'
})
export default class Login extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onRegister: PropTypes.func
  }

  state = {
    secureTextEntry: true
  }

  onPressSecureTextEntry () {
    this.setState({secureTextEntry: !this.state.secureTextEntry})
  }

  onPressBack () {
    this.props.navigation.goBack(null)
  }

  renderForgotPassword () {
    return <ForgotPassword onPress={() => this.props.navigation.navigate('tabForgotPass')}>
      <Text fontWeight={'400'} color={SHAPE.GRAYMEDIUM}>Quên mật khẩu ?</Text>
    </ForgotPassword>
  }

  renderButtonLogin () {
    const {handleSubmit, submitting} = this.props
    return <Button onPress={handleSubmit(this.props.onSubmit)}>
      {submitting && (
        <Text color='#fff' fontSize={16} fontWeight={500}>
          ĐANG XỮ LÝ...
        </Text>
      )}
      {!submitting && (
        <Text color='#fff' fontSize={16} fontWeight={500}>
          ĐĂNG NHẬP
        </Text>
      )}
    </Button>
  }

  render () {
    const {handleSubmit} = this.props
    return (
      <ScreenImage>
        <HeaderNav>
          <Icon simpleLineIcon size={30} color={'#fff'} name={'arrow-left-circle'}
            onPress={this.onPressBack.bind(this)} />
        </HeaderNav>
        <KeyboardWrapper keyboardVerticalOffset={offset} behavior='padding'>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <LogoWrapper>
              <LogoWhite />
            </LogoWrapper>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <FormWrapper>
              <FormContainer>
                <Field
                  label='Email'
                  isTopRadius
                  auto
                  placeholder='Tên tài khoản'
                  name='email'
                  icon={'user-circle'}
                  keyboardType='email-address'
                  component={FGroup}
                />
                <Field
                  label='Mật khẩu'
                  isBottomRadius
                  icon={'lock'}
                  placeholder='Mật khẩu'
                  name='password'
                  returnKeyType='go'
                  isIconRight
                  iconRight={'eye'}
                  onPressIcon={this.onPressSecureTextEntry.bind(this)}
                  onSubmitEditing={handleSubmit(this.props.onSubmit)}
                  secureTextEntry={this.state.secureTextEntry}
                  component={FGroup}
                />
              </FormContainer>
              <Clearfix height={24} />
              {this.renderButtonLogin()}
              {this.renderForgotPassword()}
            </FormWrapper>
          </TouchableWithoutFeedback>
        </KeyboardWrapper>
        <AbsoluteBottom onPress={() => this.props.navigation.navigate('tabRegister')}>
          <Text color={SHAPE.GRAYMEDIUM} fontSize={16}>Bạn chưa có tài khoản ?
            <Text fontWeight={'600'} fontSize={18}> Tạo ngay</Text>
          </Text>
        </AbsoluteBottom>
      </ScreenImage>
    )
  }
}
