import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import {withNavigation} from 'react-navigation'
import {Platform, Keyboard, TouchableWithoutFeedback, Alert} from 'react-native'
import {createValidateComponent} from 'components/elements/redux-form-validate'
import {reduxForm, Field} from 'redux-form'
import {
  FormWrapper, KeyboardWrapper, HeaderNav,
  ScreenImage, Button, FormContainer, ViewText
} from './style'
import InputWithIcon from './InputWithIcon'
import Icon from 'components/elements/Icon'
import Text from 'components/elements/text'
import Clearfix from 'components/elements/clearfix'
import AuthApi from 'api/AuthApi'
import LoginSuccess from 'components/loading-lottie/LoginSuccess'

const FGroup = createValidateComponent(InputWithIcon)

const offset = (Platform.OS === 'android') ? -200 : 0
@withNavigation
@reduxForm({
  form: 'ForgotPassConFirmForm'
})
@autobind
export default class ForgotPassConfirm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func
  }

  onPressGoBack () {
    Keyboard.dismiss()
    this.props.navigation.goBack(null)
  }

  state = {
    secureTextEntry: true
  }

  onPressSecureTextEntry () {
    this.setState({
      secureTextEntry: !this.state.secureTextEntry
    })
  }

  renderButtonLogin () {
    const {handleSubmit, submitting} = this.props
    return <Button onPress={handleSubmit(this.props.onSubmit)}>
      {submitting && (
        <Text color='#fff' fontSize={16} fontWeight={500}>
          ĐANG XỮ LÝ...
        </Text>
      )}
      {!submitting && (
        <Text color='#fff' fontSize={16} fontWeight={500}>
          CẬP NHẬT MẬT KHẨU
        </Text>
      )}
    </Button>
  }

  renderHeaderText () {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <ViewText>
          <Icon name={'code'} size={55} color={'#fff'} />
          <Clearfix height={8} />
          <Text color={'#fff'} fontWeight={'500'} fontSize={24}>Nhập mã xác nhận</Text>
        </ViewText>
      </TouchableWithoutFeedback>
    )
  }

  onPressSend () {
    AuthApi.forgotPassword(this.props.initialValues.email.toLowerCase()).then((userRes, error) => {
      const {success} = userRes
      if (success) {
        Alert.alert('Thành Công', 'Đã gửi mã xác nhận thành công.')
      }
    })
  }

  render () {
    const {handleSubmit} = this.props
    return (
      <ScreenImage>
        <HeaderNav>
          <Icon simpleLineIcon size={30} color={'#fff'} name={'arrow-left-circle'}
            onPress={this.onPressGoBack} />
        </HeaderNav>
        <KeyboardWrapper keyboardVerticalOffset={offset} behavior='padding'>
          {this.renderHeaderText()}
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <FormWrapper>
              <FormContainer>
                <Field
                  label='Email'
                  icon={'envelope'}
                  editable={false}
                  isTopRadius
                  isIconRight
                  onPressIcon={this.onPressSend.bind(this)}
                  iconRight={'reload'}
                  simpleLineIcon
                  placeholder='Email của bạn'
                  name='email'
                  component={FGroup}
                />
                <Field
                  label='Mã code'
                  icon={'code'}
                  noRadius
                  auto
                  placeholder='Mã code'
                  name='code'
                  component={FGroup}
                />
                <Field
                  label='Password'
                  icon={'lock'}
                  isBottomRadius
                  placeholder='Mật khẩu mới'
                  name='password'
                  component={FGroup}
                  isIconRight
                  iconRight={'eye'}
                  onPressIcon={this.onPressSecureTextEntry.bind(this)}
                  onSubmitEditing={handleSubmit(this.props.onSubmit)}
                  secureTextEntry={this.state.secureTextEntry}
                  returnKeyType='go'
                />
              </FormContainer>
              <Clearfix height={20} />
              {this.renderButtonLogin()}
            </FormWrapper>
          </TouchableWithoutFeedback>
        </KeyboardWrapper>
        {this.props.isSuccess && <LoginSuccess />}
      </ScreenImage>
    )
  }
}
