import React from 'react'
import styled from 'styled-components/native'
import {ActivityIndicator, Alert, Modal} from 'react-native'
import AuthApi from 'api/AuthApi'
import {SubmissionError} from 'redux-form'
import {Answers} from 'react-native-fabric'
import {LoginManager, AccessToken, GraphRequest, GraphRequestManager} from 'react-native-fbsdk'
import {SIZE} from 'constants/font'
import {autobind} from 'core-decorators'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {userLoginSuccess, userRegisterSuccess} from '@redux/actions/authAction'
import {getFeaturedTags} from '@redux/actions/tagAction'
import RegisterFB from './RegisterByFacebook'
import TextIcon from '../elements/text-icon'
import LoadingSuccess from '../loading-lottie/LoginSuccess'
import md5 from 'react-native-md5'

const ButtonFacebook = styled.TouchableOpacity`
  background-color: #4267B2;
  height: 50px;
  border-radius: 4px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  `
const ButtonFacebookContainer = styled.View``

@connectAwaitAutoDispatch(state => ({}), {
  userLoginSuccess,
  userRegisterSuccess,
  getFeaturedTags
})
@autobind
export default class LoginByFb extends React.Component {
  state = {
    userChecking: false,
    userChecked: false,
    accessToken: '',
    userInfo: {},
    isModalConfirmFacebook: false,
    isConfirmSubmitting: false,
    isLoginSuccess: false,
    isRegisterSuccess: false,
    avatar_url: ''
  }

  toggleModalConfirm () {
    this.setState({
      isModalConfirmFacebook: !this.state.isModalConfirmFacebook
    })
  }

  onPressCancel () {
    this.toggleModalConfirm()
    this.setState({userChecking: false})
  }

  handleSubmitConfirm (values) {
    let userData = {
      token: this.state.accessToken,
      ...values,
      username: values.username.toLowerCase(),
      email: values.email.toLowerCase(),
      password: md5.hex_md5(values.fullname),
      social: 'facebook'
    }
    return AuthApi.socialConfirmUser(userData).then(userRes => {
      if (userRes.success === false) {
        throw new SubmissionError({
          ...userRes.errors,
          _error: 'Login facebook failed'
        })
      } else {
        this.props.getFeaturedTags()
        this.setState({isRegisterSuccess: true})
        setTimeout(() => {
          this.props.userRegisterSuccess(userRes)
        }, 2500)
      }
    })
  }

  renderModalConfirmFacebook () {
    return <Modal
      onRequestClose={this.onPressCancel}
      visible={this.state.isModalConfirmFacebook}
      animationType={'fade'}>
      {this.state.userInfo.email &&
      <RegisterFB onPressCancel={this.onPressCancel}
        isRegisterSuccess={this.state.isRegisterSuccess}
        initialValues={this.state.userInfo}
        avatar_url={this.state.avatar_url}
        title={this.state.userInfo.fullname}
        onSubmit={this.handleSubmitConfirm} />}
    </Modal>
  }

  renderLoginSuccess () {
    return <Modal visible={this.state.isLoginSuccess}
      onRequestClose={() => this.setState({isLoginSuccess: false})}
      animationType={'fade'}>
      {this.state.isLoginSuccess && <LoadingSuccess />}
    </Modal>
  }

  handleError () {
    this.setState({userChecking: false})
    Alert.alert('Đăng nhập facebook lỗi',
      'Vui lòng cấp quyền cho tungtung.vn để đăng nhập'
    )
  }

  handlePermissions () {
    const context = this
    context.setState({userChecking: true})
    LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(
      function (result) {
        if (result.isCancelled) {
          context.setState({userChecking: false})
        } else {
          AccessToken.getCurrentAccessToken().then(data => {
            AuthApi.socialCheckLogin('facebook', data.accessToken).then(result => {
              if (result.success) {
                Answers.logLogin(result.user.email, true)
                context.setState({isLoginSuccess: true})
                setTimeout(() => {
                  context.props.userLoginSuccess({token: result.token, user: result.user})
                }, 1000)
              } else {
                context.callbackFacebook(data.accessToken)
                setTimeout(() => {
                  context.toggleModalConfirm()
                }, 1000)
                context.setState({accessToken: data.accessToken})
              }
            })
          })
        }
      },
      function (error) {
        context.handleError()
        console.tron.log(error)
      })
  }

  _responseInfoCallback (error: ?Object, result: ?Object) {
    if (error) {
      this.handleError()
    } else {
      console.tron.log(result)
      this.setState({
        userInfo: {
          fullname: result.name,
          email: result.email
        },
        avatar_url: result.picture.data.url ? `http://graph.facebook.com/` + result.id + `/picture?type=large` : null
      })
    }
  }

  callbackFacebook (token) {
    const infoRequest = new GraphRequest(
      '/me',
      {
        parameters: {
          fields: {
            string: 'email,name, picture'
          },
          access_token: {
            string: token
          }
        }
      },
      this._responseInfoCallback
    )
    new GraphRequestManager().addRequest(infoRequest).start()
  }

  render () {
    return <ButtonFacebookContainer>
      <ButtonFacebook onPress={this.handlePermissions}>
        {this.state.userChecking ? <ActivityIndicator color={'#fff'} />
          : <TextIcon center color={'#fff'} fontWeight={400} icon={'facebook-official'}
            size={SIZE.BIG}>{`  Đăng nhập bằng facebook`}</TextIcon>}
      </ButtonFacebook>
      {this.renderModalConfirmFacebook()}
      {this.renderLoginSuccess()}
    </ButtonFacebookContainer>
  }
}
