import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import {withNavigation} from 'react-navigation'
import {Platform, Keyboard, TouchableWithoutFeedback} from 'react-native'
import {createValidateComponent} from 'components/elements/redux-form-validate'
import {reduxForm, Field} from 'redux-form'
import {
  FormWrapper, KeyboardWrapper, HeaderNav,
  ScreenImage, Button, FormContainer, ViewText
} from './style'
import InputWithIcon from './InputWithIcon'
import Icon from 'components/elements/Icon'
import Text from 'components/elements/text'
import Clearfix from 'components/elements/clearfix'

const FGroup = createValidateComponent(InputWithIcon)

const offset = (Platform.OS === 'android') ? -200 : 0
@withNavigation
@reduxForm({
  form: 'ForgotPassForm'
})
@autobind
export default class ForgotPass extends Component {
  static propTypes = {
    onSubmit: PropTypes.func
  }

  state = {
    isHide: false
  }

  onPressGoBack () {
    Keyboard.dismiss()
    this.props.navigation.goBack(null)
  }

  renderButton () {
    const {handleSubmit, submitting} = this.props
    return <Button onPress={handleSubmit(this.props.onSubmit)}>
      {submitting && (
        <Text color='#fff' fontSize={16} fontWeight={500}>
          ĐANG XỮ LÝ...
        </Text>
      )}
      {!submitting && (
        <Text color='#fff' fontSize={16} fontWeight={500}>
          GỬI MÃ XÁC NHẬN
        </Text>
      )}
    </Button>
  }

  renderHeaderText () {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <ViewText>
          <Icon simpleLineIcon name={'lock'} size={55} color={'#fff'} />
          <Clearfix height={8} />
          <Text color={'#fff'} fontWeight={'500'} fontSize={24}>Quên mật khẩu ?</Text>
          <Clearfix height={16} />
          <Text style={{textAlign: 'center'}} color={'#fff'} fontWeight={'400'} fontSize={14}>Chúng tôi cần email
            đăng kí của bạn để gửi mã xác nhận cho bạn.</Text>
        </ViewText>
      </TouchableWithoutFeedback>
    )
  }

  render () {
    const {handleSubmit} = this.props
    return (
      <ScreenImage>
        <HeaderNav>
          <Icon simpleLineIcon size={30} color={'#fff'} name={'arrow-left-circle'}
            onPress={this.onPressGoBack} />
        </HeaderNav>
        <KeyboardWrapper keyboardVerticalOffset={offset} behavior='padding'>
          {this.renderHeaderText()}
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <FormWrapper>
              <FormContainer>
                <Field
                  label='Email'
                  icon={'envelope'}
                  auto
                  isRadius
                  placeholder='Email của bạn'
                  name='email'
                  component={FGroup}
                  returnKeyType='go'
                  onSubmitEditing={handleSubmit(this.props.onSubmit)}
                />
              </FormContainer>
              <Clearfix height={20} />
              {this.renderButton()}
            </FormWrapper>
          </TouchableWithoutFeedback>
        </KeyboardWrapper>
      </ScreenImage>
    )
  }
}
