import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import {withNavigation} from 'react-navigation'
import {Platform, Keyboard, TouchableWithoutFeedback} from 'react-native'
import {createValidateComponent} from 'components/elements/redux-form-validate'
import {validateUsername} from 'utils/validate'
import validator from 'validator'
import {reduxForm, Field} from 'redux-form'
import {FormWrapper, KeyboardWrapper, RegisterImage} from './style'
import HeaderNav from './HeaderNav/index'
import InputWithIcon from './InputWithIcon'

const FGroup = createValidateComponent(InputWithIcon)

const validate = values => {
  let errors = {}
  if (!values.email) {
    errors.email = 'Vui lòng nhập email'
  } else if (!validator.isEmail(values.email)) {
    errors.email = 'Định dạng email không đúng'
  }
  if (validateUsername(values.username) !== true) {
    errors.username = validateUsername(values.username)
  }
  if (!values.password) {
    errors.password = 'Vui lòng nhập mật khẩu'
  } else if (values.password.length < 6) {
    errors.password = 'Mật khẩu phải lớn hơn 6 ký tự'
  }
  if (!values.fullname) errors.fullname = 'Vui lòng nhập họ tên'
  return errors
}
const offset = (Platform.OS === 'android') ? -200 : 0
@withNavigation
@reduxForm({
  form: 'RegisterForm',
  validate
})
@autobind
export default class Register extends Component {
  static propTypes = {
    onSubmit: PropTypes.func
  }

  onPressGoBack () {
    Keyboard.dismiss()
    this.props.navigation.goBack(null)
  }

  state = {
    secureTextEntry: true
  }

  onPressSecureTextEntry () {
    this.setState({
      secureTextEntry: !this.state.secureTextEntry
    })
  }

  render () {
    const {handleSubmit, submitting} = this.props
    return (
      <RegisterImage>
        <KeyboardWrapper keyboardVerticalOffset={offset} behavior='padding'>
          <HeaderNav title={`Đăng kí`} onPressGoBack={this.onPressGoBack} submitting={submitting}
            onPressSubmit={handleSubmit(this.props.onSubmit)} />
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <FormWrapper>
              <Field
                auto
                isTopRadius
                icon={'user-circle'}
                label='Tên tài khoản'
                placeholder='Tên tài khoản'
                name='username'
                component={FGroup}
              />
              <Field
                noRadius
                label='Email'
                icon={'envelope'}
                placeholder='Email đăng ký'
                name='email'
                component={FGroup}
              />
              < Field
                noRadius
                label='Họ và tên'
                icon={'address-card'}
                placeholder='Họ và tên đầy đủ'
                name='fullname'
                component={FGroup}
              />
              <Field
                isBottomRadius
                label='Mật khẩu'
                placeholder='Mật khẩu'
                icon={'lock'}
                isIconRight
                iconRight={'eye'}
                onPressIcon={this.onPressSecureTextEntry.bind(this)}
                name='password'
                secureTextEntry={this.state.secureTextEntry}
                component={FGroup}
                returnKeyType='go'
                onSubmitEditing={handleSubmit(this.props.onSubmit)} />
            </FormWrapper>
          </TouchableWithoutFeedback>
        </KeyboardWrapper>
      </RegisterImage>
    )
  }
}
