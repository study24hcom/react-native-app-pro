import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import HeadingLine from 'components/elements/heading-line/index'
import styled from 'styled-components/native'
import TagList from 'components/tag/tag-list/index'
import {SIZE} from 'constants/font'
import {autobind} from 'core-decorators'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {SHAPE} from 'constants/color'
import {tag} from 'constants/description'

const View = styled.View``
const TagWrapper = styled.View`
  padding: 0 16px;
  `
@connectAwaitAutoDispatch(
  state => ({
    data: state.tag.featured.data
  }),
  {}
)
@autobind
export default class TagListFeature extends PureComponent {
  static propTypes = {
    data: PropTypes.array,
    onPress: PropTypes.func,
    onPressViewAll: PropTypes.func,
    pagination: PropTypes.shape({
      totalItem: PropTypes.number,
      page: PropTypes.number
    }),
    isSuccess: PropTypes.bool
  }

  render () {
    return <View>
      <HeadingLine description={tag.featured} size={SIZE.HEADING3} color={SHAPE.PINK} icon={'tag'}
        simpleLineIcon totalItem={this.props.data.length} noLine title={'Tag nổi bật'}
        onPress={this.props.onPressViewAll} isShow viewAllText={'Xem thêm'} />
      <TagWrapper>
        <TagList onPress={this.props.onPress} tags={this.props.data.slice(0, 20)} size={SIZE.NORMAL} />
      </TagWrapper>
    </View>
  }
}
