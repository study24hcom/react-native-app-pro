import React, {Component} from 'react'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'
import {SIZE, WEIGHT} from 'constants/font'
import {SHAPE} from 'constants/color'
import Text from 'components/elements/text'
import Icon from 'components/elements/Icon'

const MetaWrapper = styled.View`
  padding: 8px;
  display: flex;
  justify-content: space-between;
  background-color: #fff;
  flex-direction: row;
  align-items: center;
  ${props =>
  props.noLine
    ? null
    : `
    border-bottom-width: 1px;
    border-bottom-color: #eee;
  `}; 
`

const Meta = styled(Text)`
  display: flex;
  align-items: center;
  font-weight: ${WEIGHT.BOLD};
`
export default class HeadingLine extends Component {
  static propTypes = {
    icon: PropTypes.string,
    color: PropTypes.string,
    title: PropTypes.string,
    totalItem: PropTypes.number,
    onPress: PropTypes.func,
    simpleLineIcon: PropTypes.bool,
    hideTotalItem: PropTypes.bool,
    isShow: PropTypes.bool,
    noLine: PropTypes.bool,
    viewAllText: PropTypes.string,
    size: PropTypes.number,
    isDelete: PropTypes.bool
  }

  static defaultProps = {
    size: SIZE.HEADING5
  }

  render () {
    return (
      <MetaWrapper noLine={this.props.noLine}>
        <Meta>
          {this.props.icon && (
            <Icon size={this.props.size} color={this.props.color}
              simpleLineIcon={this.props.simpleLineIcon}
              name={this.props.icon} />)}
          {this.props.icon && <Text>{`  `}</Text>}
          <Text color={this.props.color} fontWeight={500} fontSize={this.props.size}>{this.props.title}</Text>
        </Meta>
        {this.props.isDelete &&
        <Meta>
          <Text onPress={this.props.onPress} color={SHAPE.RED} fontWeight={600} fontSize={this.props.size}>Xóa lịch
            sử</Text>
        </Meta>}
      </MetaWrapper>
    )
  }
}
