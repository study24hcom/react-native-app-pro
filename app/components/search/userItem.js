import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { autobind } from 'core-decorators'
import styled from 'styled-components/native'
import UserAvatar from '../elements/user-avatar'
import Avatar from '../elements/user-avatar/avatar'
import { TEXT } from '../../constants/color'
import { SIZE, WEIGHT } from '../../constants/font'

const UserWrapper = styled.TouchableOpacity`
  display: flex;
  align-items: center;
  flex-direction: row;
  padding: 8px 16px;
  border-bottom-width: 1px;
  border-bottom-color: #f1f1f1;
  background-color: #fff;
`

const SpanUsername = styled.Text`
  padding: 4px 0;
  font-size: ${SIZE.NORMAL};
  font-weight: ${WEIGHT.NORMAL}
`

const UserNameWrapper = styled.View`
  display: flex;
  flex-direction: column;
  margin-left: 8px;
  `

const SpanFullName = styled.Text`
  padding: 4px 0;
  font-size: ${SIZE.NORMAL};
  color: ${TEXT.GRAY}
`
@autobind
export default class UserViewItem extends PureComponent {
  static propTypes = {
    user: PropTypes.shape(UserAvatar.propTypes),
    avatarSize: PropTypes.number,
    usernameSize: PropTypes.number,
    onPressUser: PropTypes.func
  }

  static defaultProps = {
    avatarSize: 35,
    usernameSize: 15
  }

  onPress () {
    this.props.onPressUser(this.props)
  }

  render () {
    const {avatar, username, fullname} = this.props
    return (
      <UserWrapper onPress={this.onPress} activeOpacity={1}>
        <Avatar avatar={avatar} avatarSize={this.props.avatarSize} username={username} />
        <UserNameWrapper>
          <SpanUsername>{username}</SpanUsername>
          {fullname && <SpanFullName>{fullname}</SpanFullName>}
        </UserNameWrapper>
      </UserWrapper>
    )
  }
}
