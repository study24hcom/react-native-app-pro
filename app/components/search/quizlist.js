import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import QuizlistViewItem from './quizlistItem'
import Heading from '../elements/heading/index'

const QuizListWrapper = styled.View``
export default class QuizlistView extends PureComponent {
  static propTypes = {
    onPressQuizList: PropTypes.func,
    data: PropTypes.array,
    isLoading: PropTypes.bool,
    isShow: PropTypes.bool
  }

  render () {
    if (this.props.data.length === 0) return null
    return <QuizListWrapper>
      <Heading title='Đề thi' isLoading={this.props.isLoading} />
      {this.props.data.map(quizlist =>
        this.props.isShow &&
        <QuizlistViewItem key={quizlist.id} {...quizlist}
          onPressQuizList={this.props.onPressQuizList} />)}
    </QuizListWrapper>
  }
}
