import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import SearchItem from './searchItem'
import {SHAPE} from '../../constants/color'

const SearchWrapper = styled.ScrollView`
  flex: 1
  `
export default class SearchList extends PureComponent {
  static propTypes = {
    onPressItem: PropTypes.func,
    keywordSuggest: PropTypes.array,
    recentSuggest: PropTypes.array,
    keyword: PropTypes.string
  }

  render () {
    if (this.props.keywordSuggest.length === 0 && this.props.recentSuggest.length === 0 && this.props.keywords !== '') {
      return <SearchItem name={this.props.keywords} onPressItem={this.props.onPressItem} />
    }
    return <SearchWrapper>
      {this.props.recentSuggest.map((searchItem, index) =>
        <SearchItem color={SHAPE.RED} key={index} {...searchItem} onPressItem={this.props.onPressItem} />)}
      {this.props.keywordSuggest.map((searchItem, index) =>
        <SearchItem color={SHAPE.BLACK} key={index} {...searchItem} onPressItem={this.props.onPressItem} />)}
    </SearchWrapper>
  }
}
