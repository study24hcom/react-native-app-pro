import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import { TEXT } from '../../constants/color'
import Text from '../elements/text/index'
import { SIZE } from '../../constants/font'
import { autobind } from 'core-decorators'

const QuizlistWrapper = styled.TouchableOpacity`
  display: flex;
  align-items: center;
  flex-direction: row;
  padding: 16px 16px;
  border-bottom-width: 1px;
  border-bottom-color: #f1f1f1;
  background-color: #fff;
`
@autobind
export default class QuizlistViewItem extends PureComponent {
  static propTypes = {
    name: PropTypes.string,
    onPressQuizList: PropTypes.func
  }

  onPress () {
    this.props.onPressQuizList(this.props)
  }

  render () {
    return (
      <QuizlistWrapper onPress={this.onPress} activeOpacity={1}>
        <Text fontSize={SIZE.TITLE} color={TEXT.PRIMARYBOLD} fontWeight={600}>{this.props.name}</Text>
      </QuizlistWrapper>
    )
  }
}
