import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import SearchItem from './searchItem'
import HeadingLine from './HeadingLine'
import CaterogyBox from 'components/home/caterogy'
import PartnerList from 'components/home/partner'
import {SIZE} from 'constants/font'
import TagListFeature from './tagFeature'

const SearchWrapper = styled.ScrollView`
  flex: 1;
  `
export default class SearchHistory extends PureComponent {
  static propTypes = {
    onPressItem: PropTypes.func,
    data: PropTypes.array,
    keyword: PropTypes.string,
    isShow: PropTypes.bool,
    onPressDelete: PropTypes.func,
    openQuizList: PropTypes.func,
    onPressUser: PropTypes.func,
    onPressViewAll: PropTypes.func,
    onPressTag: PropTypes.func,
    onPressTagViewAll: PropTypes.func
  }

  renderPartner () {
    return <PartnerList onPressViewAll={this.props.onPressViewAll} onPressItem={this.props.onPressUser} />
  }

  renderTagFeature () {
    return <TagListFeature onPress={this.props.onPressTag} onPressViewAll={this.props.onPressTagViewAll} />
  }

  render () {
    return <SearchWrapper>
      {this.props.data.length !== 0 &&
      <HeadingLine size={SIZE.NORMAL} onPress={this.props.onPressDelete} isDelete title={'Lịch sử tìm kiếm'} />}
      {this.props.data.map((searchItem, index) =>
        <SearchItem key={index} {...searchItem} onPressItem={this.props.onPressItem} />)}
      <CaterogyBox onPress={this.props.openQuizList} />
      {this.renderPartner()}
      {this.renderTagFeature()}
    </SearchWrapper>
  }
}
