import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import UserViewItem from './userItem'
import Heading from '../elements/heading/index'

const UserWrapper = styled.View`
  `
export default class UserView extends PureComponent {
  static propTypes = {
    onPressUser: PropTypes.func,
    data: PropTypes.array,
    isLoading: PropTypes.bool,
    isShow: PropTypes.bool
  }

  render () {
    if (this.props.data.length === 0) return null
    return <UserWrapper>
      <Heading title='Thành viên' isLoading={this.props.isLoading} />
      {this.props.data.map(user =>
        this.props.isShow && <UserViewItem key={user.id} {...user}
          onPressUser={this.props.onPressUser} />)}
    </UserWrapper>
  }
}
