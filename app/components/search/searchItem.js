import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {Dimensions} from 'react-native'
import styled from 'styled-components/native'
import {SHAPE} from 'constants/color'
import Text from 'components/elements/text/index'
import {SIZE} from 'constants/font'
import {autobind} from 'core-decorators'
import Icon from 'components/elements/Icon'

const {width} = Dimensions.get('window')
const SearchWrapper = styled.View`
  display: flex;
  align-items: center;
  flex-direction: row;
  width: ${width};
  height: ${SIZE.WIDTH_DEFAUTL};
  border-bottom-width: 1px;
  border-bottom-color: #f1f1f1;
  background-color: #fff;
`
const IconWrapper = styled.View`
  height: ${SIZE.HEIGHT_DEFAULT};
  width: ${SIZE.WIDTH_DEFAUTL};
  align-items: center;
  justify-content: center;
`
const TextView = styled.TouchableOpacity`
  flex: 1;
`
@autobind
export default class SearchItem extends PureComponent {
  static propTypes = {
    name: PropTypes.string,
    onPressItem: PropTypes.func,
    color: PropTypes.string
  }

  onPress () {
    this.props.onPressItem(this.props.name)
  }

  static defaultProps = {
    color: SHAPE.BLACK
  }

  render () {
    return (
      <SearchWrapper>
        <IconWrapper>
          <Icon simpleLineIcon name={'magnifier'} size={SIZE.SMALL} />
        </IconWrapper>
        <TextView onPress={this.onPress} activeOpacity={1}>
          <Text numberOfLines={1} fontSize={SIZE.BIG} color={this.props.color}>{this.props.name}</Text>
        </TextView>
      </SearchWrapper>
    )
  }
}
