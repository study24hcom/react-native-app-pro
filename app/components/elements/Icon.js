import React, { Component } from 'react'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'

const IconWrapper = styled.Text`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: transparent;
  ${props => props.paddingRight ? `padding-right: ${props.paddingRight}px` : null}
`
export default class Icon extends Component {
  static propTypes = {
    fontAwesome: PropTypes.bool,
    simpleLineIcon: PropTypes.bool,
    color: PropTypes.string,
    size: PropTypes.number,
    name: PropTypes.string,
    paddingRight: PropTypes.number
  }

  static defaultProps = {
    fontAwesome: true
  }

  render () {
    let WrapComponent = FontAwesome
    if (this.props.simpleLineIcon) WrapComponent = SimpleLineIcons
    return <IconWrapper paddingRight={this.props.paddingRight}>
      <WrapComponent {...this.props} />
    </IconWrapper>
  }
}
