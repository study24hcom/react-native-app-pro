import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {StyleSheet} from 'react-native'
import {sliderWidth} from 'utils/slider'

const Box = styled.View`
  display: flex;
  background-color: #ffffff;
  padding: 16px 20px;
  ${props => props.margin && `margin: ${props => props.margin}px`};
  ${props => props.horizontal && `width: ${sliderWidth}px`};
`
Box.propTypes = {
  padding: PropTypes.any,
  horizontal: PropTypes.bool,
  margin: PropTypes.number
}
Box.defaultProps = {
  padding: 24,
  marginVertical: 4
}

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#d4d4d4',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 3,
    shadowOpacity: 0.25
  }
})

export default function BoxWithShadow (props) {
  return <Box elevation={0.18} style={[styles.shadow, props.style]} {...props} />
}
