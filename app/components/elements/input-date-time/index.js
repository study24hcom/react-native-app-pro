import React from 'react'
import styled from 'styled-components/native'
import DatePicker from 'react-native-datepicker'
import Label from '../Label/index'
import moment from 'moment'
import {SIZE} from 'constants/font'
import {SHAPE} from 'constants/color'
import {TEXT} from '../../../constants/color'

const Wrapper = styled.View`
  flex-direction: row;
  align-items: center;
  `
const LabelView = styled.View`
  width: 100px;
  `
const InputView = styled.View`
  flex: 1;
  flex-direction: row
  `

export default function InputDateTime ({label, value, onChange, dateFormat, ...props}) {
  if (dateFormat && moment(value).isValid()) {
    value = moment(value).format(dateFormat)
  }

  return (
    <Wrapper>
      <LabelView>
        {label && <Label>{label}</Label>}
      </LabelView>
      <InputView>
        <DatePicker
          {...props}
          date={value}
          mode='date'
          placeholder='Chọn ngày sinh'
          format={dateFormat}
          confirmBtnText='Xác nhận'
          showIcon={false}
          cancelBtnText='Hủy'
          customStyles={{
            dateInput: {
              borderRadius: 4,
              borderWidth: 0,
              borderBottomWidth: 1,
              borderColor: SHAPE.GRAYMEDIUM,
              alignItems: 'flex-start',
              paddingHorizontal: 12
            },
            dateText: {
              fontSize: SIZE.NORMAL,
              fontWeight: '400',
              fontFamily: 'OpenSans-Regular',
              color: TEXT.NORMAL

            }
          }}
          style={{
            backgroundColor: '#fff', flex: 1
          }}
          onDateChange={(date) => onChange(date)} />
      </InputView>
    </Wrapper>
  )
}
