import React, { PureComponent } from 'react'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'
import { SHAPE } from 'constants/color'
import Text from 'components/elements/text'
import { SIZE, WEIGHT } from 'constants/font'

const TitleStyle = styled(Text)`
  color: ${SHAPE.PRIMARYBOLD};
  font-size: ${props => (props.isPress ? SIZE.TITLE : SIZE.HEADING1)}px;
  font-weight: ${WEIGHT.BOLDER};
  font-family: OpenSans-Bold;
`

export default class Title extends PureComponent {
  static propTypes = {
    isPress: PropTypes.bool,
    numberOfLines: PropTypes.number
  }
  render () {
    return (
      <TitleStyle numberOfLines={this.props.numberOfLines} isPress={this.props.isPress}>
        {this.props.children}
      </TitleStyle>
    )
  }
}
