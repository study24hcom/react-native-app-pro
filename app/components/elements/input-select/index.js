import React from 'react'
import styled from 'styled-components/native'
import SelectInput from 'react-native-select-input-ios'
import Label from '../Label/index'
import {SHAPE} from 'constants/color'
import {SIZE} from 'constants/font'
import {TEXT} from '../../../constants/color'

const View = styled.View`
  flex-direction: row;
  align-items: center;
  `
const LabelView = styled.View`
  width: 100px;
  `
const InputView = styled.View`
  flex: 1;
  `

export default function InputSelect ({label, value, onChange, options, ...props}) {
  return (
    <View>
      <LabelView>
        {label && <Label>{label}</Label>}
      </LabelView>
      <InputView>
        <SelectInput
          {...props}
          buttonsTextColor={SHAPE.BLACK}
          value={value}
          onSubmitEditing={value => onChange(value)}
          options={options}
          labelStyle={{fontSize: SIZE.NORMAL, color: TEXT.NORMAL, fontWeight: '500'}}
          buttonsTextSize={SIZE.NORMAL}
          style={{
            flexDirection: 'row',
            borderRadius: 8,
            alignItems: 'center',
            paddingHorizontal: 16,
            backgroundColor: '#fff',
            borderWidth: 0,
            padding: 10,
            flex: 1,
            borderBottomWidth: 1,
            borderColor: SHAPE.GRAYMEDIUM
          }}
        />
      </InputView>
    </View>

  )
}
