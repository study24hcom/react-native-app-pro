import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Icon from '../Icon'
import Text from 'components/elements/text'
import {SIZE} from 'constants/font'

const BadgeWrapper = styled.View`
  width: 30px;
  height: 30px;
  justify-content: center;
  align-items: center
`
const BadgeContainer = styled.View`
  position: absolute;
  top:-2px;
  right:-2px;
  minWidth:20px;
  height:20px;
  border-radius:10px;
  align-items: center;
  justify-content: center;
  background-color: red;
`
export default class Badge extends PureComponent {
  static propTypes = {
    icon: PropTypes.string,
    badge: PropTypes.number
  }

  render () {
    return (
      <BadgeWrapper>
        <Icon color='#fff' size={SIZE.HEADING1} name={this.props.icon} />
        {this.props.badge !== 0 &&
        <BadgeContainer>
          <Text color='#fff' fontSize={10}>{this.props.badge}</Text>
        </BadgeContainer>}
      </BadgeWrapper>
    )
  }
}
