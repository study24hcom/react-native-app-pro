import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Icon from '../Icon'
import { SIZE } from '../../../constants/font'
import { SHAPE } from '../../../constants/color'
import Text from '../text'
import Loading from '../loading/index'

const Botton = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  height: 60px;
  `
const IconWrapper = styled.View`
  padding: 16px
  `
const Title = styled(Text)``
const Messenger = styled(Text)``
const TextWrapper = styled.View`
  justify-content: center;
  `
export default class ButtonModal extends React.PureComponent {
  static propTypes = {
    onPress: PropTypes.func,
    title: PropTypes.string,
    messenger: PropTypes.string,
    icon: PropTypes.string,
    isChecking: PropTypes.bool
  }

  render () {
    return (
      <Botton onPress={this.props.onPress}>
        <IconWrapper>
          <Icon size={SIZE.HEADING6} color={SHAPE.BLACK} name={this.props.icon} />
        </IconWrapper>
        <TextWrapper>
          {this.props.isChecking && <Loading padding={0} />}
          {!this.props.isChecking && <Title fontSize={SIZE.BIG}>{this.props.title}</Title>}
          {this.props.messenger && !this.props.isChecking && <Messenger color={SHAPE.GRAYBOLDER} fontWeight={100}
            fontSize={SIZE.SMALL}>{this.props.messenger}</Messenger>}
        </TextWrapper>
      </Botton>
    )
  }
}
