import React from 'react'
import PropTypes from 'prop-types'
import Image from 'react-native-image-progress'
import ProgressCircle from 'react-native-progress/Circle'
import styled from 'styled-components/native'
import {getBackgroundType} from 'utils/playlist'

const BoxImageContainer = styled.View`
  overflow: hidden;
`
const WrapperFeaturedImage = styled.TouchableOpacity`
  flex: 1;
  justify-content: center;
  `
const Background = styled.TouchableOpacity` 
  flex: 1
  `
const BackgroundImageOver = styled.ImageBackground`
  background-color: ${props => props.color};
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 16px 32px;
  height: 180px
`
const ImageText = styled.Text`
  font-size: 21px;
  color: ${props => (props.color ? props.color : '#ffffff')};
  font-weight: 600;
  text-align: center;
`

export default class BoxImage extends React.PureComponent {
  static propTypes = {
    alt: PropTypes.string,
    src: PropTypes.string,
    onPress: PropTypes.func
  }

  render () {
    const {src, alt} = this.props
    let backgroundType = getBackgroundType(alt)
    return (
      <BoxImageContainer>
        {src
          ? <WrapperFeaturedImage activeOpacity={0.9} onPress={this.props.onPress}>
            <Image indicator={ProgressCircle} style={{height: 180}} resizeMode='cover'
              source={{uri: src}} />
          </WrapperFeaturedImage> : null}
        {!src &&
        <Background activeOpacity={0.9} onPress={this.props.onPress}>
          <BackgroundImageOver resizeMode='cover' color={backgroundType.background}
            source={backgroundType.image}>
            <ImageText color={backgroundType.color}>
              {alt}
            </ImageText>
          </BackgroundImageOver>
        </Background>}
        {this.props.children}
      </BoxImageContainer>
    )
  }
}
