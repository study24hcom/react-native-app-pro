import React, {Component} from 'react'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'
import {getColorFromString} from 'utils/color'
import {SIZE} from 'constants/font'

const TextView = styled.Text`
  font-family: OpenSans-Regular;
  background-color: transparent;
  ${props => props.customColor ? `color: ${getColorFromString(props.customColor)};` : ''};
  font-size: ${props => props.fontSize ? `${props.fontSize}px;` : `${SIZE.NORMAL}px`};
  ${props => props.fontWeight ? `font-weight: ${props.fontWeight};` : ''};
  ${props => props.color ? `color: ${props.color};` : 'black'};
  ${props => props.padding ? `padding: ${props.padding};` : ''};
  text-align: ${props => props.align};
`

export default class Text extends Component {
  static propTypes = {
    customColor: PropTypes.string,
    color: PropTypes.string,
    fontSize: PropTypes.number,
    fontWeight: PropTypes.any,
    padding: PropTypes.string,
    numberOfLines: PropTypes.number,
    align: PropTypes.oneOf(['auto', 'left', 'right', 'center', 'justify'])
  }

  static defaultProps = {
    align: 'left'
  }

  render () {
    return <TextView {...this.props} numberOfLines={this.props.numberOfLines} />
  }
}
