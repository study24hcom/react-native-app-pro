import React from 'react'
import { Switch, View } from 'react-native'
import Label from '../Label/index'
import Clearfix from '../clearfix/index'

export default function Toggle ({value, label, onChange, defaultChecked, ...props}) {
  let newValue = !!value
  let newDefaultChecked = defaultChecked ? true : newValue
  return (
    <View>
      {label && <Label>{label}</Label>}
      <Clearfix height={8} />
      <Switch
        {...props}
        value={newDefaultChecked}
        onValueChange={value => onChange(value)} />
    </View>
  )
}
