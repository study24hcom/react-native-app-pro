import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Icon from '../Icon'

import { SHAPE } from 'constants/color'

const View = styled.View`
  flex-direction: row;
  display: flex;
`

const WrapperIcon = styled.TouchableOpacity`
  margin-horizontal: ${props => props.readOnly ? `2px` : `5px`};
`

const stars = [1, 2, 3, 4, 5]

export class Star extends PureComponent {
  render () {
    const {active, onPress, size, readOnly} = this.props
    return (
      <WrapperIcon readOnly={readOnly} onPress={onPress} activeOpacity={1}>
        <Icon
          color={active ? SHAPE.YELLOW : SHAPE.GRAYBOLD}
          name='star'
          size={size}
          style={{paddingRight: 5}}
        />
      </WrapperIcon>
    )
  }
}

export default function StarView ({value, size = 16, onChange, readOnly}) {
  return (
    <View>
      {stars.map((star, index) => (
        <Star
          size={size}
          readOnly={readOnly}
          key={index}
          onPress={() => {
            if (!readOnly) onChange(index + 1)
          }}
          active={index < value}
        />
      ))}
    </View>
  )
}

StarView.propTypes = {
  value: PropTypes.number,
  size: PropTypes.number,
  readOnly: PropTypes.bool,
  onChange: PropTypes.func
}
