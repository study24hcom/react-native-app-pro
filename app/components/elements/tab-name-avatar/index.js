import React from 'react'
import PropTypes from 'prop-types'
import Avatar from '../user-avatar/avatar/index'
import {connectAutoDispatch} from '@redux/connect'
import styled from 'styled-components/native'

const AvatarWrapper = styled.View`
  position: relative;
  top: 10px;
  border-width: 1px;
  border-radius: ${props => props.avatarSize ? `${props.avatarSize / 2}px` : 0}
  border-color: ${props => props.color ? `${props.color}` : 'transparent'}
  width: ${props => props.avatarSize ? `${props.avatarSize + 2}px` : `0`}
  height: ${props => props.avatarSize ? `${props.avatarSize + 2}px` : `0`}
  `

@connectAutoDispatch(state => ({
  username: state.auth.user.username,
  avatar: state.auth.user.avatar

}), {})
export default class AvatarTabName extends React.Component {
  static propTypes = {
    color: PropTypes.string,
    size: PropTypes.number,
    username: PropTypes.string,
    avatar: PropTypes.string
  }

  render () {
    return <AvatarWrapper color={this.props.color} avatarSize={this.props.size}>
      <Avatar avatarSize={this.props.size} username={this.props.username} avatar={this.props.avatar} />
    </AvatarWrapper>
  }
}
