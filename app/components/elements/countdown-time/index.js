import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import styled from 'styled-components/native'
import moment from 'moment'
import {View, StyleSheet, Dimensions} from 'react-native'
import Text from '../text/index'
import {updateTimestamp} from '@redux/actions/quizAction'
import {connectAutoDispatch} from '@redux/connect'
import {SHAPE} from 'constants/color'

const {width} = Dimensions.get('window')
const TimeCountdownContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  `
const ButtonPlay = styled.View`
  padding: 16px;
  padding-top: 8px;
  justify-content: center;
  align-items: center;
  width: ${(width - 32) / 3 - 4};
  background-color: #fff
`
const styles = StyleSheet.create({
  shadow: {
    shadowColor: 'rgb(235, 232, 232)',
    shadowOffset: {
      width: 1,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1
  }
})
@connectAutoDispatch(
  state => ({
    timestamp: state.quiz.property.timestamp
  }),
  {updateTimestamp}
)
@autobind
export default class CountdownTime extends Component {
  static propTypes = {
    deadline: PropTypes.string,
    onFinish: PropTypes.func,
    title: PropTypes.string,
    titleSize: PropTypes.number,
    onlyIncremnent: PropTypes.bool,
    color: PropTypes.string
  }

  static defaultProps = {
    titleSize: 16
  }

  constructor (props) {
    super(props)
    this.timestamp = moment(this.props.timestamp)
    let timeObject = this.getTimeRemaining(props.deadline)
    this.state = {
      ...timeObject
    }
  }

  static checkTimeExpried (time) {
    let timeObject = new CountdownTime().getTimeRemaining(time)
    return timeObject.total <= 0
  }

  getTimeRemaining (time) {
    var t = Date.parse(time) - Date.parse(this.timestamp.toString())
    var seconds = Math.floor(t / 1000 % 60)
    var minutes = Math.floor(t / 1000 / 60 % 60)
    var days = Math.floor(t / (1000 * 60 * 60 * 24))
    var hours = Math.floor(t / (1000 * 60 * 60) % 24 + days * 24)
    return {
      total: t,
      // days: days,
      hours: hours,
      minutes: minutes,
      seconds: seconds
    }
  }

  updateClock () {
    this.timestamp.add(1, 's')
    // update window clock stamp
    if (this.props.onlyIncremnent) return
    let timeObject = this.getTimeRemaining(this.props.deadline)
    this.setState(timeObject)
    if (timeObject.total <= 0) {
      if (this.props.onFinish) {
        this.props.onFinish()
      }
      clearInterval(this.timeinterval)
    }
  }

  componentDidMount () {
    this.timeinterval = setInterval(this.updateClock, 1000)
  }

  componentWillUnmount () {
    clearInterval(this.timeinterval)
    this.props.updateTimestamp(this.timestamp.toString())
  }

  renderItemTime (key, description, autoZero = true) {
    if (this.state[key] === 0 && autoZero) return null
    return <ButtonPlay style={styles.shadow}>
      <Text color={this.props.color} fontWeight={600} fontSize={40}>{`${this.state[key]}`}</Text>
      <Text color={SHAPE.GRAYTEXT} fontWeight={400} fontSize={19}>{`${description}`}</Text>
    </ButtonPlay>
  }

  render () {
    if (this.props.onlyIncremnent) return <View />
    return (
      <TimeCountdownContainer>
        {/* {this.renderItemTime('days', 'ngày')} */}
        {this.renderItemTime('hours', 'giờ', false)}
        {this.renderItemTime('minutes', 'phút', false)}
        {this.renderItemTime('seconds', 'giây', false)}
      </TimeCountdownContainer>
    )
  }
}
