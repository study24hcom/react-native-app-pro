import React, {PureComponent} from 'react'
import styled from 'styled-components/native'
import {INPUT} from 'constants/color'
import {autobind} from 'core-decorators'
import {TextInput} from 'react-native'
import shadow from 'themes/shadow'

const Input = styled(TextInput)`
  padding-horizontal: 16px;
  padding-vertical: 16px;
  height: 55px;
  font-family: OpenSans-Regular;
  font-weight: 600;
  border-color: ${props =>
  props.isFocus ? `${INPUT.FOCUS}` : `#d4d4d4`};
  background-color: #ffffff;
  border-radius: 8px;
  ${props => props.noRadius && `border-radius: 0px`};
  ${props => props.isTopRadius &&
  `border-top-left-radius: 8px;
  border-top-right-radius: 8px;
  border-bottom-left-radius: 0px;
  border-bottom-right-radius: 0`}
  ${props => props.isBottomRadius &&
  `border-top-left-radius: 0px;
  border-top-right-radius: 0px
  border-bottom-left-radius: 8px;
  border-bottom-right-radius: 8px;`}
`

const ViewShadow = styled.View`
border-color: transparent;
border-bottom-color: ${props => props.isFocus ? `${INPUT.FOCUS}` : `transparent`};
border-width: 0.5px;
border-radius: ${props => props.isFocus ? '0px' : `8px`}
`
@autobind()
export default class MyTextInput extends PureComponent {
  state = {
    isFocus: false
  }

  _handleFocus (e) {
    this.setState({isFocus: true})
    if (this.props.onFocus) this.props.onFocus(e)
  }

  _handleBlur (e) {
    this.setState({isFocus: false})
    if (this.props.onBlur) this.props.onBlur(e)
  }

  componentDidMount () {
    if (this.props.auto) {
      setTimeout(() => {
        this.input.focus()
      }, 500)
    }
  }

  render () {
    return (
      <ViewShadow isFocus={this.state.isFocus} style={[shadow.shadow]}>
        <Input
          isFocus={this.state.isFocus}
          autoCapitalize='none'
          {...this.props}
          innerRef={comp => {
            this.input = comp
          }}
          autoCorrect={false}
          underlineColorAndroid={'transparent'}
          onFocus={this._handleFocus}
          onBlur={this._handleBlur}
        />
      </ViewShadow>
    )
  }
}
