import React from 'react'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'
import Icon from '../Icon'
import {SIZE} from '../../../constants/font'
import {ActivityIndicator} from 'react-native'

const Button = styled.TouchableOpacity`
  width: 45px;
  height: 45px;
  justify-content: center;
  align-items: center;
  border-radius: 22.5px;
  `

export default class ButtonRight extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    icon: PropTypes.string,
    simpleLineIcon: PropTypes.bool,
    submitting: PropTypes.bool
  }

  render () {
    const {onPress, icon, simpleLineIcon} = this.props
    return <Button activeOpacity={0.5} onPress={onPress}>
      {this.props.submitting && <ActivityIndicator color='#fff' />}
      {!this.props.submitting && <Icon name={icon} color='#fff'
        simpleLineIcon={simpleLineIcon} size={SIZE.TITLE} />}
    </Button>
  }
}
