import React from 'react'
import styled from 'styled-components/native'
import {SHAPE, INPUT} from 'constants/color'

//
function getChildrenColor (props) {
  let defaultColor = INPUT.BORDER
  if (props.isError) defaultColor = SHAPE.RED
  if (props.isWarning) defaultColor = SHAPE.ORANGE
  if (!defaultColor) return ''
  return defaultColor
}

const InputContainer = styled.View``

const FormFeedback = styled.Text`
  font-size: 14px;
  color: ${props => getChildrenColor(props)};
  padding-vertical: 4px;
  margin-top: 2px;
`

export function createValidateComponent (Component) {
  return props => <ReduxFormValidate componentChildren={Component} {...props} />
}

export function Input ({isError, isWarning, input, componentChildren, ...otherProps}) {
  const Inputs = componentChildren
  return (
    <Inputs
      isError={isError}
      isWarning={isWarning}
      {...input}
      {...otherProps}
    />
  )
}

const InputCustom = styled(Input)`
  ${props => (props.isError ? `border-color: ${SHAPE.RED}` : ``)} ${props =>
  props.isWarning ? `border-color: ${SHAPE.ORANGE}` : ``};
`

export default function ReduxFormValidate ({
                                            input, meta: {touched, error, warning},
                                            componentChildren, ...otherProps
                                          }) {
  return (
    <InputContainer>
      <InputCustom
        isError={touched && error}
        isWarning={touched && warning}
        input={input}
        {...otherProps}
        componentChildren={componentChildren}
      />
      {error &&
      touched && (
        <FormFeedback isError={touched && error}>{error}</FormFeedback>
      )}
      {touched &&
      warning && (
        <FormFeedback isWarning={touched && warning}>{warning}</FormFeedback>
      )}
    </InputContainer>
  )
}
