import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Modal, ActivityIndicator} from 'react-native'
import styled from 'styled-components/native'
import Clearfix from '../clearfix/index'
import Text from '../text/index'
import {SIZE} from 'constants/font'

const BackgroundModal = styled.View`
    flex: 1;
    align-items: center;
    flex-direction: column;
    justify-content: space-around;
    background-color: #00000040;
  `
const ActivityIndicatorWrapper = styled.View`
    background-color: #FFFFFF;
    height: 100px;
    width: 100px;
    border-radius: 10px;
    display: flex;
    align-items: center;
    justify-content: center;
`
export default class Loader extends Component {
  static propTypes = {
    loading: PropTypes.bool
  }

  render () {
    return <Modal
      transparent
      animationType={'none'}
      visible={this.props.loading}
      onRequestClose={() => {
        console.log('close modal')
      }}>
      <BackgroundModal>
        <ActivityIndicatorWrapper>
          <ActivityIndicator size={'large'} animating={this.props.loading} />
          <Clearfix height={8} />
          <Text fontSize={SIZE.NORMAL}>Đang tải...</Text>
        </ActivityIndicatorWrapper>
      </BackgroundModal>
    </Modal>
  }
}
