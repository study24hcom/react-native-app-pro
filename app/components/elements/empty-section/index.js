import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Text from '../text'
import Icon from '../Icon'
import {SIZE} from 'constants/font'

const EmptySectionContainer = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 16px;
`

export default class EmptySection extends PureComponent {
  static propTypes = {
    text: PropTypes.string,
    textStrong: PropTypes.string,
    icon: PropTypes.string,
    simpleLineIcon: PropTypes.bool
  }

  render () {
    const {text, icon, textStrong, simpleLineIcon} = this.props
    return (
      <EmptySectionContainer>
        {text ? <Text style={{textAlign: 'center'}} fontSize={SIZE.NORMAL} customColor='graybold'>
          {icon ? <Icon simpleLineIcon={simpleLineIcon} size={SIZE.NORMAL} name={icon} /> : null}{' '}{text}
          {textStrong ? <Text fontSize={SIZE.HEADING3} customColor='red'>{' '}{textStrong}</Text> : null}
        </Text>
          : null}
        {this.props.children}
      </EmptySectionContainer>
    )
  }
}
