import React, {PureComponent} from 'react'
import {StyleSheet} from 'react-native'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Icon from '../Icon'
import Text from '../text/index'
import {TEXT} from 'constants/color'
import {SIZE} from '../../../constants/font'

const MessageAlert = styled.View`
  padding-horizontal: 16px;
  background-color: #fff;
  margin-vertical: 4px;
  justify-content: center;
  align-items: center;
  height: 150px;
`

const IconWrapper = styled.Text`
  color: ${TEXT.GRAY};
  text-align: center;
  font-size: ${SIZE.NORMAL};
  line-height: 23px;
`
const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#d4d4d4',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 3,
    shadowOpacity: 0.2
  }
})
export default class BoxAlert extends PureComponent {
  static propTypes = {
    icon: PropTypes.string,
    content: PropTypes.string,
    simpleLineIcon: PropTypes.bool
  }

  render () {
    return (
      <MessageAlert style={styles.shadow}>
        <IconWrapper>
          <Icon size={SIZE.NORMAL} simpleLineIcon={this.props.simpleLineIcon} name={this.props.icon} />
          <Text color={TEXT.GRAY}>{this.props.content}</Text>
        </IconWrapper>
      </MessageAlert>
    )
  }
}
