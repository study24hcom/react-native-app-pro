import React from 'react'
import styled from 'styled-components/native'
import InputProfile from '../../profile/input/index'
import Label from '../Label/index'

const View = styled.View`
  flex-direction: row;
  align-items: center;
  `
const LabelView = styled.View`
  width: 100px;
  `
const InputView = styled.View`
  flex: 1;
  `
export default function InputLabel ({label, ...props}) {
  return (
    <View>
      <LabelView>
        {label && <Label>{label}</Label>}
      </LabelView>
      <InputView>
        <InputProfile {...props} />
      </InputView>
    </View>
  )
}

InputLabel.propTypes = {
  ...InputProfile.propTypes
}
