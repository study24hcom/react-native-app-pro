import React from 'react'
import Text from '../text/index'
import {SIZE} from 'constants/font'

export default function Label ({children}) {
  return <Text fontSize={SIZE.BIG} color='#000' fontWeight={'400'}>{children}</Text>
}
