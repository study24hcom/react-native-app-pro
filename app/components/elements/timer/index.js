import React, { PureComponent, PropTypes } from 'react'
import { Text } from 'react-native'
import { SHAPE } from 'constants/color'
import TextFont from '../text'

export default class TimerSeconds extends PureComponent {
  static propTypes = {
    isStop: PropTypes.bool,
    isStart: PropTypes.bool,
    seconds: PropTypes.number.isRequired,
    onFinish: PropTypes.func
  }

  constructor () {
    super(...arguments)
    this.state = {
      time_length: parseInt(this.props.seconds, 10)
    }
    this.timerInterval = () => {}
  }

  componentDidMount () {
    this.timerInterval = setInterval(() => {
      if (this.props.isStart) {
        this.setState({ time_length: this.state.time_length - 1 })
      }
      if (this.state.time_length <= 0 || this.props.isStop) {
        if (this.props.onFinish) {
          let type = 'auto'
          if (this.props.isStop) type = 'handle'
          this.props.onFinish(this.state.time_length, type)
          clearInterval(this.timerInterval)
        }
      }
    }, 1000)
  }

  componentDidUpdate (prevProps) {
    if (prevProps.seconds !== this.props.seconds) {
      this.setState({ time_length: this.props.seconds })
    }
  }

  componentWillUnmount () {
    clearInterval(this.timerInterval)
  }

  convertWithZero (number) {
    let string = number.toString()
    if (string.length > 1) {
      return string
    } else {
      return '0' + string
    }
  }

  renderTime () {
    let hours = parseInt(this.state.time_length / 3600, 10)
    let minutes = parseInt((this.state.time_length % 3600) / 60, 10)
    let seconds = (this.state.time_length % 3600) % 60
    return (
      <TextFont color={SHAPE.PRIMARYBOLD} fontSize={20} fontWeight='medium'>
        <Text>{this.convertWithZero(hours).toString()}</Text>
        :
        <Text>{this.convertWithZero(minutes).toString()}</Text>
        :
        <Text>{this.convertWithZero(seconds).toString()}</Text>
      </TextFont>
    )
  }

  render () {
    return this.renderTime()
  }
}
