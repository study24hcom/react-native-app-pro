import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Icon from 'components/elements/Icon'
import Text from 'components/elements/text'

const WraperText = styled.View`
  flex-direction: row;
  align-items: center;
  flex: 1;
  ${props => props.center ? `justify-content: center` : ``}
`
export default class TextIcon extends PureComponent {
  static propTypes = {
    simpleLineIcon: PropTypes.bool,
    children: PropTypes.string,
    size: PropTypes.number,
    fontWeight: PropTypes.number,
    icon: PropTypes.string,
    color: PropTypes.string,
    center: PropTypes.bool
  }

  render () {
    return (
      <WraperText center={this.props.center}>
        <Icon color={this.props.color} size={this.props.size} simpleLineIcon={this.props.simpleLineIcon}
          name={this.props.icon} />
        <Text color={this.props.color} numberOfLines={1} fontWeight={this.props.fontWeight}
          fontSize={this.props.size}>{`  ${this.props.children}`}</Text>
      </WraperText>
    )
  }
}
