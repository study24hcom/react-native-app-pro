import React, {Component} from 'react'
import {Dimensions} from 'react-native'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'
import {SHAPE} from 'constants/color'
import Modal from 'react-native-modal'
import {SIZE, WEIGHT} from 'constants/font'
import Text from 'components/elements/text'
import Icon from '../Icon'
import Clearfix from 'components/elements/clearfix'

const {height} = Dimensions.get('window')
const MetaWrapper = styled.View`
  padding: 16px;
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  align-items: center;
  ${props => props.noLine ? null
  : `border-bottom-width: 1px;
   border-bottom-color: #eee;
  `};
`
const IconWrapper = styled.View`
    position: absolute;
    right: -16px;
    top: 0px;
`
const Meta = styled.TouchableOpacity`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  position: relative;
`

const Header = styled.View`
  border-top-right-radius: 4px;
  border-top-left-radius: 4px;
  border-color: #f0f0f0;
  border-bottom-width: 1px;
  padding: 12px;
  align-items: center;
  background-color: #f0f0f0
`

const Body = styled.View`
  padding-horizontal: 24px;
  align-items: center;
  justify-content: center;
  flex: 1;
`
const ModalWrapper = styled.View`
  height: ${0.25 * height};
  background-color: #fff;
  border-radius: 4px;
`
export default class HeadingLine extends Component {
  static propTypes = {
    icon: PropTypes.string,
    color: PropTypes.string,
    title: PropTypes.string,
    totalItem: PropTypes.number,
    onPress: PropTypes.func,
    simpleLineIcon: PropTypes.bool,
    hideTotalItem: PropTypes.bool,
    isShow: PropTypes.bool,
    noLine: PropTypes.bool,
    viewAllText: PropTypes.string,
    size: PropTypes.number,
    description: PropTypes.string
  }

  static defaultProps = {
    viewAllText: 'Xem tất cả',
    size: SIZE.HEADING1
  }

  state = {
    isVisible: false
  }

  isShowTextRight() {
    return (this.props.isShow && this.props.totalItem > 0)
  }

  onPressHeading() {
    if (!this.props.description) return
    this.setState({isVisible: !this.state.isVisible})
  }

  renderModalView() {
    return <ModalWrapper>
      <Header>
        <Text fontWeight={500} fontSize={SIZE.HEADING2} color={SHAPE.PRIMARYBOLD}
              numberOfLines={1}>{this.props.title}</Text>
      </Header>
      <Body>
      <Text align={'center'} fontSize={SIZE.SMALL} fontWeight={400}>{this.props.description}</Text>
      </Body>
    </ModalWrapper>
  }

  render() {
    return <MetaWrapper noLine={this.props.noLine}>
      <Meta activeOpacity={0.8} onPress={this.onPressHeading.bind(this)}>
        {this.props.icon && (
          <Icon size={this.props.size} color={this.props.color}
                simpleLineIcon={this.props.simpleLineIcon} name={this.props.icon}/>)}
        {this.props.icon && <Clearfix width={8}/>}
        <Text fontWeight={WEIGHT.BOLD} color={this.props.color}
              fontSize={this.props.size}>{this.props.title}</Text>
        {this.props.totalItem > 0 && !this.props.hideTotalItem && (
          <Text fontWeight={WEIGHT.BOLD} color={this.props.color}
                fontSize={this.props.size}>{` (${this.props.totalItem})`}</Text>)}
        {this.props.description &&
        <IconWrapper>
          <Icon name={'question-circle'} size={13} color={'#999'}/>
        </IconWrapper>}
      </Meta>


      {this.isShowTextRight() && (
        <Meta activeOpacity={0.9} onPress={this.props.onPress}>
          <Text fontSize={SIZE.SMALL} color={SHAPE.GRAYTEXT}>{this.props.viewAllText}</Text>
        </Meta>
      )}
      {this.props.description &&
      <Modal backdropColor={'rgba(0,0,0,0.5)'} isVisible={this.state.isVisible}
             onBackdropPress={() => this.setState({isVisible: false})}>
        {this.renderModalView()}
      </Modal>}
    </MetaWrapper>
  }
}
