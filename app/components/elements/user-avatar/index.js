import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Avatar from './avatar'
import {TEXT} from 'constants/color'
import Text from 'components/elements/text'
import {WEIGHT} from 'constants/font'

const View = styled.View`
  display: flex;
  align-items: center;
  flex-direction: row;
`

const Info = styled.View`
  display: flex;
  flex-direction: column;
  margin-left: 8px;
`

const SpanUsername = styled(Text)`
  font-weight: ${WEIGHT.BOLD};
  font-size: ${props => props.size}px;
  ${props => props.color ? `color: ${props.color}` : ''}
  padding: 2px
`

const SpanHello = styled(Text)`
  font-size: ${props => props.size}px;
  color: ${TEXT.GRAY};
  padding: 2px
`

export default class UserAvatar extends PureComponent {
  static propTypes = {
    username: PropTypes.string,
    usernameColor: PropTypes.string,
    avatar: PropTypes.string,
    avatarSize: PropTypes.number,
    usernameSize: PropTypes.number,
    fullname: PropTypes.any
  };

  render () {
    const {avatar, username, avatarSize, usernameSize, usernameColor, fullname} = this.props
    return (
      <View>
        <Avatar avatar={avatar} avatarSize={avatarSize} username={username} />
        <Info>
          <SpanUsername color={usernameColor} size={usernameSize}>
            {username}
          </SpanUsername>
          {fullname
            ? <SpanHello size={usernameSize}>
              {fullname}
            </SpanHello> : null}
        </Info>
      </View>
    )
  }
}
