import React from 'react'
import styled from 'styled-components/native'

import Icon from '../Icon'
import {SIZE} from 'constants/font'
import InputProfile from '../../profile/input/index'

const View = styled.View`
  flex-direction: row;
  align-items: center;
  `

export default function InputLabelIcon ({icon, ...props}) {
  return (
    <View>
      <Icon name={icon} size={SIZE.NORMAL} color='rgba(0,0,0,.5)' />
      <InputProfile {...props} />
    </View>
  )
}

InputLabelIcon.propTypes = {
  ...InputProfile.propTypes
}
