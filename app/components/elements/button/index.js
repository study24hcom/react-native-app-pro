import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {SHAPE} from 'constants/color'
import {ActivityIndicator} from 'react-native'

function getBackgroundColor (props) {
  let backgroundColor = ''
  switch (props.customColor) {
    case 'red':
      backgroundColor = SHAPE.RED
      break
    case 'green':
      backgroundColor = SHAPE.GREEN
      break
    case 'orange':
      backgroundColor = SHAPE.ORANGE
      break
    case 'purple':
      backgroundColor = SHAPE.PURPLE
      break
    case 'primary':
      backgroundColor = SHAPE.PRIMARY
      break
    case 'primarybold':
      backgroundColor = SHAPE.PRIMARYBOLD
      break
    case 'yellow':
      backgroundColor = SHAPE.YELLOW
      break
    default:
  }
  if (!backgroundColor) return ``
  return `
    background-color: ${backgroundColor};
  `
}

const ButtonStyle = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  ${props => props.noRadius ? `` : `border-radius : 2px`};
  ${props => props.customBorder ? `border-color: ${props.customBorder}` : ''};
  ${props => props.customBorder ? `border-width : 1px` : ''};
  ${props => getBackgroundColor(props)};
  ${props => props.customPadding ? `padding: ${props.customPadding}px` : ''};
`

export default class ButtonCustom extends PureComponent {
  render () {
    return (
      <ButtonStyle
        {...this.props}
        onPress={this.props.onClick}
        activeOpacity={1}
      >
        {this.props.isLoading && <ActivityIndicator color='#fff' />}
        {!this.props.isLoading ? this.props.children : null}
      </ButtonStyle>
    )
  }
}

ButtonCustom.propTypes = {
  customColor: PropTypes.string,
  onClick: PropTypes.func,
  customPadding: PropTypes.number,
  customBorder: PropTypes.string,
  isLoading: PropTypes.bool,
  noRadius: PropTypes.bool
}
