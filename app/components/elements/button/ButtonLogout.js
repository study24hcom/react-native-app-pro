import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {SIZE, WEIGHT} from 'constants/font'
import Button from 'components/elements/button'
import Text from 'components/elements/text'

const ButtonWrapper = styled.View`
  position: relative;  
  bottom: 0;
  left: 0;
  right: 0;
`
export default class ButtonLogout extends PureComponent {
  static propTypes = {
    onPress: PropTypes.func
  }

  render () {
    return <ButtonWrapper>
      <Button noRadius customPadding={10} customColor='red' onClick={this.props.onPress}>
        <Text color='#fff' fontSize={SIZE.NORMAL} fontWeight={WEIGHT.BOLD}>ĐĂNG XUẤT</Text>
      </Button>
    </ButtonWrapper>
  }
}
