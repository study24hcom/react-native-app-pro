import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'

const View = styled.View`
  ${props => (props.width ? `width: ${props.width}px;` : '')} 
  ${props => props.height ? `height: ${props.height}px;` : ''};
`

class Clearfix extends PureComponent {
  static propTypes = {
    width: PropTypes.number,
    height: PropTypes.number
  }

  render () {
    return (
      <View width={this.props.width} height={this.props.height} />
    )
  }
}

export default Clearfix
