import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {ActivityIndicator} from 'react-native'

const LoadingWrapper = styled.View`
  padding: ${props => props.padding >= 0 ? `${props.padding}px` : '50px'};
  margin: ${props => props.margin >= 0 ? `${props.margin}px` : `0px`};
  justify-content: center;
  align-items: center;
`
export default class Loading extends PureComponent {
  static propTypes = {
    padding: PropTypes.number,
    margin: PropTypes.number
  }

  render () {
    return (
      <LoadingWrapper margin={this.props.margin} padding={this.props.padding}>
        <ActivityIndicator />
      </LoadingWrapper>
    )
  }
}
