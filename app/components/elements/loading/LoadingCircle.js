import React, { PureComponent } from 'react'
import * as Progress from 'react-native-progress'
import styled from 'styled-components/native'

const LoadingWrapper = styled.View`
  padding: ${props => props.padding >= 0 ? `${props.padding}px` : '50px'};
  justify-content: center;
  align-items: center;
`
export default class LoadingCircle extends PureComponent {
  render () {
    return (
      <LoadingWrapper>
        <Progress.CircleSnail spinDuration={1000} size={50} color={['red', 'green', 'blue']} />
      </LoadingWrapper>
    )
  }
}
