import React, {Component} from 'react'
import styled from 'styled-components/native'
import {ActivityIndicator} from 'react-native'
import PropTypes from 'prop-types'
import {TEXT} from 'constants/color'
import {SIZE, WEIGHT} from 'constants/font'
import Text from '../text/index'

const BoxShadow = styled.View`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  align-items: center;
  border-bottom-width: 1px;
  border-bottom-color: #E0E0E0;
  padding-horizontal: 16px;
`

export default class Heading extends Component {
  static propTypes = {
    title: PropTypes.string,
    isLoading: PropTypes.bool

  }

  render () {
    return <BoxShadow>
      <Text fontWeight={WEIGHT.BOLDER} fontSize={SIZE.NORMAL} padding='8px 0px'
        color={TEXT.GRAY}>{this.props.title}</Text>
      {this.props.isLoading && <ActivityIndicator />}
    </BoxShadow>
  }
}
