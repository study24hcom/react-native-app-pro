import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import EmptySectionContainer from 'components/elements/empty-section'
import { autobind } from 'core-decorators'
import { FlatList, ActivityIndicator, Dimensions } from 'react-native'
import NotificationItem from '../notification-item'

const window = Dimensions.get('window')

const NotificationListContainer = styled.View`
  flex: 1;
  `

const LoadingWrapper = styled.View`
  padding: 16px;
  justify-content: center;
  align-items: center;
`
@autobind
export default class NotificationList extends PureComponent {
  static propTypes = {
    data: PropTypes.arrayOf(
      PropTypes.shape({
        user: PropTypes.shape({
          username: PropTypes.string,
          avatar: PropTypes.string
        }),
        type: PropTypes.string,
        action: PropTypes.string
      })
    ),
    onLoadMore: PropTypes.func,
    isLoading: PropTypes.bool,
    isLoadingFirst: PropTypes.bool,
    onRefresh: PropTypes.func,
    onPressItem: PropTypes.func
  }

  renderItem ({item, index}) {
    return <NotificationItem key={index} onPress={this.props.onPressItem} item={item} {...item} />
  }

  renderLoading () {
    if (!this.props.isLoading) return
    return (
      <LoadingWrapper>
        <ActivityIndicator />
      </LoadingWrapper>
    )
  }

  render () {
    if (this.props.data.length === 0) {
      return (
        <EmptySectionContainer
          text=' Bạn chưa có thông báo nào '
          icon='bell'
          height={window.height}
        />
      )
    }
    return (
      <NotificationListContainer>
        <FlatList
          data={this.props.data}
          keyExtractor={(item, index) => index}
          renderItem={this.renderItem}
          onEndReached={this.props.onLoadMore}
          onEndReachedThreshold={0}
          onRefresh={this.props.onRefresh}
          refreshing={this.props.isLoadingFirst}
          ListFooterComponent={this.renderLoading()}
        />
      </NotificationListContainer>
    )
  }
}
