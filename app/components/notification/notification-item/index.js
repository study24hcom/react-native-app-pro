import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import Notification from 'constants/notificationType'
import styled from 'styled-components/native'
import Icon from '../../elements/Icon'
import Text from 'components/elements/text'
import {getNewNotifications, markChecked} from '@redux/actions/notificationAction'
import Avatar from 'components/elements/user-avatar/avatar'
import {SHAPE, TEXT} from 'constants/color'
import {connectAutoDispatch} from '@redux/connect'
import TimeAgo from 'react-native-timeago'
import {autobind} from 'core-decorators'
import toShortString from 'to-short-string'
import {Answers} from 'react-native-fabric'

const AVATAR_SIZE = 50

const NotificationItemContainer = styled.TouchableOpacity`
  background-color: ${props => (props.isChecked ? '#ffffff' : SHAPE.GRAYLIGHT)};
  flex-direction: row;
  align-items: center;
  padding: 16px
`
const AvatarWrapper = styled.View`
  width: ${AVATAR_SIZE}px;
  margin-right:  8px;
`

const DescriptionWrapper = styled.View`
  flex: 1;
  flex-direction: column;
`
const TextWrapper = styled.Text`
  flex: 1;
  font-size: 16px;
  line-height: 23px;
  color: ${TEXT.GRAY};
`

const SpanUsername = styled.Text`
  color: ${SHAPE.PRIMARYBOLD};
  font-weight: 500
`

const TimeAgoWrapper = styled.Text`
  color: ${SHAPE.GRAYTEXT};
  font-size: 13px;
  flex: 1;
  padding-top: 8px
`

const Quizlistname = styled.Text`
  color: ${SHAPE.PRIMARY};
  font-weight: 500
`

const BoxShadow = styled.View`
  border-bottom-color: #f1f1f1;
  border-bottom-width: 1px
  `
@connectAutoDispatch(state => ({}), {
  markChecked, getNewNotifications
})
@autobind
export default class NotificationItem extends PureComponent {
  static propTypes = {
    type: PropTypes.string,
    status: PropTypes.string,
    action: PropTypes.string,
    quizList: PropTypes.shape({
      name: PropTypes.string,
      slug: PropTypes.string
    }),
    user: PropTypes.shape({
      id: PropTypes.string,
      username: PropTypes.string
    }),
    data: PropTypes.object,
    markChecked: PropTypes.func,
    onPress: PropTypes.func
  }

  getAction () {
    switch (this.props.action) {
      case Notification.action.COMPLETE_QUIZ_LIST:
        return ' vừa làm đề thi '
      case Notification.action.RATE_QUIZ_LIST:
        return ' vừa đánh giá '
      default:
        return this.props.action
    }
  }

  isChecked () {
    return this.props.status === Notification.status.CHECKED
  }

  getContentAfter () {
    const {data} = this.props
    switch (this.props.action) {
      case Notification.action.RATE_QUIZ_LIST:
        return toShortString(data.content, 9, 7)
    }
    return ''
  }

  handleClick (e) {
    e.preventDefault()
    const {quizList} = this.props
    Answers.logContentView(quizList.name, 'QUIZLIST', quizList.slug)
    this.props.markChecked(this.props.id)
    this.props.onPress(this.props.item)
    this.props.getNewNotifications({autoFetch: false})
  }

  render () {
    const {user, quizList} = this.props
    return (
      <BoxShadow>
        <NotificationItemContainer
          activeOpacity={0.9}
          isChecked={this.isChecked()}
          onPress={this.handleClick}>
          <AvatarWrapper>
            {user.username &&
            <Avatar avatar={user.avatarUrl} avatarSize={AVATAR_SIZE} username={user.username} />}
          </AvatarWrapper>
          <DescriptionWrapper>
            <TextWrapper>
              <SpanUsername>{user.username}</SpanUsername>
              <Text>{this.getAction()}</Text>
              <Quizlistname>{quizList.name}</Quizlistname>
              {this.props.data.content
                ? <Text>{` ${this.getContentAfter()}`}</Text> : null}
            </TextWrapper>
            <TimeAgoWrapper>
              <Icon name='clock-o' size={13} />
              <Text>{` `}</Text>
              <TimeAgo time={this.props.createdAt} interval={60000} />
            </TimeAgoWrapper>
          </DescriptionWrapper>
        </NotificationItemContainer>
      </BoxShadow>
    )
  }
}
