import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import Carousel from 'react-native-snap-carousel'
import styled from 'styled-components/native'
import {sliderWidth} from 'utils/slider'
import {autobind} from 'core-decorators'
import QuizListItem from 'components/quizlist/quizlist-item'
import HeadingLine from 'components/elements/heading-line'
import {SHAPE} from 'constants/color'
import PlaceholderView from 'components/placeholder'
import {quizList} from 'constants/description'

const QuizlistListContainer = styled.View``

@autobind
export class QuizlistList extends PureComponent {
  static propTypes = {
    data: PropTypes.array,
    onPressUser: PropTypes.func,
    onPressItem: PropTypes.func
  }

  _renderItem ({item, index}) {
    return <QuizListItem key={index} isPressUser horizontal onPressUser={this.props.onPressUser} isPress
      onPress={this.props.onPressItem} {...item} />
  }

  render () {
    if (this.props.data.length === 0) return <PlaceholderView height={185} />
    return (
      <Carousel
        ref={(c) => {
          this._carousel = c
        }}
        data={this.props.data}
        renderItem={this._renderItem}
        sliderWidth={sliderWidth}
        itemWidth={sliderWidth}
        inactiveSlideScale={1}
        inactiveSlideOpacity={1}
        removeClippedSubviews={false}
        loop
        autoplay
        autoplayDelay={500}
        autoplayInterval={5000}
      />
    )
  }
}

export default class HotQuizlist extends PureComponent {
  static propTypes = {
    quizListsTopDay: PropTypes.array,
    onPressItem: PropTypes.func,
    onPressUser: PropTypes.func
  }

  render () {
    return (
      <QuizlistListContainer>
        <HeadingLine description={quizList.today} noLine title='Thi gì hôm nay' color={SHAPE.GREEN} />
        <QuizlistList onPressUser={this.props.onPressUser} onPressItem={this.props.onPressItem}
          data={this.props.quizListsTopDay} />
      </QuizlistListContainer>
    )
  }
}
