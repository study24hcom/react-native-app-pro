import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {getQuizListsByNews} from '@redux/actions/quizListsAction'
import connectQuizLists from 'hoc/quizlists-wrapper'
import {QUIZ_LISTS_BY_NEWS} from 'constants/quizLists'
import QuizlistList from './quizlistList'

@connectQuizLists({
  action: getQuizListsByNews,
  key: QUIZ_LISTS_BY_NEWS.key,
  payload: QUIZ_LISTS_BY_NEWS.payload
})
export default class QuizListNews extends Component {
  static propTypes = {
    propsForFlatlist: PropTypes.object,
    onPressUser: PropTypes.func,
    onPressItem: PropTypes.func,
    isLoadingFirst: PropTypes.bool,
    _isRefreshing: PropTypes.bool,
    _onRefresh: PropTypes.func,
    headerFlatlist: PropTypes.any,
    _isLoadingFirst: PropTypes.bool
  }

  render () {
    return (
      <QuizlistList isPressUser
        onPressUser={this.props.onPressUser}
        onPressItem={this.props.onPressItem}
        _isLoadingFirst={this.props._isLoadingFirst}
        _isRefreshing={this.props._isRefreshing}
        headerFlatlist={this.props.headerFlatlist}
        _onRefresh={this.props._onRefresh}
        {...this.props.propsForFlatlist} />
    )
  }
}
