import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import connectQuizLists from 'hoc/quizlists-wrapper'
import {getQuizListsByFollowed} from '@redux/actions/quizListsAction'
import {QUIZ_LISTS_BY_FOLLOWED} from 'constants/quizLists'
import QuizlistList from './quizlistList'

@connectQuizLists({
  action: getQuizListsByFollowed,
  key: QUIZ_LISTS_BY_FOLLOWED.key,
  payload: QUIZ_LISTS_BY_FOLLOWED.payload
})
export default class QuizListFollowed extends PureComponent {
  static propTypes = {
    propsForFlatlist: PropTypes.object,
    onPressUser: PropTypes.func,
    onPressItem: PropTypes.func,
    _isLoadingFirst: PropTypes.bool,
    onPressViewAll: PropTypes.func,
    _isRefreshing: PropTypes.bool,
    _onRefresh: PropTypes.func,
    headerFlatlist: PropTypes.any,
    isLoadSuccess: PropTypes.bool
  }

  render () {
    return (
      <QuizlistList isPressUser
        onPressUser={this.props.onPressUser}
        onPressItem={this.props.onPressItem}
        onPressViewAll={this.props.onPressViewAll}
        _isRefreshing={this.props._isRefreshing}
        _isLoadingFirst={this.props._isLoadingFirst}
        headerFlatlist={this.props.headerFlatlist}
        _onRefresh={this.props._onRefresh}
        isLoadSuccess={this.props.isLoadSuccess}
        {...this.props.propsForFlatlist} />
    )
  }
}
