import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {FlatList, View} from 'react-native'
import {autobind} from 'core-decorators'
import Placeholder from 'rn-placeholder'
import QuizListItem from 'components/quizlist/quizlist-item'
import BoxShadow from 'components/elements/WhiteBoxShadow'
import Clearfix from 'components/elements/clearfix'
import Loading from 'components/elements/loading'
import EmptySectionContainer from 'components/elements/empty-section'
import ButtonCustom from 'components/elements/button'

@autobind
export default class QuizlistList extends PureComponent {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape(QuizListItem.propTypes)),
    onScrollToBottom: PropTypes.func,
    onRefresh: PropTypes.func,
    isLoadingMore: PropTypes.bool,
    onPressItem: PropTypes.func,
    onPressUser: PropTypes.func,
    isPressUser: PropTypes.bool,
    _isRefreshing: PropTypes.bool,
    _onRefresh: PropTypes.func,
    _isLoadingFirst: PropTypes.bool,
    isLoadSuccess: PropTypes.bool
  }

  renderItem ({item}) {
    return <QuizListItem isPressUser={this.props.isPressUser} isPress
      onPressUser={this.props.onPressUser} {...item}
      onPress={this.props.onPressItem} />
  }

  renderLoading () {
    if (!this.props.isLoadingMore) return
    return (
      <Loading margin={8} padding={0} />
    )
  }

  renderPlaceholder () {
    return <BoxShadow style={{marginBottom: 20}}>
      <Placeholder.Paragraph
        lineNumber={3}
        textSize={16}
        lineSpacing={5}
        color='#eeeeee'
        animate='shine'
        width='100%'
        lastLineWidth='70%'
        firstLineWidth='50%' />
    </BoxShadow>
  }

  onClickButton () {

  }

  renderHeader () {
    if (this.props._isLoadingFirst && !this.props.isLoadingMore) {
      return <View>
        {this.props.headerFlatlist}
        {this.renderPlaceholder()}
        {this.renderPlaceholder()}
        {this.renderPlaceholder()}
      </View>
    }
    if (this.props.data.length === 0 && this.props.isLoadSuccess) {
      return <View>
        {this.props.headerFlatlist}
        <View style={{height: 150, justifyContent: 'center', alignItems: 'center'}}>
          <EmptySectionContainer text={'Bạn chưa theo dõi tag hoặc user nào'}>
            <ButtonCustom onClick={this.onClickButton} />
          </EmptySectionContainer>
        </View>
      </View>
    }
    return <View>
      {this.props.headerFlatlist}
    </View>
  }

  render () {
    return (
      <FlatList
        data={this.props.data}
        keyExtractor={item => item.id}
        renderItem={this.renderItem}
        ListHeaderComponent={this.renderHeader}
        ItemSeparatorComponent={() => <Clearfix height={4} />}
        ListFooterComponent={this.renderLoading()}
        onEndReached={this.props.onScrollToBottom}
        onEndReachedThreshold={0.2}
        onRefresh={this.props._onRefresh}
        refreshing={this.props._isRefreshing}
      />
    )
  }
}
