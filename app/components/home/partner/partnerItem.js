import React, {PureComponent} from 'react'
import {SIZE, WEIGHT} from 'constants/font'
import {Dimensions} from 'react-native'
import {autobind} from 'core-decorators'
import {SHAPE, TEXT} from 'constants/color'
import toShortString from 'to-short-string'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Avatar from 'components/elements/user-avatar/avatar/index'
import HeartFollow from './heartFollow'
import Text from 'components/elements/text'
import TextIcon from 'components/elements/text-icon'

const {width} = Dimensions.get('window')
const InfoContainer = styled.View`
  margin-bottom: 0px;
  border-width: 0.5px;
  border-color: #E0E0E0;
  background-color: #ffffff;
  border-radius: 4px;
  width: ${width - 8};
  margin: 4px;
`
const View = styled.View`
  flex-direction: row;
  padding: 16px;
  background-color: #ffffff;
`
const Info = styled.View`
  margin-left: 12px;
  height: 60px;
  flex-direction: column;
  justify-content: space-around;
  background-color: #fff;
  flex: 1;
`
const SpanUsername = styled(Text)`
  font-weight: ${WEIGHT.BOLD};
  font-size: ${props => props.size}px;
  ${props => props.color ? `color: ${props.color}` : ''};
  background-color: #ffffff;
`
// const Meta = styled(Text)`
//   ${props => props.color ? `color: ${props.color};` : ''}
//   font-weight: ${WEIGHT.BOLD};
//   font-size: ${SIZE.NORMAL};
//   font-family: OpenSans-SemiBold;
//   padding-right: 16px;
//     background-color: #ffffff;
//   `

const Meta = styled.View`
  flex: ${props => props.flex};
`
const InfoUser = styled.TouchableOpacity`
  flex: 1;
  flex-direction: row;
  background-color: #ffffff;
`
const Follower = styled.View`
  align-items: center;
  justify-content: center;
  background-color: #ffffff;
`
const MetaWrapper = styled.View`
  flex-direction: row;
  background-color: #ffffff;
  align-items: center;
  flex: 1;
`
const Description = styled.View`
  padding: 0 16px 16px 16px;
  background-color: #ffffff;
`
@autobind
export default class PartnerItem extends PureComponent {
  static propTypes = {
    username: PropTypes.string,
    usernameColor: PropTypes.string,
    totalQuizlist: PropTypes.number,
    avatar: PropTypes.string,
    avatarSize: PropTypes.number,
    usernameSize: PropTypes.number,
    totalFollower: PropTypes.number,
    description: PropTypes.string,
    onPress: PropTypes.func,
    shortDescription: PropTypes.bool
  }
  static defaultProps = {
    avatarSize: 60,
    usernameColor: TEXT.NORMAL,
    usernameSize: SIZE.NORMAL
  }

  onPress () {
    this.props.onPress(this.props)
  }

  getShortDescription () {
    const {description} = this.props
    return toShortString(description, 50, 7)
  }

  render () {
    return (
      <InfoContainer>
        <View>
          <InfoUser activeOpacity={0.8} onPress={this.onPress}>
            <Avatar avatar={this.props.avatar}
              avatarSize={this.props.avatarSize}
              username={this.props.username} />
            <Info>
              <MetaWrapper>
                <SpanUsername color={this.props.usernameColor}
                  size={this.props.usernameSize}>
                  {this.props.username}
                </SpanUsername>
              </MetaWrapper>
              <MetaWrapper>
                <Meta flex={1}>
                  <TextIcon color={SHAPE.GREEN} fontWeight={600} size={SIZE.NORMAL}
                    icon={'clock-o'}>{`${this.props.totalQuizlist} đề`}</TextIcon>
                </Meta>
                <Meta flex={1.5}>
                  <TextIcon color={SHAPE.RED} size={SIZE.NORMAL} fontWeight={600}
                    icon={'heart'}>{`${this.props.totalFollower} theo dõi`}</TextIcon>
                </Meta>
              </MetaWrapper>
            </Info>
          </InfoUser>
          <Follower>
            <HeartFollow userId={this.props.username} />
          </Follower>
        </View>
        {this.props.description ? <Description>
          {this.props.shortDescription
            ? <Text color={TEXT.GRAY}>{this.getShortDescription()}</Text>
            : <Text color={TEXT.GRAY}>{this.props.description}</Text>}
        </Description> : null}
      </InfoContainer>
    )
  }
}
