import React, {Component} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import PartnerItem from './partnerItem'
import HeadingLine from 'components/elements/heading-line'
import {SHAPE} from 'constants/color'
import {connectAutoDispatch} from '@redux/connect'
import {SIZE} from 'constants/font'
import {tag} from 'constants/description'

const View = styled.View``

@autobind
@connectAutoDispatch((state) => ({
  data: state.user.partnerList.data,
  pagination: state.user.partnerList.pagination
}), {})
export default class PartnerList extends Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape(PartnerItem.propTypes)),
    onPressItem: PropTypes.func,
    onPressViewAll: PropTypes.func,
    pagination: PropTypes.object
  }

  renderItem ({item, index}) {
    return <PartnerItem key={index} onPress={this.props.onPressItem} {...item} />
  }

  render () {
    if (this.props.data.length === 0) return null
    return <View>
      <HeadingLine description={tag.featured} size={SIZE.HEADING4} icon={'people'} simpleLineIcon
        onPress={this.props.onPressViewAll} viewAllText={'Xem thêm'}
        totalItem={this.props.pagination.totalItem}
        noLine isShow color={SHAPE.BLACK} title={'Sáng tạo nội dung'} />
      {this.props.data.slice(0, 10).map((partner, index) => <PartnerItem shortDescription key={index} {...partner}
        onPress={this.props.onPressItem} />)}
    </View>
  }
}
