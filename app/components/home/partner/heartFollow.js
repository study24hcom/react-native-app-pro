import React from 'react'
import PropTypes from 'prop-types'
import hocFollowUser from 'hoc/follow-user'
import Icon from '../../elements/Icon'

const HeartFollow = ({isFollowed, onClick}) => (
  <Icon name={isFollowed ? 'heart' : 'heart-o'} onPress={onClick} color={isFollowed ? 'red' : '#000'} size={30} />
)

HeartFollow.propTypes = {
  userId: PropTypes.string
}

export default hocFollowUser(HeartFollow)
