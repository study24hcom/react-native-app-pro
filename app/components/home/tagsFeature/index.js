import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import HeadingLine from '../../elements/heading-line/index'
import Clearfix from '../../elements/clearfix/index'
import styled from 'styled-components/native'
import tagCharacters from 'constants/tagCharacter'
import TagList from '../../tag/tag-list/index'
import {SIZE} from '../../../constants/font'
import {autobind} from 'core-decorators'

const View = styled.View``
const TagListWrapper = styled.ScrollView`
  flex: 1;
  `
const TagWrapper = styled.View`
  padding: 0 16px;
  `
@autobind
export default class TagsFeatureGroup extends PureComponent {
  static propTypes = {
    tags: PropTypes.array,
    onPress: PropTypes.func
  }

  cleanDataTags (tags) {
    return tags.map(tag => ({
      id: tag.id,
      tagName: tag.name,
      name: `${tag.name} (${tag.quizListsLength} đề)`,
      slug: tag.slug,
      is_featured: tag.isFeatured,
      groupBy: tag.groupBy

    }))
  }

  getTagsByCharacter (character) {
    return this.cleanDataTags(this.props.tags).filter(
      tag => tag.name[0].toLowerCase() === character.toLowerCase()
    )
  }

  renderGroup (character, index) {
    if (this.getTagsByCharacter(character).length === 0) return null
    return (
      <View key={index}>
        <HeadingLine title={character.toUpperCase()} />
        <Clearfix height={16} />
        <TagWrapper>
          <TagList onPress={this.props.onPress} tags={this.getTagsByCharacter(character)} size={SIZE.NORMAL} />
        </TagWrapper>
      </View>
    )
  }

  render () {
    return <TagListWrapper>
      {tagCharacters.map((character, index) => this.renderGroup(character, index))}
    </TagListWrapper>
  }
}
