import React, {PureComponent} from 'react'
import {Dimensions} from 'react-native'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import TextIcon from 'components/elements/text-icon'
import {SIZE} from 'constants/font'

const {width} = Dimensions.get('window')

const CategoryContainer = styled.TouchableOpacity`
  width: ${props => props.fullwidth ? width : width / 2};
  border-width: 0.5px;
  border-color: #f4f4f4;
  flex-direction: row;
  background-color: #fff;
  align-items: center;
  justify-content: center;
  height: ${SIZE.HEIGHT_DEFAULT}
`

const IconWrapper = styled.View`
  background-color: #fff;
  align-items: center;
  justify-content: center;
  flex: 1;
  `
@autobind
export default class CaterogyBoxItem extends PureComponent {
  static propTypes = {
    size: PropTypes.number,
    color: PropTypes.string,
    icon: PropTypes.string,
    name: PropTypes.string,
    simpleLineIcon: PropTypes.bool,
    onPressItem: PropTypes.func,
    fullwidth: PropTypes.bool
  }
  static defaultProps = {
    size: 25
  }

  onPress () {
    this.props.onPressItem(this.props.keyRequest, this.props.name, this.props.slug)
  }

  render () {
    return (
      <CategoryContainer fullwidth={this.props.fullwidth} color={this.props.color} activeOpacity={0.9}
        onPress={this.onPress}>
        <IconWrapper>
          <TextIcon fontWeight={600} color={this.props.color} icon={this.props.icon} size={SIZE.HEADING5}
            simpleLineIcon={this.props.simpleLineIcon}>{this.props.name}</TextIcon>
        </IconWrapper>
      </CategoryContainer>
    )
  }
}
