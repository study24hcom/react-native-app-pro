import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import categories from 'constants/categories'
import CaterogyBoxItem from './caterogy-box-item'
import {autobind} from 'core-decorators'
import Headingline from 'components/elements/heading-line'
import {SHAPE} from 'constants/color'
import {SIZE} from 'constants/font'
import {category} from 'constants/description'

const CategoryWapper = styled.View`
  flex-wrap: wrap;
  flex-direction: row;
`
const CategoryContainer = styled.View``

@autobind
export default class CaterogyBox extends PureComponent {
  static propTypes = {
    onPress: PropTypes.func
  }

  render () {
    return (
      <CategoryContainer>
        <Headingline description={category.hot} noLine size={SIZE.HEADING4} icon={'fire'}
          simpleLineIcon color={SHAPE.RED} title='Chuyên mục hot' />
        <CategoryWapper>
          {categories.map((category, index) => (
            <CaterogyBoxItem
              key={category.id}
              {...category}
              simpleLineIcon={category.simpleLineIcon}
              onPressItem={this.props.onPress}
              fullwidth={index % 2 === 0 && categories.length - 1 === index}
            />
          ))}
        </CategoryWapper>
      </CategoryContainer>
    )
  }
}
