import React, {Component} from 'react'
import {Dimensions} from 'react-native'
import Image from 'react-native-image-progress'
import CircleSnail from 'react-native-progress/CircleSnail'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'

const {width} = Dimensions.get('window')
const WIDTH = width
const HEIGHT = WIDTH * 400 / 746

const TouchableOpacity = styled.TouchableOpacity`
  width: ${width};
  background-color: #fff;
`

@autobind
export default class SliderEntry extends Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
    onPress: PropTypes.func
  }

  onPressItem () {
    this.props.onPress(this.props.data)
  }

  render () {
    const {data} = this.props
    return (
      <TouchableOpacity activeOpacity={1} onPress={this.onPressItem}>
        <Image
          indicator={CircleSnail}
          style={{width: WIDTH, height: HEIGHT}}
          source={{uri: data.image}} />
      </TouchableOpacity>
    )
  }
}
