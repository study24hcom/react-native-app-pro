import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import Carousel from 'react-native-snap-carousel'
import {Dimensions} from 'react-native'
import SliderEntry from '../SliderEntry/index'
import {autobind} from 'core-decorators'
import PlaceholderPlaylist from '../../../placeholder/index'

const sliderWidth = Dimensions.get('window').width
@autobind
export default class SliderCarousel extends PureComponent {
  static propTypes = {
    onPressItem: PropTypes.func,
    data: PropTypes.array,
    isFirst: PropTypes.bool
  }

  _renderItem ({item, index}) {
    return <SliderEntry key={index} data={item} onPress={this.props.onPressItem} />
  }

  render () {
    if (this.props.data.length === 0 && this.props.isFirst) return <PlaceholderPlaylist height={222} />
    return (
      <Carousel
        ref={(c) => {
          this._carousel = c
        }}
        data={this.props.data}
        renderItem={this._renderItem}
        sliderWidth={sliderWidth}
        itemWidth={sliderWidth}
        inactiveSlideScale={1}
        inactiveSlideOpacity={1}
        activeSlideAlignment={'start'}
        removeClippedSubviews={false}
        loop autoplay
        autoplayDelay={500}
        autoplayInterval={5000}
      />
    )
  }
}
