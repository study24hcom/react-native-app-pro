import React, { Component } from 'react'
import { Dimensions } from 'react-native'
import Image from 'react-native-image-progress'
import CircleSnail from 'react-native-progress/CircleSnail'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import { getBackgroundType } from 'utils/playlist'
import { autobind } from 'core-decorators'

const { width } = Dimensions.get('window')

const TouchableOpacity = styled.TouchableOpacity`
  width: ${width};
  background-color: #fff;
`
const BackgroundImageOver = styled.ImageBackground`
  background-color: ${props => props.color};
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 16px 32px;
  height: 180px;
`
const BoxImageContainer = styled.View``

const ImageText = styled.Text`
  font-size: 21px;
  color: ${props => (props.color ? props.color : '#ffffff')};
  font-weight: 600;
  text-align: center;
`

@autobind
export default class SliderEntry extends Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
    onPress: PropTypes.func
  }

  get image () {
    const { data: { info } } = this.props
    let backgroundType = getBackgroundType(info.name)
    return (
      <BoxImageContainer>
        {info.image ? (
          <Image
            indicator={CircleSnail}
            style={{ width: width, height: 180 }}
            source={{ uri: info.image }}
          />
        ) : null}
        {!info.image ? (
          <BackgroundImageOver
            resizeMode='cover'
            color={backgroundType.background}
            source={backgroundType.image}
          >
            <ImageText color={backgroundType.color}>{info.name}</ImageText>
          </BackgroundImageOver>
        ) : null}
      </BoxImageContainer>
    )
  }

  onPressItem () {
    this.props.onPress(this.props.data)
  }

  render () {
    return (
      <TouchableOpacity activeOpacity={1} onPress={this.onPressItem}>
        {this.image}
      </TouchableOpacity>
    )
  }
}
