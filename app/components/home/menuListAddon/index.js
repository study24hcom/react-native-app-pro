import React from 'react'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'
import {Dimensions, StyleSheet} from 'react-native'
import {SIZE} from 'constants/font'
import {SHAPE} from 'constants/color'
import Text from 'components/elements/text'
import Icon from 'components/elements/Icon'

const {width} = Dimensions.get('window')

const MenuListAddonContainer = styled.View`
  margin: 8px 16px;
  flex-direction: row;
  justify-content: space-between;
  `
const AddonItem = styled.TouchableOpacity`
  padding: 8px;
  width: ${(width - 32) / 3 - 8};
  background-color: #fff;
  align-items: center;
  border-radius: 4px;
  
  `
const IconWrapper = styled.View`
  background-color: ${props => props.color ? `${props.color}` : ''};
  border-radius: 17.5px;
  width: 35px;
  height: 35px;
  align-items: center;
  justify-content: center;
  margin-bottom: 4px;
  `

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#d4d4d4',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 3,
    shadowOpacity: 0.4
  }
})
export default class MenuListAddon extends React.Component {
  static propTypes = {
    onPressAddonActivity: PropTypes.func,
    onPressWorked: PropTypes.func,
    onPressAddonCreated: PropTypes.func
  }

  render () {
    return (
      <MenuListAddonContainer>
        <AddonItem activeOpacity={0.5} onPress={this.props.onPressAddonActivity} style={styles.shadow}>
          <IconWrapper color={SHAPE.PRIMARY}>
            <Icon simpleLineIcon name={'globe'} size={SIZE.SMALL} color={'#fff'} />
          </IconWrapper>
          <Text numberOfLines={1} fontWeight={500} fontSize={SIZE.SMALLER}>Hoạt động mới</Text>
        </AddonItem>
        <AddonItem activeOpacity={0.5} onPress={this.props.onPressWorked} style={styles.shadow}>
          <IconWrapper color={SHAPE.YELLOW}>
            <Icon simpleLineIcon name={'note'} size={SIZE.SMALL} color={'#fff'} />
          </IconWrapper>
          <Text numberOfLines={1} fontWeight={500} fontSize={SIZE.SMALLER}>Đề đã làm</Text>
        </AddonItem>
        <AddonItem activeOpacity={0.5} onPress={this.props.onPressAddonCreated} style={styles.shadow}>
          <IconWrapper color={SHAPE.GREEN}>
            <Icon simpleLineIcon name={'plus'} size={SIZE.SMALL} color={'#fff'} />
          </IconWrapper>
          <Text numberOfLines={1} fontWeight={500} fontSize={SIZE.SMALLER}>Đề đã tạo</Text>
        </AddonItem>
      </MenuListAddonContainer>
    )
  }
}
