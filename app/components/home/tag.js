import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import HeadingLine from 'components/elements/heading-line/index'
import styled from 'styled-components/native'
import TagList from 'components/tag/tag-list/index'
import {SIZE} from 'constants/font'
import {autobind} from 'core-decorators'
import PlaceholderView from 'components/placeholder'
import EmptySection from 'components/elements/empty-section'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {tag} from 'constants/description'

const View = styled.View``
const TagWrapper = styled.View`
  padding: 0 16px;
  `
@connectAwaitAutoDispatch(
  state => ({
    tags: state.follow.tags.data,
    pagination: state.follow.tags.pagination
  }),
  {}
)
@autobind
export default class TagListHome extends PureComponent {
  static propTypes = {
    tags: PropTypes.array,
    onPress: PropTypes.func,
    onPressViewAll: PropTypes.func,
    pagination: PropTypes.shape({
      totalItem: PropTypes.number,
      page: PropTypes.number
    }),
    isLoadingFirst: PropTypes.bool,
    isSuccess: PropTypes.bool
  }

  renderPlaceholder () {
    return <PlaceholderView height={130} />
  }

  render () {
    if (this.props.tags.length === 0 && this.props.isSuccess) {
      return (
        <EmptySection text={'Bạn chưa theo dõi tag nào. Hãy theo dõi ngay'} />
      )
    }
    return <View>
      <HeadingLine description={tag.follow} size={SIZE.HEADING3} icon={'tag'} simpleLineIcon
        totalItem={this.props.pagination.totalItem} onPress={this.props.onPressViewAll} isShow
        viewAllText={'Xem thêm'} noLine hideTotalItem title={'Tag bạn theo dõi'} />
      <TagWrapper>
        {this.props.isLoadingFirst && this.renderPlaceholder()}
        <TagList onPress={this.props.onPress} tags={this.props.tags.slice(0, 5)} size={SIZE.NORMAL} />
      </TagWrapper>
    </View>
  }
}
