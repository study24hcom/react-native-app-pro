import React, {PureComponent} from 'react'
import {FlatList, ActivityIndicator, View} from 'react-native'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import PlaceholderPlaylist from '../placeholder'
import {autobind} from 'core-decorators'
import Activity from './Activity'

const LoadingWrapper = styled.View`
  padding: 16px;
  justify-content: center;
  align-items: center;
`

@autobind
export default class Activties extends PureComponent {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape(Activity.propTypes)),
    handleLoadMore: PropTypes.func,
    renderItem: PropTypes.func,
    onRefresh: PropTypes.func,
    onPressItem: PropTypes.func,
    isLoadingMore: PropTypes.bool,
    _isRefreshing: PropTypes.bool,
    isLoadingFirst: PropTypes.bool
  };

  renderItem ({item}) {
    return <Activity onPress={this.props.onPressItem} item={item} {...item} />
  }

  renderLoading () {
    if (!this.props.isLoadingMore) return
    return (
      <LoadingWrapper>
        <ActivityIndicator />
      </LoadingWrapper>
    )
  }

  renderLoadFirst () {
    return <View style={{flex: 1}}>
      <PlaceholderPlaylist margin={10} height={50} />
      <PlaceholderPlaylist margin={10} height={50} />
      <PlaceholderPlaylist margin={10} height={50} />
      <PlaceholderPlaylist margin={10} height={50} />
      <PlaceholderPlaylist margin={10} height={50} />
      <PlaceholderPlaylist margin={10} height={50} />
      <PlaceholderPlaylist margin={10} height={50} />
      <PlaceholderPlaylist margin={10} height={50} />
    </View>
  }

  render () {
    if (this.props.data.length === 0 && this.props.isLoadingFirst) {
      return this.renderLoadFirst()
    }
    return (
      <FlatList
        data={this.props.data}
        keyExtractor={(item, index) => index}
        renderItem={this.renderItem}
        onEndReached={this.props.handleLoadMore}
        onEndReachedThreshold={0}
        onRefresh={this.props.onRefresh}
        refreshing={this.props._isRefreshing}
        ListFooterComponent={this.renderLoading()}
      />
    )
  }
}
