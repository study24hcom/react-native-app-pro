import styled from 'styled-components/native'
import {TEXT} from 'constants/color'
import Text from 'components/elements/text'

export const ActivityContainer = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
`

export const AvatarImage = styled.Image`
  width: 40px;
  height: 40px;
  border-radius: 20px;
`

export const Detail = styled(Text)`
  flex: 1;
  color: ${TEXT.GRAY};
  line-height: 22px;
  font-size: 16px
`

export const Content = styled(Text)``

export const Avatar = styled.View`
  margin-right: 8px;
  `

export const Link = styled(Text)`
  font-weight: 500;
  color: ${TEXT.PRIMARY};
`

export const Username = styled(Text)`
  font-weight: 500;
    color: ${TEXT.PRIMARY};
  `

export const BoxShadow = styled.View`
  border-bottom-width: 0.5px;
  border-color: rgb(235, 232, 232);
  padding: 16px;
  background-color: #fff
`

export default {
  ActivityContainer,
  Avatar,
  AvatarImage,
  Link,
  Detail,
  Username,
  Content,
  BoxShadow
}
