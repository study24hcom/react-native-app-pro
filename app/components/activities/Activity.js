import React from 'react'
import PropTypes from 'prop-types'
import {
  ActivityContainer,
  Avatar,
  AvatarImage,
  Link,
  Detail,
  Username,
  Content,
  BoxShadow
} from './style'
import Character from 'components/elements/user-avatar/character'
import {autobind} from 'core-decorators'

@autobind
export default class Activity extends React.Component {
  static propTypes = {
    item: PropTypes.shape({
      user: PropTypes.shape({
        username: PropTypes.string,
        avatar: PropTypes.string
      }),
      name: PropTypes.string,
      content: PropTypes.string,
      onPress: PropTypes.func
    }
    )
  }

  onPressItem () {
    this.props.onPress(this.props.item)
  }

  render () {
    const {user, content, name} = this.props
    return (
      <BoxShadow>
        <ActivityContainer activeOpacity={0.9} onPress={this.onPressItem}>
          <Avatar>
            {user.avatar
              ? <AvatarImage source={{uri: user.avatar}} />
              : <Character username={user.username} size={40} />}
          </Avatar>
          <Detail>
            <Username>{user.username + ' '}</Username>
            <Content>{content + ' '}</Content>
            <Link>{name}</Link>
          </Detail>
        </ActivityContainer>
      </BoxShadow>
    )
  }
}
