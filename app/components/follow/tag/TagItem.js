import React from 'react'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import styled from 'styled-components/native'
import {SHAPE} from 'constants/color'
import {SIZE} from 'constants/font'
import Text from 'components/elements/text/index'
import QuizApi from 'api/QuizApi'
import {setFollowTag, unFollowTag} from '@redux/actions/followAction'
import {connectAutoDispatch} from '@redux/connect'

const Button = styled.TouchableOpacity`
  border-radius: 4px;
  background-color: 'rgba(140, 197, 255, 0.08)';
  border: ${props => props.isFollow ? `1px solid  rgba(46, 204, 113, 0.4)` : `1px solid rgba(0, 126, 255, 0.4)`};
  flex-direction: row;
  justify-content: center;
  padding: 8px 16px;
  align-items: center;
  `
const Wrap = styled.View`
  ${props => (!props.isLast ? 'margin-right: 10px;' : '')}
  padding-bottom: 10px;
`
const Tag = styled(Text)`
  color: ${props => props.isFollow ? SHAPE.GREEN : SHAPE.PRIMARY}!important;
  font-size: ${SIZE.SMALL}
`

@connectAutoDispatch(
  state => ({
    followedTags: state.follow.followedTags
  }),
  {setFollowTag, unFollowTag}
)
@autobind
export default class TagItem extends React.Component {
  static propTypes = {
    index: PropTypes.number,
    tag: PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      tagName: PropTypes.string,
      isFeatured: PropTypes.bool
    }),
    length: PropTypes.number
  }

  getFollowExists () {
    const follow = this.props.followedTags.find(
      follow => this.props.tag.tagName === follow.name
    )
    return !follow ? false : follow
  }

  isFollow () {
    const follow = this.getFollowExists()
    if (!follow) return false
    return follow.followed
  }

  onPressFollow () {
    if (!this.isFollow()) {
      QuizApi.followTag(this.props.tag.tagName)
      this.props.setFollowTag(this.props.tag.tagName)
    } else {
      QuizApi.unFollowTag(this.props.tag.tagName)
      this.props.unFollowTag(this.props.tag.tagName)
    }
  }

  render () {
    return (
      <Wrap isLast={this.props.index === this.props.length - 1} key={this.props.tag.id}>
        <Button onPress={this.onPressFollow} isFollow={this.isFollow()} activeOpacity={0.9}>
          <Tag isFollow={this.isFollow()}>{this.props.tag.tagName}</Tag>
        </Button>
      </Wrap>
    )
  }
}
