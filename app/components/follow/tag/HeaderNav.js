import React from 'react'
import {autobind} from 'core-decorators'
import PropTypes from 'prop-types'
import {StyleSheet, TouchableOpacity} from 'react-native'
import NavigationBar from 'react-native-navbar'
import Text from 'components/elements/text/index'
import {SHAPE} from 'constants/color'

const styles = StyleSheet.create({
  buttonNav: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

@autobind
export default class HeaderNav extends React.PureComponent {
  static propTypes = {
    onPressSubmit: PropTypes.func
  }

  renderRight () {
    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={this.props.onPressSubmit}
        style={[styles.buttonNav, {paddingRight: 8, paddingLeft: 8}]}
        underlayColor='transparent'>
        <Text fontWeight={500} color={'#fff'}>Xong</Text>
      </TouchableOpacity>
    )
  }

  render () {
    return (
      <NavigationBar
        statusBar={{style: 'light-content'}}
        containerStyle={{backgroundColor: SHAPE.GREEN}}
        style={{backgroundColor: SHAPE.GREEN}}
        rightButton={this.renderRight()}
        title={{title: 'Lựa chọn tag quan tâm', style: {color: '#fff', textAlign: 'center'}}}
      />
    )
  }
}
