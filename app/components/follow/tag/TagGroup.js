import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import HeadingLine from 'components/elements/heading-line'
import styled from 'styled-components/native'
import TagItem from './TagItem'
import {autobind} from 'core-decorators'
import _ from 'lodash'

function groupTags (tags) {
  const groups = _.uniq(tags.map(tag => tag.groupBy))
  const groupWithTags = groups.map(group => {
    return {
      name: group,
      tags: tags.filter(tag => tag.groupBy === group)
    }
  })
  return groupWithTags
}

const View = styled.View``
const TagListWrapper = styled.ScrollView`
  flex: 1;
  `
const TagWrapper = styled.View`
  flex: 1;
  flex-wrap: wrap;
  flex-direction: row;
  padding: 0 10px;
  `
@autobind
export default class TagGroup extends PureComponent {
  static propTypes = {
    tags: PropTypes.array,
    onPress: PropTypes.func
  }

  cleanDataTags (tags) {
    return tags.map(tag => ({
      key: tag.id,
      id: tag.id,
      tagName: tag.name,
      name: `${tag.name} (${tag.quizListsLength} đề)`,
      slug: tag.slug,
      isFeatured: tag.isFeatured,
      groupBy: tag.groupBy

    }))
  }

  renderGroup (group, index) {
    return (
      <View key={index}>
        <HeadingLine noLine key={index} title={group.name ? group.name : 'Chưa định danh'} />
        <TagWrapper>
          {this.cleanDataTags(group.tags).map((tag, index) =>
            <TagItem key={index} tag={tag} index={index} />)}
        </TagWrapper>
      </View>
    )
  }

  render () {
    const groups = groupTags(this.props.tags)
    return <TagListWrapper>
      {groups.map((group, index) => this.renderGroup(group, index))}
    </TagListWrapper>
  }
}
