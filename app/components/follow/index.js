import React from 'react'
import {ScrollView} from 'react-native'
import PropTypes from 'prop-types'
import TagGroup from 'components/follow/tag/TagGroup'
import {SHAPE} from 'constants/color'
import {SIZE} from 'constants/font'
import Text from 'components/elements/text/index'

export default class Follow extends React.Component {
  static propTypes = {
    data: PropTypes.array,
    description: PropTypes.string
  }

  render () {
    return <ScrollView style={{flex: 1}}>
      {this.props.description && <Text style={{textAlign: 'center', margin: 16}}
        color={SHAPE.GRAYTEXT} fontSize={SIZE.NORMAL}
        fontWeight={200}>{this.props.description}</Text>}
      <TagGroup tags={this.props.data} />
    </ScrollView>
  }
}
