import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import {ShareDialog} from 'react-native-fbsdk'
import QuizlistList from 'components/quizlist/quizlist-list'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {setInfo} from '@redux/actions/quizAction'
import {getQuizListsCategory} from '../../@redux/actions/caterogyAction'
import {awaitCheckPending} from '../../utils/await'
import {checkLoadMore} from '../../utils/pagination'
import payload from 'constants/payload'
import ButtonRight from '../../components/elements/botton-header-right/index'

@connectAwaitAutoDispatch((state, ownProps) => ({
  categoryKey: ownProps.navigation.state.params.category,
  data: state.category[ownProps.navigation.state.params.category].data,
  pagination: state.category[ownProps.navigation.state.params.category].pagination,
  isLoadMore: state.category[ownProps.navigation.state.params.category].loadMore
}),
  {setInfo, getQuizListsCategory})
@autobind
export default class CategoryScreen extends PureComponent {
  static propTypes = {
    propsForFlatlist: PropTypes.object,
    isLoadMore: PropTypes.bool
  }

  static navigationOptions = ({navigation: {state}}) => ({
    title: state.params.name,
    headerRight: <ButtonRight onPress={state.params.onPressShare} simpleLineIcon icon='share-alt' />
  })

  constructor (props) {
    super(props)
    const shareLinkContent = {
      contentType: 'link',
      contentUrl: `https://tungtung.vn/chuyen-muc/${this.props.navigation.state.params.slug}`
    }
    this.state = {
      shareLinkContent: shareLinkContent
    }
  }

  onPressShare () {
    var tmp = this
    ShareDialog.canShow(this.state.shareLinkContent).then(
      function (canShow) {
        if (canShow) {
          return ShareDialog.show(tmp.state.shareLinkContent)
        }
      }
    )
  }

  componentDidMount () {
    this.props.navigation.setParams({onPressShare: this.onPressShare})
    if (this.props.data.length === 0) {
      this.props.getQuizListsCategory({
        categoryKey: this.props.categoryKey,
        page: 1,
        itemPerPage: 10,
        loadMore: false
      })
    }
  }

  isPending () {
    return awaitCheckPending(this.props, `${payload.GET_QUIZLISTS_CATEGORY}_${this.props.categoryKey}`)
  }

  onScrollToBottom () {
    if (
      !this.isPending() &&
      checkLoadMore(this.props.pagination)
    ) {
      this.props.getQuizListsCategory({
        categoryKey: this.props.categoryKey,
        page: this.props.pagination.page + 1,
        itemPerPage: this.props.pagination.itemPerPage,
        loadMore: true
      })
    }
  }

  onRefresh () {
    if (!this.isPending()) {
      this.props.getQuizListsCategory({categoryKey: this.props.categoryKey, page: 1, itemPerPage: 10, loadMore: false})
    }
  }

  isLoadingFirst () {
    return this.isPending() && !this.props.isLoadMore && this.props.data.length === 0
  }

  isRefreshing () {
    return this.isPending() && !this.props.isLoadMore && this.props.data.length !== 0
  }

  isLoadingMore () {
    return this.isPending() && this.props.isLoadMore
  }

  handleNavigateToQuizlistDetail (quizListItem) {
    if (!this.delay) {
      this.props.setInfo(quizListItem)
      this.props.navigation.navigate('QuizListDetailScreen', {
        title: quizListItem.name
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  onPressUser (user) {
    if (!this.delay) {
      this.props.navigation.navigate('UserScreen', {username: user.username})
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  render () {
    return (
      <QuizlistList isPressUser onPressUser={this.onPressUser}
        onPressItem={this.handleNavigateToQuizlistDetail}
        data={this.props.data}
        pagination={this.props.pagination}
        onScrollToBottom={this.onScrollToBottom}
        isLoadingFirst={this.isLoadingFirst()}
        isResfreshing={this.isRefreshing()}
        isLoadingMore={this.isLoadingMore()}
        onRefresh={this.onRefresh}
      />
    )
  }
}
