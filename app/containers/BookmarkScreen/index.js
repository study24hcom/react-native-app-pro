import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import {getQuizListsByBookmark} from '@redux/actions/quizListsAction'
import {QUIZ_LISTS_BY_BOOKMARK} from 'constants/quizLists'
import connectQuizLists from 'hoc/quizlists-wrapper'
import QuizlistList from 'components/quizlist/quizlist-list'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {setInfo} from '@redux/actions/quizAction'
import EmptySection from 'components/elements/empty-section'
import HeadingLine from 'components/elements/heading-line'
import {SIZE} from 'constants/font'
import {SHAPE} from 'constants/color'
import {quizList} from 'constants/description'

@connectAwaitAutoDispatch(state => ({
  bookmark: state.bookmark.bookmarkQuizlists
}), {setInfo, getQuizListsByBookmark})
@connectQuizLists({
  action: getQuizListsByBookmark,
  key: QUIZ_LISTS_BY_BOOKMARK.key,
  payload: QUIZ_LISTS_BY_BOOKMARK.payload
})
@autobind
export default class BookmarkScreen extends Component {
  static propTypes = {
    propsForFlatlist: PropTypes.object
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.bookmark !== this.props.bookmark) {
      setTimeout(() => {
        this.props.getQuizListsByBookmark({itemPerPage: 10, page: 1})
      }, 500)
    }
  }

  handleNavigateToQuizlistDetail (quizListItem) {
    if (!this.delay) {
      this.props.setInfo(quizListItem)
      this.props.navigation.navigate('QuizListDetailScreen', {
        title: quizListItem.name
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  onPressUser (user) {
    if (!this.delay) {
      this.props.navigation.navigate('UserScreen', {username: user.username})
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  renderHeader () {
    return <HeadingLine size={SIZE.HEADING3} color={SHAPE.BLACK} noLine title={'Đề thi đã lưu'}
      description={quizList.save} />
  }

  render () {
    if (this.props.isLoadSuccess && this.props.data.length === 0) {
      return (
        <EmptySection
          text=' Bạn chưa lưu đề thi nào. Hãy lưu trữ thật nhiều đề để tìm kiếm thật dể dàng bạn nhé 😉'
          icon='bookmark-o'
        />
      )
    }
    return (
      <QuizlistList isPressUser
        header={this.renderHeader()}
        onPressUser={this.onPressUser}
        onPressItem={this.handleNavigateToQuizlistDetail}
        {...this.props.propsForFlatlist} />
    )
  }
}
