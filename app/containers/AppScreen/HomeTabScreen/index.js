import React, {Component} from 'react'
import {withNavigation} from 'react-navigation'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import {awaitCheckPending, awaitCheckSuccess} from 'utils/await'
import {getQuizListsByFollowed} from '@redux/actions/quizListsAction'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {setInfo} from '@redux/actions/quizAction'
import QuizListFollowed from 'components/home/quizlist/quizListByFollowed'
import MenuListAddon from 'components/home/menuListAddon'
import {getPartnerList} from '@redux/actions/userAction'
import {getTagsFollowed} from '@redux/actions/followAction'
import TagListHome from 'components/home/tag'
import {getFeaturedTags} from '@redux/actions/tagAction'
import HeadingLine from 'components/elements/heading-line/index'
import {quizList} from 'constants/description'
import {SIZE} from 'constants/font'

const HeaderFlatlist = styled.View``
@connectAwaitAutoDispatch(
  state => ({
    username: state.auth.user.username,
    partner: state.user.partnerList.data,
    tags: state.follow.tags.data,
    tagsFeature: state.tag.featured.data

  }),
  {setInfo, getQuizListsByFollowed, getPartnerList, getTagsFollowed, getFeaturedTags}
)
@withNavigation
@autobind
export default class HomeTabScreen extends Component {
  state = {
    isFirst: true
  }

  componentDidMount () {
    if (this.props.partner.length === 0) {
      this.props.getPartnerList({page: 1, itemPerPage: 10})
    }
    if (this.props.tags.length === 0) {
      this.props.getTagsFollowed({page: 1, itemPerPage: 10})
    }
    if (this.props.tagsFeature.length === 0) {
      this.props.getFeaturedTags({page: 1, itemPerPage: 10})
    }
  }

  _isRefreshing () {
    return (awaitCheckPending(this.props, 'getQuizListsByFollowed') && !this.state.isFirst)
  }

  _onRefresh () {
    this.setState({isFirst: false})
    this.props.getQuizListsByFollowed({page: 1, itemPerPage: 10})
  }

  _isLoadingFirst () {
    return (awaitCheckPending(this.props, 'getQuizListsByFollowed') && this.state.isFirst)
  }

  autoDelay (callback) {
    if (!this.delay) {
      callback()
      this.delay = true
    }
    setTimeout(() => {
      this.delay = false
    }, 600)
  }

  onPressQuizlistItem (quizListItem) {
    this.autoDelay(() => {
      this.props.setInfo(quizListItem)
      this.props.navigation.navigate('QuizListDetailScreen', {title: quizListItem.name})
    })
  }

  onPressUser (user) {
    this.autoDelay(() => {
      this.props.navigation.navigate('UserScreen', {username: user.username})
    })
  }

  onPressAddonCreated () {
    this.autoDelay(() => {
      this.props.navigation.navigate('QuizListsByMeViewAll', {
        username: this.props.username,
        title: 'Đề thi đã tạo'
      })
    })
  }

  onPressWorked () {
    this.autoDelay(() => {
      this.props.navigation.navigate('QuizlistsByHistoryViewAll', {
        username: this.props.username,
        title: 'Đề thi đã làm'
      })
    })
  }

  onPressTag (tagItem) {
    this.autoDelay(() => {
      this.props.navigation.navigate('TagsScreen', {title: tagItem.name})
    })
  }

  onPressTagViewAll () {
    this.autoDelay(() => {
      this.props.navigation.navigate('TagNavigator')
    })
  }

  onPressAddonActivity () {
    this.autoDelay(() => {
      this.props.navigation.navigate('SocialScreen')
    })
  }

  renderTagFollow () {
    return <TagListHome isSuccess={awaitCheckSuccess(this.props, 'getTagsFollowed')}
      isLoadingFirst={awaitCheckPending(this.props, 'getTagsFollowed') && this.state.isFirst}
      onPress={this.onPressTag} onPressViewAll={this.onPressTagViewAll} />
  }

  renderMenuList () {
    return <MenuListAddon onPressAddonCreated={this.onPressAddonCreated}
      onPressAddonActivity={this.onPressAddonActivity}
      onPressWorked={this.onPressWorked} />
  }

  renderHeaderFlatlist () {
    return <HeaderFlatlist>
      {this.renderMenuList()}
      {this.renderTagFollow()}
      <HeadingLine description={quizList.follow} icon={'folder-alt'} simpleLineIcon
        size={SIZE.HEADING3} noLine title='Gợi ý cho bạn' />
    </HeaderFlatlist>
  }

  render () {
    return <QuizListFollowed
      _isLoadingFirst={this._isLoadingFirst()}
      onPressUser={this.onPressUser}
      _isRefreshing={this._isRefreshing()}
      _onRefresh={this._onRefresh.bind(this)}
      headerFlatlist={this.renderHeaderFlatlist()}
      onPressItem={this.onPressQuizlistItem} />
  }
}
