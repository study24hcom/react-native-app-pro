import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {Linking, Alert} from 'react-native'
import {withNavigation} from 'react-navigation'
import {awaitCheckPending} from 'utils/await'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import HotQuizlist from 'components/home/quizlist/hotQuizlist/index'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {getQuizListsTopDay, getGallerySlider} from '@redux/actions/homeAction'
import {setInfo} from '@redux/actions/quizAction'
import {getQuizListsByNews} from '@redux/actions/quizListsAction'
import {setInfoPlaylist} from "@redux/actions/playlistAction";
import QuizListNews from 'components/home/quizlist/quizListByNews'
import SliderCarousel from 'components/home/slider/sliderCarousel'
import HeadingLine from 'components/elements/heading-line'
import {quizList} from 'constants/description'

const HeaderFlatlist = styled.View``
@connectAwaitAutoDispatch(
  state => ({
    quizListsTopDay: state.home.quizListsTopDay,
    newQuizLists: state.quizLists.byNews.data,
    gallerySlider: state.home.gallerySlider
  }),
  {
    getQuizListsTopDay,
    getQuizListsByNews,
    getGallerySlider,
    setInfo,
    setInfoPlaylist
  }
)
@withNavigation
@autobind
export default class ExploreTabScreen extends PureComponent {
  static propTypes = {
    quizListsTopDay: PropTypes.array
  }

  componentDidMount () {
    const {quizListsTopDay} = this.props
    if (quizListsTopDay.length === 0) {
      this.props.getQuizListsTopDay()
    }
    if (this.props.gallerySlider.length === 0) {
      this.props.getGallerySlider()
    }
  }

  state = {
    isFirst: true
  }

  _isRefreshing () {
    return ((awaitCheckPending(this.props, 'getQuizListsTopDay') ||
      awaitCheckPending(this.props, 'getGallerySlider') ||
      awaitCheckPending(this.props, 'getQuizListsByNews')) && !this.state.isFirst)
  }

  _isLoadingFirst () {
    return ((awaitCheckPending(this.props, 'getQuizListsTopDay') ||
      awaitCheckPending(this.props, 'getGallerySlider') ||
      awaitCheckPending(this.props, 'getQuizListsByNews')) && this.state.isFirst)
  }

  _onRefresh () {
    this.setState({isFirst: false})
    this.props.getQuizListsTopDay()
    this.props.getQuizListsByNews()
    this.props.getGallerySlider()
  }

  onPressQuizList (quizListItem) {
    if (!this.delay) {
      this.props.setInfo(quizListItem)
      this.props.navigation.navigate('QuizListDetailScreen', {
        title: quizListItem.name
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  onPressUser (user) {
    if (!this.delay) {
      this.props.navigation.navigate('UserScreen', {
        username: user.username
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  onPressSlider (item) {
    const {slug, type, link, messenger} = item.url
    const {name} = item
    if (!this.delay) {
      if (type === 'tag') {
        this.props.navigation.navigate('TagsScreen', {title: slug})
      }
      if (type === 'playlist') {
        this.props.setInfoPlaylist({name})
        this.props.navigation.navigate('PlaylistDetailScreen', {
          title: name, slug: slug
        })
      }
      if (type === 'quizlist') {
        this.props.setInfo(name)
        this.props.navigation.navigate('QuizListDetailScreen', {
          title: name, slug: slug
        })
      }
      if (type === 'link') {
        Linking.openURL(link)
      }
      if (type === 'messenger') {
        Alert.alert(messenger)
      }
    }
  }

  renderSlider () {
    return <SliderCarousel onPressItem={this.onPressSlider} data={this.props.gallerySlider}
      isFirst={awaitCheckPending(this.props, 'getGallerySlider') && this.state.isFirst} />
  }

  renderHeader () {
    return <HeaderFlatlist>
      {this.renderSlider()}
      <HotQuizlist
        onPressUser={this.onPressUser}
        quizListsTopDay={this.props.quizListsTopDay}
        onPressItem={this.onPressQuizList} />
      <HeadingLine description={quizList.new} noLine title='Mới phát hành' />
    </HeaderFlatlist>
  }

  render () {
    return <QuizListNews onPressUser={this.onPressUser}
      _isLoadingFirst={this._isLoadingFirst()}
      _isRefreshing={this._isRefreshing()}
      _onRefresh={this._onRefresh}
      headerFlatlist={this.renderHeader()}
      onPressItem={this.onPressQuizList} />
  }
}
