import React, {Component} from 'react'
import ScrollableTabView from 'react-native-scrollable-tab-view'
import TungtungTabBar from 'components/TungtungTabBar'
import HomeTabScreen from './HomeTabScreen'
import ExploreTabScreen from './ExploreTabScreen'

export default class AppScreenIOS extends Component {
  render () {
    return (
      <ScrollableTabView renderTabBar={() => <TungtungTabBar />}>
        <HomeTabScreen tabLabel='Dashboard' />
        <ExploreTabScreen tabLabel='Khám phá' />
      </ScrollableTabView>
    )
  }
}
