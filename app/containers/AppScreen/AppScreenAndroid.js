import React, {Component} from 'react'
import ScrollableTabView from 'react-native-scrollable-tab-view'
import styled from 'styled-components/native'
import {ActivityIndicator} from 'react-native'
import TungtungTabBar from 'components/TungtungTabBar'
import HomeTabScreen from './HomeTabScreen'
import ExploreTabScreen from './ExploreTabScreen'

const AwaitScreen = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center
`
const ScrollableTabViewContainer = styled.View`
  flex: 1;
  `
export default class AppScreenAndroid extends Component {
  state = {
    delayShowScrollTableView: false
  }

  componentDidMount () {
    this.timer = setTimeout(() => {
      this.setState({
        delayShowScrollTableView: true
      })
    }, 100)
  }

  render () {
    return (
      <ScrollableTabViewContainer>
        {this.state.delayShowScrollTableView
          ? <ScrollableTabView renderTabBar={() => <TungtungTabBar />}>
            <HomeTabScreen tabLabel='Dashboard' />
            <ExploreTabScreen tabLabel='Khám phá' />
          </ScrollableTabView>
          : <AwaitScreen>
            <ActivityIndicator animating color='#ccc' />
          </AwaitScreen>}
      </ScrollableTabViewContainer>
    )
  }
}
