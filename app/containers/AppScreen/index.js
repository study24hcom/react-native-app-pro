import React from 'react'
import {Linking, Platform} from 'react-native'
import AppScreenAndroid from './AppScreenAndroid'
import AppScreenIOS from './AppScreenIOS'
import DeepLinking from 'react-native-deep-linking'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {getQuizList} from '@redux/actions/quizAction'
import {getPlaylist} from '@redux/actions/playlistAction'

@connectAwaitAutoDispatch(
  state => ({
    quizListInfo: state.quiz.info,
    playlistInfo: state.playlist.current.info
  }),
  {getQuizList, getPlaylist}
)
export default class AppScreen extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isNavigateToPlaylist: true,
      isNavigateToQuizList: true
    }
  }

  componentDidMount () {
    DeepLinking.addScheme('tungtung://')
    Linking.addEventListener('url', this.handleUrl)
    DeepLinking.addRoute('/de-thi/:slug', (response) => {
      this.props.getQuizList(response.slug)
      this.setState({isNavigateToQuizList: false})
    })

    DeepLinking.addRoute('/suu-tap/:slug', (response) => {
      this.props.getPlaylist(response.slug)
      this.setState({isNavigateToPlaylist: false})
    })

    Linking.getInitialURL().then((url) => {
      if (url) {
        Linking.openURL(url)
      }
    }).catch(err => console.error('An error occurred', err))
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.awaitStatuses.getQuizList !== 'pending' && nextProps.awaitStatuses.getQuizList === 'success' && !this.state.isNavigateToQuizList) {
      this.setState({isNavigateToQuizList: true})
      if (!this.delay) {
        this.props.navigation.navigate('QuizListDetailScreen', {title: nextProps.quizListInfo.name})
        this.delay = true
        setTimeout(() => {
          this.delay = false
        }, 2000)
      }
    }

    if (nextProps.awaitStatuses.getPlaylist !== 'pending' && nextProps.awaitStatuses.getPlaylist === 'success' && !this.state.isNavigateToPlaylist) {
      this.setState({isNavigateToPlaylist: true})
      if (!this.delay) {
        this.props.navigation.navigate('PlaylistDetailScreen', {title: nextProps.playlistInfo.name})
        this.delay = true
        setTimeout(() => {
          this.delay = false
        }, 2000)
      }
    }
  }

  componentWillUnmount () {
    Linking.removeEventListener('url', this.handleUrl)
  }

  handleUrl = ({url}) => {
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        DeepLinking.evaluateUrl(url)
      }
    })
  }

  render () {
    if (Platform.OS === 'android') return <AppScreenAndroid />
    return <AppScreenIOS />
  }
}
