import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import connectNotification from 'hoc/connect-notification'
import {connectAutoDispatch} from '@redux/connect'
import {setInfo} from '@redux/actions/quizAction'
import NotificationList from 'components/notification/notification-list'

@connectAutoDispatch(null, {setInfo})
@connectNotification
@autobind
export default class NotificationScreen extends Component {
  static navigationOptions = {
    title: 'Thông Báo'
  }
  static propTypes = {
    newNotifications: PropTypes.number,
    notifications: PropTypes.array,
    isLoading: PropTypes.bool,
    isLoadingFirst: PropTypes.bool,
    onRefresh: PropTypes.func,
    onLoadMore: PropTypes.func,
    onResetNewNotifications: PropTypes.func,
    refreshNotification: PropTypes.func,
    updateNewNotifications: PropTypes.func
  }

// Chưa xong
  onPressItem (quizListItem) {
    if (!this.delay) {
      this.props.navigation.navigate('QuizListDetailScreen', {
        title: quizListItem.quizList.name,
        slug: quizListItem.quizList.slug
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  componentWillUnmount () {
    this.props.onResetNewNotifications()
  }

  render () {
    return (
      <NotificationList
        onPressItem={this.onPressItem}
        data={this.props.notifications}
        onLoadMore={this.props.onLoadMore}
        isLoading={this.props.isLoading}
        isLoadingFirst={this.props.isLoadingFirst}
        onRefresh={this.props.onRefresh} />
    )
  }
}
