import React, {Component} from 'react'
import {StatusBar, Platform} from 'react-native'
import {connect} from 'react-redux'
import StartupActions from '../@redux/reducers/startup'
import ReduxPersist from '../config/ReduxPersist'
import styled from 'styled-components/native'
import ProtectScreen from './ProtectScreen'
import SplashScreen from 'react-native-splash-screen'

const AppView = styled.View`
  flex: 1;
`

class RootContainer extends Component {
  componentDidMount () {
    if (!ReduxPersist.active) {
      this.props.startup()
    }
    if (Platform.OS === 'ios') {
      SplashScreen.hide()
    }
  }

  render () {
    return (
      <AppView>
        <StatusBar barStyle='light-content' />
        <ProtectScreen />
      </AppView>
    )
  }
}

// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = dispatch => ({
  startup: () => dispatch(StartupActions.startup())
})
export default connect(null, mapDispatchToProps)(RootContainer)
