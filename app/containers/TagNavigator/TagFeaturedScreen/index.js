import React from 'react'
import {View} from 'react-native'
import {autobind} from 'core-decorators'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {getFeaturedTags} from '@redux/actions/tagAction'
import PlaceholderView from 'components/placeholder'
import {awaitCheckPending, awaitCheckSuccess} from 'utils/await'
import TagFeatureGroup from 'components/tag/tag-group'

@connectAwaitAutoDispatch(state => ({
  data: state.tag.featured.data,
  pagination: state.tag.featured.pagination
}), {getFeaturedTags}
)
@autobind
export default class TagFeaturedScreen extends React.Component {
  state = {
    isLoading: false
  }

  componentDidMount () {
    if (this.props.data.length === 0) {
      this.props.getFeaturedTags()
    }
  }

  onPressItemTags (tagItem) {
    if (!this.delay) {
      this.props.navigation.navigate('TagsScreen', {title: tagItem.tagName})
    }
    this.delay = true
    setTimeout(() => {
      this.delay = false
    }, 1200)
  }

  isLoading () {
    return awaitCheckPending(this.props, 'getFeaturedTags')
  }

  isSuccess () {
    return awaitCheckSuccess(this.props, 'getFeaturedTags')
  }

  renderPlaceHolder () {
    return <View>
      <PlaceholderView margin={8} />
      <PlaceholderView margin={8} />
      <PlaceholderView margin={8} />
      <PlaceholderView margin={8} />
    </View>
  }

  renderTagsFeature () {
    return <TagFeatureGroup onPress={this.onPressItemTags} tags={this.props.data} />
  }

  render () {
    return <View style={{flex: 1}}>
      {this.isLoading() && this.renderPlaceHolder()}
      {this.isSuccess() && this.renderTagsFeature()}
    </View>
  }
}
