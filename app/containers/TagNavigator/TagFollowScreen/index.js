import React from 'react'
import {View} from 'react-native'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {autobind} from 'core-decorators'
import PlaceholderView from 'components/placeholder'
import {awaitCheckPending, awaitCheckSuccess} from 'utils/await'
import {getTagsFollowed} from '@redux/actions/followAction'
import TagGroup from '../../../components/tag/tag-group/index'
import EmptySection from '../../../components/elements/empty-section'

@connectAwaitAutoDispatch(state => ({
  data: state.follow.tags.data,
  pagination: state.follow.tags.pagination
}), {getTagsFollowed}
)
@autobind
export default class TagFollowScreen extends React.Component {
  state = {
    isLoading: false
  }

  componentDidMount () {
    this.props.getTagsFollowed({page: 1, itemPerPage: 100})
  }

  onPressItemTags (tagItem) {
    if (!this.delay) {
      this.props.navigation.navigate('TagsScreen', {title: tagItem.tagName})
    }
    this.delay = true
    setTimeout(() => {
      this.delay = false
    }, 1200)
  }

  isLoading () {
    return awaitCheckPending(this.props, 'getTagsFollowed')
  }

  isSuccess () {
    return awaitCheckSuccess(this.props, 'getTagsFollowed')
  }

  renderPlaceHolder () {
    return <View>
      <PlaceholderView margin={8} />
      <PlaceholderView margin={8} />
      <PlaceholderView margin={8} />
      <PlaceholderView margin={8} />
    </View>
  }

  renderTagsFeature () {
    return <TagGroup onPress={this.onPressItemTags} tags={this.props.data} />
  }

  render () {
    if (this.props.data.length === 0 && this.isSuccess()) {
      return <EmptySection text={`Bạn chưa theo dõi tag nào`} />
    }
    return <View style={{flex: 1}}>
      {this.isLoading() && this.renderPlaceHolder()}
      {this.isSuccess() && this.renderTagsFeature()}
    </View>
  }
}
