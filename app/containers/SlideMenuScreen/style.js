import {SHAPE} from 'constants/color'
import {Platform} from 'react-native'
import styled from 'styled-components/native'

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : 0

const StatusBar = styled.View`
  height: ${STATUSBAR_HEIGHT}px;
  background-color: ${SHAPE.PRIMARY}
`
const SlideContainer = styled.View`
  background-color: #f1f1f1;
  flex: 1;
  `
const ButtonWrapper = styled.View`
  position: relative;  
  bottom: 0;
  left: 0;
  right: 0;
`
const List = styled.View`
  border-top-width: 0.5px;
  border-bottom-width: 0.5px;
  border-color: ${SHAPE.GRAYBOLD};
  background-color: #fff;
  `
const SlideScrollView = styled.ScrollView`
  flex: 1
`

export {
  List,
  STATUSBAR_HEIGHT,
  SlideContainer,
  StatusBar,
  SlideScrollView,
  ButtonWrapper
}
