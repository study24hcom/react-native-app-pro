import React, {Component} from 'react'
import {Platform, Linking, PermissionsAndroid} from 'react-native'
import * as StoreReview from 'react-native-store-review';
import {autobind} from 'core-decorators'
import RNImmediatePhoneCall from 'react-native-immediate-phone-call'
import email from 'react-native-email'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {SHAPE} from 'constants/color'
import Clearfix from 'components/elements/clearfix'
import {
  SlideContainer,
  StatusBar,
  SlideScrollView
} from './style'
import {userLogout} from '@redux/actions/authAction'
import AvatarMenu from 'components/menu/avatar'
import ListMenu from 'components/menu/ListMenu'
import ButtonLogout from 'components/elements/button/ButtonLogout'

function onPressRate() {
  const WriteReview = 'https://itunes.apple.com/app/id1312065386?action=write-review'
  const Google = "market://details?id=com.tungtung"
  if (Platform.OS == 'ios') {
    if (StoreReview.isAvailable) {
      StoreReview.requestReview();
    } else {
      Linking.canOpenURL(WriteReview).then(supported => {
        if (supported) {
          Linking.openURL(WriteReview)
        }
      })
    }
  }
  else if (Platform.OS == 'android') {
    Linking.canOpenURL(Google).then(supported => {
      if (supported) {
        Linking.openURL(Google)
      }
    })
  }
}

function onPressWelcome() {
  const link = `https://goo.gl/LpYGFR`
  Linking.canOpenURL(link).then(supported => {
    supported && Linking.openURL(link)
  }, (err) => console.log(err))
}

function onPressDonate() {
  const link = `https://goo.gl/Pwnic5`
  Linking.canOpenURL(link).then(supported => {
    supported && Linking.openURL(link)
  }, (err) => console.log(err))
}

async function onPressCall() {
  if (Platform.OS === 'ios') {
    RNImmediatePhoneCall.immediatePhoneCall('01652237832')
  } else {
    if (await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CALL_PHONE)) {
      RNImmediatePhoneCall.immediatePhoneCall('01652237832')
    } else {
      requestCameraPermission()
    }
  }
}

async function requestCameraPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CALL_PHONE,
      {
        'title': 'Tung Tung App phone call Permission',
        'message': 'Tung Tung App needs to open phone call application'
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('You can use the camera')
      RNImmediatePhoneCall.immediatePhoneCall('01652237832')
    } else {
      console.log('Camera permission denied')
    }
  } catch (err) {
    console.warn(err)
  }
}

function onPressFanpage() {
  const link = `https://urlgeni.us/facebook/YRHl`
  Linking.canOpenURL(link).then(supported => {
    supported && Linking.openURL(link)
  }, (err) => console.log(err))
}

async function onPressEmail() {
  const to = ['admin@tungtung.vn']
  email(to, {
    cc: ['nguyentrong@tungtung.vn', 'thanhtung@tungtung.vn', 'toantien@tungtung.vn']
  }).catch(console.error)
}

async function onPressFeedBack() {
  const to = ['admin@tungtung.vn']
  email(to, {
    cc: ['nguyentrong@tungtung.vn', 'thanhtung@tungtung.vn'],
    subject: 'Góp ý ứng dụng'
  }).catch(console.error)
}

const lists = [
  {
    icon: 'star',
    color: SHAPE.PRIMARYBOLD,
    title: 'Đánh giá ứng dụng',
    onPress: onPressRate
  },
  {
    icon: 'info',
    color: SHAPE.PURPLE,
    title: 'Giới thiệu',
    onPress: onPressWelcome
  },
  {
    icon: 'paypal',
    color: SHAPE.ORANGE,
    title: 'Tài trợ',
    onPress: onPressDonate
  }
]

const contacts = [
  {
    icon: 'phone',
    color: SHAPE.GREEN,
    title: 'Điện thoại',
    onPress: onPressCall
  },
  {
    icon: 'facebook',
    color: '#4468B0',
    title: 'Fanpage',
    onPress: onPressFanpage
  },
  {
    icon: 'envelope',
    color: SHAPE.ORANGE,
    title: 'Email',
    onPress: onPressEmail,
    simpleLineIcon: true
  }
]

const feedbacks = [
  {
    icon: 'bubbles',
    color: SHAPE.PURPLE,
    simpleLineIcon: true,
    title: 'Góp ý',
    onPress: onPressFeedBack
  }
]

@connectAwaitAutoDispatch(
  state => ({
    auth: state.auth.user
  }), {userLogout})
@autobind
export default class SlideMenuScreen extends Component {
  onPressUser() {
    this.props.navigation.navigate('tabInfo')
  }

  logout() {
    if (!this.delay) {
      this.props.userLogout()
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  render() {
    return <SlideContainer>
      <StatusBar/>
      <SlideScrollView>
        <AvatarMenu auth={this.props.auth}/>
        <Clearfix height={20}/>
        <ListMenu lists={lists}/>
        <Clearfix height={4}/>
        <ListMenu title={'LIÊN HỆ'} lists={contacts}/>
        <Clearfix height={4}/>
        <ListMenu title={'GÓP Ý'} lists={feedbacks}/>
      </SlideScrollView>
      <ButtonLogout onPress={this.logout}/>
    </SlideContainer>
  }
}
