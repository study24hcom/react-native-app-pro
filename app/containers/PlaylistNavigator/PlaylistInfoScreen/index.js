import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Dimensions} from 'react-native'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import Markdown from 'react-native-simple-markdown'
import * as Animatable from 'react-native-animatable'
import {getPlaylist} from '@redux/actions/playlistAction'
import {awaitCheckSuccess, awaitCheckPending} from 'utils/await'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {SHAPE, TEXT} from 'constants/color'
import PlaylistBox from 'components/playlist/playlist-box'
import PlaylistQuizlist from 'components/playlist/playlist-quizlist'
import Clearfix from 'components/elements/clearfix/index'
import Loading from 'components/elements/loading/index'
import Loader from 'components/elements/loading-screen/index'
import EmptySection from 'components/elements/empty-section/index'

const window = Dimensions.get('window')
const PlayListWrapper = styled.ScrollView`
  flex: 1
  `
const markdownStyles = {
  text: {
    color: TEXT.NORMAL,
    fontSize: 14
  },
  image: {
    height: 180,
    width: window.width - 32
  },
  link: {
    color: SHAPE.PRIMARY
  }
}

@connectAwaitAutoDispatch(
  state => ({
    playlistInfo: state.playlist.current.info,
    quizListsLength: state.playlist.current.info.quizListsLength,
    quizLists: state.playlist.current.quizLists
  }),
  {getPlaylist}
)
@autobind
export default class PlaylistInfoScreen extends Component {
  static propTypes = {
    playlistInfo: PropTypes.shape(PlaylistBox.propTypes),
    quizListsLength: PropTypes.number,
    quizLists: PropTypes.arrayOf(PropTypes.shape(PlaylistQuizlist.propTypes)),
    navigation: PropTypes.any
  }

  componentDidMount () {
    const {slug} = this.props.navigation.state.params
    if (slug) {
      this.props.getPlaylist(this.props.navigation.state.params.slug)
    } else if (this.props.quizLists.length !== this.props.quizListsLength) {
      this.props.getPlaylist(this.props.playlistInfo.slug)
    }
  }

  isShowLoading () {
    return awaitCheckPending(this.props, 'getPlaylist') && !this.props.playlistInfo.slug && this.props.playlistInfo.success !== false
  }

  isShowBox () {
    return !awaitCheckPending(this.props, 'getPlaylist') || awaitCheckSuccess(this.props, 'getPlaylist') || this.props.playlistInfo.slug
  }

  onPressViewAll () {
    if (!this.delay) {
      this.props.navigation.navigate('tabQuizList', {title: this.props.navigation.state.params.title})
    }
    this.delay = true
    setTimeout(() => {
      this.delay = false
    }, 1200)
  }

  onPressUser (user) {
    if (!this.delay) {
      this.props.navigation.navigate('UserScreen', {
        username: user.username
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  render () {
    const {playlistInfo, quizListsLength, quizLists} = this.props
    if (this.isShowLoading()) return <Loader loading={this.isShowLoading()} />
    if (playlistInfo.success === false) return <EmptySection text={'Bộ sưu tập này không tồn tại'} />
    return (
      <PlayListWrapper>
        {this.isShowBox() &&
        <PlaylistBox info={playlistInfo} quizListsLength={quizListsLength}>
          <Clearfix height={16} />
          <Markdown styles={markdownStyles}>{playlistInfo.description}</Markdown>
        </PlaylistBox>}
        < Clearfix height={16} />
        {awaitCheckSuccess(this.props, 'getPlaylist',
          <Animatable.View style={{flex: 1}} animation='fadeIn'>
            <PlaylistQuizlist
              isPressUser
              onPressUser={this.onPressUser}
              quizLists={quizLists}
              quizListsLength={quizListsLength}
              onPress={() => this.onPressViewAll()}
              navigation={this.props.navigation} />
          </Animatable.View>)}
        {(this.isShowBox() && awaitCheckPending(this.props, 'getPlaylist')) && <Loading />}
      </PlayListWrapper>
    )
  }
}
