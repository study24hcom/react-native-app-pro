import React, {Component} from 'react'
import {FlatList} from 'react-native'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {connect} from 'react-redux'
import {autobind} from 'core-decorators'
import {connectAutoDispatch} from '@redux/connect'
import {setInfo} from '@redux/actions/quizAction'
import QuizListItem from 'components/quizlist/quizlist-item/index'

const Clearfix = styled.View`
  height: 16px;
`
@connectAutoDispatch(null, {setInfo})
@connect(state => ({
  quizLists: state.playlist.current.quizLists,
  playlistInfo: state.playlist.current.info
}))
@autobind
export default class PlaylistQuizlistScreen extends Component {
  static navigationOptions = ({navigation: {state}}) => ({
    title: state.params.title
  })
  static propTypes = {
    quizLists: PropTypes.arrayOf(PropTypes.shape(QuizListItem.propTypes)),
    quizListsLength: PropTypes.number,
    onPress: PropTypes.func
  }

  componentDidMount () {
    this.props.navigation.setParams({title: this.props.playlistInfo.name})
  }

  onPressItem (quizListItem) {
    if (!this.delay) {
      this.props.setInfo(quizListItem)
      this.props.navigation.navigate('QuizListDetailScreen', {
        title: quizListItem.name
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  onPressUser (user) {
    if (!this.delay) {
      this.props.navigation.navigate('UserScreen', {
        username: user.username
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  renderItem ({item}) {
    return (
      <QuizListItem
        isPressUser
        onPressUser={this.onPressUser}
        isPress
        onPress={() => this.onPressItem(item)}
        {...item}
      />
    )
  }

  render () {
    return (
      <FlatList
        data={this.props.quizLists}
        ItemSeparatorComponent={() => <Clearfix />}
        contentContainerStyle={{
          backgroundColor: 'transparent',
          padding: 8,
          paddingBottom: 0
        }}
        keyExtractor={item => item.id}
        renderItem={this.renderItem}
      />
    )
  }
}
