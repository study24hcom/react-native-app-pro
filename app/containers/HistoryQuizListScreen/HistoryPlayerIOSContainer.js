import React from 'react'
import PropTypes from 'prop-types'
import {View} from 'react-native'
import {withNavigation} from 'react-navigation'
import {connectAutoDispatch} from '@redux/connect'
import {setAuthRating, addRating} from '@redux/actions/quizAddonAction'
import {autobind} from 'core-decorators'
import QuizHistoryIOS from 'components/tungtung-browser/quiz-history-ios'
import ModalRating from 'components/quizlist/modal-rating-box'

@withNavigation
@connectAutoDispatch(state => ({
  isAuthRating: state.quizAddon.authRating
}), {setAuthRating, addRating})
@autobind
export default class HistoryPlayerIOSContainer extends React.Component {
  static propTypes = {
    getAuthScore: PropTypes.func,
    setAuthRating: PropTypes.func,
    addRating: PropTypes.func
  }

  getSlug () {
    return this.props.navigation.state.params.slug
  }

  getHistoryId () {
    return this.props.navigation.state.params.historyId
  }

  handlePressDone () {
    if (this.props.isAuthRating) {
      this.props.navigation.goBack(null)
    } else {
      this.modal.open()
    }
  }

  render () {
    return (
      <View style={{flex: 1}}>
        <QuizHistoryIOS
          slug={this.getSlug()}
          onPressDone={this.handlePressDone}
          historyId={this.getHistoryId()}
        />
        <ModalRating
          setAuthRating={this.props.setAuthRating}
          addRating={this.props.addRating}
          ref={(modal) => {
            this.modal = modal
          }}
          navigation={this.props.navigation}
          quizListId={this.getSlug()} />
      </View>
    )
  }
}
