import PlayerAndroid from './HistoryPlayerAndroidContainer'
import PlayeriOS from './HistoryPlayerIOSContainer'
import { Platform } from 'react-native'

export default (Platform.OS === 'android' ? PlayerAndroid : PlayeriOS)
