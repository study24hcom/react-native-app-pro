/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react'
import {
    // Platform,
    // StyleSheet,
    Text,
    View,
    Dimensions,
    FlatList,
    ScrollView,
    TouchableOpacity
} from 'react-native'
import RichTextView from '../../components/RichTextVew/index'

var {height, width} = Dimensions.get('window')

export default class App extends Component<{}> {
  constructor () {
    super()
    this.state = {
      selectedIndex: -1
    }
    this.renderState = this.renderState.bind(this)
    this.renderQuiz = this.renderQuiz.bind(this)
  }

  render () {
    let data = [
      {
        key: "Ta có $y'=3{{x}^{5}}-6x=3x\\left( x-2 \\right)\\Rightarrow y'=0\\Leftrightarrow \\left[ \\begin{matrix} x=0  x=2 \\end{matrix} \\right.$ Từ đây ta có xét dấu của $y'$ như sau: ",
        a: 'aaaa',
        b: 'bbb',
        c: 'ccc',
        d: "$y'=3{{x}^{2}}-6x=3x\\left( x-2 \\right)\\Rightarrow y'=0\\Leftrightarrow \\left[ \\begin{matrix} x=0  x=2 \\end{matrix} \\right.$"
      },
      {
        key: "Ta có $y'=3{{x}^{2}}-6x=3x\\left( x-2 \\right)\\Rightarrow y'=0\\Leftrightarrow \\left[ \\begin{matrix} x=0  x=2 \\end{matrix} \\right.$ Từ đây ta có xét dấu của $y'$ như sau: ",
        a: 'aaaa',
        b: 'bbb',
        c: 'ccc',
        d: "$y'=3{{x}^{2}}-6x=3x\\left( x-2 \\right)\\Rightarrow y'=0\\Leftrightarrow \\left[ \\begin{matrix} x=0  x=2 \\end{matrix} \\right.$"
      },
      {
        key: "Ta có $y'=3{{x}^{2}}-6x=3x\\left( x-2 \\right)\\Rightarrow y'=0\\Leftrightarrow \\left[ \\begin{matrix} x=0  x=2 \\end{matrix} \\right.$ Từ đây ta có xét dấu của $y'$ như sau: ",
        a: 'aaaa',
        b: 'bbb',
        c: 'ccc',
        d: "$y'=3{{x}^{2}}-6x=3x\\left( x-2 \\right)\\Rightarrow y'=0\\Leftrightarrow \\left[ \\begin{matrix} x=0  x=2 \\end{matrix} \\right.$"
      },
      {
        key: "Ta có $y'=3{{x}^{2}}-6x=3x\\left( x-2 \\right)\\Rightarrow y'=0\\Leftrightarrow \\left[ \\begin{matrix} x=0  x=2 \\end{matrix} \\right.$ Từ đây ta có xét dấu của $y'$ như sau: ",
        a: 'aaaa',
        b: 'bbb',
        c: 'ccc',
        d: "$y'=3{{x}^{2}}-6x=3x\\left( x-2 \\right)\\Rightarrow y'=0\\Leftrightarrow \\left[ \\begin{matrix} x=0  x=2 \\end{matrix} \\right.$"
      },
      {
        key: "Ta có $y'=3{{x}^{2}}-6x=3x\\left( x-2 \\right)\\Rightarrow y'=0\\Leftrightarrow \\left[ \\begin{matrix} x=0  x=2 \\end{matrix} \\right.$ Từ đây ta có xét dấu của $y'$ như sau: ",
        a: 'aaaa',
        b: 'bbb',
        c: 'ccc',
        d: "$y'=3{{x}^{2}}-6x=3x\\left( x-2 \\right)\\Rightarrow y'=0\\Leftrightarrow \\left[ \\begin{matrix} x=0  x=2 \\end{matrix} \\right.$"
      },
            {key: "Ta có $y'=3{{x}^{2}}-6x=3x\\left( x-2 \\right)\\Rightarrow y'=0\\Leftrightarrow \\left[ \\begin{matrix} x=0  x=2 \\end{matrix} \\right.$ Từ đây ta có xét dấu của $y'$ như sau: "},
            {key: "Ta có $y'=3{{x}^{2}}-6x=3x\\left( x-2 \\right)\\Rightarrow y'=0\\Leftrightarrow \\left[ \\begin{matrix} x=0  x=2 \\end{matrix} \\right.$ Từ đây ta có xét dấu của $y'$ như sau: "},
            {key: "Ta có $y'=3{{x}^{2}}-6x=3x\\left( x-2 \\right)\\Rightarrow y'=0\\Leftrightarrow \\left[ \\begin{matrix} x=0  x=2 \\end{matrix} \\right.$ Từ đây ta có xét dấu của $y'$ như sau: "},
            {key: 'b'}
    ]
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <FlatList style={{height: height - 50}} data={data}
          horizontal
          pagingEnabled
          keyExtractor={this._keyExtractor}
          ref={(ref) => { this.list = ref }}
          scrollToIndex={this.state.selectedIndex}
          renderItem={this.renderQuiz} />
        <FlatList style={{height: 50}} data={data}
          horizontal
          keyExtractor={this._keyExtractor}
          renderItem={this.renderState} />
      </View>
    )
  }

  _keyExtractor = (item, index) => index;

  renderState ({item, index}) {
    return (
      <TouchableOpacity onPress={() => {
        this.setState({selectedIndex: index})
        this.list.scrollToIndex({ index: index })
      }} style={{padding: 20}}>
        <Text style={{color: this.state.selectedIndex === index ? 'red' : 'orange'}}>{index + 1}</Text>
      </TouchableOpacity>
    )
  }

  renderQuiz ({item, index}) {
    return (
      <ScrollView style={{flex: 1, flexDirection: 'column', width: width, padding: 10}}>
        <Text >Cau {index + 1}</Text>
        <RichTextView style={{flex: 1, height: 200}} text={item.key} />
        <View style={{flex: 1}}>
          {item.a && <TouchableOpacity style={{flexDirection: 'row', padding: 10}}>
            <Text >A. </Text>
            <RichTextView style={{flex: 1}} text={item.a} />
            </TouchableOpacity>}
          {item.b && <TouchableOpacity style={{flexDirection: 'row', padding: 10}}>
            <Text >B. </Text>
            <RichTextView style={{flex: 1}} text={item.b} />
            </TouchableOpacity>
                    }
          {item.c && <TouchableOpacity style={{flexDirection: 'row', padding: 10}}>
            <Text >C. </Text>
            <RichTextView style={{flex: 1}} text={item.c} />
            </TouchableOpacity>
                    }
          {item.d && <TouchableOpacity style={{flexDirection: 'row', padding: 10}}>
            <Text >D. </Text>
            <RichTextView style={{flex: 1}} text={item.d} />
            </TouchableOpacity>
                    }

        </View>
      </ScrollView>
    )
  }
}
