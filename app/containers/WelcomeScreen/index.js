import React, {Component} from 'react'
import {autobind} from 'core-decorators'
import Welcome from 'components/welcome/index'

@autobind
export default class WelcomeScreen extends Component {
  static navigationOptions = {
    header: null
  };

  render () {
    return <Welcome />
  }
}
