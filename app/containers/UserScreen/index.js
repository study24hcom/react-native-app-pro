import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import { connectAwaitAutoDispatch, connectAutoDispatch } from '@redux/connect'
import { setInfo } from '@redux/actions/quizAction'
import { setInfoPlaylist } from '@redux/actions/playlistAction'
import { autobind } from 'core-decorators'
import ProfileUser from './ProfileUser'
import ProfileMe from './ProfileMe'

const UserScreenWrapper = styled.View`
  flex: 1
  `
@connectAwaitAutoDispatch(
  state => ({
    auth: state.auth.user.username
  }), {})
@connectAutoDispatch(null, {setInfo, setInfoPlaylist})
@autobind
export default class UserScreen extends Component {
  static propTypes = {
    auth: PropTypes.string,
    username: PropTypes.string
  }
  static navigationOptions = ({navigation: {state}}) => ({
    title: state.params.username
  })

  onPressToQuizlistDetal (quizListItem) {
    if (!this.delay) {
      this.props.setInfo(quizListItem)
      this.props.navigation.navigate('QuizListDetailScreen', {
        title: quizListItem.name
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  onPressToPlaylistDetal (PlaylistListItem) {
    if (!this.delay) {
      this.props.setInfoPlaylist(PlaylistListItem.info)
      this.props.navigation.navigate('PlaylistDetailScreen', {
        title: PlaylistListItem.info.name
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  propsForUser () {
    return {
      onPressQuizList: this.onPressToQuizlistDetal,
      onPressPlaylist: this.onPressToPlaylistDetal
    }
  }

  render () {
    return (
      <UserScreenWrapper>
        {(this.props.navigation.state.params.username === this.props.auth)
          ? <ProfileMe {...this.propsForUser()} />
          : <ProfileUser username={this.props.navigation.state.params.username} {...this.propsForUser()} />}
      </UserScreenWrapper>
    )
  }
}
