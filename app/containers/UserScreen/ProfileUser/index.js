import React, {Component} from 'react'
import {withNavigation} from 'react-navigation'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import styled from 'styled-components/native'
import {awaitCheckPending} from 'utils/await'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {getProfileInfo} from '@redux/actions/profileAction'
import UserInfo from 'components/profile/UserInfo'
import Clearfix from 'components/elements/clearfix'
import ButtonFollowUser from 'components/profile/button-follow'
import QuizlistBoxCreated from 'components/profile/ProfileUser/QuizlistCreated'
import PlaylistBoxCreated from 'components/profile/ProfileUser/PlaylistCreated'
import FollowSuccess from 'components/loading-lottie/FollowSuccess'
import QuizlistBoxWorked from 'components/profile/ProfileMe/QuizlistsWorked'

const UserScreenWrapper = styled.View`
  flex: 1;
  `

const UserScreenContainer = styled.ScrollView`
  background-color: transparent;
  flex: 1;
`
const ButtonWrapper = styled.View`
  position: relative;  
  bottom: 0;
  left: 0;
  right: 0;
  `
@connectAwaitAutoDispatch(
  state => ({
    info: state.profile.info,
    isShowBox: state.follow.isShowBox
  }), {getProfileInfo})
@withNavigation
@autobind
export default class ProfileUser extends Component {
  static propTypes = {
    username: PropTypes.string,
    onPressQuizList: PropTypes.func,
    onPressPlaylist: PropTypes.func
  }

  componentDidMount () {
    this.props.getProfileInfo(this.props.username)
  }

  onPressViewAllPlaylist () {
    if (!this.delay) {
      this.props.navigation.navigate('PlaylistsViewAllProfileUser', {
        title: this.props.info.username,
        username: this.props.info.username
      })
    }
    this.delay = true
    setTimeout(() => {
      this.delay = false
    }, 1200)
  }

  onPressViewAllQuizlistWorked () {
    if (!this.delay) {
      this.props.navigation.navigate('QuizlistsByHistoryViewAll', {
        username: this.props.info.username,
        title: this.props.info.username
      })
    }
    this.delay = true
    setTimeout(() => {
      this.delay = false
    }, 1200)
  }

  onPressViewAllQuizlist () {
    if (!this.delay) {
      this.props.navigation.navigate('QuizlistsByUserViewAll', {
        username: this.props.username
      })
    }
    this.delay = true
    setTimeout(() => {
      this.delay = false
    }, 1200)
  }

  onPressUserFollow () {
    if (!this.delay) {
      this.props.navigation.navigate('UserFollowScreen')
    }
    this.delay = true
    setTimeout(() => {
      this.delay = false
    }, 1200)
  }

  render () {
    return (
      <UserScreenWrapper>
        <UserScreenContainer>
          <UserInfo isShow onPressPlaylist={this.onPressViewAllPlaylist}
            onPressUserFollow={this.onPressUserFollow} onPressQuizList={this.onPressViewAllQuizlist}
            {...this.props.info} isLoading={awaitCheckPending(this.props, 'getProfileInfo')} />
          <Clearfix height={24} />
          <QuizlistBoxCreated onPressViewAll={this.onPressViewAllQuizlist}
            username={this.props.username}
            onPressItem={this.props.onPressQuizList} />
          <Clearfix height={24} />
          <QuizlistBoxWorked onPressViewAll={this.onPressViewAllQuizlistWorked}
            username={this.props.username}
            onPressItem={this.props.onPressQuizList} />
          <Clearfix height={24} />
          <PlaylistBoxCreated onPressViewAll={this.onPressViewAllPlaylist}
            username={this.props.username}
            onPressItem={this.props.onPressPlaylist} />
        </UserScreenContainer>
        <ButtonWrapper>
          <ButtonFollowUser userId={this.props.username} />
        </ButtonWrapper>
        {this.props.isShowBox && <FollowSuccess />}
      </UserScreenWrapper>
    )
  }
}
