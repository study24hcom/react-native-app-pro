import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {withNavigation} from 'react-navigation'
import styled from 'styled-components/native'
import UserInfo from 'components/profile/UserInfo'
import {autobind} from 'core-decorators'
import {SIZE, WEIGHT} from 'constants/font'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {userLogout} from '@redux/actions/authAction'
import Clearfix from 'components/elements/clearfix'
import QuizlistBoxCreated from 'components/profile/ProfileMe/QuizlistCreated'
import QuizlistBoxWorked from 'components/profile/ProfileMe/QuizlistsWorked'
import PlaylistBoxCreated from 'components/profile/ProfileMe/PlaylistCreated'
import Button from 'components/elements/button'
import Text from 'components/elements/text/index'

const UserScreenContainer = styled.ScrollView`
  background-color: transparent;
  display: flex
`

@connectAwaitAutoDispatch(
  state => ({
    info: state.auth.user
  }), {userLogout})
@withNavigation
@autobind
export default class ProfileMe extends PureComponent {
  static propTypes = {
    onPressQuizList: PropTypes.func,
    onPressPlaylist: PropTypes.func
  }

  onPressViewAllPlaylist () {
    if (!this.delay) {
      this.props.navigation.navigate('PlaylistsViewAllProfileMe', {
        title: this.props.info.username,
        username: this.props.info.username
      })
    }
    this.delay = true
    setTimeout(() => {
      this.delay = false
    }, 1200)
  }

  onPressViewAllQuizlistCreated () {
    if (!this.delay) {
      this.props.navigation.navigate('QuizListsByMeViewAll', {
        title: this.props.info.username,
        username: this.props.info.username
      })
    }
    this.delay = true
    setTimeout(() => {
      this.delay = false
    }, 1200)
  }

  onPressViewAllQuizlistWorked () {
    if (!this.delay) {
      this.props.navigation.navigate('QuizlistsByHistoryViewAll', {
        username: this.props.info.username,
        title: this.props.info.username
      })
    }
    this.delay = true
    setTimeout(() => {
      this.delay = false
    }, 1200)
  }

  onPressUserFollow () {
    if (!this.delay) {
      this.props.navigation.navigate('UserFollowScreen')
    }
    this.delay = true
    setTimeout(() => {
      this.delay = false
    }, 1200)
  }

  onPressSetting () {
    if (!this.delay) {
      this.props.navigation.navigate('SettingScreen')
    }
    this.delay = true
    setTimeout(() => {
      this.delay = false
    }, 1200)
  }

  // isShow() {
  //   return awaitCheckSuccess(this.props, 'getQuizListsByMe') &&
  //     awaitCheckSuccess(this.props, 'getPlaylistsMe') &&
  //     awaitCheckSuccess(this.props, 'getQuizListsByHistory')
  // }

  logout () {
    if (!this.delay) {
      this.props.userLogout()
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  render () {
    return (
      <UserScreenContainer>
        <UserInfo onPressQuizList={this.onPressViewAllQuizlistCreated}
          onPressPlaylist={this.onPressViewAllPlaylist}
          onPressUserFollow={this.onPressUserFollow}
          onPressSetting={this.onPressSetting} {...this.props.info} />
        <Clearfix height={24} />
        <QuizlistBoxCreated isMe onPressViewAll={this.onPressViewAllQuizlistCreated}
          onPressItem={this.props.onPressQuizList} />
        <Clearfix height={24} />
        <QuizlistBoxWorked isMe onPressViewAll={this.onPressViewAllQuizlistWorked}
          username={this.props.info.username}
          onPressItem={this.props.onPressQuizList} />
        <Clearfix height={24} />
        <PlaylistBoxCreated isMe onPressViewAll={this.onPressViewAllPlaylist}
          onPressItem={this.props.onPressPlaylist} />
        <Clearfix height={24} />
        <Button customPadding={10} customColor='red' onClick={this.logout}>
          <Text color='#fff' fontSize={SIZE.NORMAL} fontWeight={WEIGHT.BOLD}>ĐĂNG XUẤT</Text>
        </Button>
      </UserScreenContainer>
    )
  }
}
