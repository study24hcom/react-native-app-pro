import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import { connectAutoDispatch } from '@redux/connect'
import { userLoginSuccess } from '@redux/actions/authAction'
import { Keyboard } from 'react-native'
import { autobind } from 'core-decorators'
import AuthApi from 'api/AuthApi'
import Login from 'components/auth/Login'
import { Answers } from 'react-native-fabric'
import LoginSucess from 'components/loading-lottie/LoginSuccess'

const LoginScreenContainer = styled.View`
  flex: 1;
`
@connectAutoDispatch(null, {userLoginSuccess})
@autobind
export default class LoginScreen extends Component {
  static propTypes = {
    userLoginSuccess: PropTypes.func
  }

  state = {
    isSuccess: false
  }

  // async getFcmToken () {
  //   const messaging = Firebase.messaging()
  //   const fcmToken = await messaging.getToken()
  //   PushApi.registerDeviceToken({
  //     platform: 'ios',
  //     deviceToken: fcmToken
  //   }).then(result => {
  //     console.tron.log(result)
  //   })
  // }

  handleSubmit ({email, password}) {
    if (!email || !password) {
      alert('Vui lòng nhập đầy đủ email, password')
      return
    }
    Keyboard.dismiss()
    return AuthApi.authLogin({email: email.toLowerCase(), password}).then(
      (userRes, error) => {
        const {success, token, user} = userRes
        if (success) {
          this.setState({isSuccess: true})
          // this.getFcmToken()
          Answers.logLogin(email, true)
          setTimeout(() => {
            this.props.userLoginSuccess({token, user})
          }, 1000)
        } else {
          Answers.logLogin(email, false)
          alert('Tài khoản, hoặc mật khẩu không đúng')
        }
      }
    )
  }

  handleRegister () {
    this.props.navigation.navigate('tabRegister')
  }

  render () {
    return (
      <LoginScreenContainer>
        <Login onRegister={this.handleRegister} onSubmit={this.handleSubmit} />
        {this.state.isSuccess && <LoginSucess />}
      </LoginScreenContainer>
    )
  }
}
