import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import { autobind } from 'core-decorators'
import { getQuizListsByNews } from '@redux/actions/quizListsAction'
import { QUIZ_LISTS_BY_NEWS } from 'constants/quizLists'
import connectQuizLists from 'hoc/quizlists-wrapper'
import QuizlistList from 'components/quizlist/quizlist-list'
import { connectAutoDispatch } from '@redux/connect'
import { setInfo } from '@redux/actions/quizAction'

const LaunchScreenContainer = styled.View`
  flex: 1;
`
@connectAutoDispatch(null, {setInfo})
@connectQuizLists({
  action: getQuizListsByNews,
  key: QUIZ_LISTS_BY_NEWS.key,
  payload: QUIZ_LISTS_BY_NEWS.payload
})
@autobind
export default class LaunchScreen extends Component {
  static navigationOptions = {
    title: 'Mới nhất'
  }

  static propTypes = {
    propsForFlatlist: PropTypes.object
  }

  handleNavigateToQuizlistDetail (quizListItem) {
    if (!this.delay) {
      this.props.setInfo(quizListItem)
      this.props.navigation.navigate('QuizListDetailScreen', {
        title: quizListItem.name
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  onPressUser (user) {
    if (!this.delay) {
      this.props.navigation.navigate('UserScreen', {username: user.username})
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  render () {
    return (
      <LaunchScreenContainer>
        <QuizlistList isPressUser
          onPressUser={this.onPressUser}
          onPressItem={this.handleNavigateToQuizlistDetail}
          {...this.props.propsForFlatlist} />
      </LaunchScreenContainer>
    )
  }
}
