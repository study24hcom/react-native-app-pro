import React, { Component } from 'react'
import styled from 'styled-components/native'
import CreatePlaylist from 'components/playlist/create'

const CreateContainerView = styled.ScrollView`
  flex: 1;
  `
export default class CreatePlaylistScreen extends Component {
  static navigationOptions = {
    title: 'Thêm Bộ Sưu Tập'
  }

  render () {
    return <CreateContainerView>
      <CreatePlaylist />
    </CreateContainerView>
  }
}
