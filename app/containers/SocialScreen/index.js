import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import {connectAwaitAutoDispatch, connectAutoDispatch} from '@redux/connect'
import {getActivities, loadMoreActivities} from '@redux/actions/activityAction'
import {awaitCheckPending} from 'utils/await'
import {setInfo} from '@redux/actions/quizAction'
import {checkLoadMore} from 'utils/pagination'
import ActivitiesLists from 'components/activities'

@connectAutoDispatch(null, {setInfo})
@connectAwaitAutoDispatch(
  ({activity}) => ({
    data: activity.list.data,
    pagination: activity.list.pagination
  }),
  {getActivities, loadMoreActivities}
)
@autobind()
export default class SocialScreen extends Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape(ActivitiesLists.propTypes)),
    pagination: PropTypes.shape({
      page: PropTypes.number,
      itemPerPage: PropTypes.number
    })
  };
  static navigationOptions = {
    title: 'Hoạt động mới'
  }

  state = {
    isFirst: true
  }

  componentDidMount () {
    if (this.props.data.length === 0) {
      this.props.getActivities({})
    }
  }

  handleLoadMore () {
    if (!this.isLoading() && checkLoadMore(this.props.pagination)) {
      this.props.loadMoreActivities({
        page: this.props.pagination.page + 1,
        itemPerPage: this.props.pagination.itemPerPage
      })
    }
  }

  onRefresh () {
    if (this.state.isFirst) {
      this.setState({isFirst: false})
    }
    if (!this.isLoading()) {
      this.props.getActivities({})
    }
  }

  isLoadingFirst () {
    return !awaitCheckPending(this.props, 'loadMoreActivities') && awaitCheckPending(this.props, 'getActivities') && this.state.isFirst
  }

  isLoading () {
    return awaitCheckPending(this.props, 'loadMoreActivities') || awaitCheckPending(this.props, 'getActivities')
  }

  _isRefreshing () {
    return (awaitCheckPending(this.props, 'loadMoreActivities') || awaitCheckPending(this.props, 'getActivities')) && !this.state.isFirst
  }

  isLoadingMore () {
    return awaitCheckPending(this.props, 'loadMoreActivities') && !awaitCheckPending(this.props, 'getActivities')
  }

  cleanDataActivities () {
    return this.props.data.map(activity => ({
      id: activity.id,
      user: activity.lastUser,
      username: activity.quizList.user.username,
      name: activity.quizList.name,
      quizList: activity.quizList,
      content: activity.content
    }))
  }

  onPressItem (quizListItem) {
    if (!this.delay) {
      this.props.navigation.navigate('QuizListDetailScreen', {
        title: quizListItem.name
      })
      this.props.setInfo(quizListItem.quizList)
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  render () {
    return <ActivitiesLists
      onPressItem={this.onPressItem}
      isLoadingMore={this.isLoadingMore()}
      handleLoadMore={this.handleLoadMore}
      onRefresh={this.onRefresh}
      isLoadingFirst={this.isLoadingFirst()}
      _isRefreshing={this._isRefreshing()}
      data={this.cleanDataActivities()} />
  }
}
