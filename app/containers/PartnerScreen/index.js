import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {withNavigation} from 'react-navigation'
import {autobind} from 'core-decorators'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {getPartnerList, loadMorePartnerList} from '@redux/actions/userAction'
import {awaitCheckPending} from 'utils/await'
import {checkLoadMore} from 'utils/pagination'
import {FlatList} from 'react-native'
import PartnerItem from 'components/home/partner/partnerItem'
import Loading from 'components/elements/loading'

@connectAwaitAutoDispatch(
  state => ({
    data: state.user.partnerList.data,
    pagination: state.user.partnerList.pagination
  }),
  {
    getPartnerList, loadMorePartnerList
  }
)
@withNavigation
@autobind
export default class PartnerScreen extends Component {
  static navigationOptions = ({navigation: {state}}) => ({
    title: `Nhà sáng tạo nội dung (${state.params.totalItem})`
  })
  static propTypes = {
    data: PropTypes.array,
    pagination: PropTypes.shape({
      page: PropTypes.number,
      totalItem: PropTypes.number
    })
  }

  componentDidMount () {
    const {data} = this.props
    if (data.length === 0) {
      this.props.getPartnerList({page: 1, itemPerPage: 12})
    }
  }

  onScrollToBottom () {
    if (
      !awaitCheckPending(this.props, 'getPartnerList') &&
      !awaitCheckPending(this.props, 'loadMorePartnerList') &&
      checkLoadMore(this.props.pagination)
    ) {
      this.props.loadMorePartnerList({
        page: this.props.pagination.page + 1,
        itemPerPage: this.props.pagination.itemPerPage
      })
    }
  }

  onRefresh () {
    if (!awaitCheckPending(this.props, 'getPartnerList') &&
      !awaitCheckPending(this.props, 'loadMorePartnerList')) {
      this.props.getPartnerList({page: 1, itemPerPage: 12})
    }
  }

  onPressItem (user) {
    if (!this.delay) {
      this.props.navigation.navigate('UserScreen', {username: user.username})
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  renderLoading () {
    if (!checkLoadMore(this.props.pagination)) return
    return <Loading />
  }

  isRefreshing () {
    return awaitCheckPending(this.props, 'getPartnerList') &&
      !awaitCheckPending(this.props, 'loadMorePartnerList') &&
      this.props.data.length !== 0
  }

  isLoadingMore () {
    return awaitCheckPending(this.props, 'loadMorePartnerList') &&
      !awaitCheckPending(this.props, 'getPartnerList')
  }

  renderItem ({item, index}) {
    return <PartnerItem key={index} onPress={this.onPressItem} {...item} />
  }

  render () {
    return (
      <FlatList
        data={this.props.data}
        keyExtractor={item => item.id}
        renderItem={this.renderItem}
        onEndReached={this.onScrollToBottom}
        onEndReachedThreshold={0.1}
        onRefresh={this.onRefresh}
        refreshing={this.isRefreshing()}
        ListFooterComponent={this.renderLoading()}
      />
    )
  }
}
