import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {ShareDialog} from 'react-native-fbsdk'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import {getQuizListsByTag} from '@redux/actions/quizListsAction'
import {QUIZ_LISTS_BY_TAG} from 'constants/quizLists'
import connectQuizLists from 'hoc/quizlists-wrapper'
import QuizlistList from 'components/quizlist/quizlist-list'
import {connectAutoDispatch} from '@redux/connect'
import {setInfo} from '@redux/actions/quizAction'
import ButtonFollowTag from 'components/tag/button-follow'
import Icon from 'components/elements/Icon'
import {SIZE} from 'constants/font'
import FollowSuccess from 'components/loading-lottie/FollowSuccess'
import EmptySection from '../../components/elements/empty-section/index'

const ButtonWrapper = styled.View`
  position: absolute;
  bottom: 0;  
  left: 0;
  right: 0;
`
const QuizlistListWrapper = styled.View`
  margin-bottom: 52px;
  flex: 1;
`
const LaunchScreenContainer = styled.View`
  flex: 1;
`
const Button = styled.TouchableOpacity`
  width: 45px;
  height: 45px;
  justify-content: center;
  align-items: center;
  border-radius: 22.5px;
 `
@connectAutoDispatch(state => ({
  isShowBox: state.follow.isShowBox
}), {setInfo})
@connectQuizLists({
  action: getQuizListsByTag,
  key: QUIZ_LISTS_BY_TAG.key,
  payload: QUIZ_LISTS_BY_TAG.payload,
  didmountReload: true,
  customPassAction: props => {
    return {
      tagName: props.navigation.state.params.title
    }
  }
})
@autobind
export default class TagsScreen extends Component {
  static navigationOptions = ({navigation: {state}}) => ({
    title: state.params.title,
    headerRight: <Button activeOpacity={0.5} onPress={state.params.onPressShare}>
      <Icon name='share-alt' color='#fff' simpleLineIcon size={SIZE.TITLE} />
    </Button>
  })

  getShareLinkContent () {
    return {
      contentType: 'link',
      contentUrl: `https://tungtung.vn/tag/${this.props.navigation.state.params.title}`
    }
  }

  componentDidMount () {
    this.props.navigation.setParams({onPressShare: this.onPressShare})
  }

  onPressShare () {
    var tmp = this
    ShareDialog.canShow(this.getShareLinkContent()).then(
      function (canShow) {
        if (canShow) {
          return ShareDialog.show(tmp.getShareLinkContent())
        }
      }
    )
  }

  static propTypes = {
    propsForFlatlist: PropTypes.object
  }

  onPressQuizListItem (quizListItem) {
    if (!this.delay) {
      this.props.setInfo(quizListItem)
      this.props.navigation.navigate('QuizListDetailScreen', {
        title: quizListItem.name
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  onPressUser (user) {
    if (!this.delay) {
      this.props.navigation.navigate('UserScreen', {
        username: user.username
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  render () {
    if (this.props.data.length === 0 && this.props.isLoadSuccess) {
      return <EmptySection
        text={`Không có đề thi nào trong `} textStrong={this.props.navigation.state.params.title} />
    }
    return (
      <LaunchScreenContainer>
        <QuizlistListWrapper>
          <QuizlistList
            isPressUser
            onPressUser={this.onPressUser}
            onPressItem={this.onPressQuizListItem}
            {...this.props.propsForFlatlist} />
        </QuizlistListWrapper>
        <ButtonWrapper>
          <ButtonFollowTag tagName={this.props.navigation.state.params.title} />
        </ButtonWrapper>
        {this.props.isShowBox && <FollowSuccess />}
      </LaunchScreenContainer>
    )
  }
}
