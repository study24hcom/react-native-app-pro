import React, {Component} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {FlatList, ActivityIndicator} from 'react-native'
import {autobind} from 'core-decorators'
import {awaitCheckPending} from 'utils/await'
import {checkLoadMore} from 'utils/pagination'
import {getScores, loadMoreScores} from '@redux/actions/quizAddonAction'
import {connectAwaitAutoDispatch} from '@redux/connect'
import ScoreItem from 'components/quizlist/score-lists/ScoreItem'

const LoadingWrapper = styled.View`
  padding: 16px;
  justify-content: center;
  align-items: center;
`
@connectAwaitAutoDispatch(
  state => ({
    quizListSlug: state.quiz.info.slug,
    scores: state.quizAddon.scores.data,
    pagination: state.quizAddon.scores.pagination
  }), {getScores, loadMoreScores}
)
@autobind
export default class ScoreDetailScreen extends Component {
  static navigationOptions = ({navigation}) => ({
    title: `Bảng Xếp Hạng (${navigation.state.params.totalItem})`
  });
  static propTypes = {
    scores: PropTypes.array
  };

  renderItem ({item, index}) {
    return <ScoreItem onPressUser={this.onPressUser} stt={index + 1} {...item} />
  }

  onPressUser (user) {
    if (!this.delay) {
      this.props.navigation.navigate('UserScreen', {
        username: user.username
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  handleLoadMore () {
    if (!awaitCheckPending(this.props, 'loadMoreScores') &&
      checkLoadMore(this.props.pagination)) {
      this.props.loadMoreScores({quizListKey: this.props.quizListSlug, page: this.props.pagination.page + 1})
    }
  }

  renderLoading () {
    if (!awaitCheckPending(this.props, 'loadMoreScores')) return
    return (
      <LoadingWrapper>
        <ActivityIndicator />
      </LoadingWrapper>
    )
  }

  onRefresh () {
    if (!awaitCheckPending(this.props, 'getScores') && !awaitCheckPending(this.props, 'loadMoreScores')) {
      this.props.getScores({quizListKey: this.props.quizListSlug})
    }
  }

  isLoadingFirst () {
    return awaitCheckPending(this.props, 'getScores') && !awaitCheckPending(this.props, 'loadMoreScores')
  }

  render () {
    return (
      <FlatList
        data={this.props.scores}
        keyExtractor={item => item.id}
        renderItem={this.renderItem}
        onEndReachedThreshold={0}
        onEndReached={this.handleLoadMore}
        contentContainerStyle={{marginTop: 4}}
        ListFooterComponent={this.renderLoading()}
        onRefresh={this.onRefresh}
        refreshing={this.isLoadingFirst()}
      />
    )
  }
}
