import React, {Component} from 'react'
import styled from 'styled-components/native'
import AuthApi from '../../api/AuthApi'
import {Keyboard} from 'react-native'
import {
  convertFormValuesToApi,
  convertStateToInitialValuesProfile
} from 'components/profile/profile-manager/utils'
import ProfileInformationForm from 'components/profile/profile-manager/index'
import {connectAutoDispatch} from '@redux/connect'
import {updateProfile} from '@redux/actions/authAction'
import {autobind} from 'core-decorators'
import UpdateSuccess from 'components/loading-lottie/UpdateSuccess'

const SettingSCreenContainer = styled.View`
  flex: 1;
  `
@connectAutoDispatch(
  state => ({
    initialValues: convertStateToInitialValuesProfile(state),
    isAuthenticated: state.auth.isAuthenticated,
    username: state.auth.user.username
  }),
  {updateProfile}
)
@autobind
export default class SettingScreen extends Component {
  static propTypes = {}
  static navigationOptions = {
    header: null
  }

  state = {
    isSuccess: false
  }

  onSubmit (values) {
    Keyboard.dismiss()
    this.props.updateProfile(values)
    AuthApi.updateProfile(convertFormValuesToApi(values)).then(res => {
      if (res.success === true) {
        this.setState({isSuccess: true})
        setTimeout(() => {
          this.setState({isSuccess: false})
          this.props.navigation.goBack(null)
        }, 1200)
      } else {
        alert('Cập nhật bị lỗi, vui lòng thử lại sau')
      }
    })
  }

  render () {
    return <SettingSCreenContainer>
      <ProfileInformationForm onSubmit={this.onSubmit}
        initialValues={this.props.initialValues} />
      {this.state.isSuccess && <UpdateSuccess />}
    </SettingSCreenContainer>
  }
}
