import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {getPlaylists, loadMorePlaylists, setInfoPlaylist} from '@redux/actions/playlistAction'
import {awaitCheckPending} from 'utils/await'
import {checkLoadMore} from 'utils/pagination'
import PlaylistList from 'components/playlist/playlist-list'
import HeadingLine from 'components/elements/heading-line'
import {SIZE} from 'constants/font'
import {SHAPE} from 'constants/color'
import {playlist} from 'constants/description'

@connectAwaitAutoDispatch(
  state => ({
    data: state.playlist.lists.data,
    pagination: state.playlist.lists.pagination
  }),
  {getPlaylists, loadMorePlaylists, setInfoPlaylist}
)
@autobind
export default class PlaylistListScreen extends Component {
  static propTypes = {
    data: PropTypes.array,
    pagination: PropTypes.shape({
      page: PropTypes.number,
      totalItem: PropTypes.number
    })
  }

  state = {
    isFirst: true
  }

  componentWillMount () {
    if (this.props.data.length === 0) {
      this.props.getPlaylists({})
    }
  }

  onScrollToBottom () {
    if (
      !awaitCheckPending(this.props, 'getPlaylists') &&
      !awaitCheckPending(this.props, 'loadMorePlaylists') &&
      checkLoadMore(this.props.pagination)
    ) {
      this.props.loadMorePlaylists({page: this.props.pagination.page + 1})
    }
  }

  onRefresh () {
    if (this.state.isFirst) {
      this.setState({isFirst: false})
    }
    if (!awaitCheckPending(this.props, 'getPlaylists') &&
      !awaitCheckPending(this.props, 'loadMorePlaylists')) {
      this.props.getPlaylists({})
    }
  }

  onPressPlaylistItem (PlaylistListItem) {
    if (!this.delay) {
      this.props.setInfoPlaylist(PlaylistListItem.info)
      this.props.navigation.navigate('PlaylistDetailScreen', {title: PlaylistListItem.info.name})
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  isLoadingFirst () {
    return awaitCheckPending(this.props, 'getPlaylists') &&
      !awaitCheckPending(this.props, 'loadMorePlaylists') && this.state.isFirst
  }

  isRefreshing () {
    return awaitCheckPending(this.props, 'getPlaylists') &&
      !awaitCheckPending(this.props, 'loadMorePlaylists') && !this.state.isFirst
  }

  isLoadingMore () {
    return awaitCheckPending(this.props, 'loadMorePlaylists') &&
      !awaitCheckPending(this.props, 'getPlaylists')
  }

  onPressUser (user) {
    if (!this.delay) {
      this.props.navigation.navigate('UserScreen', {username: user.username})
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  renderHeader () {
    return <HeadingLine size={SIZE.HEADING3} color={SHAPE.BLACK} noLine title={'Bộ sưu tập'}
      description={playlist.list} />
  }

  render () {
    return (
      <PlaylistList
        isPressUser
        header={this.renderHeader()}
        onPressUser={this.onPressUser}
        isLoadingMore={this.isLoadingMore()}
        data={this.props.data}
        isLoadingFirst={this.isLoadingFirst()}
        onRefresh={this.onRefresh}
        isRefreshing={this.isRefreshing()}
        onPressItem={this.onPressPlaylistItem}
        onScrollToBottom={this.onScrollToBottom} />
    )
  }
}
