import React, { Component } from 'react'
import styled from 'styled-components/native'
import ScrollableTabView from 'react-native-scrollable-tab-view'
import TungtungTabBar from 'components/TungtungTabBar/index'
import PlaylistListScreen from './PlaylistListScreen/index'

const TabBarWrapper = styled.View`
  flex: 1;
`
export default class PlaylistScreen extends Component {
  render () {
    const {navigation} = this.props
    return (
      <TabBarWrapper>
        <ScrollableTabView renderTabBar={() => <TungtungTabBar />}>
          <PlaylistListScreen navigation={navigation} tabLabel='Của Tôi' />
          <PlaylistListScreen navigation={navigation} tabLabel='Gợi Ý' />
          <PlaylistListScreen navigation={navigation} tabLabel='Cộng Đồng' />
        </ScrollableTabView>
      </TabBarWrapper>
    )
  }
}
