import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Alert} from 'react-native'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import {QUIZ_LISTS_BY_ME} from 'constants/quizLists'
import connectQuizLists from 'hoc/quizlists-wrapper'
import QuizlistList from 'components/quizlist/quizlist-list'
import {connectAutoDispatch} from '@redux/connect'
import {setInfo} from '@redux/actions/quizAction'
import {getQuizListsByMe} from '@redux/actions/quizListsAction'
import EmptySection from 'components/elements/empty-section'
import HeadingLine from 'components/elements/heading-line'
import ButtonRight from 'components/elements/botton-header-right'
import {SIZE} from 'constants/font'
import {SHAPE} from 'constants/color'
import {quizList} from 'constants/description'

const LaunchScreenContainer = styled.View`
  flex: 1;
`
@connectAutoDispatch(null, {setInfo})
@connectQuizLists({
  action: getQuizListsByMe,
  key: QUIZ_LISTS_BY_ME.key,
  payload: QUIZ_LISTS_BY_ME.payload
})
@autobind
export default class QuizListsByMeViewAll extends Component {
  static navigationOptions = ({navigation: {state}}) => ({
    title: state.params.title,
    headerRight: <ButtonRight
      onPress={() => Alert.alert('Tạo đề thi', 'Tính năng này đang được phát triển. Vui lòng quay trở lại sau.')}
      icon='plus' />
  })

  static propTypes = {
    propsForFlatlist: PropTypes.object
  }

  handleNavigateToQuizlistDetail (quizListItem) {
    if (!this.delay) {
      this.props.setInfo(quizListItem)
      this.props.navigation.navigate('QuizListDetailScreen', {
        title: quizListItem.name
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  renderHeader () {
    return <HeadingLine size={SIZE.HEADING3} color={SHAPE.BLACK} noLine title={'Đề thi đã tạo'}
      description={quizList.created.byMe} />
  }

  render () {
    if (this.props.data.length === 0 && this.props.isLoadSuccess) {
      return <EmptySection text={'Bạn chưa tạo đề thi nào.'} />
    }
    return (
      <LaunchScreenContainer>
        <QuizlistList header={this.renderHeader()} onPressItem={this.handleNavigateToQuizlistDetail}
          {...this.props.propsForFlatlist} />
      </LaunchScreenContainer>
    )
  }
}
