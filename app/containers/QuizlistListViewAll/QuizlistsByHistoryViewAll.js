import React, {Component} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import {QUIZ_LISTS_BY_HISTORY} from 'constants/quizLists'
import connectQuizLists from 'hoc/quizlists-wrapper'
import QuizlistList from 'components/quizlist/quizlist-list'
import {connectAutoDispatch} from '@redux/connect'
import {setInfo} from '@redux/actions/quizAction'
import {getQuizListsByHistory} from '@redux/actions/quizListsAction'
import EmptySection from 'components/elements/empty-section'
import HeadingLine from 'components/elements/heading-line'
import {SIZE} from 'constants/font'
import {SHAPE} from 'constants/color'
import {quizList} from 'constants/description'

const LaunchScreenContainer = styled.View`
  flex: 1;
`
@connectAutoDispatch((state, ownProps) => ({
  isMe: ownProps.navigation.state.params.username === state.auth.user.username
}), {setInfo})
@connectQuizLists({
  action: getQuizListsByHistory,
  key: QUIZ_LISTS_BY_HISTORY.key,
  payload: QUIZ_LISTS_BY_HISTORY.payload,
  customPassAction: props => {
    return {
      username: props.navigation.state.params.username
    }
  }
})
@autobind
export default class QuizlistsByHistoryViewAll extends Component {
  static navigationOptions = ({navigation: {state}}) => ({
    title: state.params.title
  })

  static propTypes = {
    propsForFlatlist: PropTypes.object
  }

  handleNavigateToQuizlistDetail (quizListItem) {
    if (!this.delay) {
      this.props.setInfo(quizListItem)
      this.props.navigation.navigate('QuizListDetailScreen', {
        title: quizListItem.name
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  renderHeader () {
    return <HeadingLine size={SIZE.HEADING3} color={SHAPE.BLACK} noLine title={'Đề thi đã làm'}
      description={this.props.isMe ? quizList.worked.byMe : quizList.worked.byUser} />
  }

  render () {
    if (this.props.data.length === 0 && this.props.isLoadSuccess) {
      return <EmptySection text={this.props.isMe ? 'Bạn chưa làm đề thi nào.' : 'User này chưa làm đề thi nào.'} />
    }
    return (
      <LaunchScreenContainer>
        <QuizlistList header={this.renderHeader()} onPressItem={this.handleNavigateToQuizlistDetail}
          {...this.props.propsForFlatlist} />
      </LaunchScreenContainer>
    )
  }
}
