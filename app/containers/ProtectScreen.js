import React from 'react'
import PropTypes from 'prop-types'
import {ActivityIndicator} from 'react-native'
import {connectAutoDispatch} from '@redux/connect'
import styled from 'styled-components/native'
import {userAutoAuth} from '@redux/actions/authAction'
import createReduxNavigation from '../navigation/ReduxNavigation'
import {getAuthToken} from 'utils/auth'
import {WelcomeNavigator, FollowScreen, HomeNav} from '../navigation/AppNavigation'

const AppView = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`

@createReduxNavigation
@connectAutoDispatch((state) => ({
  isChecking: state.auth.loading,
  isAuthenticated: state.auth.isAuthenticated,
  register: state.auth.register
}), {userAutoAuth})
export default class ProtectScreen extends React.Component {
  static propTypes = {
    isChecking: PropTypes.bool,
    isAuthenticated: PropTypes.bool,
    userAutoAuth: PropTypes.func,
    register: PropTypes.bool
  }

  state = {
    authToken: '',
    isLoadingAuthToken: true
  }

  renderLoading () {
    return <AppView>
      <ActivityIndicator />
    </AppView>
  }

  async componentDidMount () {
    const authToken = await getAuthToken(false)
    this.setState({authToken, isLoadingAuthToken: false})
    if (authToken) {
      this.props.userAutoAuth()
    }
  }

  isShowLogin () {
    return ((!this.state.isLoadingAuthToken && !this.state.authToken && !this.props.register) ||
      (!this.props.isChecking && this.state.authToken !== '' && !this.props.isAuthenticated))
  }

  isShowLoading () {
    return (this.state.isLoadingAuthToken || (this.props.isChecking && this.state.authToken))
  }

  isShowHome () {
    return (this.props.isAuthenticated && !this.props.register && this.state.authToken !== '')
  }

  isShowTag () {
    return (this.props.register && this.props.isAuthenticated && this.state.authToken !== '')
  }

  render () {
    if (this.isShowHome()) return <HomeNav />
    if (this.state.isLoadingAuthToken) return null
    if (this.isShowLoading()) return this.renderLoading()
    if (this.isShowLogin()) return <WelcomeNavigator />
    if (this.isShowTag()) return <FollowScreen />
  }
}
