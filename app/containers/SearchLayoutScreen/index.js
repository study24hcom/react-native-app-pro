import React, {Component} from 'react'
import {autobind} from 'core-decorators'
import PropTypes from 'prop-types'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {setKeywordsSearch} from '@redux/actions/optionAction'
import SearchLayout from 'components/searchLayout'
import SearchList from 'components/search/searchlist'
import SearchHistory from 'components/search/SearchHistory'
import {deleteHistories, saveHistories} from '@redux/actions/historiesAction'
import {Answers} from 'react-native-fabric'

@connectAwaitAutoDispatch(state => ({
  keywords: state.option.keywords,
  histories: state.histories.history,
  keywordSuggest: state.search.keywords.keywordSuggest,
  recentSuggest: state.search.keywords.recentSuggest,
  paginationPartner: state.user.partnerList.pagination
}), {setKeywordsSearch, deleteHistories, saveHistories})
@autobind
export default class SearchLayoutScreen extends Component {
  static navigationOptions = {
    header: null
  }

  static propTypes = {
    keywords: PropTypes.string,
    data: PropTypes.array
  }

  autoDelay (callback) {
    if (!this.delay) {
      callback()
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  onPressItem (keywords) {
    this.autoDelay(() => {
      Answers.logSearch(keywords)
      this.props.saveHistories(keywords)
      this.props.setKeywordsSearch({keywords})
      this.props.navigation.navigate('SearchScreen')
    })
  }

  onPressDeleteHistory () {
    this.props.deleteHistories()
  }

  openQuizList (category, name, slug) {
    this.autoDelay(() => {
      this.props.navigation.navigate('CategoryScreen', {
        name: name,
        category: category,
        slug: slug
      })
    })
  }

  onPressUser (user) {
    this.autoDelay(() => {
      this.props.navigation.navigate('UserScreen', {
        username: user.username
      })
    })
  }

  onPressViewAll () {
    this.autoDelay(() => {
      this.props.navigation.navigate('PartnerScreen', {
        totalItem: this.props.paginationPartner.totalItem
      })
    })
  }

  onPressTagViewAll () {
    this.autoDelay(() => {
      this.props.navigation.navigate('tabTagSocial')
    })
  }

  onPressTag (tagItem) {
    this.autoDelay(() => {
      this.props.navigation.navigate('TagsScreen', {title: tagItem.name})
    })
  }

  renderHistories () {
    return <SearchLayout>
      <SearchHistory onPressTag={this.onPressTag} onPressUser={this.onPressUser} openQuizList={this.openQuizList}
        onPressDelete={this.onPressDeleteHistory} data={this.props.histories.slice(0, 3)}
        onPressItem={this.onPressItem} onPressTagViewAll={this.onPressTagViewAll}
        onPressViewAll={this.onPressViewAll} />
    </SearchLayout>
  }

  render () {
    if (!this.props.keywords) return this.renderHistories()
    if (this.props.keywords || this.props.keywordSuggest.length !== 0 || this.props.recentSuggest.length !== 0) {
      return <SearchLayout>
        <SearchList recentSuggest={this.props.recentSuggest} keywordSuggest={this.props.keywordSuggest}
          onPressItem={this.onPressItem} keywords={this.props.keywords} />
      </SearchLayout>
    }
  }
}
