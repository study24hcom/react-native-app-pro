import React from 'react'
import {Alert, View} from 'react-native'
import {autobind} from 'core-decorators'
import {userLoginSuccess} from '@redux/actions/authAction'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {awaitCheckPending, awaitCheckSuccess} from 'utils/await'
import HeaderNav from 'components/follow/tag/HeaderNav'
import FadeInView from 'components/Animated/fadeIn'
import Follow from 'components/follow'
import PlaceholderView from 'components/placeholder'
import FollowTagsSuccess from 'components/loading-lottie/FollowTagsSuccess'

@connectAwaitAutoDispatch(state => ({
  data: state.tag.featured.data,
  pagination: state.tag.featured.pagination,
  followedTags: state.follow.followedTags,
  token: state.auth.token,
  user: state.auth.user
}), {userLoginSuccess}
)
@autobind
export default class FollowScreen extends React.Component {
  static navigationOptions = {
    header: null
  }

  state = {
    isLoading: false
  }

  isLoading () {
    return awaitCheckPending(this.props, 'getFeaturedTags')
  }

  isSuccess () {
    return awaitCheckSuccess(this.props, 'getFeaturedTags')
  }

  onSubmit () {
    if (this.props.followedTags.filter(data => (data.followed === true)).length < 2) {
      Alert.alert('Vui lòng theo dõi ít nhất 2 tag để chúng tôi phục vụ bạn tốt hơn.')
    } else {
      this.setState({isLoading: true})
      setTimeout(() => {
        this.props.userLoginSuccess({token: this.props.token, user: this.props.user})
      }, 1000)
    }
  }

  renderPlaceHolder () {
    return <View>
      <PlaceholderView margin={8} />
      <PlaceholderView margin={8} />
      <PlaceholderView margin={8} />
      <PlaceholderView margin={8} />
    </View>
  }

  renderFollowTag () {
    return <FadeInView>
      <Follow description={`Điều này sẽ giúp tungtung.vn gợi ý các đề thi phù hợp với các lĩnh vực bạn yêu thích 😉`}
        data={this.props.data} />
    </FadeInView>
  }

  render () {
    return <View style={{flex: 1}}>
      <HeaderNav onPressSubmit={this.onSubmit} />
      {this.isLoading() && this.renderPlaceHolder()}
      {this.isSuccess() && this.renderFollowTag()}
      {this.state.isLoading && <FollowTagsSuccess />}
    </View>
  }
}
