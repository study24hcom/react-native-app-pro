import React, {Component} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {getPlaylistsMe, loadMorePlaylistsMe, setInfoPlaylist} from '@redux/actions/playlistAction'
import {awaitCheckPending} from 'utils/await'
import {checkLoadMore} from 'utils/pagination'
import PlaylistList from 'components/playlist/playlist-list'

const PlaylistScreenContainer = styled.View`
  flex: 1;
`
@connectAwaitAutoDispatch(
  state => ({
    data: state.playlist.listsMe.data,
    pagination: state.playlist.listsMe.pagination
  }),
  {getPlaylistsMe, loadMorePlaylistsMe, setInfoPlaylist}
)
@autobind
export default class PlaylistsViewAllProfileMe extends Component {
  static navigationOptions = ({navigation: {state}}) => ({
    title: state.params.title
  })

  static propTypes = {
    data: PropTypes.array,
    pagination: PropTypes.shape({
      page: PropTypes.number,
      totalItem: PropTypes.number
    })
  }

  componentWillMount () {
    if (this.props.data.length === 0) {
      this.props.getPlaylistsMe()
    }
  }

  onScrollToBottom () {
    if (
      !awaitCheckPending(this.props, 'getPlaylistsMe') &&
      !awaitCheckPending(this.props, 'loadMorePlaylistsMe') &&
      checkLoadMore(this.props.pagination)
    ) {
      this.props.loadMorePlaylistsMe({page: this.props.pagination.page + 1})
    }
  }

  state = {
    isFirst: true
  }

  onRefresh () {
    if (this.state.isFirst) {
      this.setState({isFirst: false})
    }
    if (!awaitCheckPending(this.props, 'getPlaylistsMe') && !awaitCheckPending(this.props, 'loadMorePlaylistsMe')) {
      this.props.getPlaylistsMe()
    }
  }

  isLoadingFirst () {
    return awaitCheckPending(this.props, 'getFeaturedPlaylistsByUser') &&
      !awaitCheckPending(this.props, 'loadMorePlaylistsMe') && this.state.isFirst
  }

  isRefreshing () {
    return awaitCheckPending(this.props, 'getFeaturedPlaylistsByUser') &&
      !awaitCheckPending(this.props, 'loadMorePlaylistsMe') && !this.state.isFirst
  }

  isLoadingMore () {
    return awaitCheckPending(this.props, 'loadMorePlaylistsMe') && !awaitCheckPending(this.props, 'getPlaylistsMe')
  }

  onPressUser (user) {
    if (!this.delay) {
      this.props.navigation.navigate('UserScreen', {username: user.username})
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  handleNavigateToPlaylistDetail (PlaylistListItem) {
    if (!this.delay) {
      this.props.setInfoPlaylist(PlaylistListItem.info)
      this.props.navigation.navigate('PlaylistDetailScreen', {title: PlaylistListItem.info.name})
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  render () {
    return (
      <PlaylistScreenContainer>
        <PlaylistList
          onPressUser={this.onPressUser}
          isLoadingMore={this.isLoadingMore()}
          data={this.props.data}
          isLoadingFirst={this.isLoadingFirst()}
          isRefreshing={this.isRefreshing()}
          onRefresh={this.onRefresh}
          onPressItem={this.handleNavigateToPlaylistDetail}
          onScrollToBottom={this.onScrollToBottom} />
      </PlaylistScreenContainer>
    )
  }
}
