import React, {Component} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {
  getFeaturedPlaylistsByUser,
  loadMoreFeaturedPlaylistsByUser,
  setInfoPlaylist
} from '@redux/actions/playlistAction'
import {awaitCheckPending} from 'utils/await'
import {checkLoadMore} from 'utils/pagination'
import PlaylistList from 'components/playlist/playlist-list'

const PlaylistScreenContainer = styled.View`
  flex: 1;
`
@connectAwaitAutoDispatch(
  state => ({
    data: state.playlist.featuredListsByUser.data,
    pagination: state.playlist.featuredListsByUser.pagination
  }),
  {getFeaturedPlaylistsByUser, loadMoreFeaturedPlaylistsByUser, setInfoPlaylist}
)
@autobind
export default class PlaylistsViewAllProfileUser extends Component {
  static navigationOptions = ({navigation: {state}}) => ({
    title: state.params.title
  })

  static propTypes = {
    data: PropTypes.array,
    pagination: PropTypes.shape({
      page: PropTypes.number,
      totalItem: PropTypes.number
    })
  }

  componentWillMount () {
    if (this.props.data.length === 0) {
      this.props.getFeaturedPlaylistsByUser({username: this.props.navigation.state.params.username})
    }
  }

  onScrollToBottom () {
    if (
      !awaitCheckPending(this.props, 'getFeaturedPlaylistsByUser') &&
      !awaitCheckPending(this.props, 'loadMorePlaylistsMe') &&
      checkLoadMore(this.props.pagination)
    ) {
      this.props.loadMorePlaylistsMe({
        page: this.props.pagination.page + 1,
        username: this.props.navigation.state.params.title
      })
    }
  }

  state = {
    isFirst: true
  }

  onRefresh () {
    if (this.state.isFirst) {
      this.setState({isFirst: false})
    }
    if (!awaitCheckPending(this.props, 'getFeaturedPlaylistsByUser') && !awaitCheckPending(this.props, 'loadMorePlaylistsMe')) {
      this.props.getFeaturedPlaylistsByUser({username: this.props.navigation.state.params.username})
    }
  }

  isLoadingFirst () {
    return awaitCheckPending(this.props, 'getFeaturedPlaylistsByUser') &&
      !awaitCheckPending(this.props, 'loadMorePlaylistsMe') && this.state.isFirst
  }

  isRefreshing () {
    return !awaitCheckPending(this.props, 'loadMorePlaylistsMe') &&
      awaitCheckPending(this.props, 'getFeaturedPlaylistsByUser') && !this.state.isFirst
  }

  isLoadingMore () {
    return awaitCheckPending(this.props, 'loadMorePlaylistsMe') && !awaitCheckPending(this.props, 'getFeaturedPlaylistsByUser')
  }

  onPressUser (user) {
    if (!this.delay) {
      this.props.navigation.navigate('UserScreen', {username: user.username})
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  handleNavigateToPlaylistDetail (PlaylistListItem) {
    if (!this.delay) {
      this.props.setInfoPlaylist(PlaylistListItem.info)
      this.props.navigation.navigate('PlaylistDetailScreen', {title: PlaylistListItem.info.name})
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  render () {
    return (
      <PlaylistScreenContainer>
        <PlaylistList
          onPressUser={this.onPressUser}
          isLoadingMore={this.isLoadingMore()}
          data={this.props.data}
          isLoadingFirst={this.isLoadingFirst()}
          isRefreshing={this.isRefreshing()}
          onRefresh={this.onRefresh}
          onPressItem={this.handleNavigateToPlaylistDetail}
          onScrollToBottom={this.onScrollToBottom} />
      </PlaylistScreenContainer>
    )
  }
}
