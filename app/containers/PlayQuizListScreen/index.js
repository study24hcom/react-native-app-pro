import PlayerAndroid from './PlayerScreenAndroid'
import PlayeriOS from './QuizPlayerIOSContainer'
import { Platform } from 'react-native'

export default (Platform.OS === 'android' ? PlayerAndroid : PlayeriOS)
