import React from 'react'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import {Alert, View} from 'react-native'
import QuizPlayerIOS from 'components/tungtung-browser/quiz-player-ios'
import {connectAutoDispatch} from '@redux/connect'
import {setAuthRating, addRating, getAuthScore} from '@redux/actions/quizAddonAction'
import ModalRating from 'components/quizlist/modal-rating-box'

@connectAutoDispatch(
  state => ({
    authScore: state.quizAddon.authScore,
    authRating: state.quizAddon.authRating
  }),
  {getAuthScore, setAuthRating, addRating}
)
@autobind
export default class QuizPlayerIOSContainer extends React.Component {
  static propTypes = {
    getAuthScore: PropTypes.func,
    setAuthRating: PropTypes.func,
    addRating: PropTypes.func
  }

  state = {
    isViewHistory: false,
    history: null
  }

  handleQuizListSubmitted (history) {
    if (this.props.authScore._id !== history.quiz_list_id) {
      this.props.getAuthScore({quizListKey: history.quiz_list_id})
    }
  }

  handleDone () {
    if (this.props.authRating) {
      this.props.navigation.goBack(null)
    } else {
      this.modal.open()
    }
  }

  getSlug () {
    return this.props.navigation.state.params.slug
  }

  handleExit (isPlaying) {
    Alert.alert(
      'Xác nhận',
      isPlaying
        ? 'Bạn đang trong quá trình làm bài, bạn có chắc chắn muốn thoát không.'
        : 'Bạn có muốn thoát không.',
      [
        {
          text: 'Quay lại',
          onPress: () => {
          }
        },
        {
          text: 'Thoát ngay',
          onPress: () => {
            this.props.navigation.goBack(null)
          },
          style: 'cancel'
        }
      ],
      {cancelable: false}
    )
  }

  render () {
    return (
      <View style={{flex: 1}}>
        <QuizPlayerIOS
          onQuizListSubmitted={this.handleQuizListSubmitted}
          onPressDone={this.handleDone}
          onPressExit={this.handleExit}
          slug={this.getSlug()}
        />
        <ModalRating ref={(modal) => {
          this.modal = modal
        }}
          addRating={this.props.addRating}
          setAuthRating={this.props.setAuthRating}
          quizListId={this.getSlug()}
          navigation={this.props.navigation} />
      </View>
    )
  }
}
