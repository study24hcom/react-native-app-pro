import React, {Component} from 'react'
import {SubmissionError} from 'redux-form'
import {Keyboard, Alert, Modal} from 'react-native'
import styled from 'styled-components/native'
import {autobind} from 'core-decorators'
import validator from 'validator'
import AuthApi from 'api/AuthApi'
import ForgotPass from 'components/auth/ForgotPass'
import ForgotPassConfirm from 'components/auth/ForgotPassConfirm'
import {connectAutoDispatch} from '@redux/connect'
import {userLoginSuccess} from '@redux/actions/authAction'
import Success from 'components/loading-lottie/Success'

const ForgotContainer = styled.View`
 flex: 1;
`
@connectAutoDispatch(() => ({}), {userLoginSuccess})
@autobind
export default class ForgotPassScreen extends Component {
  static navigationOptions = ({navigation: {state}}) => ({
    header: null
  })
  state = {
    isSuccess: false,
    isShowConfirm: false,
    email: ''
  }
  static propTypes = {}

  handleSubmit (values) {
    Keyboard.dismiss()
    if (!values.email) {
      Alert.alert('Lỗi', 'Vui lòng nhập email')
    } else if (!validator.isEmail(values.email)) {
      Alert.alert('Lỗi', 'Định dạng email không đúng')
    } else {
      return AuthApi.forgotPassword(values.email.toLowerCase()).then((userRes, error) => {
        const {success} = userRes
        if (success) {
          this.setState({isSuccess: true})
          setTimeout(() => {
            this.setState({isSuccess: false})
          }, 500)
          setTimeout(() => {
            this.setState({email: values.email, isShowConfirm: true})
          }, 200)
        } else {
          throw new SubmissionError({
            ...userRes.errors,
            _error: 'Register failed'
          })
        }
      })
    }
  }

  handlSubmitPass (values) {
    Keyboard.dismiss()
    if (!values.code || !values.password) {
      Alert.alert('Lỗi', 'Vui lòng nhập đầy đủ mã xác nhận và mật khẩu')
    } else {
      return AuthApi.forgotPasswordConfirm(values.email.toLowerCase(), values.code, values.password).then((userRes, error) => {
        const {success, token, user} = userRes
        if (success) {
          this.setState({isSuccess: true})
          setTimeout(() => {
            this.setState({isSuccess: false})
            this.props.userLoginSuccess({token, user})
          }, 500)
        } else {
          Alert.alert('Lỗi', 'Mã xác nhận không đúng')
          throw new SubmissionError({
            ...userRes.errors,
            _error: 'Register failed'
          })
        }
      })
    }
  }

  onPressCancel () {
    this.setState({isShowConfirm: !this.state.isShowConfirm})
  }

  renderModalConfirmForgotPass () {
    return <Modal
      onRequestClose={this.onPressCancel}
      visible={this.state.isShowConfirm}
      animationType={'fade'}>
      {this.state.email &&
      <ForgotPassConfirm isSuccess={this.state.isSuccess} initialValues={{email: this.state.email}}
        onSubmit={this.handlSubmitPass} />}
    </Modal>
  }

  render () {
    return (
      <ForgotContainer>
        <ForgotPass onSubmit={this.handleSubmit} />
        {this.renderModalConfirmForgotPass()}
        {this.state.isSuccess && <Success />}
      </ForgotContainer>
    )
  }
}
