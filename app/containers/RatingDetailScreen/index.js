import React, {Component} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import {FlatList, ActivityIndicator} from 'react-native'
import {autobind} from 'core-decorators'
import {awaitCheckPending} from 'utils/await'
import {checkLoadMore} from 'utils/pagination'
import {getRatings, loadMoreRatings} from '@redux/actions/quizAddonAction'
import {connectAwaitAutoDispatch} from '@redux/connect'
import RatingItem from 'components/quizlist/rating-lists/RatingItem'
import Clearfix from 'components/elements/clearfix'

const LoadingWrapper = styled.View`
  padding: 16px;
  justify-content: center;
  align-items: center;
`
@connectAwaitAutoDispatch(
  state => ({
    quizListSlug: state.quiz.info.slug,
    ratings: state.quizAddon.ratings.data,
    pagination: state.quizAddon.ratings.pagination
  }), {getRatings, loadMoreRatings}
)
@autobind
export default class RatingDetailScreen extends Component {
  static navigationOptions = ({navigation}) => ({
    title: `Đánh Giá (${navigation.state.params.totalItem})`
  });
  static propTypes = {
    ratings: PropTypes.array
  };

  renderItem ({item}) {
    return <RatingItem onPressUser={this.onPressUser} isDetails {...item} />
  }

  onPressUser (user) {
    if (!this.delay) {
      this.props.navigation.navigate('UserScreen', {
        username: user.username
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  handleLoadMore () {
    if (!awaitCheckPending(this.props, 'loadMoreRatings') &&
      checkLoadMore(this.props.pagination)) {
      this.props.loadMoreRatings({quizListKey: this.props.quizListSlug, page: this.props.pagination.page + 1})
    }
  }

  onRefresh () {
    if (!awaitCheckPending(this.props, 'getRatings') && !awaitCheckPending(this.props, 'loadMoreRatings')) {
      this.props.getRatings({quizListKey: this.props.quizListSlug})
    }
  }

  isLoadingFirst () {
    return awaitCheckPending(this.props, 'getRatings') && !awaitCheckPending(this.props, 'loadMoreRatings')
  }

  renderLoading () {
    if (!awaitCheckPending(this.props, 'loadMoreRatings')) return
    return (
      <LoadingWrapper>
        <ActivityIndicator />
      </LoadingWrapper>
    )
  }

  render () {
    return (
      <FlatList
        data={this.props.ratings}
        ItemSeparatorComponent={() => <Clearfix height={4} />}
        keyExtractor={item => item.id}
        renderItem={this.renderItem}
        onEndReachedThreshold={0.5}
        contentContainerStyle={{marginTop: 4}}
        onEndReached={this.handleLoadMore}
        ListFooterComponent={this.renderLoading()}
        onRefresh={this.onRefresh}
        refreshing={this.isLoadingFirst()}
      />
    )
  }
}
