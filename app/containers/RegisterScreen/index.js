import React, {Component} from 'react'
import {SubmissionError} from 'redux-form'
import {Keyboard} from 'react-native'
import styled from 'styled-components/native'
import {connectAutoDispatch} from '@redux/connect'
import {userLoginSuccess, userRegisterSuccess} from '@redux/actions/authAction'
import {getFeaturedTags} from '@redux/actions/tagAction'
import {autobind} from 'core-decorators'
import AuthApi from 'api/AuthApi'
import Register from 'components/auth/Register'
import RegisterSuccess from 'components/loading-lottie/RegisterSuccess'

const RegisterScreenContainer = styled.View`
 flex: 1;
`
@connectAutoDispatch(() => ({}), {userLoginSuccess, userRegisterSuccess, getFeaturedTags})
@autobind
export default class RegisterScreen extends Component {
  static navigationOptions = ({navigation: {state}}) => ({
    header: null
  })
  state = {
    isSuccess: false
  }
  static propTypes = {}

  handleSubmit (values) {
    Keyboard.dismiss()
    let newValues = {
      ...values,
      username: values.username.toLowerCase(),
      email: values.email.toLowerCase(),
      full_name: values.fullname
    }
    return AuthApi.authRegister(newValues).then((userRes, error) => {
      const {success, token, user} = userRes
      if (success) {
        this.setState({isSuccess: true})
        setTimeout(() => {
          this.setState({isSuccess: false})
          this.props.getFeaturedTags()
          this.props.userRegisterSuccess({token, user})
        }, 2500)
      } else {
        throw new SubmissionError({
          ...userRes.errors,
          _error: 'Register failed'
        })
      }
    })
  }

  render () {
    return (
      <RegisterScreenContainer>
        <Register onSubmit={this.handleSubmit} />
        {this.state.isSuccess && <RegisterSuccess />}
      </RegisterScreenContainer>
    )
  }
}
