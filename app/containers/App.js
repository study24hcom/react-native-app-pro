import '../config'
import DebugConfig from '../config/DebugConfig'
// import firebase from 'react-native-firebase'
import React, {Component} from 'react'
import {Provider} from 'react-redux'
import RootContainer from './RootContainer'
import createStore from '../@redux'
import CodePush from 'react-native-code-push'
// import PushApi from 'api/PushApi'

// create our store
const store = createStore()

/**
 * Provides an entry point into our application.  Both index.ios.js and index.android.js
 * call this component first.
 *
 * We create our Redux store here, put it into a provider and then bring in our
 * RootContainer.
 *
 * We separate like this to play nice with React Native's hot reloading.
 */
class App extends Component {
  sync () {
    CodePush.sync(
      {
        installMode: CodePush.InstallMode.ON_NEXT_RESTART,
        updateDialog: false
      },
      this.codePushStatusDidChange.bind(this),
      this.codePushDownloadDidProgress.bind(this)
    )
  }

  codePushStatusDidChange (syncStatus) {
    switch (syncStatus) {
      case CodePush.SyncStatus.CHECKING_FOR_UPDATE:
        break
      case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
        break
      case CodePush.SyncStatus.AWAITING_USER_ACTION:
        break
      case CodePush.SyncStatus.INSTALLING_UPDATE:
        break
      case CodePush.SyncStatus.UP_TO_DATE:
        break
      case CodePush.SyncStatus.UPDATE_IGNORED:
        break
      case CodePush.SyncStatus.UPDATE_INSTALLED:
        break
      case CodePush.SyncStatus.UNKNOWN_ERROR:
        break
    }
  }

  codePushDownloadDidProgress (progress) {
    //
  }

  // subscribeToTopic() {
  //   firebase.messaging().subscribeToTopic(result => console.tron.log(result))
  // }

  // async getFcmToken () {
  //   const messaging = firebase.messaging()
  //   const fcmToken = await messaging.getToken()
  //   PushApi.registerDeviceToken({
  //     platform: 'ios',
  //     deviceToken: fcmToken
  //   }).then(result => {
  //     console.tron.log(result)
  //   })
  // }

  async componentDidMount () {
    // this.getFcmToken()
    // Sync jsBundle and apply in next launch
    // this.sync()
  }

  render () {
    return (
      <Provider store={store}>
        <RootContainer />
      </Provider>
    )
  }
}

// allow reactotron overlay for fast design in dev mode
export default (DebugConfig.useReactotron ? console.tron.overlay(App) : App)
