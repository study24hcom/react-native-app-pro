import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import { connectAutoDispatch } from '@redux/connect'
import { setInfo } from '@redux/actions/quizAction'
import { setInfoPlaylist } from '@redux/actions/playlistAction'
import { autobind } from 'core-decorators'
import ProfileMe from '../UserScreen/ProfileMe'

const UserScreenWrapper = styled.View`
  flex: 1
  `
@connectAutoDispatch(
  state => ({
    auth: state.auth.user.username
  }), {})
@connectAutoDispatch(null, {setInfo, setInfoPlaylist})
@autobind
export default class InfoUserScreen extends Component {
  static propTypes = {
    auth: PropTypes.string
  }

  handleNavigateToQuizlistDetail (quizListItem) {
    if (!this.delay) {
      this.props.setInfo(quizListItem)
      this.props.navigation.navigate('QuizListDetailScreen', {
        title: quizListItem.name
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  handleNavigateToPlaylistDetail (PlaylistListItem) {
    if (!this.delay) {
      this.props.setInfoPlaylist(PlaylistListItem.info)
      this.props.navigation.navigate('PlaylistDetailScreen', {
        title: PlaylistListItem.info.name
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  propsForUser () {
    return {
      onPressQuizList: this.handleNavigateToQuizlistDetail,
      onPressPlaylist: this.handleNavigateToPlaylistDetail,
      navigation: this.props.navigation
    }
  }

  render () {
    return <UserScreenWrapper>
      <ProfileMe {...this.propsForUser()} />
    </UserScreenWrapper>
  }
}
