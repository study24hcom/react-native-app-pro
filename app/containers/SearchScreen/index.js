import React, {Component} from 'react'
import {autobind} from 'core-decorators'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'
import {connectAwaitAutoDispatch} from '@redux/connect'
import QuizListTabScreen from './QuizListTabScreen'
import UserTabScreen from './UserTabScreen'
import SearchHeaderLayout from 'components/searchHeaderLayout'
import MatchedSearch from './MatchedSearch'
import {awaitCheckPending} from 'utils/await'
import Loading from 'components/elements/loading/index'
import {searchQuizlist, searchUser} from '@redux/actions/searchAction'
import EmptySection from 'components/elements/empty-section'

const Container = styled.ScrollView`
  flex: 1;
 `
const LoadingScreen = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`
@connectAwaitAutoDispatch(state => ({
  keywords: state.option.keywords,
  usersMatched: state.search.users.matched,
  quizListsMatched: state.search.quizLists[0],
  users: state.search.users.suggested,
  quizLists: state.search.quizLists
}), {searchUser, searchQuizlist})
@autobind
export default class SearchScreen extends Component {
  static navigationOptions = {
    header: null
  }

  static propTypes = {
    keywords: PropTypes.string,
    users: PropTypes.array,
    quizLists: PropTypes.array
  }

  componentDidMount () {
    this.props.searchUser({keywords: this.props.keywords})
    this.props.searchQuizlist({keywords: this.props.keywords})
  }

  isLoading () {
    return awaitCheckPending(this.props, 'searchUser') || awaitCheckPending(this.props, 'searchQuizlist')
  }

  renderLoading () {
    return <SearchHeaderLayout>
      <LoadingScreen>
        <Loading />
      </LoadingScreen>
    </SearchHeaderLayout>
  }

  renderEmpty () {
    return <SearchHeaderLayout>
      <EmptySection simpleLineIcon icon={'magnifier'} text={'Không tìm thấy nội dung bạn yêu cầu'} />
    </SearchHeaderLayout>
  }

  isEmpty () {
    return !this.props.usersMatched && !this.props.quizListsMatched &&
      this.props.users.length === 0 && this.props.quizLists.length === 0 && !this.isLoading()
  }

  render () {
    if (this.isLoading()) return this.renderLoading()
    if (this.isEmpty()) return this.renderEmpty()
    return <SearchHeaderLayout>
      <Container>
        <MatchedSearch quizList={this.props.quizListsMatched} user={this.props.usersMatched} />
        <UserTabScreen />
        <QuizListTabScreen />
      </Container>
    </SearchHeaderLayout>
  }
}
