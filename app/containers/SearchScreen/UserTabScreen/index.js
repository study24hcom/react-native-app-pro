import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {withNavigation} from 'react-navigation'
import styled from 'styled-components/native'
import UserViewItem from 'components/search/userItem'
import {autobind} from 'core-decorators'
import {connectAutoDispatch} from '@redux/connect'
import HeadingLine from 'components/search/HeadingLine'
import {SHAPE} from 'constants/color'
import {SIZE} from 'constants/font'

const UserWrapper = styled.ScrollView`
  flex: 1;
  `
@withNavigation
@connectAutoDispatch(state => ({
  keywords: state.option.keywords,
  users: state.search.users.suggested.slice(0, 3)
}), {})
@autobind
export default class UserTabScreen extends PureComponent {
  static propTypes = {
    users: PropTypes.array,
    keywords: PropTypes.string,
    isEmpty: PropTypes.bool
  }

  onPressUser (user) {
    if (!this.delay) {
      this.props.navigation.navigate('UserScreen', {username: user.username})
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  render () {
    if (this.props.users.length === 0) return null
    return <UserWrapper>
      <HeadingLine icon={'search'} color={SHAPE.GRAYTEXT} size={SIZE.NORMAL} title={'Mọi người'} />
      {this.props.users.map(user => <UserViewItem key={user.username} {...user}
        onPressUser={this.onPressUser} />)}
    </UserWrapper>
  }
}
