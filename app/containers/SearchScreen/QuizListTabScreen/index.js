import React, {PureComponent} from 'react'
import styled from 'styled-components/native'
import {withNavigation} from 'react-navigation'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import {setInfo} from '@redux/actions/quizAction'
import {connectAwaitAutoDispatch} from '@redux/connect'
import Loading from 'components/elements/loading'
import QuizListBox from 'components/quizlist/quizlist-item'
import HeadingLine from 'components/search/HeadingLine'
import {SHAPE} from 'constants/color'
import {SIZE} from 'constants/font'

const QuizListWrapper = styled.View`
  flex: 1;
  `
const LoaddingWrapper = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`
@withNavigation
@connectAwaitAutoDispatch(state => ({
  keywords: state.option.keywords,
  quizLists: state.search.quizLists.slice(1, 9)
}), {setInfo})
@autobind
export default class QuizListTabScreen extends PureComponent {
  static propTypes = {
    quizLists: PropTypes.array,
    keywords: PropTypes.string,
    isEmpty: PropTypes.bool
  }

  onPressQuizList (quizListItem) {
    if (!this.delay) {
      this.props.setInfo(quizListItem)
      this.props.navigation.navigate('QuizListDetailScreen', {
        title: quizListItem.name,
        slug: quizListItem.slug
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  renderLoading () {
    return <LoaddingWrapper>
      <Loading />
    </LoaddingWrapper>
  }

  render () {
    if (this.props.quizLists.length === 0) return null

    return <QuizListWrapper>
      <HeadingLine noLine color={SHAPE.BLACK} size={SIZE.NORMAL} title={'Đề thi'} />
      {this.props.quizLists.map(quizlist => <QuizListBox isPress key={quizlist.id} {...quizlist}
        onPress={this.onPressQuizList} />)}
    </QuizListWrapper>
  }
}
