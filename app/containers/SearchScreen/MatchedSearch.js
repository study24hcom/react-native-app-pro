import React, {PureComponent} from 'react'
import styled from 'styled-components/native'
import {withNavigation} from 'react-navigation'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import {setInfo} from '@redux/actions/quizAction'
import {connectAwaitAutoDispatch} from '@redux/connect'
import HeadingLine from 'components/search/HeadingLine'
import UserViewItem from 'components/search/userItem'
import {SHAPE} from 'constants/color'
import {SIZE} from 'constants/font'
import QuizListBox from 'components/quizlist/quizlist-item'

const MatchedSearchContainer = styled.View``
@withNavigation
@connectAwaitAutoDispatch(state => {
}, {setInfo})
@autobind
export default class MatchedSearch extends PureComponent {
  static propTypes = {
    quizList: PropTypes.object,
    user: PropTypes.object
  }

  onPressQuizList (quizListItem) {
    if (!this.delay) {
      this.props.setInfo(quizListItem)
      this.props.navigation.navigate('QuizListDetailScreen', {
        title: quizListItem.name,
        slug: quizListItem.slug
      })
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  onPressUser (user) {
    if (!this.delay) {
      this.props.navigation.navigate('UserScreen', {username: user.username})
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  render () {
    if (!this.props.user && !this.props.quizList) return null
    return <MatchedSearchContainer>
      <HeadingLine noLine color={SHAPE.BLACK} size={SIZE.NORMAL} title={'Kết quả hàng đầu'} />
      {this.props.user &&
      <UserViewItem {...this.props.user} onPressUser={this.onPressUser} />}
      {this.props.quizList &&
      <QuizListBox isPress {...this.props.quizList} onPress={this.onPressQuizList} />}
    </MatchedSearchContainer>
  }
}
