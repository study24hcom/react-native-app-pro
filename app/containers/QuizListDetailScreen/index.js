import React, {Component} from 'react'
import styled from 'styled-components/native'
import {awaitCheckPending} from 'utils/await'
import {Platform, NativeModules} from 'react-native'
import {autobind} from 'core-decorators'
import {ShareDialog} from 'react-native-fbsdk'
import {getQuizList, updateTimestamp} from '@redux/actions/quizAction'
import {connectAwaitAutoDispatch} from '@redux/connect'
import BookmarkSuccess from 'components/loading-lottie/BookmarkSuccess'
import ButtonRight from 'components/elements/botton-header-right'
import Loader from 'components/elements/loading-screen'
import EmptySection from 'components/elements/empty-section/index'
import {getTimestamp} from 'api/QuizApi'
import moment from 'moment'
import QuizListViewForNormal from './QuizListViewForNormal/index'
import IconLove from 'components/quizlist/icon-love/index'
import QuizListViewForCustomTime from './QuizListViewForCustomTime/index'
import {getAuthRating, getAuthScore, getRatings, getScores} from '@redux/actions/quizAddonAction'
import {Answers} from 'react-native-fabric'

const QuizListDetailWrapper = styled.View`
  flex: 1;
`
const Button = styled.View`
  flex-direction: row;
  `

@connectAwaitAutoDispatch(
  state => ({
    quizListInfo: state.quiz.info,
    quizListSlug: state.quiz.info.slug,
    quizListId: state.quiz.info.id,
    authToken: state.auth.token,
    customField: state.quiz.info.customField,
    currentTimeServer: state.quiz.property.timestamp,
    isError: state.quiz.error.isError,
    errorMessage: state.quiz.error.message,
    isShowBox: state.bookmark.isShowBox
  }),
  {getQuizList, updateTimestamp, getAuthScore, getAuthRating, getRatings, getScores}
)
@autobind
export default class QuizListDetailScreen extends Component {
  static navigationOptions = ({navigation: {state}}) => ({
    title: state.params.title ? state.params.title : 'Không tìm thấy nội dung',
    headerRight: <Button>
      <ButtonRight simpleLineIcon onPress={state.params.onPressShare} icon='share-alt' />
      <IconLove />
    </Button>
  })

  constructor (props) {
    super(props)
    this.state = {
      isFirst: true
    }
  }

  getShareLinkContent () {
    return {
      contentType: 'link',
      contentUrl: `https://tungtung.vn/de-thi/${this.props.quizListSlug}.view`
    }
  }

  async componentWillMount () {
    let time = await getTimestamp()
    let currentTimeServer = new Date(time.timestamp)
    this.props.updateTimestamp(currentTimeServer)
  }

  componentDidMount () {
    const {slug} = this.props.navigation.state.params
    if (this.props.isError && !this.props.quizListInfo.id) {
      return
    }
    if (slug) {
      this.props.getQuizList(slug)
      this.props.getAuthScore({quizListKey: slug})
      this.props.getScores({quizListKey: slug})
      this.props.getAuthRating({quizListKey: slug})
      this.props.getRatings({quizListKey: slug})
    } else {
      if (!this.props.quizListId || !this.props.quizListInfo.user) {
        this.props.getQuizList(this.props.quizListSlug)
      }
      this.props.getAuthScore({quizListKey: this.props.quizListSlug})
      this.props.getScores({quizListKey: this.props.quizListSlug})
      this.props.getAuthRating({quizListKey: this.props.quizListSlug})
      this.props.getRatings({quizListKey: this.props.quizListSlug})
    }
    this.props.navigation.setParams({
      onPressOption: this.onPressOption,
      onPressShare: this.shareLinkWithShareDialog
    })
  }

  isCustomTimeOpenPlay () {
    let momentCurrentTime = moment(this.props.currentTimeServer)
    let momentOpenTime = moment(this.props.customField.openTime)
    return momentCurrentTime.isAfter(momentOpenTime)
  }

  isCustomTimeShowResult () {
    let momentCurrentTime = moment(this.props.currentTimeServer)
    let momentEndTime = moment(this.props.customField.endTime)
    return momentCurrentTime.isAfter(momentEndTime)
  }

  isLoadingFirst () {
    return awaitCheckPending(this.props, 'getQuizList') && this.state.isFirst
  }

  isRefreshing () {
    return ((awaitCheckPending(this.props, 'getRatings') || awaitCheckPending(this.props, 'getScores') ||
      awaitCheckPending(this.props, 'getQuizList') || awaitCheckPending(this.props, 'getAuthScore')) && (!this.state.isFirst))
  }

  async onRefresh () {
    if (this.props.customField.isCustomTime) {
      let time = await getTimestamp()
      let currentTimeServer = new Date(time.timestamp)
      this.props.updateTimestamp(currentTimeServer)
    }
    if (this.state.isFirst) {
      this.setState({isFirst: false})
    }
    this.props.getQuizList(this.props.quizListSlug)
    this.props.getAuthScore({quizListKey: this.props.quizListSlug})
    this.props.getScores({quizListKey: this.props.quizListSlug})
    this.props.getRatings({quizListKey: this.props.quizListSlug})
    this.props.getAuthRating({quizListKey: this.props.quizListSlug})
  }

  autoDelay (callback) {
    if (!this.delay) {
      callback()
      this.delay = true
    }
    setTimeout(() => {
      this.delay = false
    }, 600)
  }

  shareLinkWithShareDialog () {
    var tmp = this
    ShareDialog.canShow(this.getShareLinkContent()).then(
      function (canShow) {
        if (canShow) {
          return ShareDialog.show(tmp.getShareLinkContent())
        }
      }
    )
  }

  _handlePlay (e) {
    Answers.logCustom('PlayQuizList', { slug: this.props.quizListInfo.slug })
    e.preventDefault()
    if (Platform.OS === 'ios') {
      this.autoDelay(() => {
        this.props.navigation.navigate('PlayQuizListScreen', {
          slug: this.props.quizListSlug
        })
      })
    } else {
      NativeModules.ActivityStarter.navigateToPlayer(this.props.quizListSlug, this.props.authToken, false)
    }
  }

  render () {
    if (this.isLoadingFirst()) {
      return <Loader loading={this.isLoadingFirst()} />
    }
    if (this.props.isError && !this.props.quizListInfo.name) {
      return (
        <EmptySection simpleLineIcon icon={'people'} text={'Bạn không có quyền truy cập đề thi này'} />)
    }
    return (
      <QuizListDetailWrapper>
        {!this.props.customField.isCustomTime &&
        <QuizListViewForNormal onPlay={this._handlePlay} onRefresh={this.onRefresh}
          isFirst={this.state.isFirst} isRefreshing={this.isRefreshing()} />}
        {this.props.customField.isCustomTime
          ? <QuizListViewForCustomTime
            isRefreshing={this.isRefreshing()}
            isFirst={this.state.isFirst}
            onPlay={this._handlePlay}
            onRefresh={this.onRefresh}
            isOpenPlay={this.isCustomTimeOpenPlay()}
            isEndShowResult={this.isCustomTimeShowResult()} /> : null}
        {this.props.isShowBox && <BookmarkSuccess modalVisible={this.props.isShowBox} />}
      </QuizListDetailWrapper>
    )
  }
}
