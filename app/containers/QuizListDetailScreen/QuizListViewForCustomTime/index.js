import React, {Component} from 'react'
import {Alert, RefreshControl} from 'react-native'
import {awaitCheckSuccess, awaitCheckPending} from 'utils/await'
import PropTypes from 'prop-types'
import {withNavigation} from 'react-navigation'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {autobind} from 'core-decorators'
import {getQuizList} from '@redux/actions/quizAction'
import QuizListBox from 'components/quizlist/quizlist-item'
import {
  ButtonPlay, ButtonWrapper, MarkdownDisplay, markdownStyles, styles,
  QuizListDetailContainer, QuizListDetailWrapper, ButtonCountdown, BoxNotification
} from '../style'
import TagList from 'components/tag/tag-list/index'
import QuizListViewForNormal from '../QuizListViewForNormal'
import CountdownTime from 'components/elements/countdown-time'
import AlertWaitingOpen from './AlertWaitingOpen'
import HeadingLine from 'components/elements/heading-line'
import AlertWaitingResult from './AlertWaitingResult'
import TextIcon from 'components/elements/text-icon'
import Clearfix from 'components/elements/clearfix'
import {SIZE} from 'constants/font'
import {SHAPE} from 'constants/color'
import Text from '../../../components/elements/text/index'

@withNavigation
@connectAwaitAutoDispatch(
  state => ({
    quizListInfo: state.quiz.info,
    quizListTags: state.quiz.info.tags,
    ratings: state.quizAddon.ratings.data,
    paginationRatings: state.quizAddon.ratings.pagination,
    quizListSlug: state.quiz.info.slug,
    isAuthCreated: (state.quiz.info.user &&
      state.quiz.info.user.id === state.auth.user.id) ||
    state.auth.user.admin,
    isAuthScore: !!state.quizAddon.authScore.id,
    customField: state.quiz.info.customField,
    openTime: state.quiz.info.customField.openTime,
    endTime: state.quiz.info.customField.endTime
  }), {getQuizList})
@autobind
export default class QuizListViewForCustomTime extends Component {
  static propTypes = {
    quizListInfo: PropTypes.shape(QuizListBox.propTypes),
    isAuthScore: PropTypes.bool,
    onPlay: PropTypes.func,
    isOpenPlay: PropTypes.bool,
    isFirst: PropTypes.bool,
    onRefresh: PropTypes.func,
    isEndShowResult: PropTypes.bool,
    isRefreshing: PropTypes.bool
  }

  constructor (props) {
    super(props)
    this.state = {
      isOpenPlay: props.isOpenPlay,
      isEndShowResult: props.isEndShowResult
    }
  }

  _onRefresh () {
    this.props.onRefresh()
  }

  handleFinishCountdownStart () {
    this.setState({
      isOpenPlay: true
    })
  }

  handleFinishShowResult () {
    this.setState({
      isEndShowResult: true
    })
  }

  isShowPlay () {
    if (!awaitCheckSuccess(this.props, 'getAuthScore')) return false
    if (this.props.isOpenPlay || this.state.isOpenPlay) {
      if (this.props.isAuthScore && !this.isShowResult()) {
        return false
      } else return true
    }
    return false
  }

  isShowBoxInfo () {
    return ((awaitCheckSuccess(this.props, 'getQuizList') && this.props.isFirst) || !awaitCheckPending(this.props, 'getQuizList') || !this.props.isFirst)
  }

  isOpenPlay () {
    return this.props.isOpenPlay || this.state.isOpenPlay
  }

  isShowTimeCountStart () {
    return !this.props.isOpenPlay && !this.state.isOpenPlay
  }

  isShowResult () {
    return this.state.isEndShowResult || this.props.isEndShowResult
  }

  isShowTimeCountShowResult () {
    return !this.props.isEndShowResult && !this.state.isEndShowResult
  }

  renderCountdownTimeStart () {
    return (
      this.isShowTimeCountStart() &&
      <ButtonCountdown>
        <HeadingLine noLine title={'Thời gian mở đề còn lại'} />
        <AlertWaitingOpen>
          <CountdownTime color={'#f2cc37'} deadline={this.props.openTime} onFinish={this.handleFinishCountdownStart} />
        </AlertWaitingOpen>
      </ButtonCountdown>
    )
  }

  renderCountdownTimeShowResult () {
    return (
      this.isShowTimeCountShowResult() && this.isOpenPlay() &&
      awaitCheckSuccess(this.props, 'getAuthScore') && this.props.isAuthScore &&
      <ButtonCountdown>
        <HeadingLine noLine title={'Thời gian mở đáp án còn lại'} />
        <AlertWaitingResult>
          <CountdownTime color={SHAPE.GREEN} deadline={this.props.endTime}
            onFinish={this.handleFinishShowResult} />
        </AlertWaitingResult>
      </ButtonCountdown>
    )
  }

  isShowDescription () {
    return (this.isShowTimeCountShowResult() && this.isOpenPlay() && awaitCheckSuccess(this.props, 'getAuthScore') &&
      this.props.isAuthScore) || (this.isShowTimeCountStart())
  }

  renderDescription () {
    return (
      (this.isShowDescription()) &&
      <ButtonCountdown>
        <HeadingLine noLine title={'Mô tả'} />
        <BoxNotification style={styles.shadow}>
          <MarkdownDisplay style={markdownStyles}>{this.props.quizListInfo.description}</MarkdownDisplay>
        </BoxNotification>
      </ButtonCountdown>
    )
  }

  renderIsPlay () {
    return (
      this.isOpenPlay() && !this.isShowTimeCountStart() && !this.isShowResult() && !this.props.isAuthScore && awaitCheckSuccess(this.props, 'getAuthScore') &&
      <ButtonCountdown>
        <HeadingLine noLine title={'Đã đến giờ làm bài'} />
        <BoxNotification style={styles.shadow}>
          <Text fontSize={16} color={SHAPE.GRAYTEXT}>{`⏰  Đã đến giờ làm bài, bắt đầu nào.\n`}</Text>
          <Text fontSize={16}
            color={SHAPE.GRAYTEXT}>{`💪  Ba mẹ tin bạn, bạn bè tin bạn, đội ngũ #tungtungvn tin bạn.\n`}</Text>
          <Text fontSize={16} color={SHAPE.GRAYTEXT}>{`😉  Hãy bình tĩnh, tự tin để làm bài thật tốt nhé.`}</Text>
        </BoxNotification>
      </ButtonCountdown>
    )
  }

  autoDelay (callback) {
    if (!this.delay) {
      callback()
      this.delay = true
      setTimeout(() => {
        this.delay = false
      }, 1200)
    }
  }

  onPressUser (user) {
    this.autoDelay(() => {
      this.props.navigation.navigate('UserScreen', {
        username: user.username
      })
    })
  }

  onPressItemTags (tagItem) {
    this.autoDelay(() => {
      this.props.navigation.navigate('TagsScreen', {title: tagItem.name})
    })
  }

  renderIsShowPlay () {
    return (
      this.isShowPlay() && !this.props.isAuthScore && awaitCheckSuccess(this.props, 'getAuthScore') &&
      <ButtonWrapper>
        <ButtonPlay activeOpacity={1} onPress={this.props.onPlay}>
          <TextIcon center color={'#fff'} fontWeight={600} size={SIZE.TITLE} icon={'play'}>Bắt đầu làm bài</TextIcon>
        </ButtonPlay>
      </ButtonWrapper>

    )
  }

  onRemind () {
    Alert.alert('Thông báo', 'Tính năng này đang được phát triển.')

    // if (this.isShowTimeCountStart()) {
    //   Alert.alert(
    //     'Thêm vào nhắc nhở', 'Bạn có muốn thêm đề thi này vào nhắc nhỡ. Chúng tôi sẽ thông báo cho bạn ngay khi bắt đầu.',
    //     [
    //       {text: 'Bỏ Qua', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
    //       {text: 'Thêm Vào', onPress: () => console.log('OK Pressed')}
    //     ],
    //     {cancelable: false}
    //   )
    // } else {
    //   Alert.alert('Thêm vào nhắc nhở', 'Bạn có muốn thêm đề thi này vào nhắc nhỡ. Chúng tôi sẽ thông báo cho bạn ngay khi có kết quả.',
    //     [
    //       {text: 'Bỏ Qua', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
    //       {text: 'Thêm Vào', onPress: () => console.log('OK Pressed')}
    //     ],
    //     {cancelable: false})
    // }
  }

  renderButtonRemind () {
    return (
      this.isShowDescription() &&
      <ButtonWrapper>
        <ButtonPlay activeOpacity={1} onPress={this.onRemind}>
          <TextIcon center color={'#fff'} fontWeight={600} simpleLineIcon size={SIZE.TITLE}
            icon={'calendar'}>Nhắc nhở</TextIcon>
        </ButtonPlay>
      </ButtonWrapper>
    )
  }

  render () {
    const {quizListInfo, isFirst, onPlay, isRefreshing, onRefresh} = this.props
    if (this.isOpenPlay() && this.isShowResult()) {
      return <QuizListViewForNormal refreshScore onRefresh={onRefresh} isRefreshing={isRefreshing}
        isFirst={isFirst} onPlay={onPlay} />
    }
    return (
      <QuizListDetailWrapper>
        {this.isShowBoxInfo() &&
        <QuizListDetailContainer refreshControl={
          <RefreshControl refreshing={this.props.isRefreshing} onRefresh={this._onRefresh} title='Loading...' />}>
          <QuizListBox isPressUser onPressUser={this.onPressUser} {...quizListInfo} isPress={false}>
            {(!this.isShowDescription())
              ? <MarkdownDisplay style={markdownStyles}>{this.props.quizListInfo.description}</MarkdownDisplay>
              : <Clearfix height={16} />}
            {this.props.quizListTags.length > 0 && (
              <TagList onPress={this.onPressItemTags} tags={this.props.quizListTags} />)}
          </QuizListBox>
          {this.renderCountdownTimeStart()}
          {this.renderIsPlay()}
          {this.renderCountdownTimeShowResult()}
          {this.renderDescription()}
        </QuizListDetailContainer>}
        {this.renderIsShowPlay()}
        {this.renderButtonRemind()}
      </QuizListDetailWrapper>
    )
  }
}
