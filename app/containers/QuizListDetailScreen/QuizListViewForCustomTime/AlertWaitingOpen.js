import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import {StyleSheet} from 'react-native'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import moment from 'moment'
import {SHAPE} from 'constants/color'
import Clearfix from 'components/elements/clearfix'
import Text from 'components/elements/text'

const AlertWaitingResultContainer = styled.View``

const BoxNotification = styled.View`
  padding: 16px;
  background-color: #fff;
  margin-vertical: 4px;
`
const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#d4d4d4',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 3,
    shadowOpacity: 0.2
  }
})
@connect(state => ({
  quizListSlug: state.quiz.info.slug,
  openTime: state.quiz.info.customField.openTime
}))
export default class AlertWaitingOpen extends PureComponent {
  static propTypes = {
    openTime: PropTypes.string
  }

  render () {
    let date = moment(this.props.openTime).format('DD/MM/YYYY').toString()
    let time = moment(this.props.openTime).format('HH:mm').toString()
    let alertWating =
      `Đề thi sẽ được mở vào ngày ${date} lúc ${time}\n\nClick Nhắc nhở để được thông báo khi đề được mở.`
    return (
      <AlertWaitingResultContainer>
        <BoxNotification style={styles.shadow}>
          {this.props.children}
          <Clearfix height={16} />
          <Text fontSize={15} color={SHAPE.GRAYTEXT}>{alertWating}</Text>
        </BoxNotification>
      </AlertWaitingResultContainer>
    )
  }
}
