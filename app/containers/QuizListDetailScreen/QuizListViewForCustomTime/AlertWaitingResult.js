import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import {StyleSheet} from 'react-native'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import Text from 'components/elements/text'
import moment from 'moment'
import {SHAPE} from 'constants/color'
import Clearfix from 'components/elements/clearfix'

const AlertWaitingResultContainer = styled.View`
  position: relative;
  `
const BoxNotification = styled.View`
  padding: 16px;
  background-color: #fff;
  margin-vertical: 4px;
`
const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#d4d4d4',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 3,
    shadowOpacity: 0.2
  }
})
@connect(state => ({
  quizListSlug: state.quiz.info.slug,
  endTime: state.quiz.info.customField.endTime
}))
export default class AlertWaitingResult extends PureComponent {
  static propTypes = {
    openTime: PropTypes.string
  }

  render () {
    let date = moment(this.props.endTime).format('DD/MM/YYYY').toString()
    let time = moment(this.props.endTime).format('HH:mm').toString()
    let alertWating =
      `Đáp án sẽ được mở vào ngày ${date} lúc ${time}\n\nClick Nhắc Nhở để được thông báo khi đáp án được mở.`
    return (
      <AlertWaitingResultContainer>
        <BoxNotification style={styles.shadow}>
          {this.props.children}
          <Clearfix height={16} />
          <Text fontSize={14} color={SHAPE.GRAYTEXT}>{alertWating}</Text>
        </BoxNotification>
      </AlertWaitingResultContainer>
    )
  }
}
