import styled from 'styled-components/native'
import {Dimensions, StyleSheet} from 'react-native'
import Markdown from 'react-native-simple-markdown'
import {SHAPE, TEXT} from 'constants/color'
import {SIZE} from 'constants/font'

const {width} = Dimensions.get('window')
const QuizListDetailWrapper = styled.View`
  flex: 1;
`
const QuizListDetailContainer = styled.ScrollView`
  flex: 1;
`
const MarkdownDisplay = styled(Markdown)``
const ButtonWrapper = styled.View`
  position: relative;
  bottom: 0;
  left: 0;
  right: 0;
`
const ButtonCountdown = styled.View``
const ButtonPlay = styled.TouchableOpacity`
  background-color: ${props => (props.color ? props.color : SHAPE.GREEN)};
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding: 12px 16px;
`
const Button = styled.TouchableOpacity`
  background-color: ${SHAPE.PRIMARYBOLD};
  justify-content: center;
  align-items: center;
  padding: 12px 16px;
  flex-direction: row;
  `
const markdownStyles = {
  link: {
    color: SHAPE.PRIMARY
  },
  text: {
    color: TEXT.GRAY,
    fontSize: SIZE.NORMAL,
    fontFamily: 'OpenSans-Regular'
  },
  image: {
    width: width - 40,
    height: 180
  },
  paragraph: {
    // marginTop: 0
  }
}
const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#d4d4d4',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 3,
    shadowOpacity: 0.2
  }
})
const BoxNotification = styled.View`
  padding: 16px;
  background-color: #fff;
  margin-vertical: 4px;
`

export {
  QuizListDetailWrapper,
  QuizListDetailContainer,
  MarkdownDisplay,
  ButtonWrapper,
  ButtonPlay,
  markdownStyles,
  Button,
  ButtonCountdown,
  BoxNotification,
  styles
}
