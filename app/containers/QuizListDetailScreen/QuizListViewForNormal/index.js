import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {withNavigation} from 'react-navigation'
import {RefreshControl, NativeModules, Platform} from 'react-native'
import {awaitCheckSuccess, awaitCheckPending} from 'utils/await'
import {autobind} from 'core-decorators'
import {SIZE} from 'constants/font'
import * as Animatable from 'react-native-animatable'
import {getScores, getRatings} from '@redux/actions/quizAddonAction'
import {connectAwaitAutoDispatch} from '@redux/connect'
import Clearfix from 'components/elements/clearfix'
import QuizListBox from 'components/quizlist/quizlist-item'
import BoxRatings from 'components/quizlist/box-ratings'
import BoxScores from 'components/quizlist/box-scores'
import BoxAuthScore from 'components/quizlist/box-auth-score'
import TagList from 'components/tag/tag-list'
import TextIcon from 'components/elements/text-icon/index'
import {
  QuizListDetailWrapper,
  QuizListDetailContainer,
  MarkdownDisplay,
  ButtonWrapper,
  ButtonPlay,
  markdownStyles
} from '../style'
import {Answers} from 'react-native-fabric'

@withNavigation
@connectAwaitAutoDispatch(
  state => ({
    quizListInfo: state.quiz.info,
    quizListSlug: state.quiz.info.slug,
    quizListTags: state.quiz.info.tags,
    isAuthScore: !!state.quizAddon.authScore.id,
    ratings: state.quizAddon.ratings.data,
    scores: state.quizAddon.scores.data,
    authToken: state.auth.token,
    authScore: state.quizAddon.authScore,
    paginationRatings: state.quizAddon.ratings.pagination,
    paginationScores: state.quizAddon.scores.pagination,
    userId: state.auth.user._id,
    isShowBox: state.bookmark.isShowBox
  }),
  {getRatings, getScores}
)
@autobind
export default class QuizListViewForNormal extends Component {
  static propTypes = {
    quizListInfo: PropTypes.shape(),
    quizListSlug: PropTypes.string,
    quizListTags: PropTypes.array,
    isAuthScore: PropTypes.bool,
    ratings: PropTypes.array,
    scores: PropTypes.array,
    authScore: PropTypes.object,
    paginationRatings: PropTypes.shape(),
    paginationScores: PropTypes.shape(),
    userId: PropTypes.string,
    isShowBox: PropTypes.bool,
    authToken: PropTypes.string,
    isFirst: PropTypes.bool,
    onRefresh: PropTypes.func,
    onPlay: PropTypes.func,
    refreshScore: PropTypes.bool,
    isRefreshing: PropTypes.bool
  }

  componentDidMount () {
    if (this.props.refreshScore) {
      this.props.getRatings({quizListKey: this.props.quizListSlug})
      this.props.getScores({quizListKey: this.props.quizListSlug})
    }
  }

  _onRefresh () {
    this.props.onRefresh()
  }

  isLoadingFirst () {
    return awaitCheckPending(this.props, 'getQuizList') && this.props.isFirst
  }

  isShowBoxInfo () {
    return ((awaitCheckSuccess(this.props, 'getQuizList') && this.props.isFirst) || !awaitCheckPending(this.props, 'getQuizList') || !this.props.isFirst)
  }

  autoDelay (callback) {
    if (!this.delay) {
      callback()
      this.delay = true
    }
    setTimeout(() => {
      this.delay = false
    }, 600)
  }

  onPressRating () {
    this.autoDelay(() => {
      this.props.navigation.navigate('RatingDetailScreen', {
        totalItem: this.props.paginationRatings.totalItem
      })
    })
  }

  onPressScore () {
    this.autoDelay(() => {
      this.props.navigation.navigate('ScoreDetailScreen', {
        totalItem: this.props.paginationScores.totalItem
      })
    })
  }

  onPressUser (user) {
    this.autoDelay(() => {
      this.props.navigation.navigate('UserScreen', {
        username: user.username
      })
    })
  }

  onPressViewHistory () {
    Answers.logCustom('ViewHistory', {slug: this.props.quizListInfo.slug})
    if (Platform.OS === 'ios') {
      this.props.navigation.navigate('HistoryQuizListScreen', {
        slug: this.props.quizListSlug,
        historyId: this.props.authScore._id,
        quizListId: this.props.quizListInfo._id
      })
    } else {
      NativeModules.ActivityStarter.navigateToPlayer(this.props.quizListInfo.slug, this.props.authToken, true)
    }
  }

  onPressItemTags (tagItem) {
    this.autoDelay(() => {
      this.props.navigation.navigate('TagsScreen', {title: tagItem.name})
    })
  }

  render () {
    const {
      quizListInfo,
      ratings,
      paginationRatings,
      scores,
      paginationScores
    } = this.props
    return (
      <QuizListDetailWrapper>
        {this.isShowBoxInfo() &&
        <QuizListDetailContainer refreshControl={
          <RefreshControl refreshing={this.props.isRefreshing}
            onRefresh={this._onRefresh}
            title='Loading...' />}>
          <QuizListBox isPressUser onPressUser={this.onPressUser}
            {...quizListInfo} isPress={false}>
            <Clearfix height={16} />
            <MarkdownDisplay styles={markdownStyles}>
              {quizListInfo.description}
            </MarkdownDisplay>
            <Clearfix height={16} />
            {this.props.quizListTags.length > 0 && (
              <TagList
                onPress={this.onPressItemTags}
                tags={this.props.quizListTags}
              />
            )}
          </QuizListBox>
          {awaitCheckSuccess(this.props, 'getAuthScore') &&
          this.props.authScore._id && (
            <Animatable.View style={{flex: 1}} animation='fadeIn'>
              <BoxAuthScore
                score={this.props.authScore}
                onPress={this.onPressViewHistory} />
            </Animatable.View>)}
          {awaitCheckSuccess(this.props, 'getRatings') &&
          <Animatable.View style={{flex: 1}} animation='fadeIn'>
            <BoxRatings
              onPressUser={this.onPressUser}
              ratings={ratings}
              onPressViewAll={this.onPressRating}
              pagination={paginationRatings} />
          </Animatable.View>}
          {awaitCheckSuccess(this.props, 'getScores') &&
          <Animatable.View style={{flex: 1}} animation='fadeIn'>
            <BoxScores
              onPressUser={this.onPressUser}
              scores={scores}
              onPressViewAll={this.onPressScore}
              pagination={paginationScores} />
          </Animatable.View>}
        </QuizListDetailContainer>}
        <ButtonWrapper>
          <ButtonPlay activeOpacity={1} onPress={this.props.onPlay}>
            <TextIcon center color={'#fff'} fontWeight={600} size={SIZE.TITLE} icon={'play'}>Bắt đầu làm bài</TextIcon>
          </ButtonPlay>
        </ButtonWrapper>
      </QuizListDetailWrapper>
    )
  }
}
