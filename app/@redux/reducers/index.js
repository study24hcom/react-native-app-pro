import {reducer as github} from './github'
import {reducer as nav} from './navigation'
import {reducer as awaitReducer} from 'redux-await'
import {reducer as reduxForm} from 'redux-form'
import home from './home'
import quiz from './quiz'
import user from './user'
import quizAddon from './quizAddon'
import auth from './auth'
import quizLists from './quizLists'
import option from './option'
import activity from './activity'
import playlist from './playlist'
import profile from './profile'
import follow from './follow'
import notification from './notification'
import tag from './tag'
import histories from './histories'
import category from './category'
import search from './search'
import bookmark from './bookmark'
// import * as Startup from './StartupRedux'

export default {
  nav,
  github,
  form: reduxForm,
  await: awaitReducer,
  home,
  histories,
  quiz,
  quizAddon,
  auth,
  quizLists,
  option,
  activity,
  playlist,
  profile,
  user,
  follow,
  notification,
  tag,
  category,
  search,
  bookmark
  // startup: Startup.reducer
}
