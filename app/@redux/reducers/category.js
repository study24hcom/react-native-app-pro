import update from 'react-addons-update'
import { GET_QUIZLISTS_CATEGORY, SET_QUIZ_LISTS_CATEGORY_LOAD_MORE } from '../actions/caterogyAction'
import payloadConstant from 'constants/payload'
import { cleanQuizLists, cleanPagination } from 'utils/clean'
import categories from 'constants/categories'

const defaultListsData = {
  data: [],
  loadMore: false,
  pagination: {
    itemPerPage: 10,
    page: 1
  }
}

const initialState = {
  [categories[0].keyRequest]: defaultListsData,
  [categories[1].keyRequest]: defaultListsData,
  [categories[2].keyRequest]: defaultListsData,
  [categories[3].keyRequest]: defaultListsData,
  [categories[4].keyRequest]: defaultListsData,
  [categories[5].keyRequest]: defaultListsData,
  [categories[6].keyRequest]: defaultListsData
}

export default function createReducer (state = initialState, action) {
  switch (action.type) {
    case GET_QUIZLISTS_CATEGORY:
      return getQuizListsCategory(state, action)
    case SET_QUIZ_LISTS_CATEGORY_LOAD_MORE:
      return setQuizListCategoryLoadMore(state, action)
    default:
      return state
  }
}

export function setQuizListCategoryLoadMore (state, {categoryKey, loadMore}) {
  return update(state, {
    [categoryKey]: {
      loadMore: {$set: loadMore}
    }
  })
}

export function getQuizListsCategory (state, {categoryKey, payload, loadMore}) {
  const keyPayload = `${payloadConstant.GET_QUIZLISTS_CATEGORY}_${categoryKey}`
  let quizLists = payload[keyPayload].quiz_list ? payload[keyPayload].quiz_list : payload[keyPayload].quiz_lists
  return update(state, {
    [categoryKey]: {
      data: {
        $apply: oldQuizLists => {
          if (loadMore) {
            return [...oldQuizLists, ...cleanQuizLists(quizLists)]
          } else {
            return cleanQuizLists(quizLists)
          }
        }
      },
      pagination: {
        $set: cleanPagination(payload[keyPayload].pagination)
      }
    }
  })
}
