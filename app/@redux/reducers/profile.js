import { GET_PROFILE_INFO } from '../actions/profileAction'
import update from 'react-addons-update'
import { cleanUser } from 'utils/clean'

const initialState = {
  info: {
    username: '',
    followed_users: []
  },
  quizListsCreated: {}
}

export default function createReducer (state = initialState, action) {
  switch (action.type) {
    case GET_PROFILE_INFO:
      return updateInfo(state, action)
    default:
      return state
  }
}

export function updateInfo (state, { payload }) {
  return update(state, {
    info: {
      $set: cleanUser(payload.getProfileInfo)
    }
  })
}
