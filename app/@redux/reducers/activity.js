import {GET_ACTIVITIES, LOAD_MORE_ACTIVITIES} from '../actions/activityAction'
import update from 'react-addons-update'

import {cleanActivities, cleanPagination} from 'utils/clean'

const initialState = {
  list: {
    data: [],
    pagination: {}
  }
}

export default function createReducer (state = initialState, action) {
  switch (action.type) {
    case GET_ACTIVITIES:
      return getActivities(state, action)
    case LOAD_MORE_ACTIVITIES:
      return loadMoreActivities(state, action)
    default:
      return state
  }
}

export function getActivities (state, {payload: {getActivities}}) {
  return update(state, {
    list: {
      data: {
        $set: cleanActivities(getActivities.data)
      },
      pagination: {
        $set: cleanPagination(getActivities.pagination)
      }
    }
  }
  )
}

export function loadMoreActivities (state, {payload: {loadMoreActivities}}) {
  return update(state, {
    list: {
      data: {
        $apply: data => [...data, ...cleanActivities(loadMoreActivities.data)]
      },
      pagination: {
        $set: cleanPagination(loadMoreActivities.pagination)
      }
    }
  }
  )
}
