import {
  SEARCH_KEYWORD, SEARCH_USER, SEARCH_QUIZLIST, SET_QUIZLIST_DEFAULT, SET_USER_DEFAULT
} from '../actions/searchAction'
import update from 'react-addons-update'
import {cleanUser, cleanUsers, cleanQuizLists, cleanKeyWords} from 'utils/clean'

const initialState = {
  users: {
    matched: {},
    suggested: []
  },
  quizLists: [],
  keywords: {
    keywordSuggest: [],
    recentSuggest: []
  }
}

export default function createReducer (state = initialState, action) {
  switch (action.type) {
    case SEARCH_KEYWORD:
      return searchKeyWord(state, action)
    case SEARCH_USER:
      return searchUser(state, action)
    case SEARCH_QUIZLIST:
      return searchQuizlist(state, action)
    case SET_QUIZLIST_DEFAULT:
      return setQuizListDeafault(state, action)
    case SET_USER_DEFAULT:
      return setUserDeafault(state, action)
    default:
      return state
  }
}

function searchKeyWord (state, {payload: {searchKeyWord}}) {
  if (!searchKeyWord.success) {
    return update(state, {
      keywords: {
        keywordSuggest: {
          $set: []
        },
        recentSuggest: {
          $set: []
        }
      }
    })
  } else {
    return update(state, {
      keywords: {
        keywordSuggest: {
          $set: cleanKeyWords(searchKeyWord.data.keywordSuggest)
        },
        recentSuggest: {
          $set: cleanKeyWords(searchKeyWord.data.recentSuggest)
        }
      }
    })
  }
}

function searchUser (state, {payload: {searchUser}}) {
  return update(state, {
    users: {
      matched: {
        $set: cleanUser(searchUser.matched)
      },
      suggested: {
        $set: cleanUsers(searchUser.suggested)
      }
    }
  })
}

function searchQuizlist (state, {payload: {searchQuizlist}}) {
  return update(state, {
    quizLists: {
      $set: cleanQuizLists(searchQuizlist.quiz_lists)
    }
  })
}

function setQuizListDeafault (state, {quizLists}) {
  return update(state, {
    quizLists: {
      $set: quizLists
    }
  })
}

function setUserDeafault (state, {users}) {
  return update(state, {
    users: {
      suggested: {
        $set: users
      },
      matched: {
        $set: {}
      }
    }
  })
}
