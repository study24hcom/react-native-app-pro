import update from 'react-addons-update'
import payloadConstant from 'constants/payload'
import {cleanQuizLists, cleanGallerySliders} from 'utils/clean'
import categories from 'constants/categories'
import {
  GET_HOME_QUIZLISTS_CATEGORY,
  GET_QUIZLISTS_TOP_DAY,
  SET_HOME_QUIZLISTS_CATEGORY_LOADED,
  GET_HOME_GALLERY_SLIDER
} from '../actions/homeAction'

const initialState = {
  quizListsCategory: {
    [categories[0].keyRequest]: [],
    [categories[1].keyRequest]: [],
    [categories[2].keyRequest]: [],
    [categories[3].keyRequest]: [],
    [categories[4].keyRequest]: [],
    [categories[5].keyRequest]: [],
    [categories[6].keyRequest]: []
  },
  quizListsTopDay: [],
  isQuizListsCategoryLoaded: false,
  gallerySlider: []
}

export default function createReducer (state = initialState, action) {
  switch (action.type) {
    case GET_HOME_QUIZLISTS_CATEGORY:
      return getHomeQuizListsCategory(state, action)
    case SET_HOME_QUIZLISTS_CATEGORY_LOADED:
      return setQuizListsCategoryLoaded(state)
    case GET_HOME_GALLERY_SLIDER:
      return getGallerySlider(state, action)
    case GET_QUIZLISTS_TOP_DAY:
      return getQuizListsTopDay(state, action)
    default:
      return state
  }
}

export function getHomeQuizListsCategory (state, {categoryKey, payload}) {
  const keyPayload = `${payloadConstant.GET_HOME_QUIZLISTS_CATEGORY}_${categoryKey}`
  return update(state, {
    quizListsCategory: {
      [categoryKey]: {
        $set: cleanQuizLists(payload[keyPayload].quiz_list)
      }
    }
  })
}

export function setQuizListsCategoryLoaded (state) {
  return update(state, {
    isQuizListsCategoryLoaded: {
      $set: true
    }
  })
}

export function getQuizListsTopDay (state, {payload: {getQuizListsTopDay}}) {
  return update(state, {
    quizListsTopDay: {
      $set: cleanQuizLists(
        getQuizListsTopDay.map(quizlist => quizlist.quiz_list_id)
      )
    }
  })
}

export function getGallerySlider (state, {payload: {getGallerySlider}}) {
  return update(state, {
    gallerySlider: {
      $set: cleanGallerySliders(getGallerySlider.images)
    }
  })
}
