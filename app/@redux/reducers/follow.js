import update from 'react-addons-update'
import {
  SET_FOLLOW_USER,
  UN_FOLLOW_USER,
  SET_FOLLOW_TAG,
  UN_FOLLOW_TAG,
  GET_TAGS_FOLLOWED,
  GET_USERS_FOLLOWED,
  UPDATE_IS_SHOW_BOX
} from '../actions/followAction'
import {cleanUsers, cleanPagination, cleanTagsGroup} from 'utils/clean'

const initialState = {
  followedUsers: [],
  isShowBox: false,
  followedTags: [],
  tags: {
    data: [],
    pagination: {}
  },
  users: {
    data: [],
    pagination: {}
  }
}

export default function createReducer (state = initialState, action) {
  switch (action.type) {
    case SET_FOLLOW_USER:
      return setFollowUser(state, action)
    case UN_FOLLOW_USER:
      return unFollowUser(state, action)
    case SET_FOLLOW_TAG:
      return setFollowTag(state, action)
    case UN_FOLLOW_TAG:
      return unFollowTag(state, action)
    case GET_TAGS_FOLLOWED:
      return getTagsFollowed(state, action)
    case GET_USERS_FOLLOWED:
      return getUsersFollowed(state, action)
    case UPDATE_IS_SHOW_BOX:
      return updateIsShowBox(state, action)
    default:
      return state
  }
}

export function updateFollowUser (state, {userId}, followed = true) {
  const userIndex = state.followedUsers.findIndex(f => f.id === userId)
  if (userIndex < 0) {
    return update(state, {
      followedUsers: {
        $push: [{id: userId, followed}]
      }
    })
  } else {
    return update(state, {
      followedUsers: {
        [userIndex]: {
          followed: {
            $set: followed
          }
        }
      }
    })
  }
}

export function updateIsShowBox (state, {success}) {
  return update(state, {
    isShowBox: {
      $set: success
    }
  })
}

export function updateFollowTag (state, {tagName, success}, followed = true) {
  const tagIndex = state.followedTags.findIndex(tag => tag.name === tagName)
  if (tagIndex < 0) {
    return update(state, {
      followedTags: {
        $push: [{name: tagName, followed, success}]
      }
    })
  } else {
    return update(state, {
      followedTags: {
        [tagIndex]: {
          followed: {
            $set: followed
          }
        }
      }
    })
  }
}

export function setFollowUser (state, {userId}) {
  return updateFollowUser(state, {userId}, true)
}

export function unFollowUser (state, {userId}) {
  return updateFollowUser(state, {userId}, false)
}

export function setFollowTag (state, {tagName, success}) {
  return updateFollowTag(state, {tagName, success}, true)
}

export function unFollowTag (state, {tagName}) {
  return updateFollowTag(state, {tagName}, false)
}

export function getTagsFollowed (state, {payload: {getTagsFollowed}}) {
  return update(state, {
    tags: {
      data: {
        $set: cleanTagsGroup(getTagsFollowed.data)
      },
      pagination: {
        $set: cleanPagination(getTagsFollowed.pagination)
      }
    }
  })
}

export function getUsersFollowed (state, {payload: {getUsersFollowed}}) {
  return update(state, {
    users: {
      data: {
        $set: cleanUsers(getUsersFollowed.data)
      },
      pagination: {
        $set: cleanPagination(getUsersFollowed.pagination)
      }
    }
  })
}
