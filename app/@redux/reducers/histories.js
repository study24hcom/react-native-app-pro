import {SAVE_HISTORIES, GET_HISTORIES, DELETE_HISTORIES} from '@redux/actions/historiesAction'
import update from 'react-addons-update'
import {cleanHistories, cleanHistory} from 'utils/clean'

const initialState = {
  history: []
}

export default function createReducer (state = initialState, action) {
  switch (action.type) {
    case SAVE_HISTORIES:
      return saveHistories(state, action)
    case GET_HISTORIES:
      return getHistories(state, action)
    case DELETE_HISTORIES:
      return deleteHistories(state)
    default:
      return state
  }
}

export function saveHistories (state, {history}) {
  return update(state, {
    history: {
      $apply: data => [cleanHistory(history), ...data.filter(u => u.name !== history)]
    }
  })
}

export function getHistories (state, {data}) {
  return update(state, {
    history: {
      $set: cleanHistories(data)
    }
  })
}

export function deleteHistories (state) {
  return update(state, {
    history: {
      $set: []
    }
  })
}
