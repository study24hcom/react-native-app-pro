import {
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAIL,
  USER_LOGIN_FETCHING,
  USER_LOGOUT,
  USER_REGISTER_SUCCESS,
  SET_AUTH_SOCIAL,
  RESET_AUTH_SOCIAL,
  RAISE_BALANCE,
  SUBTRACT_BALANACE,
  UPDATE_PROFILE,
  UPDATE_AVATAR
} from '../actions/authAction'
import update from 'react-addons-update'
import {cleanUser} from 'utils/clean'

const initialState = {
  token: '',
  register: false,
  loading: true,
  isAuthenticated: false,
  isLoginFailed: false,
  user: {},
  userSocial: {
    token: '',
    user: {},
    social: ''
  }
}

export default function createReducer (state = initialState, action) {
  switch (action.type) {
    case USER_LOGIN_SUCCESS:
      return userLoginSuccess(state, action)
    case USER_LOGOUT:
      return userLoginLogout(state)
    case USER_LOGIN_FAIL:
      return userLoginFail(state)
    case USER_LOGIN_FETCHING:
      return userLoginFetching(state)
    case USER_REGISTER_SUCCESS:
      return userRegisterSuccess(state, action)
    case SET_AUTH_SOCIAL:
      return setAuthSocial(state, action)
    case RESET_AUTH_SOCIAL:
      return resetAuthSocial(state)
    case RAISE_BALANCE:
      return raiseBalance(state, action)
    case SUBTRACT_BALANACE:
      return subtractBalance(state, action)
    case UPDATE_PROFILE:
      return updateProfile(state, action)
    case UPDATE_AVATAR:
      return updateAvatar(state, action)
    default:
      return state
  }
}

export function updateAvatar (state, {avatar}) {
  return update(state, {
    user: {
      avatar: {
        $set: avatar
      },
      avatarUrl: {
        $set: avatar
      }
    }
  })
}

export function updateProfile (state, {profile}) {
  return update(state, {
    user: {
      $apply: oldData => ({
        ...oldData,
        ...profile
      })
    }
  })
}

export function userLoginSuccess (state, {token, user}) {
  return {
    ...state,
    token,
    register: false,
    isLoginFailed: false,
    user: cleanUser(user),
    loading: false,
    isAuthenticated: true
  }
}

export function userRegisterSuccess (state, {token, user}) {
  return {
    ...state,
    token,
    register: true,
    isLoginFailed: false,
    user: cleanUser(user),
    loading: false,
    isAuthenticated: true
  }
}

export function userLoginFetching (state) {
  return {
    ...state,
    isLoginFailed: false,
    loading: true
  }
}

export function userLoginLogout (state) {
  return {
    ...state,
    token: '',
    user: {},
    loading: false,
    register: false,
    isLoginFailed: false,
    isAuthenticated: false
  }
}

export function userLoginFail (state) {
  return {
    ...state,
    token: '',
    user: {},
    loading: false,
    isLoginFailed: true,
    isAuthenticated: false
  }
}

export function setAuthSocial (state, {social, token, user}) {
  return update(state, {
    isLoginFailed: {
      $set: false
    },
    userSocial: {
      social: {$set: social},
      token: {$apply: value => token || value},
      user: {$set: user}
    }
  })
}

export function resetAuthSocial (state) {
  return {
    ...state,
    isLoginFailed: false,
    userSocial: {
      social: '',
      token: '',
      user: ''
    }
  }
}

export function raiseBalance (state, {amount}) {
  return update(state, {
    user: {
      balance: {
        $apply: balance => balance + parseInt(amount, 10)
      }
    }
  })
}

export function subtractBalance (state, {amount}) {
  return update(state, {
    user: {
      balance: {
        $apply: balance => balance - parseInt(amount, 10)
      }
    }
  })
}
