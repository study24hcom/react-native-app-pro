import {
  GET_AUTH_SCORE,
  SET_AUTH_SCORE,
  GET_SCORES,
  LOAD_MORE_SCORES,
  GET_RATINGS,
  GET_AUTH_RATING,
  SET_AUTH_RATING,
  LOAD_MORE_RATINGS,
  ADD_RATING,
  GET_PERMISSIONS,
  ADD_PERMISSION,
  DELETE_PERMISSION,
  GET_STATISTIC_BY_DAY,
  GET_STATISTIC_BY_WEEK,
  GET_STATISTIC_BY_OS,
  GET_STATISTIC_BY_MONTH,
  UPDATE_QUIZ_FOCUS,
  UPDATE_RATING
} from '../actions/quizAddonAction'
import {
  cleanPagination,
  cleanRatings,
  cleanHistoryToScore,
  cleanRating
} from 'utils/clean'
import update from 'react-addons-update'

const initialState = {
  scores: {
    data: [],
    pagination: {}
  },
  ratings: {
    data: [],
    pagination: {}
  },
  permissions: {
    data: [],
    pagination: {}
  },
  report: {
    byDay: [],
    byWeek: [],
    byMonth: [],
    byOS: []
  },
  authRating: false,
  authScore: {},
  quizFocus: {
    quizId: ''
  }
}

export default function createReducerQuizAddon (state = initialState, action) {
  switch (action.type) {
    case GET_AUTH_SCORE:
      return getAuthScore(state, action)
    case SET_AUTH_SCORE:
      return setAuthScore(state, action)
    case GET_SCORES:
      return getScores(state, action)
    case LOAD_MORE_SCORES:
      return loadMoreScores(state, action)
    case GET_RATINGS:
      return getRatings(state, action)
    case GET_AUTH_RATING:
      return getAuthRating(state, action)
    case SET_AUTH_RATING:
      return setAuthRating(state, action)
    case ADD_RATING:
      return addRating(state, action)
    case UPDATE_RATING:
      return updateRating(state, action)
    case LOAD_MORE_RATINGS:
      return loadMoreRatings(state, action)
    case GET_PERMISSIONS:
      return getPermissions(state, action)
    case ADD_PERMISSION:
      return addPermission(state, action)
    case DELETE_PERMISSION:
      return deletePermission(state, action)
    case GET_STATISTIC_BY_DAY:
      return getStatisticByDay(state, action)
    case GET_STATISTIC_BY_WEEK:
      return getStatisticByWeek(state, action)
    case GET_STATISTIC_BY_MONTH:
      return getStatisticByMonth(state, action)
    case GET_STATISTIC_BY_OS:
      return getStatisticByOS(state, action)
    case UPDATE_QUIZ_FOCUS:
      return updateQuizFocus(state, action)
    default:
      return state
  }
}

export function updateStatistic (state, action, reportKey, apiPayloadKey) {
  return update(state, {
    report: {
      [reportKey]: {
        $set: action.payload[apiPayloadKey]
      }
    }
  })
}

export function getStatisticByDay (state, action) {
  return updateStatistic(state, action, 'byDay', 'statisticByDay')
}

export function getStatisticByWeek (state, action) {
  return updateStatistic(state, action, 'byWeek', 'statisticByWeek')
}

export function getStatisticByMonth (state, action) {
  return updateStatistic(state, action, 'byMonth', 'statisticByMonth')
}

export function getStatisticByOS (state, action) {
  return updateStatistic(state, action, 'byOS', 'statisticByOS')
}

export function getAuthScore (state, {payload: {getAuthScore}}) {
  if (getAuthScore.success === false) {
    return update(state, {
      authScore: {$set: {}}
    })
  }
  return update(state, {
    authScore: {
      $set: cleanHistoryToScore(getAuthScore)
    }
  })
}

export function setAuthScore (state, action) {
  return update(state, {
    authScore: {
      $set: cleanHistoryToScore(action.authScore)
    }
  })
}

export function getScores (state, {payload: {getScores}}) {
  return update(state, {
    scores: {
      pagination: {$set: cleanPagination(getScores.pagination)},
      data: {
        $set: getScores.histories.map(score => cleanHistoryToScore(score))
      }
    }
  })
}

export function loadMoreScores (state, {payload: {loadMoreScores}}) {
  return update(state, {
    scores: {
      pagination: {$set: cleanPagination(loadMoreScores.pagination)},
      data: {
        $apply: data => [
          ...data,
          ...loadMoreScores.histories.map(score => cleanHistoryToScore(score))
        ]
      }
    }
  })
}

export function getRatings (state, {payload: {getRatings}}) {
  return update(state, {
    ratings: {
      pagination: {$set: cleanPagination(getRatings.pagination)},
      data: {
        $set: cleanRatings(getRatings.ratings)
      }
    }
  })
}

export function getAuthRating (state, {payload: {getAuthRating}}) {
  return update(state, {
    authRating: {
      $set: getAuthRating.success
    }
  })
}

export function setAuthRating (state, {rating}) {
  return update(state, {
    authRating: {
      $set: rating
    }
  })
}

export function addRating (state, {rating}) {
  return update(state, {
    ratings: {
      data: {
        $apply: oldData => [cleanRating(rating), ...oldData]
      }
    }
  })
}

export function updateRating (state, {ratingId, ratingData}) {
  const ratingIndex = state.ratings.data.findIndex(r => r.id === ratingId)
  if (ratingIndex < 0) return state
  return update(state, {
    ratings: {
      data: {
        [ratingIndex]: {
          $set: cleanRating(ratingData)
        }
      }
    }
  })
}

export function loadMoreRatings (state, {payload: {loadMoreRatings}}) {
  return update(state, {
    ratings: {
      pagination: {$set: cleanPagination(loadMoreRatings.pagination)},
      data: {
        $apply: data => [...data, ...cleanRatings(loadMoreRatings.ratings)]
      }
    }
  })
}

export function getPermissions (state, {payload: {getPermissions}}) {
  return update(state, {
    permissions: {
      data: {
        $set: getPermissions.data
      },
      pagination: {
        $set: getPermissions.pagination
      }
    }
  })
}

export function addPermission (state, {payload: {addPermission}}) {
  return update(state, {
    permissions: {
      data: {
        $apply: oldData => ({
          ...oldData,
          addPermission
        })
      }
    }
  })
}

export function deletePermission (state, {permissionId}) {
  const permissionIndex = state.permissions.data.findIndex(
    permission => permission.id === permissionId
  )
  console.log(permissionIndex)
  return update(state, {
    permissions: {
      data: {
        $splice: [[permissionIndex, 1]]
      }
    }
  })
}

export function updateQuizFocus (state, {quizId}) {
  return update(state, {
    quizFocus: {
      quizId: {
        $set: quizId
      }
    }
  })
}
