import {
  GET_PLAYLIST,
  GET_PLAYLISTS,
  GET_PLAYLISTS_ME,
  SET_PLAYLIST,
  SET_INFO_PLAYLIST,
  LOAD_MORE_PLAYLISTS,
  LOAD_MORE_PLAYLISTS_ME,
  GET_FEATURED_PLAYLISTS,
  GET_FEATURED_PLAYLISTS_BY_USER,
  LOAD_MORE_FEATURED_PLAYLISTS_BY_USER,
  SET_PLAYLIST_CURRENT_QUIZLISTS,
  SET_PLAYLIST_CURRENT_INFO,
  ADD_QUIZLIST_TO_CURRENT,
  RESET_PLAYLIST_CURRENT
} from '../actions/playlistAction'
import update from 'react-addons-update'
import {cleanPlaylists, cleanPagination, cleanQuizLists, cleanInfoPlaylist} from 'utils/clean'

export const initialState = {
  lists: {
    data: [],
    pagination: {}
  },
  listsMe: {
    data: [],
    pagination: {}
  },
  listsFeatured: {
    data: [],
    pagination: {}
  },
  featuredListsByUser: {
    data: [],
    pagination: {}
  },
  current: {
    info: {},
    quizLists: []
  }
}

export default function createReducer (state = initialState, action) {
  switch (action.type) {
    case GET_PLAYLISTS:
      return updateStateDataPaginationByKey(
        state,
        'lists',
        action.payload.getPlaylists
      )
    case LOAD_MORE_PLAYLISTS:
      return updateStateDataPaginationByKey(
        state,
        'lists',
        action.payload.loadMorePlaylists,
        true
      )
    case GET_PLAYLISTS_ME:
      return updateStateDataPaginationByKey(
        state,
        'listsMe',
        action.payload.getPlaylistsMe
      )
    case LOAD_MORE_PLAYLISTS_ME:
      return updateStateDataPaginationByKey(
        state,
        'listsMe',
        action.payload.loadMorePlaylistsMe,
        true
      )
    case GET_FEATURED_PLAYLISTS_BY_USER:
      return updateStateDataPaginationByKey(
        state,
        'featuredListsByUser',
        action.payload.getFeaturedPlaylistsByUser
      )
    case LOAD_MORE_FEATURED_PLAYLISTS_BY_USER:
      return updateStateDataPaginationByKey(
        state,
        'featuredListsByUser',
        action.payload.loadMoreFeaturedPlaylistsByUser,
        true
      )
    case GET_FEATURED_PLAYLISTS:
      return updateStateDataPaginationByKey(
        state,
        'listsFeatured',
        action.payload.getFeaturedPlaylists
      )
    case GET_PLAYLIST:
      return getPlaylist(state, action)
    case SET_PLAYLIST:
      return setPlaylist(state, action)
    case SET_INFO_PLAYLIST:
      return setInfoPlaylist(state, action)
    case SET_PLAYLIST_CURRENT_QUIZLISTS:
      return setPlaylistCurrentQuizlists(state, action)
    case SET_PLAYLIST_CURRENT_INFO:
      return setPlaylistCurrentInfo(state, action)
    case ADD_QUIZLIST_TO_CURRENT:
      return addQuizlistToCurrent(state, action)
    case RESET_PLAYLIST_CURRENT:
      return resetPlaylistCurrent(state)
    default:
      return state
  }
}

export function updateStateDataPaginationByKey (state,
                                               key,
                                               {playlists, pagination},
                                               isMerge = false) {
  let newPlaylists = cleanPlaylists(playlists)
  return update(state, {
    [key]: {
      data: {
        $apply: oldData =>
          (isMerge ? [...oldData, ...newPlaylists] : newPlaylists)
      },
      pagination: {
        $set: cleanPagination(pagination)
      }
    }
  })
}

export function getPlaylist (state, {payload: {getPlaylist}}) {
  if (getPlaylist.success === false) {
    return update(state, {
      current: {
        quizLists: {
          $set: []
        },
        info: {
          success: {
            $set: false
          }
        }
      }
    })
  }
  return update(state, {
    current: {
      quizLists: {
        $set: cleanQuizLists(getPlaylist.quiz_lists)
      },
      info: {
        $set: cleanInfoPlaylist(getPlaylist)
      }
    }
  })
}

export function setPlaylist (state, {playlist}) {
  return update(state, {
    current: {
      $apply: data => ({
        ...data,
        ...playlist
      })
    }
  })
}

export function setInfoPlaylist (state, action) {
  return {
    ...state,
    current: {
      info: {
        ...action.info
      },
      quizLists: []
    }
  }
}

export function setPlaylistCurrentQuizlists (state, {quizLists}) {
  return update(state, {
    current: {
      quizLists: {
        $set: quizLists
      }
    }
  })
}

export function setPlaylistCurrentInfo (state, {info}) {
  return update(state, {
    current: {
      info: {
        $set: info
      }
    }
  })
}

export function addQuizlistToCurrent (state, {quizList}) {
  return update(state, {
    current: {
      quizLists: {
        $push: [quizList]
      }
    }
  })
}

export function resetPlaylistCurrent (state) {
  return update(state, {
    current: {
      info: {$set: {}},
      quizLists: {$set: []}
    }
  })
}
