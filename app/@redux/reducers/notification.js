import {
  GET_NOTIFICATIONS,
  LOAD_MORE_NOTIFICATIONS,
  UPDATE_NEW_NOTIFICATIONS,
  MARK_VIEWED_ALL,
  MARK_CHECKED
} from '../actions/notificationAction'
import { cleanNotifications, cleanPagination } from 'utils/clean'
import update from 'react-addons-update'
import Notification from 'constants/notificationType'

const initialState = {
  list: {
    data: [],
    pagination: {}
  },
  newNotifications: 0
}

export default function createReducer (state = initialState, action) {
  switch (action.type) {
    case GET_NOTIFICATIONS:
      return getNotifications(state, action)
    case LOAD_MORE_NOTIFICATIONS:
      return loadMoreNotifications(state, action)
    case UPDATE_NEW_NOTIFICATIONS:
      return updateNewNotifications(state, action)
    case MARK_VIEWED_ALL:
      return markViewedAllNotifications(state)
    case MARK_CHECKED:
      return markCheckedNotification(state, action)
    default:
      return state
  }
}

export function getNotifications (state, { payload: { getNotifications } }) {
  return update(state, {
    list: {
      data: {
        $set: cleanNotifications(getNotifications.notifications)
      },
      pagination: {
        $set: cleanPagination(getNotifications.pagination)
      }
    }
  })
}

export function loadMoreNotifications (
  state,
  { payload: { loadMoreNotifications } }
) {
  return update(state, {
    list: {
      data: {
        $apply: oldData => [
          ...oldData,
          ...cleanNotifications(loadMoreNotifications.notifications)
        ]
      },
      pagination: {
        $set: cleanPagination(loadMoreNotifications.pagination)
      }
    }
  })
}

export function updateNewNotifications (state, { newNotifications }) {
  return update(state, {
    newNotifications: {
      $set: newNotifications
    }
  })
}

export function markViewedAllNotifications (state) {
  return update(state, {
    list: {
      data: {
        $apply: oldData =>
          oldData.map(nf => ({
            ...nf,
            status: Notification.status.VIEWED
          }))
      }
    }
  })
}

export function markCheckedNotification (state, { notificationId }) {
  const notificationIndex = state.list.data.findIndex(
    n => n.id === notificationId
  )
  if (notificationIndex < 0) return state
  return update(state, {
    list: {
      data: {
        [notificationIndex]: {
          status: {
            $set: Notification.status.CHECKED
          }
        }
      }
    }
  })
}
