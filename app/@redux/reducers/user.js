import { GET_PARTNER_LIST, LOAD_MORE_PARTNER_LIST } from '../actions/userAction'
import update from 'react-addons-update'
import { cleanPagination, cleanUsers } from 'utils/clean'

const initialState = {
  partnerList: {
    data: [],
    pagination: {}
  }
}

export default function createReducer (state = initialState, action) {
  switch (action.type) {
    case GET_PARTNER_LIST:
      return getPartnerList(state, action)
    case LOAD_MORE_PARTNER_LIST:
      return loadMorePartnerList(state, action)
    default:
      return state
  }
}

function getPartnerList (state, { payload: { getPartnerList } }) {
  return update(state, {
    partnerList: {
      data: {
        $set: cleanUsers(getPartnerList.data)
      },
      pagination: {
        $set: cleanPagination(getPartnerList.pagination)
      }
    }
  })
}

function loadMorePartnerList (state, { payload: { loadMorePartnerList } }) {
  return update(state, {
    partnerList: {
      data: {
        $apply: oldData => [...oldData, ...cleanUsers(loadMorePartnerList.data)]
      },
      pagination: {
        $set: cleanPagination(loadMorePartnerList.pagination)
      }
    }
  })
}
