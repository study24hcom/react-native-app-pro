import {
  GET_QUIZ_LISTS_BY_KEY,
  SET_QUIZ_LISTS_BY_KEY_LOAD_MORE
} from '../actions/quizListsAction'
import update from 'react-addons-update'
import {cleanQuizLists, cleanPagination} from 'utils/clean'
import {
  QUIZ_LISTS_BY_CATEGORY,
  QUIZ_LISTS_BY_ME,
  QUIZ_LISTS_BY_HISTORY,
  QUIZ_LISTS_BY_SEARCH,
  QUIZ_LISTS_BY_USER,
  QUIZ_LISTS_BY_NEWS,
  QUIZ_LISTS_BY_TAG,
  QUIZ_LISTS_BY_FOLLOWED,
  QUIZ_LISTS_BY_BOOKMARK
} from 'constants/quizLists'

const defaultListsData = {
  data: [],
  loadMore: false,
  pagination: {
    itemPerPage: 10,
    page: 1
  }
}

const initialState = {
  byHome: {
    featured: [],
    category: {
      math: defaultListsData,
      physical: defaultListsData,
      chemistry: defaultListsData,
      biology: defaultListsData,
      english: defaultListsData,
      programming: defaultListsData,
      other: defaultListsData
    }
  },
  [QUIZ_LISTS_BY_CATEGORY.key]: defaultListsData,
  [QUIZ_LISTS_BY_ME.key]: defaultListsData,
  [QUIZ_LISTS_BY_HISTORY.key]: defaultListsData,
  [QUIZ_LISTS_BY_SEARCH.key]: defaultListsData,
  [QUIZ_LISTS_BY_USER.key]: defaultListsData,
  [QUIZ_LISTS_BY_NEWS.key]: defaultListsData,
  [QUIZ_LISTS_BY_TAG.key]: defaultListsData,
  [QUIZ_LISTS_BY_FOLLOWED.key]: defaultListsData,
  [QUIZ_LISTS_BY_BOOKMARK.key]: defaultListsData
}

export default function createReducer (state = initialState, action) {
  switch (action.type) {
    case GET_QUIZ_LISTS_BY_KEY:
      return setQuizListsByKey(state, action)
    case SET_QUIZ_LISTS_BY_KEY_LOAD_MORE:
      return setQuizListsByKeyLoadMore(state, action)
    default:
      return state
  }
}

function setQuizListsByKeyLoadMore (state, {key, loadMore}) {
  return update(state, {
    [key]: {
      loadMore: {
        $set: loadMore
      }
    }
  })
}

function setQuizListsByKey (state, {key, payloadKey, payload, loadMore}) {
  let quizLists = payload[payloadKey].quiz_list
    ? payload[payloadKey].quiz_list
    : payload[payloadKey].quiz_lists
  return update(state, {
    [key]: {
      loadMore: {
        $set: false
      },
      data: {
        $apply: oldQuizLists => {
          if (loadMore) {
            return [...oldQuizLists, ...cleanQuizLists(quizLists)]
          } else {
            return cleanQuizLists(quizLists)
          }
        }
      },
      pagination: {$set: cleanPagination(payload[payloadKey].pagination)}
    }
  })
}
