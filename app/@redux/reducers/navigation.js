import {HomeNav} from '../../navigation/AppNavigation'

export const reducer = (state, action) => {
  const newState = HomeNav.router.getStateForAction(action, state)
  return newState || state
}
