import {
  GET_QUIZ_LIST,
  GET_QUIZ_LIST_AUTH,
  PLAY_QUIZ_LIST,
  SET_QUIZZES,
  SET_INFO,
  SET_QUIZ_LIST_FROM_RAW,
  APPEND_QUIZZES,
  SHUFFLE_QUIZZES,
  ADD_QUIZ,
  UPDATE_QUIZ_QUESTION,
  UPDATE_QUIZ_CORRECT_ANSWER_INDEX,
  UPDATE_QUIZ_CHOOSE_ANSWER_INDEX,
  UPDATE_QUIZ_SUGGEST_ANSWER,
  UPDATE_QUIZ_TOGGLE_SUGGEST_ANSWER,
  REMOVE_QUIZ,
  QUIZ_ADD_ANSWER,
  QUIZ_UPDATE_ANSWER,
  QUIZ_REMOVE_ANSWER,
  UPDATE_INFO,
  UPDATE_PROPERTY,
  UPDATE_QUIZ_KEY,
  UPDATE_ACCESS_LOG_ID,
  GET_QUIZ_LIST_QUIZZES,
  SET_QUIZ_LIST_PURCHASED,
  SET_PDF_FILE,
  TOGGLE_VIEW_PDF,
  RESET_QUIZ_LIST, RESET_INFO
} from '../actions/quizAction'
import {shuffleArray} from 'utils'
import update from 'react-addons-update'
import uuid from 'uuid'

import {cleanQuizzes, cleanQuizList} from 'utils/clean'

import quizListTypes from '../../constants/quiz-list-types'

// import quizzesDefault from 'fake-data/quizzes-data/math'

export function createAnswer (): AnswerType {
  const id = uuid.v4()
  return {
    id,
    _id: id,
    answer: ''
  }
}

export function createQuiz (): QuizType {
  const id = uuid.v4()
  return {
    id,
    _id: id,
    question: '',
    correctAnswerIndex: -1,
    tags: [],
    level: '',
    timePlay: 120,
    suggestAnswer: '',
    isSuggestAnswer: false,
    answers: [createAnswer()]
  }
}

export const initialState: QuizListReducerType = {
  error: {
    isError: false,
    message: ''
  },
  info: {
    name: '',
    tags: [],
    description: '',
    type: quizListTypes.FREE,
    timeLength: 0,
    pdfFile: '',
    customField: {},
    totalAccess: 0,
    totalQuestion: 0,
    user: {}
  },
  quizzes: [createQuiz()],
  // quizzes: cleanQuizzes(quizzesDefault.slice(0, 2)),
  play: {
    isPlay: false,
    accessLogId: false
  },
  property: {
    isViewPdf: false,
    isDraftPreview: false
  }
}

export default function createReducer (state = initialState,
                                      action): QuizListReducerType {
  switch (action.type) {
    case GET_QUIZ_LIST:
      return getQuizList(state, action)
    case GET_QUIZ_LIST_AUTH:
      return getQuizListAuth(state, action)
    case PLAY_QUIZ_LIST:
      return playQuizList(state)
    case UPDATE_ACCESS_LOG_ID:
      return updateAccessLogId(state, action)
    case GET_QUIZ_LIST_QUIZZES:
      return getQuizListQuizzes(state, action)
    case ADD_QUIZ:
      return addQuiz(state)
    case SET_QUIZZES:
      return setQuizzes(state, action)
    case SHUFFLE_QUIZZES:
      return shuffleQuizzes(state, action)
    case SET_INFO:
      return setInfo(state, action)
    case RESET_INFO:
      return initialState
    case SET_QUIZ_LIST_FROM_RAW:
      return setQuizListFromRaw(state, action)
    case APPEND_QUIZZES:
      return appendQuizzes(state, action)
    case UPDATE_INFO:
      return updateInfo(state, action)
    case UPDATE_QUIZ_QUESTION:
      return updateQuizQuestion(state, action)
    case UPDATE_QUIZ_CORRECT_ANSWER_INDEX:
      return updateQuizCorrectAnswerIndex(state, action)
    case UPDATE_QUIZ_CHOOSE_ANSWER_INDEX:
      return updateQuizChooseAnswerIndex(state, action)
    case UPDATE_QUIZ_TOGGLE_SUGGEST_ANSWER:
      return updateQuizToggleSuggestAnswer(state, action)
    case UPDATE_QUIZ_SUGGEST_ANSWER:
      return updateQuizSuggestAnswer(state, action)
    case UPDATE_PROPERTY:
      return updateProperty(state, action)
    case REMOVE_QUIZ:
      return removeQuiz(state, action)
    case QUIZ_ADD_ANSWER:
      return addQuizAnswer(state, action)
    case QUIZ_UPDATE_ANSWER:
      return updateQuizAnswer(state, action)
    case QUIZ_REMOVE_ANSWER:
      return removeQuizAnswer(state, action)
    case UPDATE_QUIZ_KEY:
      return updateKeyOfQuiz(state, action.quizId, action.key, action.value)
    case SET_QUIZ_LIST_PURCHASED:
      return setQuizListPurchased(state)
    case SET_PDF_FILE:
      return setPdfFile(state, action)
    case TOGGLE_VIEW_PDF:
      return toggleViewPdf(state, action)
    case RESET_QUIZ_LIST:
      return initialState
    default:
      return state
  }
}

export function setPdfFile (state, action) {
  return update(state, {
    info: {
      pdfFile: {
        $set: action.pdfFile
      }
    }
  })
}

export function toggleViewPdf (state) {
  return update(state, {
    property: {
      isViewPdf: {
        $apply: oldData => !oldData
      }
    }
  })
}

export function playQuizList (state) {
  return update(state, {
    play: {
      isPlay: {$set: true}
    }
  })
}

export function shuffleQuizzes (state) {
  return update(state, {
    quizzes: {
      $set: shuffleArray(state.quizzes)
    }
  })
}

export function setInfo (state, action) {
  return {
    ...state,
    info: {
      ...action.info
    }
  }
}

export function setQuizListFromRaw (state, {quizListRaw}) {
  return update(state, {
    info: {
      $set: cleanQuizList(quizListRaw)
    },
    quizzes: {
      $set: cleanQuizzes(quizListRaw.quizzes)
    }
  })
}

export function setQuizListPurchased (state) {
  return update(state, {
    info: {
      purchased: {
        $set: true
      }
    }
  })
}

export function updateAccessLogId (state, {payload: {playQuizList}}) {
  return update(state, {
    play: {
      accessLogId: {$set: playQuizList.data.id}
    }
  })
}

export function getQuizList (state, {payload: {getQuizList}}) {
  if (getQuizList.success === false) {
    return update(state, {
      error: {
        isError: {$set: true},
        message: {$set: getQuizList.message}
      },
      info: {
        $set: {}
      }
    })
  } else {
    return update(state, {
      info: {
        $set: cleanQuizList(getQuizList)
      },
      error: {
        isError: {$set: false},
        message: {$set: ''}
      }
    })
  }
}

export function getQuizListAuth (state, {payload: {getQuizList}}) {
  return update(state, {
    info: {
      $set: cleanQuizList(getQuizList)
    },
    quizzes: {
      $set: cleanQuizzes(getQuizList.quizzes)
    }
  })
}

export function getQuizListQuizzes (state, {payload: {getQuizListQuizzes}}) {
  return update(state, {
    quizzes: {
      $set: cleanQuizzes(getQuizListQuizzes)
    }
  })
}

export function getQuizIndex (state, quizId) {
  return state.quizzes.findIndex(quiz => quiz.id === quizId)
}

export function updateKeyOfQuiz (state,
                                quizId,
                                key,
                                value): QuizListReducerType {
  const quizIndex = getQuizIndex(state, quizId)
  if (quizIndex === -1) return state
  return update(state, {
    quizzes: {
      [quizIndex]: {
        [key]: {$set: value}
      }
    }
  })
}

export function addQuiz (state): QuizListReducerType {
  return update(state, {
    quizzes: {
      $push: [createQuiz()]
    }
  })
}

export function updateQuizQuestion (state,
                                   {quizId, question}): QuizListReducerType {
  return updateKeyOfQuiz(state, quizId, 'question', question)
}

export function updateQuizToggleSuggestAnswer (state,
                                              {quizId, isSuggestAnswer}): QuizListReducerType {
  return updateKeyOfQuiz(state, quizId, 'isSuggestAnswer', isSuggestAnswer)
}

export function updateQuizSuggestAnswer (state,
                                        {quizId, suggestAnswer}): QuizListReducerType {
  return updateKeyOfQuiz(state, quizId, 'suggestAnswer', suggestAnswer)
}

export function updateQuizCorrectAnswerIndex (state,
                                             {quizId, correctAnswerIndex}): QuizListReducerType {
  return updateKeyOfQuiz(
    state,
    quizId,
    'correctAnswerIndex',
    correctAnswerIndex
  )
}

export function updateQuizChooseAnswerIndex (state,
                                            {quizId, chooseAnswerIndex}): QuizListReducerType {
  return updateKeyOfQuiz(state, quizId, 'chooseAnswerIndex', chooseAnswerIndex)
}

export function removeQuiz (state, {quizId}): QuizListReducerType {
  const quizIndex = getQuizIndex(state, quizId)
  if (quizIndex === -1) return state
  return update(state, {
    quizzes: {
      $splice: [[quizIndex, 1]]
    }
  })
}

export function addQuizAnswer (state, {quizId}): QuizListReducerType {
  const quizIndex = getQuizIndex(state, quizId)
  if (quizIndex === -1) return state
  return update(state, {
    quizzes: {
      [quizIndex]: {
        answers: {
          $push: [createAnswer()]
        }
      }
    }
  })
}

export function updateQuizAnswer (state,
                                 {quizId, answerId, answer}): QuizListReducerType {
  const quizIndex = getQuizIndex(state, quizId)
  if (quizIndex === -1) return state
  const quiz = state.quizzes[quizIndex]
  const answerIndex = quiz.answers.findIndex(answer => answer._id === answerId)
  return update(state, {
    quizzes: {
      [quizIndex]: {
        answers: {
          [answerIndex]: {
            answer: {$set: answer}
          }
        }
      }
    }
  })
}

export function removeQuizAnswer (state,
                                 {quizId, answerId}): QuizListReducerType {
  const quizIndex = getQuizIndex(state, quizId)
  if (quizIndex === -1) return state
  const quiz = state.quizzes[quizIndex]
  const answerIndex = quiz.answers.findIndex(answer => answer.id === answerId)

  if (answerIndex === -1) return state
  return update(state, {
    quizzes: {
      [quizIndex]: {
        answers: {
          $splice: [[answerIndex, 1]]
        }
      }
    }
  })
}

export function updateInfo (state, {key, value}): QuizListReducerType {
  return update(state, {
    info: {
      [key]: {
        $set: value
      }
    }
  })
}

export function updateProperty (state, {key, value}): QuizListReducerType {
  return update(state, {
    property: {
      [key]: {
        $set: value
      }
    }
  })
}

export function setQuizzes (state, {quizzes}): QuizListReducerType {
  return update(state, {
    quizzes: {
      $set: quizzes
    }
  })
}

export function appendQuizzes (state, {quizzes}): QuizListReducerType {
  return update(state, {
    quizzes: {
      $push: quizzes
    }
  })
}
