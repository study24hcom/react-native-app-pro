import {GET_TAGS, CREATE_FAKE_TAG, GET_FEATURED_TAGS, GET_TAGS_GROUP} from '../actions/tagAction'
import update from 'react-addons-update'
import {cleanTags, cleanTagsGroup, cleanPagination} from 'utils/clean'

const initialState = {
  list: {
    data: [],
    pagination: {}
  },
  group: {
    data: [],
    pagination: {}
  },
  featured: {
    data: [],
    pagination: {}
  }
}

export default function createReducer (state = initialState, action) {
  switch (action.type) {
    case GET_TAGS:
      return getTags(state, action)
    case GET_FEATURED_TAGS:
      return getFeaturedTags(state, action)
    case CREATE_FAKE_TAG:
      return createFakeTag(state, action)
    case GET_TAGS_GROUP:
      return getTagsGroup(state, action)
    default:
      return state
  }
}

function getTags (state, {payload: {getTags}}) {
  return update(state, {
    list: {
      data: {
        $set: cleanTags(getTags)
      }
    }
  })
}

function getFeaturedTags (state, {payload: {getFeaturedTags}}) {
  return update(state, {
    featured: {
      data: {
        $set: cleanTagsGroup(getFeaturedTags)
      }
    }
  })
}

function getTagsGroup (state, {payload: {getTagsGroup}}) {
  return update(state, {
    group: {
      data: {
        $set: cleanTagsGroup(getTagsGroup.data)
      },
      pagination: {
        $set: cleanPagination(getTagsGroup.pagination)
      }
    }
  })
}

function createFakeTag (state, {tag}) {
  if (!tag) return state
  return update(state, {
    list: {
      data: {
        $push: [tag]
      }
    }
  })
}
