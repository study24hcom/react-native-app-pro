import update from 'react-addons-update'
import {
  SET_BOOKMARK_QUIZLIST,
  UN_BOOKMARK_QUIZLIST,
  CHECK_BOOKMARK,
  UPDATE_IS_SHOW_BOX
} from '../actions/bookmarkAction'

const initialState = {
  bookmarkQuizlists: [],
  isShowBox: false
}

export default function createReducer (state = initialState, action) {
  switch (action.type) {
    case SET_BOOKMARK_QUIZLIST:
      return setBookmarkQuizlist(state, action)
    case UN_BOOKMARK_QUIZLIST:
      return unBookmarkQuizlist(state, action)
    case CHECK_BOOKMARK:
      return checkBookmark(state, action)
    case UPDATE_IS_SHOW_BOX:
      return updateIsShowBox(state, action)
    default:
      return state
  }
}

export function updateBookmarkQuizlist (state, {quizlistId}, bookmarked = true) {
  const quizlistIndex = state.bookmarkQuizlists.findIndex(b => b.id === quizlistId)
  if (quizlistIndex < 0) {
    return update(state, {
      bookmarkQuizlists: {
        $push: [{id: quizlistId, bookmarked}]
      }
    })
  } else {
    return update(state, {
      bookmarkQuizlists: {
        [quizlistIndex]: {
          bookmarked: {$set: bookmarked}
        }
      }
    })
  }
}

export function setBookmarkQuizlist (state, {quizlistId}) {
  return updateBookmarkQuizlist(state, {quizlistId}, true)
}

export function unBookmarkQuizlist (state, {quizlistId}) {
  return updateBookmarkQuizlist(state, {quizlistId}, false)
}

export function checkBookmark (state, {payload: {checkBookmark}, quizlistId}) {
  let bookmarked = checkBookmark.exists
  if (bookmarked) return setBookmarkQuizlist(state, {quizlistId})
  else return unBookmarkQuizlist(state, {quizlistId})
}

export function updateIsShowBox (state, {isShowBox}) {
  return update(state, {
    isShowBox: {
      $set: isShowBox
    }
  })
}
