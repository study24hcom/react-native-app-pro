import {
  SET_IS_MOBILE,
  SET_IS_SERVER,
  SET_KEYWORDS
} from '../actions/optionAction'

const initialState = {
  isMobile: false,
  isServer: false,
  keywords: ''
}

export default function createReducer (state = initialState, action) {
  switch (action.type) {
    case SET_IS_MOBILE:
      return {
        ...state,
        isMobile: action.isMobile
      }
    case SET_IS_SERVER:
      return {
        ...state,
        isServer: action.isServer
      }
    case SET_KEYWORDS:
      return {
        ...state,
        keywords: action.keywords
      }
    default:
      return state
  }
}
