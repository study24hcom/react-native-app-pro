import QuizApi from 'api/QuizApi'
import { AWAIT_MARKER } from 'redux-await'
import payload from 'constants/payload'

export const GET_QUIZLISTS_CATEGORY = 'CATEGORY/get-quizlist-category'
export const SET_QUIZ_LISTS_CATEGORY_LOAD_MORE = 'CATEGORY/set-quizlist-category-load-more'

export function getQuizListsCategory ({categoryKey, page = 1, itemPerPage = 10, loadMore = false}) {
  return dispatch => {
    dispatch({
      type: SET_QUIZ_LISTS_CATEGORY_LOAD_MORE,
      categoryKey,
      loadMore
    })
    dispatch({
      type: GET_QUIZLISTS_CATEGORY,
      AWAIT_MARKER,
      categoryKey,
      loadMore,
      payload: {
        [`${payload.GET_QUIZLISTS_CATEGORY}_${categoryKey}`]: QuizApi.getQuizListsByCategory({
          key: categoryKey,
          page,
          itemPerPage
        })
      }
    })
  }
}
