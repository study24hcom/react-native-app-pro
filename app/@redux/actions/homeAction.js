import QuizApi from 'api/QuizApi'
import GalleryApi from 'api/GalleryApi'
import {AWAIT_MARKER} from 'redux-await'
import payload from 'constants/payload'

export const GET_HOME_QUIZLISTS_CATEGORY = 'HOME/get-home-quizlist-category'
export const GET_HOME_GALLERY_SLIDER = 'HOME/get-home-gallery-slider'
export const GET_QUIZLISTS_TOP_DAY = 'HOME/get-quizlists-top-day'
export const SET_HOME_QUIZLISTS_CATEGORY_LOADED =
  'HOME/set-quizlist-cagegory-loaded'

export function getHomeQuizListsCategory (categoryKey) {
  return {
    type: GET_HOME_QUIZLISTS_CATEGORY,
    AWAIT_MARKER,
    categoryKey,
    payload: {
      [`${payload.GET_HOME_QUIZLISTS_CATEGORY}_${categoryKey}`]: QuizApi.getQuizListsByCategory(
        {
          key: categoryKey,
          page: 1,
          itemPerPage: 6
        }
      )
    }
  }
}

export function getGallerySlider () {
  return {
    type: GET_HOME_GALLERY_SLIDER,
    AWAIT_MARKER,
    payload: {
      getGallerySlider: GalleryApi.getGallerySlider()
    }
  }
}

export function getQuizListsTopDay () {
  return {
    type: GET_QUIZLISTS_TOP_DAY,
    AWAIT_MARKER,
    payload: {
      getQuizListsTopDay: QuizApi.getQuizListsByTopDay()
    }
  }
}

export function setHomeQuizListsCategoryLoaded () {
  return {
    type: SET_HOME_QUIZLISTS_CATEGORY_LOADED
  }
}
