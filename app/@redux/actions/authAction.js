import {setAuthToken, getAuthToken, resetAuthToken} from 'utils/auth'
import {LoginManager} from 'react-native-fbsdk'
import AuthApi from 'api/AuthApi'

export const GET_USER_AUTH = 'AUTH/get-user-auth'
export const USER_LOGIN_SUCCESS = 'AUTH/user-login-success'
export const USER_LOGIN_FETCHING = 'AUTH/user-login-fetching'
export const USER_LOGIN_FAIL = 'AUTH/user-login-fail'
export const USER_REGISTER_SUCCESS = 'AUTH/user-register-success'
export const USER_LOGOUT = 'AUTH/user-logout'
export const SET_AUTH_SOCIAL = 'AUTH-SOCIAL/set-auth-social'
export const RESET_AUTH_SOCIAL = 'AUTH-SOCIAL/reset-auth-social'
export const RAISE_BALANCE = 'AUTH/raise balance'
export const SUBTRACT_BALANACE = 'AUTH/subtract balance'
export const UPDATE_PROFILE = 'AUTH/update-profile'
export const UPDATE_AVATAR = 'AUTH/update-avatar'

// resetAuthToken()

export function userAutoAuth (ctx = {}) {
  return async dispatch => {
    let token = await getAuthToken(false, ctx)
    if (!token) return false
    dispatch(userLoginFetching())
    AuthApi.getUserMe()
      .then(userRes => {
        if (userRes.success) {
          dispatch(userLoginSuccess({user: userRes.data, token, updateToken: false}))
        } else {
          dispatch(userLoginFail())
        }
      })
      .catch(() => {
        dispatch(userLoginFail())
      })
  }
}

export function getUserAuth (token) {
  return {
    type: GET_USER_AUTH
  }
}

export function userLoginSuccess ({token, user, updateToken = true}) {
  if (token && updateToken) setAuthToken(token)
  return {
    type: USER_LOGIN_SUCCESS,
    token,
    user: {
      ...user,
      fullname: user.fullname,
      avatar: user.avatar_url ? user.avatar_url : user.avatar
    }
  }
}

export function userRegisterSuccess ({token, user, updateToken = true}) {
  if (token && updateToken) setAuthToken(token)
  return {
    type: USER_REGISTER_SUCCESS,
    token,
    user: {
      ...user,
      fullname: user.fullname,
      avatar: user.avatar_url
    }
  }
}

export function userLoginFail () {
  return {
    type: USER_LOGIN_FAIL
  }
}

export function userLoginFetching () {
  return {
    type: USER_LOGIN_FETCHING
  }
}

export function userLogout () {
  resetAuthToken()
  LoginManager.logOut()
  return {
    type: USER_LOGOUT
  }
}

export function setAuthSocialFacebook ({token, user}) {
  return {
    type: SET_AUTH_SOCIAL,
    social: 'facebook',
    token,
    user
  }
}

export function resetAuthSocial () {
  return {
    type: RESET_AUTH_SOCIAL
  }
}

export function raiseBalance (amount) {
  return {
    type: RAISE_BALANCE,
    amount
  }
}

export function subtractBalance (amount) {
  return {
    type: RAISE_BALANCE,
    amount
  }
}

export function updateProfile (profile) {
  return {
    type: UPDATE_PROFILE,
    profile
  }
}

export function updateAvatar (avatar) {
  return {
    type: UPDATE_AVATAR,
    avatar
  }
}

export default {
  userLoginSuccess,
  userRegisterSuccess,
  userLoginFail,
  userLoginFetching,
  userLogout,
  setAuthSocialFacebook,
  resetAuthSocial,
  userAutoAuth,
  raiseBalance,
  subtractBalance,
  updateProfile,
  updateAvatar
}
