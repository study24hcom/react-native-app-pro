import {AWAIT_MARKER} from 'redux-await'
import SearchApi from 'api/SearchApi'

export const SEARCH_KEYWORD = 'SEARCH/search-keyword'
export const SEARCH_USER = 'SEARCH/search-user'
export const SEARCH_QUIZLIST = 'SEARCH/search-quizlist'
export const SET_QUIZLIST_DEFAULT = 'SEARCH/set-quizlist-default'
export const SET_USER_DEFAULT = 'SEARCH/set-user-default'

export function searchKeyWord ({keywords}) {
  return {
    type: SEARCH_KEYWORD,
    AWAIT_MARKER,
    payload: {
      searchKeyWord: SearchApi.getKeywordsBySearch({keywords})
    }
  }
}

export function searchUser ({keywords}) {
  return {
    type: SEARCH_USER,
    AWAIT_MARKER,
    payload: {
      searchUser: SearchApi.getUsersBySearch({keywords})
    }
  }
}

export function searchQuizlist ({keywords}) {
  return {
    type: SEARCH_QUIZLIST,
    AWAIT_MARKER,
    payload: {
      searchQuizlist: SearchApi.getQuizListsBySearch({keywords, itemPerPage: 10})
    }
  }
}

export function setQuizlistDefault (quizLists) {
  return {
    type: SET_QUIZLIST_DEFAULT,
    quizLists
  }
}

export function setUserDefault (users) {
  return {
    type: SET_USER_DEFAULT,
    users
  }
}
