import Api from 'api/AuthApi'
import {AWAIT_MARKER} from 'redux-await'

export const GET_PARTNER_LIST = 'USER/get-partner-list'
export const LOAD_MORE_PARTNER_LIST = 'USER/load-more-partner-list'

export function getPartnerList ({page = 1, itemPerPage = 12} = {}) {
  return {
    type: GET_PARTNER_LIST,
    AWAIT_MARKER,
    payload: {
      getPartnerList: Api.getPartnerList({page, itemPerPage})
    }
  }
}

export function loadMorePartnerList ({page = 1, itemPerPage = 12} = {}) {
  return {
    type: LOAD_MORE_PARTNER_LIST,
    AWAIT_MARKER,
    payload: {
      loadMorePartnerList: Api.getPartnerList({page, itemPerPage})
    }
  }
}
