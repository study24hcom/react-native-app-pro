import { AWAIT_MARKER } from 'redux-await'
import QuizApi from 'api/QuizApi'

export const SET_QUIZZES = 'QUIZ/set-quizzes'
export const ADD_QUIZZES = 'QUIZ/add-quizzes'
export const SET_INFO = 'QUIZ/set-info'
export const RESET_INFO = 'QUIZ/reset-info'
export const APPEND_QUIZZES = 'QUIZ/append-quizzes'
export const UPDATE_INFO = 'QUIZ/update-info'
export const UPDATE_PROPERTY = 'QUIZ/update-property'
export const ADD_QUIZ = 'QUIZ/add-quiz'
export const REMOVE_QUIZ = 'QUIZ/remove-quiz'
export const UPDATE_QUIZ_QUESTION = 'QUIZ/update-question'
export const UPDATE_QUIZ_KEY = 'QUIZ/update-quiz-key'
export const UPDATE_QUIZ_CORRECT_ANSWER_INDEX =
  'QUIZ/update-correct-answer-index'
export const UPDATE_QUIZ_CHOOSE_ANSWER_INDEX = 'QUIZ/update-choose-answer-index'
export const UPDATE_QUIZ_SUGGEST_ANSWER = 'QUIZ/update-suggest-answer'
export const UPDATE_QUIZ_TOGGLE_SUGGEST_ANSWER =
  'QUIZ/update-toggle-suggest-answer'
export const QUIZ_ADD_ANSWER = 'QUIZ/add-answer'
export const QUIZ_UPDATE_ANSWER = 'QUIZ/update-answer'
export const QUIZ_REMOVE_ANSWER = 'QUIZ/remove-answer'
export const GET_QUIZ_LIST = 'QUIZ/get-quiz-list'
export const GET_QUIZ_LIST_AUTH = 'QUIZ/get-quiz-list-for-auth'
export const GET_QUIZ_LIST_QUIZZES = 'QUIZ/get-quiz-list-quizzes'
export const SHUFFLE_QUIZZES = 'QUIZ/shuffle-quizzes'
export const PLAY_QUIZ_LIST = 'QUIZ/play-quiz-list'
export const UPDATE_ACCESS_LOG_ID = 'QUIZ/update-access-log-id'
export const SET_QUIZ_LIST_PURCHASED = 'QUIZ/set-quiz-list-purchased'
export const SET_QUIZ_LIST_FROM_RAW = 'QUIZ/set-quiz-list-from-raw'
export const SET_PDF_FILE = 'QUIZ/set-pdf-file'
export const TOGGLE_VIEW_PDF = 'QUIZ/toggle-view-pdf'
export const RESET_QUIZ_LIST = 'QUIZ/reset-quiz-list'

export function getQuizList (slug, type = 'only-description') {
  return {
    type: GET_QUIZ_LIST,
    AWAIT_MARKER,
    payload: {
      getQuizList: QuizApi.getQuizList(slug, type)
    }
  }
}

export function setPdfFile (pdfFile) {
  return {
    type: SET_PDF_FILE,
    pdfFile: pdfFile
  }
}

export function toggleViewPdf () {
  return {
    type: TOGGLE_VIEW_PDF
  }
}

export function toggleDraftPreview (value) {
  return dispatch => {
    dispatch({
      type: UPDATE_PROPERTY,
      key: 'isDraftPreview',
      value
    })
  }
}

export function shuffeeQuizzes () {
  return {
    type: SHUFFLE_QUIZZES
  }
}

export function setQuizListFromRaw (quizListRaw) {
  return {
    type: SET_QUIZ_LIST_FROM_RAW,
    quizListRaw
  }
}

export function getQuizListAuth (slug) {
  return {
    type: GET_QUIZ_LIST_AUTH,
    AWAIT_MARKER,
    payload: {
      getQuizList: QuizApi.getQuizList(slug, '')
    }
  }
}

export function setQuizListPurchased (quizListKey) {
  return {
    type: SET_QUIZ_LIST_PURCHASED
  }
}

export function playQuizList (quizListKey) {
  return dispatch => {
    dispatch({
      type: PLAY_QUIZ_LIST
    })
    dispatch({
      type: UPDATE_ACCESS_LOG_ID,
      AWAIT_MARKER,
      payload: {
        playQuizList: QuizApi.playQuizList(quizListKey)
      }
    })
  }
}

export function getQuizListQuizzes (slug, isAllField = false) {
  return {
    type: GET_QUIZ_LIST_QUIZZES,
    AWAIT_MARKER,
    payload: {
      getQuizListQuizzes: QuizApi.getQuizList(
        slug,
        isAllField ? 'all-field' : 'only-view'
      )
    }
  }
}

export function addQuiz () {
  return {
    type: ADD_QUIZ
  }
}

export function addQuizzes (quizzes) {
  return {
    type: ADD_QUIZZES,
    quizzes
  }
}

export function setQuizzes (quizzes) {
  return {
    type: SET_QUIZZES,
    quizzes
  }
}

export function setInfo (info) {
  return {
    type: SET_INFO,
    info
  }
}

export function resetInfo (info) {
  return {
    type: RESET_INFO,
    info
  }
}

export function appendQuizzes (quizzes) {
  return {
    type: APPEND_QUIZZES,
    quizzes
  }
}

export function updateInfo ({ key, value }) {
  return {
    type: UPDATE_INFO,
    key,
    value
  }
}

export function updateProperty ({ key, value }) {
  return {
    type: UPDATE_PROPERTY,
    key,
    value
  }
}

export function updateTimestamp (timestamp) {
  return dispatch => {
    dispatch(updateProperty({key: 'timestamp', value: timestamp}))
  }
}

export function updateQuizKey ({ quizId, key, value }) {
  return {
    type: UPDATE_QUIZ_KEY,
    quizId,
    key,
    value
  }
}

export function updateQuizTags ({ quizId, tags }) {
  return dispatch => {
    dispatch(
      updateQuizKey({
        quizId,
        key: 'tags',
        value: tags
      })
    )
  }
}

export function updateQuizLevels ({ quizId, level }) {
  return dispatch => {
    dispatch(
      updateQuizKey({
        quizId,
        key: 'level',
        value: level
      })
    )
  }
}

export function updateQuizTimePlay ({ quizId, timePlay }) {
  return dispatch => {
    dispatch(
      updateQuizKey({
        quizId,
        key: 'timePlay',
        value: timePlay
      })
    )
  }
}

export function updateQuizFocusId ({ quizId }) {
  return dispatch => {
    dispatch(
      updateProperty({
        key: 'quizFocusId',
        value: quizId
      })
    )
  }
}

export function updateQuizQuestion ({ quizId, question }) {
  return {
    type: UPDATE_QUIZ_QUESTION,
    quizId,
    question
  }
}

export function updateQuizSuggestAnswer ({ quizId, suggestAnswer }) {
  return {
    type: UPDATE_QUIZ_SUGGEST_ANSWER,
    quizId,
    suggestAnswer
  }
}

export function updateQuizToggleSuggestAnswer ({ quizId, isSuggestAnswer }) {
  return {
    type: UPDATE_QUIZ_TOGGLE_SUGGEST_ANSWER,
    quizId,
    isSuggestAnswer
  }
}

export function updateQuizChooseAnswerIndex ({ quizId, chooseAnswerIndex }) {
  return {
    type: UPDATE_QUIZ_CHOOSE_ANSWER_INDEX,
    quizId,
    chooseAnswerIndex
  }
}

export function updateQuizCorrectAnswerIndex ({ quizId, correctAnswerIndex }) {
  return {
    type: UPDATE_QUIZ_CORRECT_ANSWER_INDEX,
    quizId,
    correctAnswerIndex
  }
}

export function removeQuiz ({ quizId }) {
  return {
    type: REMOVE_QUIZ,
    quizId
  }
}

export function addQuizAnswer ({ quizId }) {
  return {
    type: QUIZ_ADD_ANSWER,
    quizId
  }
}

export function updateQuizAnswer ({ quizId, answerId, answer = '' }) {
  return {
    type: QUIZ_UPDATE_ANSWER,
    quizId,
    answerId,
    answer
  }
}

export function removeQuizAnswer ({ quizId, answerId }) {
  return {
    type: QUIZ_REMOVE_ANSWER,
    quizId,
    answerId
  }
}

export function resetQuizList () {
  return {
    type: RESET_QUIZ_LIST
  }
}

export default {
  getQuizList,
  setPdfFile,
  toggleViewPdf,
  playQuizList,
  updateInfo,
  setQuizzes,
  appendQuizzes,
  addQuiz,
  removeQuiz,
  updateQuizCorrectAnswerIndex,
  updateQuizChooseAnswerIndex,
  updateQuizSuggestAnswer,
  updateQuizToggleSuggestAnswer,
  updateQuizQuestion,
  addQuizAnswer,
  updateQuizAnswer,
  removeQuizAnswer,
  updateQuizFocusId,
  updateProperty,
  updateQuizKey,
  updateQuizTags,
  updateQuizTimePlay,
  updateQuizLevels,
  setQuizListPurchased,
  resetQuizList,
  toggleDraftPreview
}
