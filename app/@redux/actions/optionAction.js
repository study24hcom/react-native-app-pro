export const SET_IS_MOBILE = 'OPTION/set-is-mobile'
export const SET_IS_SERVER = 'OPTION/set-is-server'
export const SET_KEYWORDS = 'OPTION/set-keywords'

export function setOptionIsMobile ({ isMobile } = {}) {
  return {
    type: SET_IS_MOBILE,
    isMobile: !!isMobile
  }
}

export function setOptionIsServer ({ isServer } = {}) {
  return {
    type: SET_IS_SERVER,
    isServer: !!isServer
  }
}

export function setKeywordsSearch ({ keywords } = {}) {
  return {
    type: SET_KEYWORDS,
    keywords: keywords
  }
}

export default {
  setOptionIsMobile,
  setOptionIsServer,
  setKeywordsSearch
}
