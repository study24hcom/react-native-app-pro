import QuizApi from 'api/QuizApi'
import {AWAIT_MARKER} from 'redux-await'

export const GET_ACTIVITIES = 'ACTIVITY/get-activity'
export const LOAD_MORE_ACTIVITIES = 'ACTIVITY/load-more-activity'

export function getActivities ({page = 1, itemPerPage = 10} = {}) {
  return {
    type: GET_ACTIVITIES,
    AWAIT_MARKER,
    payload: {
      getActivities: QuizApi.getActivities({page, itemPerPage})
    }
  }
}
export function loadMoreActivities ({page = 1, itemPerPage = 10} = {}) {
  return {
    type: LOAD_MORE_ACTIVITIES,
    AWAIT_MARKER,
    payload: {
      loadMoreActivities: QuizApi.getActivities({page, itemPerPage})
    }
  }
}

export default {
  getActivities,
  loadMoreActivities
}
