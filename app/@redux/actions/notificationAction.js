import {AWAIT_MARKER} from 'redux-await'
import QuizApi from 'api/QuizApi'

export const GET_NOTIFICATIONS = 'NOTIFICATION/get-notifications'
export const LOAD_MORE_NOTIFICATIONS = 'NOTIFICATION/load-more-notification'
export const UPDATE_NEW_NOTIFICATIONS = 'NOTIFICATION/get-news-notification'
export const UPDATE_VIEW_NOTIFICATION = 'NOTIFICATION/update-view-notification'
export const MARK_VIEWED_ALL = 'NOTIFCATION/mark-viewed-all'
export const MARK_CHECKED = 'NOTIFCATION/mark-checked'

export function getNotifications ({page, itemPerPage = 12} = {}) {
  return {
    type: GET_NOTIFICATIONS,
    AWAIT_MARKER,
    payload: {
      getNotifications: QuizApi.getNotifications({page, itemPerPage})
    }
  }
}

export function loadMoreNotifications ({page, itemPerPage = 12} = {}) {
  return {
    type: LOAD_MORE_NOTIFICATIONS,
    AWAIT_MARKER,
    payload: {
      loadMoreNotifications: QuizApi.getNotifications({page, itemPerPage})
    }
  }
}

export function getNewNotifications ({autoFetch = true} = {}) {
  return async dispatch => {
    let res = await QuizApi.getNewNotifications()
    if (res.new_notifications_length >= 0) {
      dispatch({
        type: UPDATE_NEW_NOTIFICATIONS,
        newNotifications: res.new_notifications_length
      })
    }
    if (autoFetch) {
      dispatch(getNotifications())
    }
  }
}

export function markViewedAll () {
  QuizApi.notificationAllMarkAsViewed()
  return {
    type: MARK_VIEWED_ALL
  }
}

export function markChecked (notificationId) {
  QuizApi.notificationMarkAsChecked(notificationId)
  return {
    type: MARK_CHECKED,
    notificationId
  }
}

export function resetNewNotifications () {
  return dispatch => {
    dispatch({
      type: UPDATE_NEW_NOTIFICATIONS,
      newNotifications: 0
    })
    dispatch(markViewedAll())
  }
}
