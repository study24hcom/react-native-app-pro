import QuizApi from 'api/QuizApi'
import { AWAIT_MARKER } from 'redux-await'
import {
  QUIZ_LISTS_BY_CATEGORY,
  QUIZ_LISTS_BY_SEARCH,
  QUIZ_LISTS_BY_ME,
  QUIZ_LISTS_BY_HISTORY,
  QUIZ_LISTS_BY_USER,
  QUIZ_LISTS_BY_NEWS,
  QUIZ_LISTS_BY_TAG,
  QUIZ_LISTS_BY_FOLLOWED,
  QUIZ_LISTS_BY_BOOKMARK
} from 'constants/quizLists'

export const GET_QUIZ_LISTS_BY_KEY = 'QUIZ-LISTS/get-quiz-list-by-key'
export const SET_QUIZ_LISTS_BY_KEY_LOAD_MORE =
  'QUIZ-LISTS/set-quiz-lists-by-key-load-more'

export function getQuizListsByKey ({key, payloadKey, loadMore, api}) {
  return dispatch => {
    dispatch({
      type: SET_QUIZ_LISTS_BY_KEY_LOAD_MORE,
      key,
      loadMore
    })
    dispatch({
      type: GET_QUIZ_LISTS_BY_KEY,
      AWAIT_MARKER,
      key,
      payloadKey,
      loadMore,
      payload: {
        [payloadKey]: api
      }
    })
  }
}

export function getQuizListsByCategory ({categoryKey, page = 1, itemPerPage = 10, loadMore = false} = {}) {
  return getQuizListsByKey({
    key: QUIZ_LISTS_BY_CATEGORY.key,
    payloadKey: QUIZ_LISTS_BY_CATEGORY.payload,
    loadMore,
    api: QuizApi.getQuizListsByCategory({key: categoryKey, page, itemPerPage})
  })
}

export function getQuizListsBySearch ({keywords, page = 1, itemPerPage = 10, loadMore = false} = {}) {
  return getQuizListsByKey({
    key: QUIZ_LISTS_BY_SEARCH.key,
    payloadKey: QUIZ_LISTS_BY_SEARCH.payload,
    loadMore,
    api: QuizApi.getQuizListsBySearch({keywords, page, itemPerPage})
  })
}

export function getQuizListsByMe ({page = 1, itemPerPage = 10, loadMore = false} = {}) {
  return getQuizListsByKey({
    key: QUIZ_LISTS_BY_ME.key,
    payloadKey: QUIZ_LISTS_BY_ME.payload,
    loadMore,
    api: QuizApi.getQuizListsByMe({page, itemPerPage})
  })
}

export function getQuizListsByHistory ({page = 1, itemPerPage = 10, loadMore = false, username} = {}) {
  return getQuizListsByKey({
    key: QUIZ_LISTS_BY_HISTORY.key,
    payloadKey: QUIZ_LISTS_BY_HISTORY.payload,
    loadMore,
    api: QuizApi.getQuizListsByHistoryUser({page, itemPerPage, username})
  })
}

export function getQuizListsByUser ({page = 1, itemPerPage = 10, loadMore = false, username} = {}) {
  return getQuizListsByKey({
    key: QUIZ_LISTS_BY_USER.key,
    payloadKey: QUIZ_LISTS_BY_USER.payload,
    loadMore,
    api: QuizApi.getQuizListsByUser({page, itemPerPage, username})
  })
}

export function getQuizListsByNews ({page = 1, itemPerPage = 10, loadMore = false} = {}) {
  return getQuizListsByKey({
    key: QUIZ_LISTS_BY_NEWS.key,
    payloadKey: QUIZ_LISTS_BY_NEWS.payload,
    loadMore,
    api: QuizApi.getQuizListsByNews({page, itemPerPage})
  })
}

export function getQuizListsByTag ({page = 1, itemPerPage = 10, loadMore = false, tagName} = {}) {
  return getQuizListsByKey({
    key: QUIZ_LISTS_BY_TAG.key,
    payloadKey: QUIZ_LISTS_BY_TAG.payload,
    loadMore,
    api: QuizApi.getQuizListsByTag({page, itemPerPage, tagName})
  })
}

export function getQuizListsByFollowed ({page = 1, itemPerPage = 10, loadMore = false} = {}) {
  return getQuizListsByKey({
    key: QUIZ_LISTS_BY_FOLLOWED.key,
    payloadKey: QUIZ_LISTS_BY_FOLLOWED.payload,
    loadMore,
    api: QuizApi.getQuizListsByFollowed({page, itemPerPage})
  })
}

export function getQuizListsByBookmark ({page = 1, itemPerPage = 10, loadMore = false} = {}) {
  return getQuizListsByKey({
    key: QUIZ_LISTS_BY_BOOKMARK.key,
    payloadKey: QUIZ_LISTS_BY_BOOKMARK.payload,
    loadMore,
    api: QuizApi.getQuizListsByBookmark({page, itemPerPage})
  })
}

export default {
  getQuizListsByCategory,
  getQuizListsBySearch,
  getQuizListsByMe,
  getQuizListsByUser,
  getQuizListsByHistory,
  getQuizListsByNews,
  getQuizListsByTag,
  getQuizListsByFollowed,
  getQuizListsByBookmark
}
