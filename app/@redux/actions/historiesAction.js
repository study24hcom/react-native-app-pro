import {loadState, saveState, deleteState} from 'utils/localStorage'
import SearchApi from 'api/SearchApi'

export const SAVE_HISTORIES = 'HISTORIES/SAVE_HISTORIES'
export const GET_HISTORIES = 'HISTORIES/GET_HISTORIES'
export const DELETE_HISTORIES = 'HISTORIES/DELETE_HISTORIES'

export function saveHistories (history) {
  SearchApi.saveKeyWordBySearch({keyword: history}).then(r => {
    console.tron.log(r)
  })
  saveState(history)
  return {
    type: SAVE_HISTORIES,
    history
  }
}

export function getHistories () {
  return async dispatch => {
    let data = await loadState()
    if (data) {
      dispatch({
        type: GET_HISTORIES,
        data
      })
    }
  }
}

export function deleteHistories () {
  deleteState()
  return {
    type: DELETE_HISTORIES
  }
}
