import QuizApi from 'api/QuizApi'
import {AWAIT_MARKER} from 'redux-await'

export const GET_PLAYLISTS = 'PLAYLIST/get-playlists'
export const GET_PLAYLISTS_ME = 'PLAYLIST/get-playlists-me'
export const GET_FEATURED_PLAYLISTS = 'PLAYLIST/get-featured-playlists'
export const GET_FEATURED_PLAYLISTS_BY_USER = 'PLAYLISTS/get-featured-playlists-by-user'
export const LOAD_MORE_FEATURED_PLAYLISTS_BY_USER = 'PLAYLISTS/load-more-featured-playlists-by-user'
export const RESET_PLAYLIST_CURRENT = 'PLAYLIST/reset-sliderCarousel-current'
export const LOAD_MORE_PLAYLISTS = 'PLAYLISTS/load-more-playlists'
export const LOAD_MORE_PLAYLISTS_ME = 'PLAYLISTS/load-more-playlists-me'
export const GET_PLAYLIST = 'PLAYLIST/get-sliderCarousel'
export const SET_PLAYLIST = 'PLAYLIST/set-sliderCarousel'
export const SET_INFO_PLAYLIST = 'PLAYLIST/set-info-sliderCarousel'
export const SET_PLAYLIST_CURRENT_QUIZLISTS =
  'PLAYLIST/set-sliderCarousel-current-quizlists'
export const SET_PLAYLIST_CURRENT_INFO =
  'PLAYLIST/set-sliderCarousel-current-info-raw'
export const ADD_QUIZLIST_TO_CURRENT = 'PLAYLIST/add-quizlist-to-current'

export function getFeturedPlaylists () {
  return {
    type: GET_FEATURED_PLAYLISTS,
    AWAIT_MARKER,
    payload: {
      getFeaturedPlaylists: QuizApi.getFeaturedPlaylists()
    }
  }
}

export function getFeaturedPlaylistsByUser ({
                                             page = 1,
                                             itemPerPage = 24,
                                             username
                                           }) {
  return {
    type: GET_FEATURED_PLAYLISTS_BY_USER,
    AWAIT_MARKER,
    payload: {
      getFeaturedPlaylistsByUser: QuizApi.getFeaturedPlaylistsByUser({
        page,
        itemPerPage,
        username
      })
    }
  }
}

export function loadMoreFeaturedPlaylistsByUser ({page = 1, itemPerPage = 24, username}) {
  return {
    type: LOAD_MORE_FEATURED_PLAYLISTS_BY_USER,
    AWAIT_MARKER,
    payload: {
      loadMoreFeaturedPlaylistsByUser: QuizApi.getFeaturedPlaylistsByUser({page, itemPerPage, username})
    }
  }
}

export function getPlaylists ({page = 1, itemPerPage = 10} = {}) {
  return {
    type: GET_PLAYLISTS,
    AWAIT_MARKER,
    payload: {
      getPlaylists: QuizApi.getPlaylists({page, itemPerPage})
    }
  }
}

export function getPlaylistsMe ({page = 1, itemPerPage = 10} = {}) {
  return {
    type: GET_PLAYLISTS_ME,
    AWAIT_MARKER,
    payload: {
      getPlaylistsMe: QuizApi.getPlaylistsMe({page, itemPerPage})
    }
  }
}

export function loadMorePlaylistsMe ({page = 1, itemPerPage = 10}) {
  return {
    type: LOAD_MORE_PLAYLISTS_ME,
    AWAIT_MARKER,
    payload: {
      loadMorePlaylistsMe: QuizApi.getPlaylistsMe({page, itemPerPage})
    }
  }
}

export function loadMorePlaylists ({page = 1, itemPerPage = 10}) {
  return {
    type: LOAD_MORE_PLAYLISTS,
    AWAIT_MARKER,
    payload: {
      loadMorePlaylists: QuizApi.getPlaylists({page, itemPerPage})
    }
  }
}

export function getPlaylist (playlistKey) {
  return {
    type: GET_PLAYLIST,
    AWAIT_MARKER,
    payload: {
      getPlaylist: QuizApi.getPlaylist(playlistKey)
    }
  }
}

export function setPlaylist (playlist) {
  return {
    type: SET_PLAYLIST,
    playlist
  }
}

export function setInfoPlaylist (info) {
  return {
    type: SET_INFO_PLAYLIST,
    info
  }
}

export function setPlaylistCurrentQuizlists (quizLists) {
  return {
    type: SET_PLAYLIST_CURRENT_QUIZLISTS,
    quizLists
  }
}

export function setPlaylistCurrentInfo (info) {
  return {
    type: SET_PLAYLIST_CURRENT_INFO,
    info
  }
}

export function addQuizlistToCurrent (quizList) {
  return {
    type: ADD_QUIZLIST_TO_CURRENT,
    quizList
  }
}

export function resetPlaylistCurrent () {
  return {
    type: RESET_PLAYLIST_CURRENT
  }
}

export default {
  getPlaylists,
  getFeturedPlaylists,
  getFeaturedPlaylistsByUser,
  loadMoreFeaturedPlaylistsByUser,
  loadMorePlaylists,
  getPlaylist,
  setPlaylist,
  setPlaylistCurrentQuizlists,
  setPlaylistCurrentInfo,
  addQuizlistToCurrent,
  getPlaylistsMe,
  loadMorePlaylistsMe,
  resetPlaylistCurrent,
  setInfoPlaylist
}
