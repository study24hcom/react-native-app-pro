import { AWAIT_MARKER } from 'redux-await'
import QuizApi from 'api/QuizApi'

export const SET_FOLLOW_USER = 'FOLLOW/set-follow-user'
export const UN_FOLLOW_USER = 'FOLLOW/un-follow-user'
export const SET_FOLLOW_TAG = 'FOLLOW/set-follow-tag'
export const UN_FOLLOW_TAG = 'FOLLOW/un-follow-tag'
export const GET_TAGS_FOLLOWED = 'FOLLOW/get-tag-followed'
export const GET_USERS_FOLLOWED = 'FOLLOW/get-users-followed'
export const UPDATE_IS_SHOW_BOX = 'FOLLOW/update-is-show-box'

export function setFollowUser (userId) {
  return {
    type: SET_FOLLOW_USER,
    userId
  }
}

export function updateIsShowBox (success) {
  return {
    type: UPDATE_IS_SHOW_BOX,
    success
  }
}

export function unFollowUser (userId) {
  return {
    type: UN_FOLLOW_USER,
    userId
  }
}

export function setFollowTag (tagName) {
  return {
    type: SET_FOLLOW_TAG,
    tagName
  }
}

export function unFollowTag (tagName) {
  return {
    type: UN_FOLLOW_TAG,
    tagName
  }
}

export function getTagsFollowed ({page = 1, itemPerPage = 30} = {}) {
  return {
    type: GET_TAGS_FOLLOWED,
    AWAIT_MARKER,
    payload: {
      getTagsFollowed: QuizApi.getTagsFollowed({page, itemPerPage})
    }
  }
}

export function getUsersFollowed ({page = 1, itemPerPage = 30} = {}) {
  return {
    type: GET_USERS_FOLLOWED,
    AWAIT_MARKER,
    payload: {
      getUsersFollowed: QuizApi.getUsersFollowed({page, itemPerPage})
    }
  }
}

export default {
  setFollowUser,
  unFollowUser,
  setFollowTag,
  unFollowTag,
  getTagsFollowed,
  getUsersFollowed,
  updateIsShowBox
}
