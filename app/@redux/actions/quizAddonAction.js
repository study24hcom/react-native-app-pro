import {AWAIT_MARKER} from 'redux-await'

import QuizApi from 'api/QuizApi'
import StatisticApi from 'api/StatisticApi'

export const GET_SCORES = 'QUIZ-ADDON/get-scores'
export const LOAD_MORE_SCORES = 'QUIZ-ADDON/load-more-scores'
export const GET_RATINGS = 'QUIZ-ADDON/get-ratings'
export const GET_AUTH_RATING = 'QUIZ-ADDON/get-auth-rating'
export const SET_AUTH_RATING = 'QUIZ-ADDON/set-auth-rating'
export const ADD_RATING = 'QUIZ-ADDON/add-rating'
export const LOAD_MORE_RATINGS = 'QUIZ-ADDON/load-more-ratings'
export const ADD_COMMENT_RATING = 'QUIZ-ADDON/add-comment-rating'
export const UPDATE_RATING = 'QUIZ-ADDON/update-comment-rating'
export const GET_AUTH_SCORE = 'QUIZ-ADDON/get-auth-score'
export const SET_AUTH_SCORE = 'QUIZ-ADDON/set-auth-score'
export const ADD_PERMISSION = 'QUIZ-ADDON/add-permission'
export const GET_PERMISSIONS = 'QUIZ-ADDON/get-permission'
export const DELETE_PERMISSION = 'QUIZ-ADDON/delete-permission'
export const GET_STATISTIC_BY_DAY = 'QUIZ-ADDON/get-statistic-by-day'
export const GET_STATISTIC_BY_WEEK = 'QUIZ-ADDON/get-statistic-by-week'
export const GET_STATISTIC_BY_MONTH = 'QUIZ-ADDON/get-statistic-by-month'
export const GET_STATISTIC_BY_OS = 'QUIZ-ADDON/get-statistic-by-os'
export const UPDATE_QUIZ_FOCUS = 'QUIZ-ADDON/update-quiz-focus'

export function getScores ({quizListKey = '', page = 1, itemPerPage = 20}) {
  return {
    type: GET_SCORES,
    AWAIT_MARKER,
    payload: {
      getScores: QuizApi.getQuizListScores(quizListKey, page, itemPerPage)
    }
  }
}

export function loadMoreScores ({
                                 quizListKey = '',
                                 page = 1,
                                 itemPerPage = 20
                               }) {
  return {
    type: LOAD_MORE_SCORES,
    AWAIT_MARKER,
    payload: {
      loadMoreScores: QuizApi.getQuizListScores(quizListKey, page, itemPerPage)
    }
  }
}

export function getAuthRating ({quizListKey}) {
  return {
    type: GET_AUTH_RATING,
    AWAIT_MARKER,
    payload: {
      getAuthRating: QuizApi.checkQuizListRating({quizListKey})
    }
  }
}

export function setAuthRating (rating) {
  return {
    type: SET_AUTH_RATING,
    rating
  }
}

export function getAuthScore ({quizListKey = ''}) {
  return {
    type: GET_AUTH_SCORE,
    AWAIT_MARKER,
    payload: {
      getAuthScore: QuizApi.getQuizListAuthScore(quizListKey)
    }
  }
}

export function setAuthScore (authScore) {
  return {
    type: SET_AUTH_SCORE,
    authScore
  }
}

export function getRatings ({quizListKey = '', page = 1, itemPerPage = 10}) {
  return {
    type: GET_RATINGS,
    AWAIT_MARKER,
    payload: {
      getRatings: QuizApi.getQuizListRatings(quizListKey, page, itemPerPage)
    }
  }
}

export function addRating (rating) {
  return {
    type: ADD_RATING,
    rating
  }
}

export function loadMoreRatings ({
                                  quizListKey = '',
                                  page = 1,
                                  itemPerPage = 10
                                }) {
  return {
    type: LOAD_MORE_RATINGS,
    AWAIT_MARKER,
    payload: {
      loadMoreRatings: QuizApi.getQuizListRatings(
        quizListKey,
        page,
        itemPerPage
      )
    }
  }
}

export function updateRating ({ratingId, ratingData}) {
  return {
    type: UPDATE_RATING,
    ratingId,
    ratingData
  }
}

export function getPermissions ({
                                 quizListKey = '',
                                 page = 1,
                                 itemPerPage = 20
                               }) {
  return {
    type: GET_PERMISSIONS,
    AWAIT_MARKER,
    payload: {
      getPermissions: QuizApi.getQuizListPermissions({
        quizListKey,
        page,
        itemPerPage
      })
    }
  }
}

export function addPermission ({quizListKey = '', permission}) {
  return {
    type: ADD_PERMISSION,
    AWAIT_MARKER,
    payload: {
      addPermission: QuizApi.addQuizListPermission({
        quizListKey,
        permission
      })
    }
  }
}

export function deletePermission ({quizListKey = '', permissionId}) {
  QuizApi.deleteQuizListPermission({quizListKey, permissionId})
  return {
    type: DELETE_PERMISSION,
    AWAIT_MARKER,
    permissionId
  }
}

export function getStatisticByDay (slug) {
  return {
    type: GET_STATISTIC_BY_DAY,
    AWAIT_MARKER,
    payload: {
      statisticByDay: StatisticApi.getQuizListReport({slug, range: 1})
    }
  }
}

export function getStatisticByWeek (slug) {
  return {
    type: GET_STATISTIC_BY_WEEK,
    AWAIT_MARKER,
    payload: {
      statisticByWeek: StatisticApi.getQuizListReport({slug, range: 7})
    }
  }
}

export function getStatisticByMonth (slug) {
  return {
    type: GET_STATISTIC_BY_MONTH,
    AWAIT_MARKER,
    payload: {
      statisticByMonth: StatisticApi.getQuizListReport({slug, range: 30})
    }
  }
}

export function getStatisticByOS (slug) {
  return {
    type: GET_STATISTIC_BY_OS,
    AWAIT_MARKER,
    payload: {
      statisticByOS: StatisticApi.getOsReport(slug)
    }
  }
}

export function updateQuizFocus (quizId) {
  return {
    type: UPDATE_QUIZ_FOCUS,
    quizId
  }
}

export default {
  getScores,
  getAuthRating,
  setAuthRating,
  loadMoreScores,
  getAuthScore,
  getRatings,
  loadMoreRatings,
  addRating,
  getPermissions,
  addPermission,
  deletePermission,
  getStatisticByDay,
  getStatisticByWeek,
  getStatisticByMonth,
  getStatisticByOS,
  updateQuizFocus
}
