import {AWAIT_MARKER} from 'redux-await'
import QuizApi from 'api/QuizApi'

export const GET_TAGS = 'TAG/get-tags'
export const GET_TAG = 'TAG/get-tag'
export const GET_FEATURED_TAGS = 'TAG/get-featured-tag'
export const GET_TAGS_GROUP = 'TAG/get-tags-group'
export const CREATE_FAKE_TAG = 'TAG/create-fake-tag'

export function getTags () {
  return {
    type: GET_TAGS,
    AWAIT_MARKER,
    payload: {
      getTags: QuizApi.getTags()
    }
  }
}

export function createFakeTag (tag) {
  return {
    type: CREATE_FAKE_TAG,
    tag
  }
}

export function getFeaturedTags () {
  return {
    type: GET_FEATURED_TAGS,
    AWAIT_MARKER,
    payload: {
      getFeaturedTags: QuizApi.getFeaturedTags()
    }
  }
}

export function getTagsGroup ({page = 1, itemPerPage = 10}) {
  return {
    type: GET_TAGS_GROUP,
    AWAIT_MARKER,
    payload: {
      getTagsGroup: QuizApi.getTagsByGroup({page, itemPerPage})
    }
  }
}
