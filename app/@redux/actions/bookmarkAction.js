import { AWAIT_MARKER } from 'redux-await'
import QuizApi from 'api/QuizApi'

export const SET_BOOKMARK_QUIZLIST = 'BOOKMARK/set-bookmark-quizlist'
export const UN_BOOKMARK_QUIZLIST = 'FOLLOW/un-bookmark-quizlist'
export const CHECK_BOOKMARK = 'FOLLOW/check-bookmark'
export const UPDATE_IS_SHOW_BOX = 'FOLLOW/update-is-show-box'

export function setBookmarkQuizlist (quizlistId) {
  return {
    type: SET_BOOKMARK_QUIZLIST,
    quizlistId
  }
}

export function unBookmarkQuizlist (quizlistId) {
  return {
    type: UN_BOOKMARK_QUIZLIST,
    quizlistId
  }
}

export function checkBookmark (quizlistId) {
  return {
    type: CHECK_BOOKMARK,
    AWAIT_MARKER,
    quizlistId,
    payload: {
      checkBookmark: QuizApi.checkbookmarkQuizlist(quizlistId)
    }
  }
}

export function updateIsShowBox (isShowBox) {
  return {
    type: UPDATE_IS_SHOW_BOX,
    isShowBox
  }
}

export default {
  setBookmarkQuizlist,
  unBookmarkQuizlist,
  checkBookmark,
  updateIsShowBox
}
