import { AWAIT_MARKER } from 'redux-await'
import AuthApi from 'api/AuthApi'

export const GET_PROFILE_INFO = 'PROFILE-ACTION/user-profile-info'

export function getProfileInfo (username) {
  return {
    type: GET_PROFILE_INFO,
    AWAIT_MARKER,
    payload: {
      getProfileInfo: AuthApi.getProfileInfo(username)
    }
  }
}
