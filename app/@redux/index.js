import { combineReducers } from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../sagas/'
import Reducers from './reducers'

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers(Reducers)

  return configureStore(rootReducer, rootSaga)
}
