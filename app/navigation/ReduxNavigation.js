import React from 'react'
import {connect} from 'react-redux'
import {addNavigationHelpers} from 'react-navigation'

export default function createReduxNavigation (AppNavigationComponent) {
  @connect(state => ({
    nav: state.nav
  }))
  class ReduxNavigation extends React.Component {
    render () {
      const {dispatch, nav} = this.props
      const navigation = addNavigationHelpers({
        dispatch,
        state: nav
      })
      return <AppNavigationComponent navigation={navigation} />
    }
  }

  return ReduxNavigation
}
