import React from 'react'
import {StackNavigator, DrawerNavigator} from 'react-navigation'
import {Animated} from 'react-native'
import {
  HomeNavigator,
  PlayListNavigator,
  TagNavigator
} from './HomeNavigator'
import QuizListDetailScreen from 'containers/QuizListDetailScreen'
import WelcomeScreen from 'containers/WelcomeScreen'
import RatingDetailScreen from 'containers/RatingDetailScreen'
import ScoreDetailScreen from 'containers/ScoreDetailScreen'
import PartnerScreen from 'containers/PartnerScreen'
import PlayQuizListScreen from 'containers/PlayQuizListScreen'
import HistoryQuizListScreen from 'containers/HistoryQuizListScreen'
import PlaylistsViewAllProfileMe from 'containers/PlaylistListViewAll/ProfileMe'
import QuizListsByMeViewAll from 'containers/QuizlistListViewAll/QuizListsByMeViewAll'
import QuizlistsByHistoryViewAll from 'containers/QuizlistListViewAll/QuizlistsByHistoryViewAll'
import PlaylistsViewAllProfileUser from 'containers/PlaylistListViewAll/ProfileUser'
import QuizlistsByUserViewAll from 'containers/QuizlistListViewAll/QuizlistsByUserViewAll'
import UserScreen from 'containers/UserScreen'
import TagsScreen from 'containers/TagsScreen'
import FollowScreen from 'containers/FollowScreen'
import NotificationScreen from 'containers/NotificationScreen'
import CategoryScreen from 'containers/CategoryScreen'
import SearchLayoutScreen from 'containers/SearchLayoutScreen'
import SearchScreen from 'containers/SearchScreen'
import CreatePlaylistScreen from 'containers/CreatePlaylistScreen'
import {defaultNavigationConfig, defaultNoNavigationConfig} from './defaultNavigationConfig'
import SettingScreen from '../containers/SettingScreen/index'
import LoginScreen from '../containers/LoginScreen/index'
import RegisterScreen from '../containers/RegisterScreen/index'
import SocialScreen from '../containers/SocialScreen/index'
import TagFeaturedScreen from '../containers/TagNavigator/TagFeaturedScreen/index'
import SlideMenuScreen from '../containers/SlideMenuScreen/index'
import ForgotPassScreen from '../containers/ForgotPassScreen/index'

// Manifest of possible screens

const WelcomeNavigator = StackNavigator(
  {
    tabWelcome: {screen: WelcomeScreen},
    tabLogin: {
      screen: LoginScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    tabRegister: {
      screen: RegisterScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    tabForgotPass: {
      screen: ForgotPassScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    }
  },
  {
    initialRouteName: 'tabWelcome',
    headerMode: 'screen',
    mode: 'modal',
    ...defaultNavigationConfig
  }
)

const HeaderNav = StackNavigator(
  {
    LaunchScreen: {screen: HomeNavigator},
    NotificationScreen: {screen: NotificationScreen},
    CreatePlaylistScreen: {screen: CreatePlaylistScreen},
    SettingScreen: {screen: SettingScreen},
    SlideMenuScreen: {screen: SlideMenuScreen}
  },
  {
    initialRouteName: 'LaunchScreen',
    mode: 'modal',
    headerMode: 'screen',
    ...defaultNoNavigationConfig
  }
)

const HomeSearch = StackNavigator(
  {
    HeaderNav: {
      screen: HeaderNav,
      navigationOptions: {
        header: null
      }
    },
    SearchLayoutScreen: {screen: SearchLayoutScreen}
  },
  {
    ...defaultNavigationConfig,
    transitionConfig: () => ({
      transitionSpec: {
        duration: 0,
        timing: Animated.timing
      }
    })
  }
)

const InfoNavigator = DrawerNavigator(
  {
    tabHome: {screen: HomeSearch}
  },
  {
    initialRouteName: 'tabHome',
    contentComponent:
      props => <SlideMenuScreen {...props} />
  }
)

const HomeNav = StackNavigator(
  {
    HomeSearch: {
      screen: InfoNavigator,
      navigationOptions: {
        header: null
      }
    },
    QuizListDetailScreen: {screen: QuizListDetailScreen},
    PlayQuizListScreen: {
      screen: PlayQuizListScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    HistoryQuizListScreen: {
      screen: HistoryQuizListScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    SearchScreen: {screen: SearchScreen},
    PartnerScreen: {screen: PartnerScreen},
    RatingDetailScreen: {screen: RatingDetailScreen},
    ScoreDetailScreen: {screen: ScoreDetailScreen},
    PlaylistDetailScreen: {screen: PlayListNavigator},
    TagsScreen: {screen: TagsScreen},
    PlaylistsViewAllProfileMe: {screen: PlaylistsViewAllProfileMe},
    PlaylistsViewAllProfileUser: {screen: PlaylistsViewAllProfileUser},
    QuizListsByMeViewAll: {screen: QuizListsByMeViewAll},
    QuizlistsByUserViewAll: {screen: QuizlistsByUserViewAll},
    QuizlistsByHistoryViewAll: {screen: QuizlistsByHistoryViewAll},
    UserScreen: {screen: UserScreen},
    CategoryScreen: {screen: CategoryScreen},
    SocialScreen: {screen: SocialScreen},
    TagFeaturedScreen: {screen: TagFeaturedScreen},
    TagNavigator: {screen: TagNavigator},
    FollowScreen: {screen: FollowScreen}
  },
  {
    headerMode: 'screen',
    initialRouteName: 'HomeSearch',
    ...defaultNavigationConfig
  }
)

export {HomeNav, WelcomeNavigator, FollowScreen}
