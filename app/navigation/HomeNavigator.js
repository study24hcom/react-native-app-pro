import React from 'react'
import {TabNavigator} from 'react-navigation'
import navigations from 'themes/tabNavigation'
import PlaylistInfoScreen from '../containers/PlaylistNavigator/PlaylistInfoScreen'
import PlaylistQuizlistScreen from '../containers/PlaylistNavigator/PlaylistQuizlistScreen'
import BookmarkScreen from '../containers/BookmarkScreen'
import tabName from './tabName'
import AppScreen from '../containers/AppScreen'
import HeaderLarge from '../components/HeaderLarge'
import InfoUserScreen from '../containers/InfoUserScreen'
import PlaylistListScreen from '../containers/PlaylistScreen/PlaylistListScreen/index'
import TagFeaturedScreen from '../containers/TagNavigator/TagFeaturedScreen'
import TagFollowScreen from '../containers/TagNavigator/TagFollowScreen/index'

AppScreen.navigationOptions = tabName.Home
PlaylistListScreen.navigationOptions = tabName.Playlist
BookmarkScreen.navigationOptions = tabName.Bookmark
InfoUserScreen.navigationOptions = tabName.UserInfo
PlaylistInfoScreen.navigationOptions = tabName.InfoPlayList
PlaylistQuizlistScreen.navigationOptions = tabName.QuizListPlayList
TagFollowScreen.navigationOptions = tabName.TagFollow
TagFeaturedScreen.navigationOptions = tabName.TagSocial

// Manifest of possible screens
const HomeNavigator = TabNavigator(
  {
    tabHome: {screen: AppScreen},
    tabPlaylist: {screen: PlaylistListScreen},
    tabBookmark: {screen: BookmarkScreen},
    tabInfo: {screen: InfoUserScreen}
  },
  {
    tabBarOptions: navigations.tabBarStyle,
    navigationOptions: ({navigation}) => ({
      header: <HeaderLarge navigation={navigation} />
    }),
    tabBarPosition: 'bottom',
    swipeEnabled: true,
    lazy: true,
    animationEnabled: false
  }
)
const PlayListNavigator = TabNavigator(
  {
    tabInfo: {screen: PlaylistInfoScreen},
    tabQuizList: {screen: PlaylistQuizlistScreen}
  },
  {
    swipeEnabled: true,
    tabBarOptions: navigations.tabBarStyle,
    navigationOptions: ({navigation}) => ({
      title: navigation.state.params.title ? `${navigation.state.params.title}` : 'Không tìm thấy nội dung'

    }),
    tabBarPosition: 'bottom',
    lazy: true,
    animationEnabled: false
  }
)

const TagNavigator = TabNavigator(
  {
    tabTagFollow: {screen: TagFollowScreen},
    tabTagSocial: {screen: TagFeaturedScreen}
  },
  {
    swipeEnabled: true,
    tabBarOptions: navigations.tabBarStyle,
    navigationOptions: ({navigation}) => ({
      title: `Tag nổi bật`
    }),
    tabBarPosition: 'bottom',
    lazy: true,
    animationEnabled: false
  }
)

export {HomeNavigator, PlayListNavigator, TagNavigator}
