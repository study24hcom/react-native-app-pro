import React from 'react'
import {default as IconStyle} from 'components/elements/Icon'
import {isIos} from 'utils'
import AvatarTabName from '../components/elements/tab-name-avatar/index'

const ICON_SIZE_ANDROID = 20
const ICON_SIZE_IOS = 20

const AVATA_SIZE_ANDROID = 28
const AVATA_SIZE_IOS = 35

const Icon = props => (
  <IconStyle {...props} size={isIos() ? ICON_SIZE_IOS : ICON_SIZE_ANDROID} />
)
const Avatar = props => (
  <AvatarTabName {...props} size={isIos() ? AVATA_SIZE_IOS : AVATA_SIZE_ANDROID} />
)

export const Home = {
  tabBarLabel: 'Học hành',
  tabBarIcon: ({tintColor}) => (
    <Icon color={tintColor} name='home' simpleLineIcon />
  )
}

export const Playlist = {
  title: 'Bộ Sưu Tập',
  tabBarLabel: 'Bộ Sưu tập',
  tabBarIcon: ({tintColor}) => (
    <Icon color={tintColor} name='layers' simpleLineIcon />
  )
}

export const Bookmark = {
  title: 'Đã Lưu',
  tabBarLabel: 'Đã Lưu',
  tabBarIcon: ({tintColor}) => (
    <Icon color={tintColor} name='bookmark-o' />
  )
}

export const UserInfo = {
  tabBarLabel: ' ',
  tabBarIcon: ({tintColor}) => (
    <Avatar color={tintColor} />
  )
}

export const InfoPlayList = {
  tabBarLabel: 'Thông Tin',
  tabBarIcon: ({tintColor}) => (
    <Icon color={tintColor} name='info' simpleLineIcon />
  )
}

export const QuizListPlayList = {
  tabBarLabel: 'Đề thi',
  tabBarIcon: ({tintColor}) => (
    <Icon color={tintColor} name='list' simpleLineIcon />
  )
}
export const Login = {
  title: 'Đăng Nhập',
  tabBarLabel: 'Đăng Nhập',
  tabBarIcon: ({tintColor}) => (
    <Icon color={tintColor} name='login' simpleLineIcon />
  )
}
export const Register = {
  title: 'Đăng Kí',
  tabBarLabel: 'Đăng kí',
  tabBarIcon: ({tintColor}) => (
    <Icon color={tintColor} name='user' simpleLineIcon />
  )
}

export const TagFollow = {
  tabBarLabel: 'Theo Dõi',
  tabBarIcon: ({tintColor}) => (
    <Icon color={tintColor} name='tag' simpleLineIcon />
  )
}

export const TagSocial = {
  tabBarLabel: 'Cộng Động',
  tabBarIcon: ({tintColor}) => (
    <Icon color={tintColor} name='globe' />
  )
}

export default {
  Home,
  Playlist,
  Bookmark,
  InfoPlayList,
  QuizListPlayList,
  Login,
  Register,
  UserInfo,
  TagFollow,
  TagSocial
}
