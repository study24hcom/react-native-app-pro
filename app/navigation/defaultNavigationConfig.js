import { TEXT } from '../constants/color'

export const defaultNavigationConfig = {
  cardStyle: {
    backgroundColor: '#fafbfb'
  },
  headerTintColor: '#fff',
  navigationOptions: () => ({
    headerBackTitle: null,
    headerPressColorAndroid: '#fff',
    headerStyle: {
      backgroundColor: TEXT.PRIMARY,
      borderBottomWidth: 0,
      borderBottomColor: TEXT.PRIMARY
    },
    headerTintColor: '#fafafa'
  })
}

export const defaultNoNavigationConfig = {
  cardStyle: {
    backgroundColor: '#fafbfb'
  },
  headerTintColor: '#fff',
  navigationOptions: () => ({
    headerBackTitle: null,
    gesturesEnabled: false,
    headerPressColorAndroid: '#fff',
    headerStyle: {
      backgroundColor: TEXT.PRIMARY,
      borderBottomWidth: 0,
      borderBottomColor: TEXT.PRIMARY
    },
    headerTintColor: '#fafafa'
  })
}
export default {defaultNoNavigationConfig, defaultNavigationConfig}
