import {postFetch, getFetch, putFetch} from '../utils/fetch'
import {API_RES_AUTH, MOBILE_API_QUIZ} from 'config'

const url = API_RES_AUTH

export function getAuthUrl (path) {
  return url + '/auth/' + path
}

export function getUserUrl (path) {
  return url + '/users/' + path
}

//
// export function getUserMe() {
//   return getFetch(getAuthUrl('me'))
// }

export function getUserMe () {
  return getFetch(`${MOBILE_API_QUIZ}/users/info/me`)
}

//
// export function getPartnerList ({page = 1, itemPerPage = 12}) {
//   return getFetch(`${API_QUIZ}/users/partners?page=${page}&item_per_page=${itemPerPage}`)
// }

export function getPartnerList ({page = 1, itemPerPage = 12}) {
  return getFetch(`${MOBILE_API_QUIZ}/users/partners?page=${page}&itemPerPage=${itemPerPage}`)
}

export function updateProfile (profileData) {
  return putFetch(getAuthUrl('profile'), profileData)
}

export function updateAvatar (imageBase64) {
  return putFetch(getAuthUrl('avatar'), {image_base64: imageBase64})
}

export function saveSessionToken (token) {
  return getFetch(`/save-user?token=${token}`)
}

export function checkFieldExists (field, value) {
  return getFetch(getAuthUrl(`check-exists?field=${field}&value=${value}`))
}

export function authRegister (user) {
  return postFetch(getAuthUrl(`register`), user)
}

// export function authLogin(user) {
//   return postFetch(getAuthUrl(`login`), user)
// }

export function forgotPassword (email) {
  return postFetch(getAuthUrl(`forgot-password`), {email})
}

export function forgotPasswordConfirm (email, code, password) {
  return putFetch(getAuthUrl(`forgot-password`), {email, code, password})
}

export function authLogin (user) {
  return postFetch(`${MOBILE_API_QUIZ}/users/login`, user)
}

export function socialCheckLogin (social, token) {
  return postFetch(getAuthUrl(`social/check-login`), {social, token})
}

// export function getProfileInfo(username) {
//   return getFetch(API_RES_AUTH + `/users/${username}`)
// }

export function getProfileInfo (username) {
  return getFetch(MOBILE_API_QUIZ + `/users/info/${username}`)
}

export function socialConfirmUser (user) {
  const {social, token, username, email, password} = user
  return postFetch(getAuthUrl(`social/confirm`), {
    social,
    token,
    username,
    email,
    password
  })
}

export function followUser (userId) {
  return getFetch(getUserUrl(`${userId}/follow`))
}

export function unFollowUser (userId) {
  return getFetch(getUserUrl(`${userId}/unfollow`))
}

export function checkFollowUser (userId) {
  return getFetch(getUserUrl(`${userId}/check-followed`))
}

export default {
  getUserMe,
  getProfileInfo,
  updateProfile,
  updateAvatar,
  checkFieldExists,
  saveSessionToken,
  authRegister,
  authLogin,
  socialCheckLogin,
  socialConfirmUser,
  getPartnerList,
  followUser,
  unFollowUser,
  checkFollowUser,
  forgotPassword,
  forgotPasswordConfirm
}
