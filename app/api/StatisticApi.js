import {getFetch} from '../utils/fetch'
import {API_STATISTIC} from 'config'

export function getUrl (path) {
  return API_STATISTIC + '/api/' + path
}

export function getQuizListReport ({slug, range}) {
  return getFetch(getUrl('quiz-lists-report/' + slug + '?range=' + range))
}

export function getOsReport (slug) {
  return getFetch(getUrl(`quiz-lists-os-report/${slug}`))
}

export default {
  getQuizListReport,
  getOsReport
}
