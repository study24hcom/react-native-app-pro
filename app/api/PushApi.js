import { API_PUSH } from 'config'
import { postFetch } from 'utils/fetch'

export function registerDeviceToken (data) {
  return postFetch(API_PUSH + '/register-device', data)
}

export default {
  registerDeviceToken
}
