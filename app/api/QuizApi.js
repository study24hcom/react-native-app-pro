import {postFetch, getFetch, putFetch, deleteFetch} from '../utils/fetch'
import {API_QUIZ, API_STATISTIC, SITE_INFO, MOBILE_API_QUIZ, MANAGER_API} from 'config'
import orderType from 'constants/orderType'
import uuid from 'uuid'

import permissionList from 'fake-data/permissionList'

const url = API_QUIZ

export function getUrl (path) {
  return url + '/' + path
}

export function getMobileUrl (path) {
  return MOBILE_API_QUIZ + '/' + path
}

export function getQuizList (key, type = '') {
  return getFetch(getUrl(`quiz-lists/${key}/${type}`))
}

export function getQuizListPermissions ({
                                         quizListKey,
                                         page = 1,
                                         itemPerPage = 20
                                       }) {
  return new Promise(resolve =>
    resolve({
      data: permissionList,
      pagination: {
        page: page,
        itemPerPage: itemPerPage
      }
    })
  )
}

export function addQuizListPermission ({quizListKey, permissions = []}) {
  return new Promise(resolve =>
    resolve(
      permissions.map(permission => ({
        ...permission,
        id: uuid.v4()
      }))
    )
  )
}

export function deleteQuizListPermission ({quizListKey, permissionId}) {
  return new Promise(resolve => resolve())
}

export function getQuizListScores (quizListKey, page = 1, itemPerPage = 5) {
  return getFetch(
    getUrl(
      `histories/quiz-lists/${quizListKey}?page=${page}&item_per_page=${itemPerPage}&order_by_type=${orderType.score.SCORE_DESC}`
    )
  )
}

export function getQuizListRatings (quizListKey, page = 1, itemPerPage = 5) {
  return getFetch(
    getUrl(
      `ratings/${quizListKey}?page=${page}&item_per_page=${itemPerPage}&order_by_type=${orderType.common.CREATED_AT_DESC}`
    )
  )
}

export function addQuizListRating ({quizListKey, rating, content}) {
  return postFetch(getUrl(`ratings`), {
    quiz_list_id: quizListKey,
    rating,
    content
  })
}

export function checkQuizListRating ({quizListKey}) {
  return getFetch(getMobileUrl('rating/' + quizListKey + '/check-rating'))
}

export function addCommentRating ({ratingId, content}) {
  return putFetch(getUrl(`ratings/${ratingId}`), {content})
}

export function updateCommentRating ({ratingId, content}) {
  return putFetch(getUrl(`ratings/${ratingId}`), {content})
}

export function getQuizListAuthScore (quizListKey) {
  return getFetch(getUrl(`histories/${quizListKey}/first-history`))
}

export function getScoreHistory (historyId) {
  return getFetch(getUrl(`histories/${historyId}`))
}

export function getQuizListsByCategory ({key, itemPerPage = 10, page = 1}) {
  return getFetch(
    getUrl(
      `quiz-lists/category/${key}?page=${page}&item_per_page=${itemPerPage}`
    )
  )
}

export function getQuizListsByMe ({itemPerPage = 10, page = 1}) {
  return getFetch(
    getUrl(`quiz-lists/me/?page=${page}&item_per_page=${itemPerPage}`)
  )
}

export function getQuizListsByFollowed ({itemPerPage = 10, page = 1}) {
  return getFetch(
    getUrl(`quiz-lists/followed?page=${page}&item_per_page=${itemPerPage}`)
  )
}

export function getQuizListsByHistoryUser ({
                                            itemPerPage = 10,
                                            page = 1,
                                            username = ''
                                          }) {
  return getFetch(
    getUrl(
      `quiz-lists/user/${username}/?page=${page}&item_per_page=${itemPerPage}&getByHistory=true`
    )
  )
}

export function getQuizListsByNews ({itemPerPage = 10, page = 1}) {
  return getFetch(
    getUrl(
      `quiz-lists?page=${page}&item_per_page=${itemPerPage}&order_by_type=${orderType.common.CREATED_AT_DESC}`
    )
  )
}

export function getQuizListsByUser ({
                                     itemPerPage = 10,
                                     page = 1,
                                     username = ''
                                   }) {
  return getFetch(
    getUrl(
      `quiz-lists/user/${username}/?page=${page}&item_per_page=${itemPerPage}`
    )
  )
}

export function getQuizListsBySearch ({keywords, page = 1, itemPerPage = 5}) {
  let stringQuery = `search?keyword=${keywords}&page=${page}&item_per_page=${itemPerPage}&order_by_type=${orderType.common.CREATED_AT_DESC}`
  return getFetch(getUrl(stringQuery))
}

export function getQuizListsBySearchByMe ({
                                           keywords,
                                           page = 1,
                                           itemPerPage = 10
                                         }) {
  let stringQuery = `search/me?keyword=${encodeURI(keywords)}&page=${page}&item_per_page=${itemPerPage}&order_by_type=${orderType.common.CREATED_AT_DESC}`
  return getFetch(getUrl(stringQuery))
}

export function getUsersBySearch ({keywords}) {
  let stringQuery = `users/search?keyword=${keywords}`
  return getFetch(getUrl(stringQuery))
}

export function getQuizListsByTopDay () {
  return getFetch(`${API_STATISTIC}/api/top-access-today`)
}

export function saveHistory (quizListKey, data, accessLogId) {
  data = {
    ...data,
    quiz_list_id: quizListKey,
    access_log_id: accessLogId
  }
  return postFetch(getUrl(`histories`), data)
}

export function getPlaylists ({page = 1, itemPerPage = 10}) {
  return getFetch(getUrl(`playlists?page=${page}&item_per_page=${itemPerPage}`))
}

export function getPlaylistsMe ({page = 1, itemPerPage = 10}) {
  return getFetch(
    getUrl(`playlists/me?page=${page}&item_per_page=${itemPerPage}`)
  )
}

export function getPlaylist (playlistKey) {
  return getFetch(getUrl(`playlists/${playlistKey}`))
}

export function getFeaturedPlaylists () {
  return getFetch(getUrl(`playlists/featured-main`))
}

export function getFeaturedPlaylistsByUser ({
                                             page = 1,
                                             itemPerPage = 10,
                                             username
                                           }) {
  return getFetch(
    getUrl(
      `playlists/featured/${username}?page=${page}&itemPerPage=${itemPerPage}`
    )
  )
}

export function createPlaylist (playlistFormData) {
  return postFetch(API_QUIZ + '/playlists/', playlistFormData)
}

export function updatePlaylist (playlistKey, playlistFormData) {
  return putFetch(API_QUIZ + '/playlists/' + playlistKey, playlistFormData)
}

export function playQuizList (quizListKey) {
  return getFetch(getUrl(`quiz-lists/${quizListKey}/count-round`))
}

export function getActivities ({itemPerPage = 10, page = 1}) {
  return getFetch(
    getUrl(`activities?page=${page}&item_per_page=${itemPerPage}`)
  )
}

export function getNotifications ({itemPerPage = 10, page = 1} = {}) {
  return getFetch(
    getUrl(`notifications?page=${page}&item_per_page=${itemPerPage}`)
  )
}

export function getNewNotifications () {
  return getFetch(getUrl('notifications/new-notifications'))
}

export function notificationMarkAsChecked (notificationId) {
  return getFetch(getUrl(`notifications/${notificationId}/mark-as-checked`))
}

export function notificationAllMarkAsViewed () {
  return getFetch(getUrl(`notifications/mark-as-viewed`))
}

export function getTimestamp () {
  return getFetch(`${SITE_INFO.PUBLIC_URL}/server-timestamp`)
}

export function registerPartenerRequest (data) {
  return postFetch(getUrl('partner-request'), data)
}

export function getTags () {
  return getFetch(getUrl('tags'))
}

export function getTagsByGroup ({page, itemPerPage}) {
  return getFetch(MANAGER_API + `/admin/tags/?itemPerPage=${itemPerPage}&page=${page}`)
}

export function getTagName (tagName) {
  return getFetch(getUrl('tags?tag_name=' + tagName))
}

export function followTag (tagName) {
  return getFetch(getUrl(`tags/${tagName}/follow`))
}

export function checkFollowTag (tagName) {
  return getFetch(getUrl(`tags/${tagName}/check-followed`))
}

export function unFollowTag (tagName) {
  return getFetch(getUrl(`tags/${tagName}/unfollow`))
}

export function getQuizListsByTag ({page, itemPerPage, tagName}) {
  return getFetch(
    getUrl(
      `quiz-lists?page=${page}&item_per_page=${itemPerPage}&tag_key=${encodeURI(tagName)}`
    )
  )
}

export function getTagsFollowed ({page, itemPerPage}) {
  return getFetch(`${MOBILE_API_QUIZ}/tags/followed?page=${page}&itemPerPage=${itemPerPage}`)
}

export function getFeaturedTags () {
  return getFetch(
    getUrl(`tags/featured`)
  )
}

export function getUsersFollowed ({page, itemPerPage}) {
  return getFetch(
    getUrl(
      `users/followed?page=${page}&itemPerPage=${itemPerPage}`
    )
  )
}

export function getQuizListsByBookmark ({itemPerPage = 10, page = 1}) {
  return getFetch(
    getMobileUrl(
      `users/bookmarks?page=${page}&item_per_page=${itemPerPage}`
    )
  )
}

export function bookmarkQuizlist (quizlistId) {
  return postFetch(getMobileUrl(`users/bookmark/${quizlistId}`))
}

export function checkbookmarkQuizlist (quizlistId) {
  return getFetch(getMobileUrl(`users/bookmark/${quizlistId}/check-bookmarked`))
}

export function unBookmarkQuizlist (quizlistId) {
  return deleteFetch(getMobileUrl(`users/bookmark/${quizlistId}`))
}

export function deleteQuizlist ({quizListKey}) {
  return deleteFetch(
    getUrl(`quiz-lists/${quizListKey}`)
  )
}

export default {
  getUrl,
  getQuizList,
  getQuizListScores,
  getQuizListRatings,
  addQuizListRating,
  updateCommentRating,
  addCommentRating,
  getQuizListAuthScore,
  getScoreHistory,
  saveHistory,
  getPlaylists,
  getPlaylistsMe,
  getPlaylist,
  getFeaturedPlaylistsByUser,
  getFeaturedPlaylists,
  createPlaylist,
  updatePlaylist,
  getQuizListsByCategory,
  getQuizListsByTopDay,
  getQuizListsBySearch,
  getQuizListsBySearchByMe,
  getUsersBySearch,
  getQuizListsByMe,
  getQuizListsByFollowed,
  getQuizListsByHistoryUser,
  getQuizListsByUser,
  getQuizListsByNews,
  playQuizList,
  getActivities,
  getNotifications,
  getNewNotifications,
  notificationMarkAsChecked,
  notificationAllMarkAsViewed,
  getQuizListPermissions,
  addQuizListPermission,
  deleteQuizListPermission,
  getTimestamp,
  registerPartenerRequest,
  getTags,
  getTagName,
  followTag,
  checkFollowTag,
  unFollowTag,
  getQuizListsByTag,
  getTagsFollowed,
  getUsersFollowed,
  getFeaturedTags,
  getQuizListsByBookmark,
  bookmarkQuizlist,
  checkbookmarkQuizlist,
  unBookmarkQuizlist,
  deleteQuizlist,
  checkQuizListRating,
  getTagsByGroup
}
