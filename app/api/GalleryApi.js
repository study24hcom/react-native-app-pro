import {getFetch} from '../utils/fetch'
import {MANAGER_API} from 'config'

export function getUrl (path) {
  return MANAGER_API + '/public/gallery-sliders/' + path
}

export function getGallerySlider () {
  return getFetch(getUrl('Mobile'))
}

export default {
  getGallerySlider
}
