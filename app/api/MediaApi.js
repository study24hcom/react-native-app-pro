import { API_MEDIA } from 'config'
import { postFetch } from 'utils/fetch'

export function uploadPhoto (data) {
  return postFetch(API_MEDIA + '/photo/upload', data, {
    dataType: 'formdata'
  })
}

export default {
  uploadPhoto
}
