import {getFetch, postFetch} from 'utils/fetch'
import {SEARCH_API, API_QUIZ} from 'config'
import orderType from 'constants/orderType'

const url = SEARCH_API

export function getUrl (path) {
  return url + '/' + path
}

export function getKeywordsBySearch ({keywords}) {
  return getFetch(getUrl(`suggest-keyword?keyword=${keywords}`))
}

export function saveKeyWordBySearch ({keyword}) {
  return postFetch(getUrl(`user/save-keyword`), {keyword})
}

export function getQuizListsBySearch ({keywords, page = 1, itemPerPage = 10}) {
  let stringQuery = `elastic/search/quiz_lists?keyword=${keywords}&itemPerPage=${itemPerPage}&page=${page}&order_by_type=${orderType.common.CREATED_AT_DESC}`
  return getFetch(getUrl(stringQuery))
}

export function getUsersBySearch ({keywords, page = 1, itemPerPage = 5}) {
  let stringQuery = `users/search?keyword=${keywords}&itemPerPage=${itemPerPage}&page=${page}&order_by_type=${orderType.common.CREATED_AT_DESC}`
  return getFetch(`${API_QUIZ}/${stringQuery}`)
}

export default {
  getQuizListsBySearch,
  saveKeyWordBySearch,
  getUsersBySearch,
  getKeywordsBySearch
}
