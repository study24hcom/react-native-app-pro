const weak = [
  'Không quan trọng quá khứ bạn khắc nghiệt thế nào, bạn luôn luôn có thể bắt đầu lại. Hãy cố gắng học nữa nhé bạn hiền của tôi :smile:',
  'Nếu hôm nay bạn gục ngã không phải vì bạn thất bại, mà là bạn đang học cách để thành công. Cố gắng và cố gắng hơn nữa :sunglasses:',
  'Đừng dân chân tại đây, hãy có gắng và cố gắng hơn nữa bạn của tôi :hugging:'
]

const medium = [
  'Học tập là cả 1 quá trình, vì vậy bạn phải luôn nổ lực và cố gắng liên tục :hugging:',
  'Luôn luôn nỗ lực và cố gắng, bạn sẽ đạt nhiều điều hơn trong cuộc sống :laughing:',
  'Đừng dân chân tại đây, hãy có gắng và cố gắng hơn nữa bạn của tôi :hugging:',
  'Muốn xây dựng đất nước, trước hết phải phát triển giáo dục. Muốn trị nước, phải trọng dụng người tài :kissing_closed_eyes:'
]

const rather = [
  'Học vấn do người siêng năng đạt được, tài sản do người tinh tế sở hữu, quyền lợi do người dũng cảm nắm giữ, thiên đường do người lương thiện xây dựng :nerd:',
  'Luôn luôn nỗ lực và cố gắng, bạn sẽ đạt nhiều điều hơn trong cuộc sống :laughing:',
  'Học tập là cả 1 quá trình, vì vậy bạn phải luôn nổ lực và cố gắng liên tục :hugging:'
]

const great = [
  'Muốn xây dựng đất nước, trước hết phải phát triển giáo dục. Muốn trị nước, phải trọng dụng người tài :kissing_closed_eyes:',
  'Học vấn do người siêng năng đạt được, tài sản do người tinh tế sở hữu, quyền lợi do người dũng cảm nắm giữ, thiên đường do người lương thiện xây dựng :nerd:',
  'Tốt lắm bạn hiền :hugging:, hãy giữ nhịp độ này và bức phá trong tương lai',
  'Học tập là cả 1 quá trình, vì vậy bạn phải luôn nổ lực và cố gắng liên tục :hugging:'
]

export default {
  weak,
  medium,
  rather,
  great
}
