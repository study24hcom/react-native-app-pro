export default {
  common: {
    UPDATED_AT_ASC: 'order_by_updated_date_asc',
    UPDATED_AT_DESC: 'order_by_updated_date_desc',
    CREATED_AT_ASC: 'order_by_created_date_asc',
    CREATED_AT_DESC: 'order_by_created_date_desc',
    DEFAULT: 'default'
  },
  score: {
    SCORE_ASC: 'score_asc',
    SCORE_DESC: 'score_desc'
  }
}
