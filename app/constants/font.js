import {Dimensions} from 'react-native'

const {width} = Dimensions.get('window')

export const getAdjustedFontSize = (size) => {
  return parseInt(size) * width / 365
}

export const SIZE = {
  TITLE: getAdjustedFontSize(18),
  BIG: getAdjustedFontSize(17),
  NORMAL: getAdjustedFontSize(16),
  SMALL: getAdjustedFontSize(14),
  SMALLER: getAdjustedFontSize(13),
  HEADING1: getAdjustedFontSize(20),
  HEADING2: getAdjustedFontSize(19),
  HEADING3: getAdjustedFontSize(18),
  HEADING4: getAdjustedFontSize(17),
  HEADING5: getAdjustedFontSize(16),
  HEADING6: getAdjustedFontSize(15),
  HEADING7: getAdjustedFontSize(14),
  WIDTH_DEFAUTL: getAdjustedFontSize(50),
  HEIGHT_DEFAULT: getAdjustedFontSize(50)
}
export const WEIGHT = {
  BOLDER: 600,
  BOLD: 500,
  NORMAL: 400
}
export const HEIGHT = {
  TITLE: 26
}

export default {SIZE, WEIGHT, HEIGHT}
