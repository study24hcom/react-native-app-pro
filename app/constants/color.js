import color from 'color'

export const SHAPE = {
  RED: '#EB5C55',
  ORANGE: '#F6A623',
  PURPLE: '#A076C5',
  BLACK: '#3B3B3B',
  GREEN: '#2ECC71',
  PRIMARY: '#389BFF',
  PRIMARYBOLD: '#007EE5',
  PINK: '#FE6C88',
  GRAYLIGHT: '#fafbfb',
  GRAYMORELIGHT: '#f5f5f5',
  GRAYLIGHTMEDIUM: '#d5d5d5',
  GAINSBORO: '#dcdcdc',
  GRAYMEDIUM: '#eee',
  GRAYBOLD: '#D4D4D4',
  GRAYBOLDER: '#B5B5B5',
  GRAYTEXT: '#999999',
  YELLOW: '#f1c40f',
  FACEBOOK: '#4468B0'
}

export const TEXT = {
  NORMAL: '#3B3B3B',
  PRIMARY: '#389BFF',
  PRIMARYBOLD: '#007EE5',
  GRAY: '#999999'
}

export const INPUT = {
  BORDER: SHAPE.GRAYMEDIUM,
  PLACEHOLDER: color(SHAPE.GRAYMEDIUM).darken(0.4).string(),
  FOCUS: SHAPE.PRIMARY
}

export default {SHAPE, TEXT, INPUT}
