import { SHAPE } from 'constants/color'
import { SITE_INFO } from 'config'

function getImage (slug) {
  return `${SITE_INFO.PUBLIC_URL}/static/images/categories/${slug}.jpg`
}

export default [
  {
    _id: '57f3485eb46834a085f09f98',
    id: '57f3485eb46834a085f09f98',
    simpleLineIcon: false,
    name: 'Toán học',
    color: SHAPE.GREEN,
    icon: 'calculator',
    slug: 'toan-hoc',
    image: getImage('toan-hoc'),
    keyRequest: 'maths',
    description: 'Tổng hợp các đề thi trắc nghiệm Toán học, ôn thi THPT quốc gia 2018, luyện thi đại học'
  },
  {
    _id: '57d6a9f0d8a41f0a1682d768',
    id: '57d6a9f0d8a41f0a1682d768',
    simpleLineIcon: true,
    name: 'Vật lý',
    color: SHAPE.ORANGE,
    icon: 'speedometer',
    slug: 'vat-ly',
    image: getImage('vat-ly'),
    keyRequest: 'physics',
    description: 'Tổng hợp các đề thi trắc nghiệm Vật lý, ôn thi THPT quốc gia 2018, luyện thi đại học'
  },
  {
    _id: '57d6a9f0d8a41f0a1682d769',
    id: '57d6a9f0d8a41f0a1682d769',
    simpleLineIcon: true,
    name: 'Hóa học',
    color: SHAPE.PURPLE,
    icon: 'chemistry',
    slug: 'hoa-hoc',
    image: getImage('hoa-hoc'),
    keyRequest: 'chemistry',
    description: 'Tổng hợp các đề thi trắc nghiệm Hóa học, ôn thi THPT quốc gia 2018, luyện thi đại học'
  },
  {
    _id: '57d6a9f0d8a41f0a1682d76a',
    id: '57d6a9f0d8a41f0a1682d76a',
    simpleLineIcon: true,
    name: 'Sinh học',
    color: SHAPE.PRIMARY,
    icon: 'graph',
    slug: 'sinh-hoc',
    image: getImage('sinh-hoc'),
    keyRequest: 'biology',
    description: 'Tổng hợp các đề thi trắc nghiệm Sinh học, ôn thi THPT quốc gia 2018, luyện thi đại học'
  },
  {
    _id: '57d6a9f0d8a41f0a1682d76b',
    id: '57d6a9f0d8a41f0a1682d76b',
    simpleLineIcon: true,
    name: 'Tiếng anh',
    color: SHAPE.PINK,
    icon: 'graduation',
    slug: 'tieng-anh',
    image: getImage('tieng-anh'),
    keyRequest: 'english',
    description: 'Tổng hợp các đề thi trắc nghiệm Tiếng anh, ôn thi THPT quốc gia 2018, luyện thi đại học'
  },
  {
    _id: '58177ba7fd5540026bceb8e7',
    id: '58177ba7fd5540026bceb8e7',
    simpleLineIcon: false,
    name: 'Lập trình',
    color: SHAPE.PINK,
    icon: 'code',
    slug: 'lap-trinh',
    image: getImage('lap-trinh'),
    keyRequest: 'programming',
    description: 'Tổng hợp các đề thi trắc nghiệm Lập trình, ôn thi THPT quốc gia 2018, luyện thi đại học'
  },
  {
    _id: '57d6a9f0d8a41f0a1682d76c',
    id: '57d6a9f0d8a41f0a1682d76c',
    simpleLineIcon: true,
    name: 'Khác',
    color: SHAPE.PINK,
    icon: 'options-vertical',
    slug: 'khac',
    image: getImage('khac'),
    keyRequest: 'other',
    description: 'Tổng hợp các đề thi trắc nghiệm Xã hội, ôn thi THPT quốc gia 2018, luyện thi đại học'
  }
]
