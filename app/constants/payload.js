export const GET_ACTIVITIES = 'getActivities'
export const LOAD_MORE_ACTIVITIES = 'loadMoreActivities'
export const GET_HOME_QUIZLISTS_CATEGORY = 'getHomeQuizListsCategory'
export const GET_QUIZLISTS_CATEGORY = 'getQuizListsCategory'

export default {
  GET_ACTIVITIES,
  LOAD_MORE_ACTIVITIES,
  GET_HOME_QUIZLISTS_CATEGORY,
  GET_QUIZLISTS_CATEGORY
}
