export const RESULT_USER = 'searchResult/user'
export const RESULT_QUIZLIST = 'searchResult/quizlist'
export const RESULT_ORGANIZATION = 'searchResult/orgnization'
export const RESULT_TAG = 'searchResult/tag'

export default {
  RESULT_USER,
  RESULT_QUIZLIST,
  RESULT_ORGANIZATION,
  RESULT_TAG
}
