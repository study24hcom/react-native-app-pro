export const QUIZ_LIST_TYPE_FREE = 'QUIZLIST/free'
export const QUIZ_LIST_TYPE_PURCHASE_BEFORE_PLAY =
  'QUIZLIST/purchase-before-play'
export const QUIZ_LIST_TYPE_PURCHASE_AFTER_PLAY = 'QUIZLIST/purchase-after-play'

export default {
  FREE: QUIZ_LIST_TYPE_FREE,
  PURCHASE_BEFORE_PLAY: QUIZ_LIST_TYPE_PURCHASE_BEFORE_PLAY,
  PURCHASE_AFTER_PLAY: QUIZ_LIST_TYPE_PURCHASE_AFTER_PLAY
}
