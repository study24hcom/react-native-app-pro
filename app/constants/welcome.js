export default [
  {
    key: 1,
    title: 'Tùng Tùng là gì ?',
    detail: 'Tungtung.vn là nền tảng trắc nghiệm trực tuyến giúp bạn tạo và chia sẻ đề thi trắc nghiệm trực tuyến dễ dàng.'
  },
  {
    key: 2,
    title: 'Vì sao nên sử dụng tungtung.vn ?',
    detail: 'Tungtung.vn là nền tảng trắc nghiệm trực tuyến cung cấp cho bạn trải nghiệm chia sẻ và đánh giá kiến thức tuyệt vời nhất.'
  },
  {
    key: 3,
    title: 'Khác gì với các nền tảng khác ?',
    detail: 'Khác với Google Form, hay Word, tungtung.vn là nền tảng chuyên biệt dành cho trắc nghiệm với các tính năng đặc biệt.\n' + '\n' + 'Đẹp, dễ dàng sử dụng, nhanh chóng, tiện lợi, phù hợp với giáo dục Việt Nam.'
  },
  {
    key: 4,
    title: 'Tôn vinh giá trị sáng tạo',
    detail: 'Tại tungtung.vn, chúng tôi luôn tôn vinh giá trị sáng tạo nội dung của các bạn.'
  },
  {
    key: 5,
    title: 'Nguồn tri thức đa dạng',
    detail: 'Cộng đồng học tập cởi mở với nhiều lĩnh vực khác nhau, giúp bạn nâng cao được trình độ chuyên môn.'
  },
  {
    key: 6,
    title: 'Trải nghiệm đa nền tảng',
    detail: 'Nền tảng chuyên biệt dành cho trắc nghiệm, cung cấp cho bạn trải nghiệm tạo và làm đề thi trắc nghiệm tốt nhất trên cả 2 nền tảng website và mobile.'
  }
]
