export const QUIZ_LISTS_BY_CATEGORY = {
  key: 'byCategory',
  payload: 'getQuizListsByCategory'
}

export const QUIZ_LISTS_BY_NEWS = {
  key: 'byNews',
  payload: 'getQuizListsByNews'
}

export const QUIZ_LISTS_BY_ME = {
  key: 'byMe',
  payload: 'getQuizListsByMe'
}

export const QUIZ_LISTS_BY_USER = {
  key: 'byUser',
  payload: 'getQuizListsByUser'
}

export const QUIZ_LISTS_BY_HISTORY = {
  key: 'byHistory',
  payload: 'getQuizListsByHistory'
}

export const QUIZ_LISTS_BY_SEARCH = {
  key: 'bySearch',
  payload: 'getQuizListsBySearch'
}

export const QUIZ_LISTS_BY_TAG = {
  key: 'byTag',
  payload: 'getQuizListsByTag'
}

export const QUIZ_LISTS_BY_FOLLOWED = {
  key: 'byFollowed',
  payload: 'getQuizListsByFollowed'
}
export const QUIZ_LISTS_BY_BOOKMARK = {
  key: 'byBookmark',
  payload: 'getQuizListsByBookmark'
}

export default {
  QUIZ_LISTS_BY_CATEGORY,
  QUIZ_LISTS_BY_NEWS,
  QUIZ_LISTS_BY_ME,
  QUIZ_LISTS_BY_HISTORY,
  QUIZ_LISTS_BY_USER,
  QUIZ_LISTS_BY_TAG,
  QUIZ_LISTS_BY_FOLLOWED,
  QUIZ_LISTS_BY_BOOKMARK
}
