import { PropTypes } from 'prop-types'

export const categoryPropType = {
  _id: PropTypes.string,
  icon: PropTypes.string,
  name: PropTypes.string
}

export const userPropType = {
  _id: PropTypes.string,
  username: PropTypes.string,
  avatarImage: PropTypes.string
}

export const ratingPropType = {
  _id: PropTypes.string,
  user: PropTypes.shape(userPropType),
  rating: PropTypes.number,
  content: PropTypes.any
}

export const answerPropType = {
  _id: PropTypes.string,
  type: PropTypes.string,
  answer: PropTypes.any
}

export const quizPropType = {
  _id: PropTypes.string,
  question: PropTypes.any,
  answers: PropTypes.arrayOf(PropTypes.shape(answerPropType)),
  correctAnswerIndex: PropTypes.number,
  chooseAnswerIndex: PropTypes.number,
  isSuggestAnswer: PropTypes.bool,
  suggestAnswer: PropTypes.any
}

export const scorePropType = {
  index: PropTypes.number,
  score: PropTypes.number,
  user: PropTypes.shape(userPropType),
  totalQuestion: PropTypes.number
}

export const quizListInfoPropType = {
  _id: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  time: PropTypes.number
}

export default {
  categoryPropType,
  userPropType,
  ratingPropType,
  answerPropType,
  quizPropType,
  quizListInfoPropType
}
