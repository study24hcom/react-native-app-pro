const QUIZ_LIST_VIEW = {
  path: '/de-thi/(:slug)(.:actionType)?',
  realPath: '/quiz-list/view?slug=:slug&actionType=:actionType',
  file: '/quiz-list/view'
}

const QUIZ_LIST_PLAY = {
  path: '/de-thi/:slug/play',
  realPath: '/quiz-list/play?slug=:slug',
  file: '/quiz-list/play'
}

const QUIZ_LIST_HISTORY = {
  path: '/de-thi-lich-su/:slug/history/:historyId',
  realPath: '/quiz-list/history?slug=:slug&historyId=:historyId',
  file: '/quiz-list/history'
}

const QUIZ_LIST_PREVIEW = {
  path: '/xem-thu',
  realPath: '/quiz-list/preview',
  file: '/quiz-list/preview'
}

const QUIZ_LIST_CREATE = {
  path: '/tao-de-thi/:actionType',
  realPath: '/quiz-manager/create?actionType=:actionType',
  file: '/quiz-manager/create'
}

const QUIZ_LIST_MANAGER = {
  path: '/sua-de-thi/(:quizListId)(.:actionType)?(.:groupId)?',
  realPath: '/quiz-manager/manager?quizListId=:quizListId&actionType=:actionType&groupId=:groupId&memberId=:memberId',
  file: '/quiz-manager/manager'
}

const PLAYLIST_LIST = {
  path: '/danh-sach-suu-tap',
  realPath: '/sliderCarousel/list',
  file: '/sliderCarousel/list'
}

const PLAY_LIST_VIEW = {
  path: '/suu-tap/(:playlistSlug)(.:actionType)?',
  realPath: '/sliderCarousel/view?slug=:playlistSlug&actionType=:actionType',
  file: '/sliderCarousel/view'
}

const PLAYLIST_MANAGER_CREATE = {
  path: '/tao-suu-tap/:actionType',
  realPath: '/sliderCarousel-manager/create?actionType=:actionType',
  file: '/sliderCarousel-manager/create'
}

const PLAYLIST_MANAGER_MANAGER = {
  path: '/sua-suu-tap/(:playlistSlug)(.:actionType)?',
  realPath: '/sliderCarousel-manager/manager?playlistSlug=:playlistSlug&actionType=:actionType',
  file: '/sliderCarousel-manager/manager'
}

const QUIZ_LIST_MANAGER_GROUP = {
  path: '/quan-ly/manager-group'
}

const PROFILE_MANAGER = {
  path: '/thong-tin/:actionType',
  realPath: '/profile-manager/index?actionType=:actionType',
  file: '/profile-manager/index'
}

const CATEGORY = {
  path: '/chuyen-muc/:slug',
  realPath: '/category/index?slug=:slug',
  file: '/category/index'
}

const QUIZ_LISTS_NEW = {
  path: '/de-thi-moi-nhat',
  realPath: '/quiz-lists/new',
  file: '/quiz-lists/new'
}

const PROFILE_VIEW = {
  path: '/:username',
  realPath: '/profile-view/index?username=:username',
  file: '/profile-view/index'
}

const PROFILE_VIEW_PLAYLISTS = {
  path: '/:username/playlists',
  realPath: '/profile-view/playlists?username=:username',
  file: '/profile-view/playlists'
}

const PROFILE_VIEW_QUIZLISTS = {
  path: '/:username/de-thi',
  realPath: '/profile-view/quizlists?username=:username',
  file: '/profile-view/quizlists'
}

const PARTNER_LIST = {
  path: '/partners',
  realPath: '/partner/list',
  file: '/partner/list'
}

const ACTIVITIES_LIST = {
  path: '/hoat-dong',
  realPath: '/activities',
  file: '/activities'
}

const ORGNIZATION_VIEW = {
  path: '/o/:organizationKey',
  realPath: '/organization/view?organizationKey=:organizationKey',
  file: '/organization/view'
}

const SEARCH = {
  path: '/tim-kiem/:keywords',
  realPath: '/search/index?keywords=:keywords',
  file: '/search/index'
}

const INTRODUCTION = {
  path: '/gioi-thieu',
  realPath: '/introduction/index',
  file: '/introduction/index'
}

const LOGIN = {
  path: '/dang-nhap',
  realPath: '/auth/login',
  file: '/auth/login'
}

const REGISTER = {
  path: '/dang-ky',
  realPath: '/auth/register',
  file: '/auth/register'
}

module.exports.QUIZ_LIST_VIEW = QUIZ_LIST_VIEW
module.exports.QUIZ_LIST_PLAY = QUIZ_LIST_PLAY
module.exports.QUIZ_LIST_HISTORY = QUIZ_LIST_HISTORY
module.exports.QUIZ_LIST_MANAGER = QUIZ_LIST_MANAGER
module.exports.QUIZ_LIST_CREATE = QUIZ_LIST_CREATE
module.exports.QUIZ_LIST_PREVIEW = QUIZ_LIST_PREVIEW
module.exports.PROFILE_MANAGER = PROFILE_MANAGER
module.exports.CATEGORY = CATEGORY
module.exports.SEARCH = SEARCH
module.exports.INTRODUCTION = INTRODUCTION
module.exports.LOGIN = LOGIN
module.exports.REGISTER = REGISTER
module.exports.PROFILE_VIEW = PROFILE_VIEW
module.exports.PROFILE_VIEW_PLAYLISTS = PROFILE_VIEW_PLAYLISTS
module.exports.PROFILE_VIEW_QUIZLISTS = PROFILE_VIEW_QUIZLISTS
module.exports.PLAY_LIST_VIEW = PLAY_LIST_VIEW
module.exports.ORGNIZATION_VIEW = ORGNIZATION_VIEW
module.exports.QUIZ_LIST_MANAGER_GROUP = QUIZ_LIST_MANAGER_GROUP
module.exports.PLAYLIST_MANAGER_MANAGER = PLAYLIST_MANAGER_MANAGER
module.exports.PLAYLIST_MANAGER_CREATE = PLAYLIST_MANAGER_CREATE
module.exports.PLAYLIST_LIST = PLAYLIST_LIST
module.exports.QUIZ_LIST_PREVIEW = QUIZ_LIST_PREVIEW
module.exports.PARTNER_LIST = PARTNER_LIST
module.exports.QUIZ_LISTS_NEW = QUIZ_LISTS_NEW
module.exports.ACTIVITIES_LIST = ACTIVITIES_LIST

module.exports.default = {
  QUIZ_LIST_VIEW,
  CATEGORY,
  SEARCH,
  INTRODUCTION,
  QUIZ_LIST_HISTORY,
  QUIZ_LIST_MANAGER,
  LOGIN,
  REGISTER,
  PROFILE_VIEW,
  PROFILE_VIEW_PLAYLISTS,
  PROFILE_VIEW_QUIZLISTS,
  ORGNIZATION_VIEW,
  PLAYLIST_MANAGER_CREATE,
  PLAYLIST_MANAGER_MANAGER,
  PLAYLIST_LIST,
  PARTNER_LIST,
  QUIZ_LIST_PREVIEW,
  QUIZ_LISTS_NEW,
  ACTIVITIES_LIST
}
