import { urlPlaylistView } from 'utils/urlRouter'

const getPlaylistUrl = playlistSlug =>
  urlPlaylistView({ playlistSlug, actionType: 'info' }, true)

export default [
  {
    id: 1,
    image: '/static/home/sliders/slider_1.jpg',
    type: 'router',
    title: 'Trắc nghiệm kiến thức phiên bản siêu tốc độ',
    link: getPlaylistUrl('Trac-Nghiem-Kien-Thuc-Sieu-Toc-Do-kBHAG')
  },
  {
    id: 2,
    image: '/static/home/sliders/slider_2.jpg',
    title: 'Học tiếng anh tốt hơn mỗi ngày',
    type: 'router',
    link: getPlaylistUrl('5-cau-hoi-trac-nghiem-moi-ngay-HEM-DyvXF')
  },
  {
    id: 3,
    image: '/static/home/sliders/slider_3.jpg',
    title: 'Trắc nghiệm mô hình đặc tả thiết kế phần mềm UML',
    type: 'router',
    link: getPlaylistUrl(
      'Trac-nghiem-mo-hinh-dac-ta-thiet-ke-phan-mem-UML-f9RPg'
    )
  },
  {
    id: 4,
    image: '/static/home/sliders/slider_4.jpg',
    type: 'router',
    title: 'Tổng hợp lý thuyết trắc nghiệm môn sinh học THPT quốc gia',
    link: getPlaylistUrl('TRAC-NGHIEM-LY-THUYET-FULL-CHUONG-MON-SINH-HOC-sz4p5')
  }
]
