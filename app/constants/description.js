export const playlist = {
  list: 'Tập hợp danh sách các đề thi có cùng một chủ đề. Bạn cũng có thể tạo và chia sẻ bộ sưu tập của mình một cách dễ dàng.',
  byMe: 'Tập hợp các bộ sưu tập mà bạn đã tạo trên hệ thống tungtung.vn',
  byUser: 'Tập hợp các bộ sưu tập mà user này đã tạo trên hệ thống tungtung.vn'
}

export const quizList = {
  follow: 'Tập hợp những đề thi chúng tôi dự đoán sẽ phù hợp với bạn dựa trên những tags, users bạn theo dõi.',
  new: 'Tuyển tập tất cả đề thi mới được phát hành gần đây nhất.',
  today: 'Tuyển tập đề thi được thi nhiều nhất và là xu hướng trên tungtung.vn',
  save: 'Tập hợp các đề thi bạn đã lưu, thuận tiện cho việc lưu trữ những đề thi bạn vẫn chưa kịp thi.',
  created: {
    byMe: 'Tập hợp các đề thi mà bạn đã tạo trên hệ thống tungtung.vn',
    byUser: 'Tập hợp các đề thi mà user này đã tạo trên hệ thống tungtung.vn'
  },
  worked: {
    byMe: 'Tập hợp các đề thi mà bạn đã làm trên hệ thống tungtung.vn',
    byUser: 'Tập hợp các đề thi mà user này đã làm trên hệ thống tungtung.vn'
  }
}

export const tag = {
  follow: 'Tập hợp tất cả các tags bạn đã theo dõi trên hệ thống tungtung.',
  featured: 'Danh sách các tags đang hot trên tungtung.vn'
}

export const partner = {
  list: 'Danh sách những người được chúng tôi công nhận trở thành partner, với những quyền ưu đãi đặc biệt.'
}

export const category = {
  hot: 'Danh sách chuyên mục hot mà chúng tôi chọn lọc, bạn cũng có thể đóng góp thêm nhiều chuyên mục khác bằng cách tạo thêm những đề thi mới.'
}

export default {playlist, quizList, tag, partner, category}
