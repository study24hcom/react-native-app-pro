export const RATE_QUIZ_LIST = 'RATE_QUIZ_LIST'
export const DO_QUIZ_LIST = 'DO_QUIZ_LIST'

export default {
  RATE_QUIZ_LIST,
  DO_QUIZ_LIST
}
