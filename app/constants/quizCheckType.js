export default {
  CHECKED: 'quiz/checked',
  EMPTY: 'empty',
  CORRECT: true,
  INCORRECT: false
}
