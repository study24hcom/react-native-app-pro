import React from 'react'
import PropTypes from 'prop-types'
import { autobind } from 'core-decorators'
import { connectAutoDispatch } from '@redux/connect'
import { setFollowUser, unFollowUser, updateIsShowBox } from '@redux/actions/followAction'
import AuthApi from 'api/AuthApi'

export default function createUser (Component) {
  @connectAutoDispatch(
    state => ({
      followedUsers: state.follow.followedUsers
    }),
    {setFollowUser, unFollowUser, updateIsShowBox}
  )
  @autobind
  class FollowUserHoc extends React.PureComponent {
    static propTypes = {
      setFollowUser: PropTypes.func,
      unFollowUser: PropTypes.func,
      userId: PropTypes.string
    }

    getFollowExists () {
      const follow = this.props.followedUsers.find(
        follow => this.props.userId === follow.id
      )
      return !follow ? false : follow
    }

    isFollow () {
      const follow = this.getFollowExists()
      if (!follow) return false
      return follow.followed
    }

    handleFollow (e) {
      e.preventDefault()
      const {userId} = this.props
      if (!this.isFollow()) {
        AuthApi.followUser(userId)
        this.props.setFollowUser(userId)
        this.props.updateIsShowBox(true)
        setTimeout(() => {
          this.props.updateIsShowBox(false)
        }, 600)
      } else {
        AuthApi.unFollowUser(userId)
        this.props.unFollowUser(userId)
      }
    }

    async checkFollow () {
      const follow = await AuthApi.checkFollowUser(this.props.userId)
      if (follow.exists) {
        this.props.setFollowUser(this.props.userId)
      } else {
        this.props.unFollowUser(this.props.userId)
      }
    }

    componentDidMount () {
      if (!this.getFollowExists()) {
        this.checkFollow()
      }
    }

    render () {
      if (!this.getFollowExists()) return null
      return (
        <Component
          {...this.props}
          isFollowed={this.isFollow()}
          onClick={this.handleFollow}
        />
      )
    }
  }

  return FollowUserHoc
}
