import React from 'react'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import {connectAwaitAutoDispatch} from '@redux/connect'
import QuizApi from 'api/QuizApi'
import {setBookmarkQuizlist, unBookmarkQuizlist, checkBookmark, updateIsShowBox} from '@redux/actions/bookmarkAction'
import {awaitCheckPending} from 'utils/await'

const createBookmarkQuizlist = ({autoCheck = true} = {}) => Component => {
  @connectAwaitAutoDispatch(
    state => ({
      bookmarkQuizlists: state.bookmark.bookmarkQuizlists
    }),
    {setBookmarkQuizlist, unBookmarkQuizlist, checkBookmark, updateIsShowBox}
  )
  @autobind
  class BookmarkQuizlistHoc extends React.PureComponent {
    static propTypes = {
      setBookmarkQuizlist: PropTypes.func,
      checkBookmark: PropTypes.func,
      unBookmarkQuizlist: PropTypes.func,
      quizListId: PropTypes.string
    }

    componentDidMount () {
      if (!this.props.quizListId) {
        return
      }
      if (!this.getBookmarkExists() && autoCheck) {
        this.props.checkBookmark(this.props.quizListId)
      }
    }

    componentWillReceiveProps (nextProps) {
      if (nextProps.quizListId !== this.props.quizListId && autoCheck) {
        this.props.checkBookmark(nextProps.quizListId)
      }
    }

    getBookmarkExists () {
      const bookmark = this.props.bookmarkQuizlists.find(
        bookmark => this.props.quizListId === bookmark.id
      )
      return !bookmark ? false : bookmark
    }

    isBookmark () {
      const bookmark = this.getBookmarkExists()
      if (!bookmark) return false
      return bookmark.bookmarked
    }

    isChecking () {
      return awaitCheckPending(this.props, 'checkBookmark')
    }

    handleBookmark (e) {
      if (e) {
        e.preventDefault()
      }
      const {quizListId} = this.props
      if (!this.isBookmark()) {
        QuizApi.bookmarkQuizlist(quizListId)
        this.props.setBookmarkQuizlist(quizListId)
        this.props.updateIsShowBox(true)
        setTimeout(() => {
          this.props.updateIsShowBox(false)
        }, 600)
      } else {
        QuizApi.unBookmarkQuizlist(quizListId)
        this.props.unBookmarkQuizlist(quizListId)
      }
    }

    render () {
      return (
        <Component
          {...this.props}
          isChecking={this.isChecking()}
          isBookmarked={this.isBookmark()}
          onClick={this.handleBookmark}
        />
      )
    }
  }

  return BookmarkQuizlistHoc
}

export default createBookmarkQuizlist
