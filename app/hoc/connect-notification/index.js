import React from 'react'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {
  getNotifications,
  loadMoreNotifications,
  getNewNotifications,
  resetNewNotifications,
  markChecked,
  markViewedAll
} from '@redux/actions/notificationAction'
import {userAutoAuth} from '@redux/actions/authAction'
import {awaitCheckPending} from 'utils/await'
import {checkLoadMore} from 'utils/pagination'

export default function createConnectNotification (Component) {
  @connectAwaitAutoDispatch(
    state => ({
      newNotifications: state.notification.newNotifications,
      notifications: state.notification.list.data,
      pagination: state.notification.list.pagination,
      isAuth: state.auth.isAuthenticated,
      auth: state.auth
    }),
    {
      getNotifications,
      loadMoreNotifications,
      getNewNotifications,
      resetNewNotifications,
      markChecked,
      userAutoAuth,
      markViewedAll
    }
  )
  @autobind
  class CreateConnectNotification extends React.PureComponent {
    static navigationOptions = Component.navigationOptions

    static propTypes = {
      notifications: PropTypes.array,
      newNotifications: PropTypes.number,
      pagination: PropTypes.object,
      getNotifications: PropTypes.func,
      loadMoreNotifications: PropTypes.func,
      getNewNotifications: PropTypes.func,
      resetNewNotifications: PropTypes.func,
      markChecked: PropTypes.func
    }

    handleLoadMore () {
      if (
        checkLoadMore(this.props.pagination) &&
        !awaitCheckPending(this.props, 'getNotifications') &&
        !awaitCheckPending(this.props, 'loadMoreNotifications')
      ) {
        this.props.loadMoreNotifications({
          page: this.props.pagination.page + 1,
          itemPerPage: this.props.pagination.itemPerPage
        })
      }
    }

    refreshNotification () {
      this.props.getNewNotifications({autoFetch: false})
    }

    render () {
      if (!this.props.isAuth) return null
      const isLoading =
        awaitCheckPending(this.props, 'getNotifications') ||
        awaitCheckPending(this.props, 'loadMoreNotifications')
      const isLoadingFirst =
        awaitCheckPending(this.props, 'getNewNotifications') &&
        !awaitCheckPending(this.props, 'loadMoreNotifications')
      return (
        <Component
          isLoading={isLoading}
          isLoadingFirst={isLoadingFirst}
          onRefresh={this.refreshNotification}
          onLoadMore={this.handleLoadMore}
          onResetNewNotifications={this.props.resetNewNotifications}
          {...this.props}
        />
      )
    }
  }

  return CreateConnectNotification
}
