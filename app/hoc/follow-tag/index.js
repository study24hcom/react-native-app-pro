import React from 'react'
import PropTypes from 'prop-types'
import { autobind } from 'core-decorators'
import { connectAutoDispatch } from '@redux/connect'
import { setFollowTag, unFollowTag, updateIsShowBox } from '@redux/actions/followAction'
import QuizApi from 'api/QuizApi'

const createFollowTag = ({autoCheck = true} = {}) => Component => {
  @connectAutoDispatch(
    state => ({
      followedTags: state.follow.followedTags
    }),
    {setFollowTag, unFollowTag, updateIsShowBox}
  )
  @autobind
  class FollowTagHoc extends React.PureComponent {
    static propTypes = {
      setFollowTag: PropTypes.func,
      unFollowTag: PropTypes.func,
      tagName: PropTypes.string
    }

    state = {
      isSuccess: false
    }

    getFollowExists () {
      const follow = this.props.followedTags.find(
        follow => this.props.tagName === follow.name
      )
      return !follow ? false : follow
    }

    isFollow () {
      const follow = this.getFollowExists()
      if (!follow) return false
      return follow.followed
    }

    handleFollow (e) {
      if (e) {
        e.preventDefault()
      }
      const {tagName} = this.props
      if (!this.isFollow()) {
        QuizApi.followTag(tagName)
        this.props.setFollowTag(tagName)
        if (autoCheck) {
          this.props.updateIsShowBox(true)
          setTimeout(() => {
            this.props.updateIsShowBox(false)
          }, 600)
        }
      } else {
        QuizApi.unFollowTag(tagName)
        this.props.unFollowTag(tagName)
      }
    }

    async checkFollow () {
      const follow = await QuizApi.checkFollowTag(this.props.tagName)
      if (follow.exists) {
        this.props.setFollowTag(this.props.tagName)
      } else {
        this.props.unFollowTag(this.props.tagName)
      }
    }

    componentDidMount () {
      if (!this.getFollowExists() && autoCheck) {
        this.checkFollow()
      }
    }

    componentWillReceiveProps (nextProps) {
      if (nextProps.tagName !== this.props.tagName && autoCheck) {
        this.checkFollow()
      }
    }

    render () {
      if (!this.getFollowExists() && autoCheck) return null
      return (
        <Component
          {...this.props}
          isSuccess={this.state.isSuccess}
          isFollowed={this.isFollow()}
          onClick={this.handleFollow}
        />
      )
    }
  }

  return FollowTagHoc
}

export default createFollowTag
