import React from 'react'
import { autobind } from 'core-decorators'
import QuizApi from 'api/QuizApi'
import { cleanUsers, cleanQuizLists, cleanUser } from 'utils/clean'
import searchType from 'constants/searchType'

export default function createSearchMultipleGroup (Component) {
  @autobind class SearchMutlipleGroup extends React.Component {
    state = {
      users: [],
      quizLists: [],
      isLoadingUser: false,
      isLoadingQuizList: false
    }

    delay = false

    handleChangeSearch (keywords) {
      if (this.delay) return
      this.setState({
        isLoadingUser: true,
        isLoadingQuizList: true
      })
      QuizApi.getUsersBySearch({ keywords }).then(res => {
        const userMatched = res.matched ? cleanUser(res.matched) : null
        const usersSuggested = cleanUsers(res.suggested.slice(0, 3))
        let users = []
        if (userMatched) {
          users = [
            userMatched,
            ...usersSuggested.filter(u => u.id !== userMatched.id)
          ]
        } else {
          users = usersSuggested
        }
        this.setState({
          users: users,
          isLoadingUser: false
        })
        this.delay = false
      })
      QuizApi.getQuizListsBySearch({ keywords, itemPerPage: 4 }).then(res => {
        this.setState({
          quizLists: cleanQuizLists(res.quiz_lists),
          isLoadingQuizList: false
        })
        this.delay = false
      })
    }

    getData () {
      return {
        [searchType.USER]: this.state.users,
        [searchType.QUIZLISTS]: this.state.quizLists
      }
    }

    isShowSearch () {
      return !!(this.state.users.length || this.state.quizLists.length)
    }

    render () {
      return (
        <Component
          onChangeSearch={this.handleChangeSearch}
          searchGroupData={this.getData()}
          isShowSearch={this.isShowSearch()}
          isLoading={this.state.isLoadingQuizList || this.state.isLoadingUser}
          {...this.props}
        />
      )
    }
  }
  return SearchMutlipleGroup
}
