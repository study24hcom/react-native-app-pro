import React from 'react'
import PropTypes from 'prop-types'
import {autobind} from 'core-decorators'
import {connectAwaitAutoDispatch} from '@redux/connect'
import {checkLoadMore} from 'utils/pagination'
import {awaitCheckPending, awaitCheckSuccess} from 'utils/await'

export default ({
                  action,
                  key,
                  payload,
                  didmountReload,
                  customShouldReload = () => ({}),
                  customPassProps = () => ({}),
                  customPassAction = () => ({})
                } = {}) => Component => {
  @connectAwaitAutoDispatch(
    state => ({
      data: state.quizLists[key].data,
      pagination: state.quizLists[key].pagination,
      loadMore: state.quizLists[key].loadMore
    }),
    {
      actionLoadQuizLists: action
    }
  )
  @autobind
                  class QuizListsWrapper extends React.Component {
                    static propTypes = {
                      data: PropTypes.array,
                      pagination: PropTypes.object,
                      loadMore: PropTypes.bool
                    }

                    state = {
                      isFirst: true
                    }

                    state = {
                      isFirst: true,
                      data: []
                    }

                    static navigationOptions = Component.navigationOptions

                    getQuizLists ({loadMore = false} = {}) {
                      this.props.actionLoadQuizLists({
                        itemPerPage: this.props.pagination.itemPerPage,
                        ...customPassAction(this.props, {isStatic: false}),
                        page: loadMore ? this.props.pagination.page + 1 : 1,
                        loadMore
                      })
                    }

                    isShouldLoadMore () {
                      return (checkLoadMore(this.props.pagination) && !this.isLoadingMore())
                    }

                    handleLoadMore () {
                      if (!this.delay) {
                        if (this.isShouldLoadMore()) {
                          this.getQuizLists({loadMore: true})
                        }
                        this.delay = true
                        setTimeout(() => {
                          this.delay = false
                        }, 1500)
                      }
                    }

                    componentDidMount () {
                      this.getQuizLists()
                    }

                    isLoadingMore () {
                      return this.props.loadMore && awaitCheckPending(this.props, payload)
                    }

                    isLoadingFirst () {
                      return (!this.props.loadMore && awaitCheckPending(this.props, payload) && this.state.isFirst)
                    }

                    isResfreshing () {
                      return (!this.props.loadMore && awaitCheckPending(this.props, payload) && !this.state.isFirst)
                    }

                    isLoadSuccess () {
                      return (awaitCheckSuccess(this.props, payload))
                    }

                    onRefresh () {
                      if (this.state.isFirst) {
                        this.setState({isFirst: false})
                      }
                      this.getQuizLists()
                    }

                    propsForFlatlist () {
                      return {
                        onScrollToBottom: this.handleLoadMore,
                        onRefresh: this.onRefresh,
                        isLoadingMore: this.isLoadingMore(),
                        isLoadingFirst: this.isLoadingFirst(),
                        isResfreshing: this.isResfreshing(),
                        isLoadSuccess: this.isLoadSuccess(),
                        data: this.props.data,
                        pagination: this.props.pagination
                      }
                    }

                    render () {
                      return (
                        <Component
                          {...this.props}
                          isLoadSuccess={this.isLoadSuccess()}
                          propsForFlatlist={this.propsForFlatlist()}
                          isLoadingMore={this.isLoadingMore()}
                          isLoadingFirst={this.isLoadingFirst()}
                          isResfreshing={this.isResfreshing()}
        />
                      )
                    }
  }

                  return QuizListsWrapper
                }
