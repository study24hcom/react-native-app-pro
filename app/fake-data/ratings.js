import users from './users'

export default [
  {
    id: 1,
    user: users[0],
    content: 'Một ứng dụng rấy hay và hay mà mình từng biết',
    rating: 4
  },
  {
    id: 2,
    user: users[1],
    content: 'Một ứng dụng rấy hay và hay mà mình từng biết',
    rating: 5
  }
]
