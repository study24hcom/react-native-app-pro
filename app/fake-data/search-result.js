import searchResultType from 'constants/searchResultType'
import users from 'fake-data/users'
import quizLists from 'fake-data/quiz-lists'

export default [
  {
    id: 1,
    type: searchResultType.RESULT_USER,
    data: users[0]
  },
  {
    id: 2,
    type: searchResultType.RESULT_QUIZLIST,
    data: quizLists[0]
  },
  {
    id: 3,
    type: searchResultType.RESULT_QUIZLIST,
    data: quizLists[1]
  },
  {
    id: 4,
    type: searchResultType.RESULT_QUIZLIST,
    data: quizLists[2]
  },
  {
    id: 5,
    type: searchResultType.RESULT_ORGANIZATION,
    data: users[1]
  }
]
