/* eslint-disable */

const users: Array<UserType> = [
  {
    id: '1',
    username: 'vothuong',
    email: 'tungptkh@gmail.com',
    fullName: 'Thanh Tung',
    avatar: 'http://semantic-ui.com/images/avatar2/large/elyse.png'
  },
  {
    id: '2',
    username: 'toantien',
    fullname: 'Toan Tien',
    email: 'toantien@gmail.com',
    avatar: 'http://semantic-ui.com/images/avatar2/large/matthew.png'
  },
  {
    id: '3',
    username: 'mylien',
    fullname: 'Mỹ Liên',
    email: 'mylien@gmail.com',
    avatar: 'http://semantic-ui.com/images/avatar2/small/mark.png'
  },
  {
    id: '4',
    username: 'tiennguyen',
    fullname: 'Tiến Nguyễn',
    email: 'tiennguyen@gmail.com',
    avatar: 'http://semantic-ui.com/images/avatar2/small/lena.png'
  },
  {
    id: '5',
    username: 'thaitran',
    fullname: 'Thái Trần',
    email: 'thaitran@gmail.com',
    avatar: 'http://semantic-ui.com/images/avatar2/small/eve.png'
  },
  {
    id: '6',
    username: 'thanhtungdp',
    fullname: 'Thanh Tùng',
    email: 'thanhtung@gmail.com',
    avatar: 'http://semantic-ui.com/images/avatar2/small/molly.png'
  }
]

export default users
