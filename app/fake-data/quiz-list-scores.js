/* eslint-disable */

import users from './users'

const scores: Array<ScoreType> = [
  {
    user: users[0],
    score: 20
  },
  {
    user: users[1],
    score: 11
  },
  {
    user: users[2],
    score: 10
  },
  {
    user: users[3],
    score: 4
  },
  {
    user: users[4],
    score: 10
  },
  {
    user: users[5],
    score: 4
  }
]

export default scores
