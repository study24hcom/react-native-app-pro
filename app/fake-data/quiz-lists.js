import users from './users'

export default [
  {
    id: 1,
    name: 'Đề thi tiểu học số 1 trương Lê Hồng Hong',
    totalRating: 4,
    totalAccess: 100,
    totalTime: 30,
    user: users[0]
  },
  {
    id: 2,
    name: 'Đề thi tiểu học số 1 trương Lê Hồng Hong',
    totalRating: 4,
    totalAccess: 100,
    totalTime: 30,
    user: users[1]
  },
  {
    id: 3,
    name: 'Đề thi tiểu học số 1 trương Lê Hồng Hong',
    totalRating: 4,
    totalAccess: 100,
    totalTime: 30,
    user: users[2]
  },
  {
    id: 4,
    name: 'Đề thi tiểu học số 1 trương Lê Hồng Hong',
    totalRating: 4,
    totalAccess: 100,
    totalTime: 30,
    user: users[3]
  },
  {
    id: 5,
    name: 'Đề thi tiểu học số 1 trương Lê Hồng Hong',
    totalRating: 4,
    totalAccess: 100,
    totalTime: 30,
    user: users[4]
  },
  {
    id: 6,
    name: 'Đề thi tiểu học số 1 trương Lê Hồng Hong',
    totalRating: 4,
    totalAccess: 100,
    totalTime: 30,
    user: users[2]
  }
]
