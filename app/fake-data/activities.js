import users from './users'

export default [
  {
    id: 1,
    content: 'vừa làm bài thi',
    user: users[0],
    link: {
      name: 'Đề thi Toán lần 5 trường Nguyễn Nguyên',
      href: '/de-thi/ab'
    }
  },
  {
    id: 2,
    content: 'vừa làm bài thi Đề thi ABCD hay lắm',
    user: users[1],
    link: {
      name: 'Đề thi Toán lần 5 trường Nguyễn Nguyên',
      href: '/de-thi/ab'
    }
  },
  {
    id: 3,
    content: 'vừa làm bài thi Đề thi ABCD hay lắm',
    user: users[2],
    link: {
      name: 'Đề thi Toán lần 5 trường Nguyễn Nguyên',
      href: '/de-thi/ab'
    }
  },
  {
    id: 4,
    content: 'vừa làm bài thi Đề thi ABCD hay lắm',
    user: users[3],
    link: {
      name: 'Đề thi Toán lần 5 trường Nguyễn Nguyên',
      href: '/de-thi/ab'
    }
  }
]
