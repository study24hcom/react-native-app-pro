/* eslint-disable */
export default [
  {
    _id: '581c2495cafad62b2f92b7fa',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'test.js',
            mode: { value: 'javascript', label: 'Javascript' },
            content:
              'var foo = function foo2() {\n\tconsole.log(foo === foo);  \n};\nconsole.log(typeof foo2);'
          }
        }
      },
      blocks: [
        {
          key: 'ro1v',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 'jm09',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: 'p5sj',
          text: 'What is the result?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '2dgll',
              text: 'true',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'fcp9q',
              text: 'false',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '2djn7',
              text: 'ReferenceError',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '5ak0j',
              text: 'undefined',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 3,
    isSuggestAnswer: true,
    suggestAnswer: {
      blocks: [
        {
          key: 'a3m76',
          text:
            'Khi bạn gán hàm cho 1 biến var và đặt tên hàm đó, nó sẽ không khởi tạo hàm.',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '3gcra',
          text:
            'Vì vậy foo2() được gán cho foo, nhưng nó không được khởi tạo toàn cục, vì (typeof foo2 = undefined)',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    }
  },
  {
    _id: '581c2495cafad62b2f92b7fc',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'test.js',
            mode: 'javascript',
            content:
              'function aaa() {\n\treturn {\n\t\ttest: 1\t\n\t}\n}\nalert(typeof aaa());'
          }
        }
      },
      blocks: [
        {
          key: 'efg51',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 'pa0d',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: 'b3sad',
          text: 'What is alerted ?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '171cs',
              text: 'function',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '726a3',
              text: 'number',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'cj7i9',
              text: 'object',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '3257b',
              text: 'undefined',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 2,
    isSuggestAnswer: true,
    suggestAnswer: {
      blocks: [
        {
          key: '9dnlg',
          text: 'Câu này thì quá dễ',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    }
  },
  {
    _id: '581c2495cafad62b2f92b7fe',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'calculator.js',
            mode: 'javascript',
            content: 'Number("1") - 1 == 0'
          }
        }
      },
      blocks: [
        {
          key: '3d7e1',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 'aogkv',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: 'f8jsv',
          text: 'What is the result ?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '2b15t',
              text: 'true',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'c368s',
              text: 'false',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'e53op',
              text: 'TypeError',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 0,
    isSuggestAnswer: false,
    suggestAnswer: ''
  },
  {
    _id: '581c2496cafad62b2f92b800',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'compare.js',
            mode: 'javascript',
            content: '(true + false) > 2 + true'
          }
        }
      },
      blocks: [
        {
          key: 'eu62f',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '9ju11',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: '9e7d8',
          text: 'What is the result ?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'fb3nf',
              text: 'true',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'fhb8d',
              text: 'false',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '557l9',
              text: 'TypeError',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '29ont',
              text: 'NaN',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 1,
    isSuggestAnswer: true,
    suggestAnswer: {
      blocks: [
        {
          key: '6dd5t',
          text: 'true ứng với 1,',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 'edv10',
          text: 'false ứng với 0,',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '7pv',
          text: 'Thực hiện phép tính lớp 1:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '2h93v',
          text: '(1 + 0) >(2 +1) === false',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    }
  },
  {
    _id: '581c2496cafad62b2f92b802',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'typeof.js',
            mode: 'javascript',
            content:
              "function bar() {\n\treturn foo;\n\tfoo = 10;\n\tfunction foo() {}\n\tvar foo = '11';\n}\nalert(typeof bar());"
          }
        }
      },
      blocks: [
        {
          key: 'c6v8u',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 'bsf9m',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: '8joq8',
          text: 'What is alerted',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'cknd9',
              text: 'number',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '9utvh',
              text: 'function',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'cdbp8',
              text: 'undefined',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '21nne',
              text: 'string',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '6tp0l',
              text: 'Error',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 1,
    isSuggestAnswer: true,
    suggestAnswer: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'typeof.js',
            mode: 'javascript',
            content:
              "function bar() {\n\t/* Đưa khai báo biến, và khai báo hàm lên*/\n\tvar foo;\n\t// Biến foo được ưu tiên đưa lên đầu\n\tfunction foo() {};\n\t// Tiếp theo là khai báo function\n\t// Vì vậy kết quả trả về sẽ là: function\n\treturn foo;\n\tfoo = 10;\n\tfoo = '11';\n}\nalert(typeof bar());"
          }
        }
      },
      blocks: [
        {
          key: '5m5fe',
          text: 'Khi javascript biên dịch, nó sẽ được sắp xếp lại như sau',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '9u017',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: 'dv7uk',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    }
  },
  {
    _id: '581c2496cafad62b2f92b804',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'result.js',
            mode: 'javascript',
            content: '"1" - -"1";'
          }
        }
      },
      blocks: [
        {
          key: '683nb',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 'c6178',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: 'cgdhg',
          text: 'What is the result?\n',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '55g0i',
              text: '0',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'drf67',
              text: '2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'el9mi',
              text: '11',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '67krl',
              text: '"11"',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 1,
    isSuggestAnswer: true,
    suggestAnswer: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'result.js',
            mode: 'javascript',
            content:
              '"1" - (-"1")\n//Tự động ép kiểu -"1" thành -1 và ép luôn "1" thành 1\n// => 1 - (-1) = 2'
          }
        }
      },
      blocks: [
        {
          key: '3gk0n',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '666gl',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: '9v84r',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    }
  },
  {
    _id: '581c2496cafad62b2f92b806',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'object_func_call.js',
            mode: 'javascript',
            content:
              'var x = 3;\nvar foo = {\n\tx: 2,\n\tbaz: {\n\t\tx: 1,\n\t\tbar: function() {\n\t\t\treturn this.x;\n\t\t}\n\t}\n}\nvar go = foo.baz.bar;\nalert(go());\nalert(foo.baz.bar());'
          }
        }
      },
      blocks: [
        {
          key: '9c91u',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '713hu',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: '6ii65',
          text: 'What is the order of values alerted ?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'es7ol',
              text: '1, 2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'ane15',
              text: '1, 3',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '6d0p9',
              text: '2, 1',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '4tuuq',
              text: '2, 3',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '6l794',
              text: '3, 1',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '98ldk',
              text: '3, 2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 4,
    isSuggestAnswer: true,
    suggestAnswer: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'object_func_call.js',
            mode: 'javascript',
            content:
              '// Biến x toàn cục\nvar x = 3;\n/*\n\t...Code above\n*/\nvar go = foo.baz.bar;\nalert(go())\n/* Vì foo.baz.bar gán cho biến go, lúc này nó đang có thuộc\ntính là scope global, vì vậy context lúc này chính là \nthis = window, cho nên kết quả sẽ trả về là window.x, \ncái mà đã được khai báo đầu tiên.\n*/\nalert(foo.baz.bar());\n/* hàm bar() được gọi dưới tư cách của foo.baz vì vậy context\nlúc này sẽ là foo.baz và this = foo.baz cho nên kết quả trả\nvề là foo.baz.x\n*/'
          }
        }
      },
      blocks: [
        {
          key: '8jvlo',
          text:
            'Trong Javascript context được liên kết với từ khóa this, nó phụ thuộc vào việc hàm được gọi như thế nào, chứ không phụ thuộc vào cách khai báo.',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 'civij',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: '27l63',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    }
  },
  {
    _id: '581c2496cafad62b2f92b808',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'string.js',
            mode: 'javascript',
            content: 'new String("This is a string") instanceof String;'
          }
        }
      },
      blocks: [
        {
          key: 'fl49q',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 'e8d34',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: '84oci',
          text: 'What is the result ? ',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'ehnls',
              text: 'true',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '2ad3g',
              text: 'false',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'ensop',
              text: 'TypeError',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 0,
    isSuggestAnswer: false,
    suggestAnswer: ''
  },
  {
    _id: '581c2496cafad62b2f92b80a',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'array_sum.js',
            mode: 'javascript',
            content: "[] + [] + 'foo'.split('');"
          }
        }
      },
      blocks: [
        {
          key: '3i6',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 'ea3iq',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: '1afa3',
          text: 'What is the result ?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '8m6dm',
              text: '"f, o, o"',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'ctu0f',
              text: 'TypeError',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '81es1',
              text: '["f", "o", "o"]',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'eahdt',
              text: '[][]["f", "o", "o"]',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 0,
    isSuggestAnswer: false,
    suggestAnswer: ''
  },
  {
    _id: '581c2496cafad62b2f92b80c',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'array_to_string.js',
            mode: 'javascript',
            content: 'new Array(5).toString();'
          }
        }
      },
      blocks: [
        {
          key: '93g0m',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '56f15',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: 'ava4b',
          text: 'What is the result ?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'bh58i',
              text: '",,,,"',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'ah3ue',
              text: '[]',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '6ptvb',
              text: '"[]"',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 0,
    isSuggestAnswer: false,
    suggestAnswer: ''
  },
  {
    _id: '581c40a8cafad62b2f92b9c3',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'array_reset.js',
            mode: 'javascript',
            content:
              "var myArr = ['foo', 'bar', 'baz'];\nmyArr.length = 0;\nmyArr.push('bin');\nconsole.log(myArr);"
          }
        }
      },
      blocks: [
        {
          key: '6p8v2',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '1baa',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: '1dgfl',
          text: 'What is printed in the console?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'fb3bj',
              text: "['foo', 'baz', 'baz']",
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'uq1q',
              text: "['foo', 'bar', 'baz', 'bin']",
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '2ahuu',
              text: "['bin', 'foo', 'bar', 'baz']",
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '5klkc',
              text: "['bin']",
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 3,
    isSuggestAnswer: true,
    suggestAnswer: {
      blocks: [
        {
          key: '6ggb1',
          text: 'Thực hiện gán myArr.length =0 đồng nghĩa với việc reset mảng.',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    }
  },
  {
    _id: '581c40a8cafad62b2f92b9c5',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'string_compare.js',
            mode: 'javascript',
            content: "String('Hello') === 'Hello'"
          }
        }
      },
      blocks: [
        {
          key: '6q0ej',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 'bf6et',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: 'v1u3',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '6386q',
              text: 'true',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'e9l8e',
              text: 'false',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'eu26r',
              text: 'ReferenceError',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 0,
    isSuggestAnswer: false,
    suggestAnswer: ''
  },
  {
    _id: '581c40a8cafad62b2f92b9c7',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'function_in_function',
            mode: 'javascript',
            content:
              'var x = 0;\nfunction foo() {\n\tx++;\n\tthis.x = x;\n\treturn foo;\n}\nvar bar = new new foo;\nconsole.log(bar.x);'
          }
        }
      },
      blocks: [
        {
          key: 'fqdsl',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 'ec56u',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: 'fjecc',
          text: 'What is printed on the console?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '5irso',
              text: 'ReferenceError',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '7c9dv',
              text: 'TypeError',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '1gf69',
              text: 'undefined',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'c430s',
              text: '0',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'b5bqa',
              text: '1',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 2,
    isSuggestAnswer: true,
    suggestAnswer: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'function_in_fuction',
            mode: 'javascript',
            content:
              'console.log(typeof bar) //function\n=> bar.x === undefined'
          }
        }
      },
      blocks: [
        {
          key: '30q2e',
          text:
            'Bản chất của hàm foo là trả về khai báo của chính nó, vì vậy bar sẽ nhận trả về là 1 function, chứ không phải là object, vì vậy kết quả là undefined',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '21j2c',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: 'bflie',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    }
  },
  {
    _id: '581c40a8cafad62b2f92b9c9',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'string_compare',
            mode: 'javascript',
            content: '"This is a string" instanceof String;'
          }
        }
      },
      blocks: [
        {
          key: 'd08o2',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 'qn0',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: '76b7q',
          text: 'What is the result ?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'c6c1i',
              text: 'true',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '6jtm8',
              text: 'false',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'ak3m9',
              text: 'TypeError',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 1,
    isSuggestAnswer: true,
    suggestAnswer: {
      entityMap: {
        '0': {
          type: 'link',
          mutability: 'MUTABLE',
          data: {
            href: 'http://tek.eten.vn/kiem-tra-kieu-du-lieu-trong-javascript'
          }
        }
      },
      blocks: [
        {
          key: 'dvqli',
          text: 'Để hiểu rõ hơn về instanceOf bạn vui lòng đọc bài này .',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 46, length: 7, key: 0 }]
        },
        {
          key: 'e8ku1',
          text:
            'instanceOf chỉ hoạt động với kiểu dữ liệu không phải là nguyên thủy.',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '3njij',
          text: 'Kiểu dữ liệu nguyên thủy như là: String, Boolean, Number',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    }
  },
  {
    _id: '581c40a8cafad62b2f92b9cb',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'object_test.js',
            mode: 'javascript',
            content:
              'var bar = 1, \n\t\tfoo = {};\nfoo: {\n\tbar: 2;\n\tbaz: ++bar;\n};\nfoo.baz + foo.bar + bar;'
          }
        }
      },
      blocks: [
        {
          key: '7duro',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '6ou2p',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: 'cjssr',
          text: 'What is the result?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'edlks',
              text: 'ReferenceError',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'dvfkp',
              text: 'TypeError',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '3gcho',
              text: 'undefined',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'cuj6g',
              text: 'NaN',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'e748m',
              text: '4',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'cq5d8',
              text: '5',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 3,
    isSuggestAnswer: true,
    suggestAnswer: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'giải thích',
            mode: 'javascript',
            content:
              'foo: {\n\tbar: 2;\n\tbaz: ++bar;\n};\n/* Đoạn này không phải là gán giá trị cho object foo */'
          }
        }
      },
      blocks: [
        {
          key: '96jqe',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 't49d',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: '41j6d',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    }
  },
  {
    _id: '581c40a8cafad62b2f92b9cd',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'array_key.js',
            mode: 'javascript',
            content:
              "var myArr = ['foo', 'bar', 'baz'];\nmyArr[2];\nconsole.log('2' in myArr);"
          }
        }
      },
      blocks: [
        {
          key: 'a5jq2',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '95jpe',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: 'c2fag',
          text: 'What is the result of console.log?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'emk8j',
              text: 'true',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'cuk7k',
              text: 'false',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '391db',
              text: 'ReferenceError',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 0,
    isSuggestAnswer: true,
    suggestAnswer: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'array_key.js',
            mode: 'javascript',
            content:
              "console.log('2' in myArr);\n/*Kiểm tra key object '2' có tồn tại trong myArr, \nbởi vì bản chất của array trong javascript là object\nmyArr = ['foo', 'bar', 'baz'] tương ứng với key 1, 2, 3\nĐể kiểm trứng điều này bạn hãy chạy lệnh sau: */\nconsole.log(Object.keys(myArr));"
          }
        }
      },
      blocks: [
        {
          key: 'eimnp',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '2n4al',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: '7bhk3',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    }
  },
  {
    _id: '581c40a8cafad62b2f92b9cf',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'array.js',
            mode: 'javascript',
            content:
              "var arr = [];\narr[0]  = 'a';\narr[1]  = 'b';\narr.foo = 'c';\nalert(arr.length);"
          }
        }
      },
      blocks: [
        {
          key: 'bmgd9',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '8786h',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: 'dpnf7',
          text: 'What is alerted ?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '7bhqp',
              text: '1',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '4eape',
              text: '2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '4344f',
              text: '3',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '7h3vv',
              text: 'undefined',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 1,
    isSuggestAnswer: false,
    suggestAnswer: ''
  },
  {
    _id: '581c40a8cafad62b2f92b9d1',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'compare.js',
            mode: 'javascript',
            content: '10 > 9 > 8 === true;'
          }
        }
      },
      blocks: [
        {
          key: 'aj008',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '2i98m',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: 'bgtsv',
          text: 'What is the result ?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '9s5n4',
              text: 'true',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'e6gba',
              text: 'false',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 1,
    isSuggestAnswer: true,
    suggestAnswer: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'filename',
            mode: 'javascript',
            content:
              'var a = (10 > 9) // true = 1;\nvar b = a > 8 // false;\nvar c = (b === true) // false'
          }
        }
      },
      blocks: [
        {
          key: '45r07',
          text: 'Biểu thức được so sánh từ trái sang phải, nên ta chia như sau',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '424pr',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: '5bs3g',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    }
  },
  {
    _id: '581c40a8cafad62b2f92b9d3',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: {
            name: 'arguments.js',
            mode: 'javascript',
            content:
              'function foo(a, b) {\n\targuments[1] = 2;\n\talert(b);\n}\nfoo(1);'
          }
        }
      },
      blocks: [
        {
          key: '3a75v',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '1qvoc',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: '58pti',
          text: 'What is alerted ?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'fk83t',
              text: '2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '5a1gc',
              text: 'undefined',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'f08h9',
              text: 'ReferenceError',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 1,
    isSuggestAnswer: false,
    suggestAnswer: ''
  },
  {
    _id: '581c40a8cafad62b2f92b9d5',
    question: {
      entityMap: {
        '0': {
          type: 'code-editor-block',
          mutability: 'IMMUTABLE',
          data: { name: 'NaN', mode: 'javascript', content: 'NaN === NaN' }
        }
      },
      blocks: [
        {
          key: '5hklq',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 'fqnav',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: 'eroa6',
          text: 'What is the resullt ?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'e1dat',
              text: 'true',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'bdovb',
              text: 'false',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ],
    correctAnswerIndex: 1,
    isSuggestAnswer: true,
    suggestAnswer: {
      entityMap: {
        '0': {
          type: 'link',
          mutability: 'MUTABLE',
          data: {
            href:
              'http://adripofjavascript.com/blog/drips/the-problem-with-testing-for-nan-in-javascript.html'
          }
        }
      },
      blocks: [
        {
          key: '4vt8j',
          text:
            'NaN là một property có nghĩa đại điện cho giá trị "Not a number", nó chỉ ra 1 giá trị không phải là số phù hợp.',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '8ufi6',
          text:
            'Trong Javascript NaN có điểm khác biệt là 1 giá trị duy nhất mà không phải bằng chính nó. Vì vậy chúng ta không thể so sánh NaN với NaN.',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [
            { offset: 124, length: 3, style: 'BOLD' },
            {
              offset: 132,
              length: 3,
              style: 'BOLD'
            }
          ],
          entityRanges: []
        },
        {
          key: '5mt5d',
          text:
            'Để kiểm tra 1 giá trị có phải là NaN không, bạn nên sử dụng hàm isNaN()',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [{ offset: 64, length: 7, style: 'BOLD' }],
          entityRanges: []
        },
        {
          key: 'cgkqa',
          text:
            'http://adripofjavascript.com/blog/drips/the-problem-with-testing-for-nan-in-javascript.html',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [{ offset: 0, length: 91, style: 'UNDERLINE' }],
          entityRanges: [{ offset: 0, length: 91, key: 0 }]
        }
      ]
    }
  }
]
