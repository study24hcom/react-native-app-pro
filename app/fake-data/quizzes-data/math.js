/* eslint-disable */
export default [
  {
    _id: '582fe2fad2456a5ad69e8cd6',
    question: {
      entityMap: {
        '0': {
          type: 'image',
          mutability: 'IMMUTABLE',
          data: {
            src:
              'http://sv1.upsieutoc.com/2016/11/19/ScreenShot2016-11-19at10.54.26.png',
            alignment: 'right'
          }
        }
      },
      blocks: [
        {
          key: 'bio6s',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 'bs9qt',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: '7otns',
          text:
            'Đường cong trong hình bên là đồ thị của một hàm số trong bốn hàm số được liệt kê ở bốn phương án A, B, C, D dưới đây. Hỏi hàm số đó là hàm số nào ?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      { answer: '$y=x^4+x^2+1$' },
      { answer: '$y=x^3-2x+3$' },
      { answer: '$y=x^4-2x^2+3$' },
      { answer: '$y=-x^3-2x+3$' }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8cd8',
    question: {
      blocks: [
        {
          key: 'eha17',
          text:
            'Cho hàm số $y = \\frac{3}{x-2}$ .Số tiệm cận của đồ thị hàm số bằng',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      { answer: '0.' },
      { answer: '2.' },
      { answer: '3.' },
      {
        answer: {
          blocks: [
            {
              key: '2k705',
              text: '1.',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8cda',
    question: {
      blocks: [
        {
          key: '32lrk',
          text:
            'Cho hàm số $y = \\frac{1}{3}x^3 + mx^2 + (2m - 1) x - 1$.  Mệnh đề nào sau đây là sai?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [{ offset: 81, length: 3, style: 'BOLD' }],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '5rpm7',
              text: '$\\forall m < 1$  thì hàm số có hai điểm cực trị',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      { answer: 'Hàm số luôn luôn có cực đại và cực' },
      {
        answer: {
          blocks: [
            {
              key: '8v7jk',
              text: '$\\forall m \\neq 1$ thì hàm số có cực đại và cực tiểu',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '3dht4',
              text: '$\\forall m > 1$ thì hàm số có cực trị',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8cdc',
    question: {
      blocks: [
        {
          key: 'ib7g',
          text:
            'Kết luận nào sau đây về tính đơn điệu của hàm số $y = \\frac{2x+1}{x+1}$ là đúng?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [{ offset: 75, length: 4, style: 'BOLD' }],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '2j0pq',
              text:
                'Hàm số đồng biến trên các khoảng $(-\\infty ; -1)$ và $(-1; +\\infty)$.',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '9ne4o',
              text:
                'Hàm số luôn luôn đồng biến trên $\\mathbb{R}\\setminus \\{-1\\}$ ;',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'b40cg',
              text:
                'Hàm số nghịch biến trên các khoảng $(-\\infty ; -1)$ và $(-1; +\\infty)$;',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'fu3in',
              text:
                'Hàm số luôn luôn nghịch biến trên $\\mathbb{R}\\setminus \\{1\\}$ ;',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8cde',
    question: {
      blocks: [
        {
          key: '416ag',
          text:
            'Cho hàm số $y = \\frac{1}{3}x^3 - 2x^2 + 3x + \\frac{2}{3}$. Toạ độ điểm cực đại của hàm số là',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'aflql',
              text: '(-1; 2)',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '8u6ne',
              text: '(3;  2)',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '9dj8r',
              text: '(1; - 2)',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '2fljm',
              text: '(1; 2)',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8ce0',
    question: {
      blocks: [
        {
          key: 'cev49',
          text: 'Trên khoảng $(0; +\\infty)$ thì hàm số $y = - x^3 + 3x + 1$:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '60da4',
              text: 'Có giá trị nhỏ nhất là Min y = 3',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'f8kj',
              text: 'Có giá trị lớn nhất là Max y = –1',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '1jhia',
              text: 'Có giá trị nhỏ nhất là Min y = –1',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '86p2o',
              text: 'Có giá trị lớn nhất là Max y = 3',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8ce2',
    question: {
      blocks: [
        {
          key: '3sllf',
          text:
            'Cho hàm số $y = f(x) = ax^3+bx^2+cx+d$, $a \\neq  0$.Khẳng định nào sau đây sai ?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [{ offset: 81, length: 3, style: 'BOLD' }],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'eo140',
              text: 'Đồ thị hàm số luôn cắt trục hoành',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'f9dhg',
              text: 'Đồ thị hàm số luôn có tâm đối xứng.',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'b8u9p',
              text: 'Hàm số luôn có cực trị',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '76r3b',
              text: '$\\lim_{x \\rightarrow \\infty}f(x) = \\infty$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8ce4',
    question: {
      entityMap: {
        '0': {
          type: 'LINK',
          mutability: 'MUTABLE',
          data: { url: 'http://tungtung.vn/create/quizzes#' }
        }
      },
      blocks: [
        {
          key: 'e7k0f',
          text:
            'Khoảng cách giữa 2 điểm cực trị của đồ thi hàm số $y=\\frac{x^2 - mx + m}{x - 1}$ bằng :',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 50, length: 30, key: 0 }]
        }
      ]
    },
    answers: [
      { answer: '$2\\sqrt{5}$' },
      { answer: '$5\\sqrt{2}$' },
      { answer: '$4\\sqrt{5}$' },
      { answer: '$\\sqrt{5}$' }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8ce6',
    question: {
      entityMap: {
        '0': {
          type: 'LINK',
          mutability: 'MUTABLE',
          data: { url: 'http://tungtung.vn/create/quizzes#' }
        }
      },
      blocks: [
        {
          key: 'bb9v4',
          text: 'Hàm số $\\sqrt{2x-x^2}$  nghịch biến trên khoảng:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 7, length: 15, key: 0 }]
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '6oe7d',
              text: '(0; 1)',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          entityMap: {
            '0': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: { url: 'http://tungtung.vn/create/quizzes#' }
            }
          },
          blocks: [
            {
              key: '6uhsm',
              text: '$(1;+\\infty)$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: [{ offset: 0, length: 13, key: 0 }]
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '1f7m5',
              text: '(1; 2)',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'c57o2',
              text: '(0; 2)',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8ce8',
    question: {
      entityMap: {
        '0': {
          type: 'image',
          mutability: 'IMMUTABLE',
          data: {
            src:
              'http://sv1.upsieutoc.com/2016/11/19/ScreenShot2016-11-19at11.09.28.png'
          }
        }
      },
      blocks: [
        {
          key: '7pfcg',
          text:
            'Cho một tấm nhôm hình vuông cạnh 12 cm. Người ta cắt ở bốn góc của tấm nhôm đó bốn hình vuông bằng nhau, mỗi hình vuông có cạnh bằng x (cm), rồi gập tấm nhôm lại như hình vẽ dưới đây để được một cái hộp không nắp. Tìm x để hộp nhận được có thể tích lớn nhất.',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 'fb56',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '9guc9',
          text: ' ',
          type: 'atomic',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 0, length: 1, key: 0 }]
        },
        {
          key: '883h7',
          text: '',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '481d4',
              text: 'x = 4',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '1a4uk',
              text: 'x = 6',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '812ad',
              text: 'x = 3',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'bmfaa',
              text: 'x = 2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8cea',
    question: {
      blocks: [
        {
          key: 'as7tu',
          text:
            'Tìm tất cả các giá trị thực của tham số m sao cho hàm số $y = \\frac{\\tan - 2}{\\tan x - m}$ đồng biến trên khoảng $\\left(0;  \\frac{\\pi}{4}\\right)$',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '2rp8n',
              text: '$m \\le 0$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'bsalb',
              text: '$1 \\le m \\le 2$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'fr3je',
              text: '$m \\le 0$ hoặc $1 \\le m \\le 2$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'bdm5h',
              text: '$m > 2$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8cec',
    question: {
      blocks: [
        {
          key: '7aa88',
          text: 'Phương trình $\\log_{\\sqrt3}{x}=2$ có nghiệm x bằng:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'c8ol2',
              text: '1',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '17ou2',
              text: '9',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '93asn',
              text: '2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '40oo3',
              text: '3',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8cee',
    question: {
      blocks: [
        {
          key: 'e07ib',
          text: 'Phương trình $4^x + 2^x - 2 = 0$ có nghiệm x bằng:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '3uckp',
              text: '1',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'bt9p9',
              text: '1 và - 2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'fnk5e',
              text: '- 2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '5pg3u',
              text: '0',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8cf0',
    question: {
      entityMap: {
        '0': {
          type: 'LINK',
          mutability: 'MUTABLE',
          data: { url: 'http://tungtung.vn/create/quizzes#' }
        },
        '1': {
          type: 'LINK',
          mutability: 'MUTABLE',
          data: { url: 'http://tungtung.vn/create/quizzes#' }
        }
      },
      blocks: [
        {
          key: '43c9s',
          text: 'Cho hàm số $f(x) = x.e^x$. Giá trị của $f{"}(0)$ là:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [
            { offset: 11, length: 14, key: 0 },
            { offset: 39, length: 9, key: 1 }
          ]
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '4jdq8',
              text: '1',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '6d8cs',
              text: '2e',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'dghv0',
              text: '3e',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '3js7a',
              text: '2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8cf2',
    question: {
      blocks: [
        {
          key: 'a7q88',
          text: 'Giải bất phương trình $\\log_{3}(2x - 1) > 3$.',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'un2l',
              text: ' x > 4',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'atkoq',
              text: 'x > 14',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '3nkk5',
              text: 'x < 2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '4pmuo',
              text: '2 < x < 14',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8cf4',
    question: {
      blocks: [
        {
          key: 'e9m78',
          text:
            'Tìm tập xác định $D$ của hàm số $y = \\log_{5}(x^3 - x^2 - 2x)$ là:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'dhg2n',
              text: '(0; 1)',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'cc5vl',
              text: '$(1; +\\infty)$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '43f89',
              text: '$(-1;0) \\cup (2; +\\infty)$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '9edj3',
              text: '$(0;2) \\cup (4;+\\infty)$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8cf6',
    question: {
      blocks: [
        {
          key: 'a7ma0',
          text:
            'Giả sử ta có hệ thức $a^2 + b^2 - 7ab$ $( a, b >0)$. Hệ thức nào sau đây đúng?',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [{ offset: 73, length: 4, style: 'BOLD' }],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'du5mc',
              text: 'm:2log_2\\left(a+b\\right)=log_2a+log_2b:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '6n4mj',
              text: 'm:2log_2\\frac{a+b}{3}=log_2a+log_2b:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'f0njp',
              text: 'm:log_2\\frac{a+b}{3}=2\\left(log_2a+log_2b\\right):',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'a29ss',
              text: 'm:4log_2\\frac{a+b}{6}=log_2a+log_2b:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8cf8',
    question: {
      blocks: [
        {
          key: 'bg8kd',
          text:
            'Cho $\\log_{2}{5} = a$; $\\log_{3}{5} = b$. Khi đó $\\log_{6}{5}$ tính theo a và b là:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      { answer: '$\\frac{1}{a+b}$' },
      { answer: '$\\frac{ab}{a+b}$' },
      { answer: '$a+b$' },
      { answer: '$a^2+b^2$' }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8cfa',
    question: {
      blocks: [
        {
          key: 'bqfnm',
          text: 'Tìm mệnh đề đúng trong các mệnh đề sau:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'd8dsa',
              text:
                'Hàm số $y = a^x$ với $0 < a < 1$ là một hàm số nghịch biến trên $(-\\infty; +\\infty)$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'bddb5',
              text:
                'Hàm số $y = a^x$ với $a > 1$ là một hàm số nghịch biến trên $(-\\infty; +\\infty)$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '245n4',
              text:
                'Đồ thị hàm số $y = a^x$ $(0 < a \\neq 1)$ luôn đi qua điểm $(a; 1)$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'e55nu',
              text:
                'Đồ thị các hàm số $y = a^x$ và $y = \\left(\\frac{1}{a}\\right)^x$ $(0 < a \\neq 1)$ thì đối xứng nhau qua trục tung',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8cfc',
    question: {
      blocks: [
        {
          key: '507dm',
          text:
            "Cho hàm số $f(x) = 2^{\\frac{x-1}{x+2}}$. Đạo hàm $f'(0)$ bằng:",
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'b5o97',
              text: '2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '6viqm',
              text: 'ln2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '9qto1',
              text: '2ln2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'eugo7',
              text: 'Kết quả khác',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8cfe',
    question: {
      blocks: [
        {
          key: 'cu8il',
          text:
            'Một nguời gửi tiết kiệm với lãi suất 8,4% năm và lãi hàng năm đuợc nhập vào vốn, hỏi sau bao nhiêu năm ngưòi đó thu đuợc gấp đôi số tiền ban đầu? ',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'ckucu',
              text: '6',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '4k54p',
              text: '7',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'c4pd3',
              text: '8',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'e5nkf',
              text: '9',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8d00',
    question: {
      blocks: [
        {
          key: 'fuvrp',
          text:
            'Tìm nguyên hàm của hàm số $\\int\\left(x^2 + \\frac{3}{x} - 2\\sqrt{x}\\right)dx$',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'e58uj',
              text:
                'm:\\frac{x^3}{3}+2\\ln\\left|x\\right|-\\frac{4}{3}\\sqrt{x^3}+C:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'c7uc3',
              text: 'm:\\frac{x^3}{3}+3\\ln x-\\frac{4}{3}\\sqrt{x^3}:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '6u60f',
              text:
                'm:\\frac{x^3}{3}+3\\ln\\left|x\\right|+\\frac{4}{3}\\sqrt{x^3}+C:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'clio',
              text:
                'm:\\frac{x^3}{3}-\\ 3\\ln\\left|x\\right|-\\frac{4}{3}\\sqrt{x^3}+C:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8d02',
    question: {
      blocks: [
        {
          key: 'crsvr',
          text:
            'Giá trị m để hàm số$ F(x) =mx^3 +(3m+2)x^2-4x+3$ là một nguyên hàm của hàm số $f(x) = 3x^2 + 10x - 4$ là:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '62mcq',
              text: 'm = 3',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'cv6q',
              text: 'm = 0',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '9ul28',
              text: 'm = 1',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'c9bo4',
              text: 'm = 2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8d04',
    question: {
      blocks: [
        {
          key: 'cc9as',
          text:
            'Tính tích phân $\\int _{\\frac{\\pi }{6}}^{\\frac{\\pi }{4}}\\frac{1-\\sin ^3x}{\\sin ^2x}\\text{dx}$',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      { answer: '$\\frac{\\sqrt{3}-2}{2}$' },
      { answer: '$\\frac{\\sqrt{3}+\\sqrt{2}-2}{2}$' },
      { answer: '$\\frac{\\sqrt{3}+\\sqrt{2}}{2}$' },
      { answer: '$\\frac{\\sqrt{3}+2\\sqrt{2}-2}{2}$' }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8d06',
    question: {
      blocks: [
        {
          key: '1k6g',
          text:
            'Tính diện tích hình phẳng giới hạn bởi đồ thị hàm số $y = 2 - x^2$ và $y = x$',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '8viov',
              text: '5',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'b94n9',
              text: '7',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '628ia',
              text: '9/2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'ds95t',
              text: '11/2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8d08',
    question: {
      entityMap: {
        '0': {
          type: 'LINK',
          mutability: 'MUTABLE',
          data: { url: 'http://tungtung.vn/create/quizzes#' }
        }
      },
      blocks: [
        {
          key: '46a7b',
          text:
            'Tính diện tích hình phẳng giới hạn bởi đồ thị hàm số $y = 5x^4 - 3x^2 - 8$  trên [1; 3]',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: [{ offset: 65, length: 21, key: 0 }]
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '3cdm7',
              text: '100',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'bml6t',
              text: '150',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '7smse',
              text: '180',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '72jv5',
              text: '200',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8d0a',
    question: {
      blocks: [
        {
          key: 'c6uk0',
          text:
            'Kí hiệu (H) là hình phẳng giới hạn bởi đồ thị hàm số $y=2x - x^2$ và $y=0$. Tính thể tích  vật thể tròn xoay được sinh ra bởi hình phẳng đó khi nó quay quanh trục Ox ',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      { answer: '$\\frac{16\\pi}{15}$' },
      { answer: '$\\frac{17\\pi}{15}$' },
      { answer: '$\\frac{18\\pi}{15}$' },
      { answer: '$\\frac{19\\pi}{15}$' }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8d0c',
    question: {
      blocks: [
        {
          key: 'bbp44',
          text:
            'Parabol $y = \\frac{x^2}{2}$ chia hình tròn có tâm tại gốc tọa độ, bán kính $2\\sqrt2$ thành 2 phần. Tỉ số diện tích của chúng thuộc khoảng nào:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '856vh',
              text: '$(0,4;0,5)$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '64e4q',
              text: '$(0,5;0,6)$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          entityMap: {
            '0': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: { url: 'http://tungtung.vn/create/quizzes#' }
            }
          },
          blocks: [
            {
              key: '4jbbs',
              text: '$(0,6;0,7)$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: [{ offset: 2, length: 8, key: 0 }]
            }
          ]
        }
      },
      {
        answer: {
          entityMap: {
            '0': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: { url: 'http://tungtung.vn/create/quizzes#' }
            }
          },
          blocks: [
            {
              key: 'a3t5o',
              text: '$(0,7;0,8)$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: [{ offset: 2, length: 8, key: 0 }]
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fad2456a5ad69e8d0e',
    question: {
      blocks: [
        {
          key: '16a8u',
          text: 'Giải phương trình $2x^2 - 5x + 4 = 0$ trên tập số phức.',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '59pv',
              text:
                '$x_1=-\\frac{5}{4}+\\frac{\\sqrt{7}}{4}i$;  $x_2=-\\frac{5}{4}-\\frac{\\sqrt{7}}{4}i$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          entityMap: {
            '0': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: { url: 'http://tungtung.vn/create/quizzes#' }
            },
            '1': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: { url: 'http://tungtung.vn/create/quizzes#' }
            }
          },
          blocks: [
            {
              key: '1l03v',
              text:
                '$x_1=\\frac{5}{4}+\\frac{\\sqrt{7}}{4}i$;  $x_2=\\frac{5}{4}-\\frac{\\sqrt{7}}{4}i$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: [
                { offset: 0, length: 37, key: 0 },
                { offset: 40, length: 37, key: 1 }
              ]
            }
          ]
        }
      },
      {
        answer: {
          entityMap: {
            '0': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: { url: 'http://tungtung.vn/create/quizzes#' }
            },
            '1': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: { url: 'http://tungtung.vn/create/quizzes#' }
            }
          },
          blocks: [
            {
              key: '75s33',
              text:
                '$x_1=\\frac{5}{2}+\\frac{\\sqrt{7}}{4}i$;  $x_2=\\frac{5}{2}-\\frac{\\sqrt{7}}{4}i$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: [
                { offset: 0, length: 37, key: 0 },
                { offset: 40, length: 37, key: 1 }
              ]
            }
          ]
        }
      },
      {
        answer: {
          entityMap: {
            '0': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: { url: 'http://tungtung.vn/create/quizzes#' }
            },
            '1': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: { url: 'http://tungtung.vn/create/quizzes#' }
            }
          },
          blocks: [
            {
              key: 'e405i',
              text:
                '$x_1=\\frac{3}{4}+\\frac{\\sqrt{7}}{4}i$;  $x_2=\\frac{3}{4}-\\frac{\\sqrt{7}}{4}i$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: [
                { offset: 0, length: 37, key: 0 },
                { offset: 40, length: 37, key: 1 }
              ]
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d10',
    question: {
      blocks: [
        {
          key: 'fj8s0',
          text:
            'Gọi $z_1$, $z_2$ là hai nghiệm phức của phương trình $z^2 + 2z + 10 = 0$. Tính giá trị của biểu thức $A = |z_1|^2  + |z_2|^2$ . ',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'dff6m',
              text: '15',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '8ts64',
              text: '17',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '55m1s',
              text: '19',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'crhjp',
              text: '20',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d12',
    question: {
      blocks: [
        {
          key: '5j0bv',
          text:
            'Cho số phức thỏa mãn: $\\bar{a} = \\frac{(1-\\sqrt3 i)^2}{1 - i}$. Tìm môdun của $\\bar{z} + iz$',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      { answer: '$8\\sqrt{2}$' },
      { answer: '$8\\sqrt{3}$' },
      { answer: '$4\\sqrt{2}$' },
      { answer: '$4\\sqrt{3}$' }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d14',
    question: {
      blocks: [
        {
          key: '4029g',
          text:
            'Cho số phức z thỏ mãn: $(2 - 3i)z + (4 - i)\\bar{z} = - (1 + 3i)^2$. Xác định phần thực và phần ảo của z.',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '7hjjj',
              text: 'Phần thực – 2 ; Phần ảo 5i.',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'b2apm',
              text: 'Phần thực – 2 ; Phần ảo 5.',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '7p0l2',
              text: 'Phần thực – 2 ; Phần ảo 3.',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '9ommh',
              text: 'Phần thực – 3 ; Phần ảo 5i.',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d16',
    question: {
      blocks: [
        {
          key: '1sf74',
          text:
            'Trong mp tọa độ Oxy, tìm tập hợp điểm biểu diễn các số phức z thỏa mãn: $|z - i| = |(1 + i)z|$.',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'lqen',
              text:
                'Tập hợp các điểm biểu diễn các số phức z là đường tròn tâm $I(2, -1)$, bán kính $R = \\sqrt2$.',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'a2rnl',
              text:
                'Tập hợp các điểm biểu diễn các số phức z là đường tròn tâm $I(0, 1)$, bán kính $R = \\sqrt{3}$.',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '8ho4d',
              text:
                'Tập hợp các điểm biểu diễn các số phức z là đường tròn tâm $I(0, -1)$, bán kính $R = \\sqrt3$.',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'ercfa',
              text:
                'Tập hợp các điểm biểu diễn các số phức z là đường tròn tâm $I(0, -1)$, bán kính $R= \\sqrt2$.',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d18',
    question: {
      blocks: [
        {
          key: 'b329d',
          text:
            "Trong mặt phẳng tọa độ Oxy, gọi M là điểm biểu diễn cho số phức $z = 3 - 4i$; M’ là điểm  biểu diễn cho số phức $z' = \\frac{1+i}{2}z$. Tính diện tích tam giác OMM’.",
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'ar43u',
              text: "m:\\text{S}_{\\Delta \\text{OMM'}}=\\frac{25}{4}:",
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          entityMap: {
            '0': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: {
                url: 'http://tungtung.vn/edit/582fe2fad2456a5ad69e8cd5/quizzes#'
              }
            },
            '1': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: {
                url: 'http://tungtung.vn/edit/582fe2fad2456a5ad69e8cd5/quizzes#'
              }
            }
          },
          blocks: [
            {
              key: '5gq9m',
              text: "m:\\text{S}_{\\Delta \\text{OMM'}}=\\frac{25}{2}:",
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: [
                { offset: 2, length: 6, key: 0 },
                { offset: 19, length: 6, key: 1 }
              ]
            }
          ]
        }
      },
      {
        answer: {
          entityMap: {
            '0': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: {
                url: 'http://tungtung.vn/edit/582fe2fad2456a5ad69e8cd5/quizzes#'
              }
            },
            '1': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: {
                url: 'http://tungtung.vn/edit/582fe2fad2456a5ad69e8cd5/quizzes#'
              }
            }
          },
          blocks: [
            {
              key: '7po49',
              text: "m:\\text{S}_{\\Delta \\text{OMM'}}=\\frac{15}{4}:",
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: [
                { offset: 2, length: 6, key: 0 },
                { offset: 19, length: 6, key: 1 }
              ]
            }
          ]
        }
      },
      {
        answer: {
          entityMap: {
            '0': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: {
                url: 'http://tungtung.vn/edit/582fe2fad2456a5ad69e8cd5/quizzes#'
              }
            },
            '1': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: {
                url: 'http://tungtung.vn/edit/582fe2fad2456a5ad69e8cd5/quizzes#'
              }
            }
          },
          blocks: [
            {
              key: '8u69m',
              text: "m:\\text{S}_{\\Delta \\text{OMM'}}=\\frac{15}{2}:",
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: [
                { offset: 2, length: 6, key: 0 },
                { offset: 19, length: 6, key: 1 }
              ]
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d1a',
    question: {
      blocks: [
        {
          key: '7puqd',
          text:
            'Cho hình chóp tam giác có đường cao bằng 100 cm và các cạnh đáy bằng 20 cm, 21 cm, 29 cm. Thể tích của hình chóp đó bằng ',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '3863p',
              text: '6000 $cm^3$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '829cm',
              text: '6213 $cm^3$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '4d76p',
              text: '7000 $cm^3$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'ftfct',
              text: '$7000\\sqrt2$ $cm^3$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d1c',
    question: {
      blocks: [
        {
          key: '6t7th',
          text:
            'Cho khối chóp đều S.ABC có cạnh đáy bằng a. Tính thể tích khối chóp S.ABC biết cạnh bên bằng 2a.',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '4o0n8',
              text: 'm:_{\\text{S.ABC}}=\\frac{a^3\\sqrt{11}}{12}:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'f7947',
              text: 'm:\\text{V}_{\\text{S.ABC}}=\\frac{a^3\\sqrt{3}}{6}:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'r3a6',
              text: 'm:\\text{V}_{\\text{S.ABC}}=\\frac{a^3}{12}:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'amved',
              text: 'm:\\text{V}_{\\text{S.ABC}}=\\frac{a^3}{4}:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d1e',
    question: {
      blocks: [
        {
          key: '3e5st',
          text:
            'Cho lăng trụ ${ABCD.A_1B_1C_1D_1}$ có đáy ABCD là hình chữ nhật. $AB = a$; $AD = a\\sqrt3$. Hình chiếu vuông góc của điểm $A_1$ trên mặt phẳng (ABCD) trùng với giao điểm AC và BD. Góc giữa hai mặt phẳng $(ADD_1A_1)$ và $(ABCD)$ bằng $60^o$. Tính khoảng cách từ điểm $B_1$ đến mặt phẳng $(A_1BD)$ theo a. ',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      { answer: '$\\frac{a\\sqrt{3}}{2}$' },
      { answer: '$\\frac{a\\sqrt{3}}{3}$' },
      { answer: '$\\frac{a\\sqrt{3}}{4}$' },
      { answer: '$\\frac{a\\sqrt{3}}{6}$' }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d20',
    question: {
      blocks: [
        {
          key: 'c8pmt',
          text:
            'Cho khối chóp $\\text{S.ABCD}$ có $\\text{ABCD}$ là hình vuông cạnh 3a. Tam giác SAB cân tại S và nằm trong mặt phẳng vuông góc với đáy. Tính thể tích khối chóp $\\text{S.ABCD}$ biết góc giữa SC và (ABCD) bằng $60^o$. ',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      { answer: '$\\text{V}_{\\text{S.ABCD}}=18a^3\\sqrt{3}$' },
      { answer: '$\\text{V}_{\\text{S.ABCD}}=\\frac{9a^3\\sqrt{15}}{2}$' },
      { answer: '$\\text{V}_{\\text{S.ABCD}}=9a^3\\sqrt{3}$' },
      { answer: '$\\text{V}_{\\text{S.ABCD}}=18a^3\\sqrt{15}$' }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d22',
    question: {
      blocks: [
        {
          key: 'fgvfu',
          text:
            "Gọi S là diện tích xung quanh của hình nón tròn xoay được sinh ra bởi đoạn thẳng AC' của hình lập phương $\\text{ABCD.A'B'C'D'}$ có cạnh b khi quay xung quang trục AA'. Diện tích S là:  ",
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'fa16p',
              text: 'm:\\pi\\text{b}^2:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '6jf09',
              text: 'm:\\pi\\text{b}^2\\sqrt{2}:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '7kcjm',
              text: 'm:\\pi\\text{b}^2\\sqrt{3}:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'cpvll',
              text: 'm:\\pi\\text{b}^2\\sqrt{6}:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d24',
    question: {
      blocks: [
        {
          key: '7s128',
          text:
            'Cho hình lập phương ABCD.A’B’C’D’ có cạnh bằng a. Một hình nón có đỉnh là tâm của hình vuông ABCD và có đường tròn đáy ngoại tiếp hình vuông A’B’C’D’. Diện tích xung quanh của hình nón đó là:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '6am1o',
              text: 'm:\\frac{\\pi\\text{a}^2\\sqrt{3}}{3}:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'eu073',
              text: 'm:\\frac{\\pi\\text{a}^2\\sqrt{2}}{2}:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'd5q54',
              text: 'm:\\frac{\\pi\\text{a}^2\\sqrt{3}}{2}:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'utdr',
              text: 'm:\\frac{\\pi\\text{a}^2\\sqrt{6}}{2}:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d26',
    question: {
      blocks: [
        {
          key: 'ct8be',
          text:
            'Một hình trụ có 2 đáy là 2 hình tròn nội tiếp hai mặt của một hình lập phương cạnh a. Thể tích của khối trụ đó là:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '45cf0',
              text: 'm:\\frac{1}{2}\\text{a}^3\\pi:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'evvl7',
              text: 'm:\\frac{1}{4}\\text{a}^3\\pi:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'carru',
              text: 'm:\\frac{1}{3}\\text{a}^3\\pi:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '7havg',
              text: 'm:\\text{a}^3\\pi:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d28',
    question: {
      blocks: [
        {
          key: '9plel',
          text:
            'Người ta bỏ 3 quả bóng bàn cùng kích thước vào trong một chiếc hộp hình trụ có đáy bằng hình tròn lớn của quả bóng bàn và chiều cao bằng 3 lần đường kính của quả bóng bàn. Gọi $S_1$ là tổng diện tích của 3 quả bóng bàn, $S_2$ là diện tích xung quanh của hình trụ. Tỉ số $S_1/S_2$ bằng:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '452mk',
              text: '1',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '7n7jj',
              text: '2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '76r0h',
              text: '1,5',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'caord',
              text: '1,2',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d2a',
    question: {
      blocks: [
        {
          key: 'c5jb6',
          text:
            'Cho đường thẳng $\\Delta$ đi qua điểm $M(2;0;-1)$ và có vecto chỉ phương $\\vec{a}(4; -6;2)$ Phương trình tham số của đường thẳng là:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'ebhau',
              text:
                '$\\left\\{\\begin{matrix} x = - 2 + 4t \\\\  y = - 6t \\\\ z = 1 + 2t \\end{matrix}\\right.$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          entityMap: {
            '0': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: { url: 'http://tungtung.vn/create/quizzes#' }
            }
          },
          blocks: [
            {
              key: '2u349',
              text:
                '$\\left\\{\\begin{matrix} x = - 2 + 2t \\\\  y = - 3t \\\\ z = 1 + t \\end{matrix}\\right.$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: [{ offset: 0, length: 82, key: 0 }]
            }
          ]
        }
      },
      {
        answer: {
          entityMap: {
            '0': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: { url: 'http://tungtung.vn/create/quizzes#' }
            }
          },
          blocks: [
            {
              key: '62iag',
              text:
                '$\\left\\{\\begin{matrix} x = 2 + 2t \\\\  y = - 3t \\\\ z = 1 + t \\end{matrix}\\right.$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: [{ offset: 0, length: 80, key: 0 }]
            }
          ]
        }
      },
      {
        answer: {
          entityMap: {
            '0': {
              type: 'LINK',
              mutability: 'MUTABLE',
              data: { url: 'http://tungtung.vn/create/quizzes#' }
            }
          },
          blocks: [
            {
              key: 'b6dg9',
              text:
                '$\\left\\{\\begin{matrix} x = 4 + 2t \\\\  y = - 3t \\\\ z = 2 + t \\end{matrix}\\right.$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: [{ offset: 0, length: 80, key: 0 }]
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d2c',
    question: {
      blocks: [
        {
          key: 'dn5ol',
          text:
            'Mặt cầu (S) có tâm $I(-1;2;1)$ và tiếp xúc với mặt phẳng $(P): x - 2y - 2z - 2 = 0$',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'dqer5',
              text:
                'm:\\left(x+1\\right)^2+\\left(y-2\\right)^2+\\left(z-1\\right)^2=3:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '7p3ft',
              text:
                'm:\\left(x+1\\right)^2+\\left(y-2\\right)^2+\\left(z-1\\right)^2=9:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '4vd39',
              text:
                'm:\\left(x+1\\right)^2+\\left(y-2\\right)^2+\\left(z+1\\right)^2=3:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '5uo1u',
              text:
                'm:\\left(x+1\\right)^2+\\left(y-2\\right)^2+\\left(z+1\\right)^2=9:',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d2e',
    question: {
      blocks: [
        {
          key: '73cv2',
          text:
            'Mặt phẳng chứa 2 điểm $A(1;0;1)$ và $B(-1;2;2)$ và song song với trục 0x có phương trình là:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '4fneo',
              text: '$x + 2z - 3 = 0$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '7rof8',
              text: '$y - 2z + 2 = 0$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '2p6jm',
              text: '$2y - z + 1 = 0$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '6qiln',
              text: '$x + y - z = 0$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d30',
    question: {
      blocks: [
        {
          key: '3cvus',
          text:
            'Trong không gian với hệ toạ độ 0xyz cho $A(2;0;0)$; $B(0;3;1)$; $C(-3;6;4)$. Gọi M là điểm nằm trên cạnh BC sao cho MC = 2MB. Độ dài đoạn AM là:',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      { answer: '$3\\sqrt{3}$' },
      { answer: '$2\\sqrt{7}$' },
      { answer: '$\\sqrt{29}$' },
      { answer: '$\\sqrt{30}$' }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d32',
    question: {
      blocks: [
        {
          key: 'encbh',
          text:
            'Tìm giao điểm của $d:\\frac{x-3}{1} = \\frac{y + 1}{- 1} = \\frac{z}{2}$ và $(P): 2x - y - 7 = 0$',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '24sdt',
              text: 'm:M\\left(3;-1;0\\right):',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'cnerf',
              text: 'm:M\\left(0;2;-4\\right):',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'fr15f',
              text: 'm:M\\left(6;-4;3\\right):',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'e44ga',
              text: 'm:M\\left(1;4;-2\\right):',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d34',
    question: {
      blocks: [
        {
          key: '82j0r',
          text:
            'Khoảng cách giữa 2 mặt phẳng $(P) 2x + 2y - z - 11= 0$ và $(Q) 2x + 2y - z + 4=0$ là',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'b8bmn',
              text: '3',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'b2p7v',
              text: '5',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '9jlnl',
              text: '7',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '81pi7',
              text: '9',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d36',
    question: {
      blocks: [
        {
          key: 'aj3kh',
          text:
            'Trong không gian Oxyz cho $A(0; 1; 0)$, $B(2; 2; 2)$, $C(-2; 3; 1)$ và đuờng thẳng $d: \\frac{x - 1}{2} = \\frac{y - 2}{-1} = \\frac{z - 3}{2}$. Tìm điểm M thuộc d để thể tích tứ diện MABC bằng 3.',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: 'jtes',
              text:
                '$\\text{M}\\left(-\\frac{3}{2};-\\frac{3}{4};\\frac{1}{2}\\right)$;   $\\text{M}\\left(-\\frac{15}{2};\\frac{9}{4};-\\frac{11}{2}\\right)$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '4f66m',
              text:
                '$\\text{M}\\left(-\\frac{3}{2};-\\frac{3}{4};\\frac{1}{2}\\right)$;  $\\text{M}\\left(-\\frac{15}{2};\\frac{9}{4};\\frac{11}{2}\\right)$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '608sk',
              text:
                '$\\text{M}\\left(\\frac{3}{2};-\\frac{3}{4};\\frac{1}{2}\\right)$;  $\\text{M}\\left(\\frac{15}{2};\\frac{9}{4};\\frac{11}{2}\\right)$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'ftkrj',
              text:
                '$\\text{M}\\left(\\frac{3}{5};-\\frac{3}{4};\\frac{1}{2}\\right)$;  $\\text{M}\\left(\\frac{15}{2};\\frac{9}{2};\\frac{11}{2}\\right)$',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  },
  {
    _id: '582fe2fbd2456a5ad69e8d38',
    question: {
      blocks: [
        {
          key: '3ij2',
          text: 'Trong không gian Oxyz cho đường thẳng d và mặt cầu (S):',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: 'dd13t',
          text:
            '$(d): \\left\\{\\begin{matrix} 2x - 2y + 1 = 0 \\\\ x + 2y - 2z - 4 = 0 \\end{matrix}\\right.$;    $(S): x^2 + y^2 + z^2 + 4x - 6y + m = 0$',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        },
        {
          key: '2h9r',
          text: 'Tìm m để d cắt (S) tại hai điểm M, N sao cho MN = 8',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [],
          entityRanges: []
        }
      ]
    },
    answers: [
      {
        answer: {
          blocks: [
            {
              key: '4lvfa',
              text: 'm = 12',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'du3bo',
              text: 'm = 10',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: '61m65',
              text: 'm = - 12',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      },
      {
        answer: {
          blocks: [
            {
              key: 'bvnbc',
              text: 'm = - 10',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [],
              entityRanges: []
            }
          ]
        }
      }
    ]
  }
]
