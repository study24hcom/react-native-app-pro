import math from './math'
import code from './code'

export { math, code }
export default { math, code }
