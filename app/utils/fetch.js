/* eslint-disable */
import axios from 'axios'
import {getAuthToken, setAuthToken} from './auth'

// setAuthToken(
//   'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfZG9jIjp7ImlkIjoiNTdkODFhNTI1OWYxOTkxOGY3Y2NjYWY3IiwiX2lkIjoiNTdkODFhNTI1OWYxOTkxOGY3Y2NjYWY3In0sImlkIjoiNTdkODFhNTI1OWYxOTkxOGY3Y2NjYWY3IiwiX2lkIjoiNTdkODFhNTI1OWYxOTkxOGY3Y2NjYWY3IiwiaWF0IjoxNTEwODI0ODUwLCJleHAiOjE1MTM0MTY4NTB9.uL3PaGXMssUNWv2zhXoV7-UKYMDwKw7jbMFJAj1QOvo'
// )

// setAuthToken('')

const getHeaders = async () => {
  var headers = {
    Accept: 'application/json'
  }
  const authToken = await getAuthToken(false)
  if (authToken) {
    headers = {
      ...headers,
      Authorization: 'JWT ' + authToken
    }
  }
  return headers
}

function wrapHeaderToken(fetchUtil) {
  return (url, data, customAttributes = {}) => {
    return new Promise(async (resolve, reject) => {
      const attributes = {
        cache: true,
        headers: await getHeaders()
      }
      fetchUtil(url, data, {
        ...attributes,
        ...customAttributes
      })
        .then(res => {
          resolve(res)
        })
        .catch(e => {
          reject(e)
        })
    })
  }
}

export function postUtil(url, data, attributes) {
  return new Promise((resolve, reject) => {
    axios
      .post(url, data, attributes)
      .then(res => {
        if (res.status === 200) {
          resolve(res.data)
        } else {
          reject({error: true})
        }
      })
      .catch(e => reject(e))
  })
}

export function putUtil(url, data, attributes) {
  return new Promise((resolve, reject) => {
    axios
      .put(url, data, attributes)
      .then(res => {
        if (res.status === 200) {
          resolve(res.data)
        } else {
          reject({error: true})
        }
      })
      .catch(e => reject(e))
  })
}

export function getUtil(url, data, attributes) {
  return new Promise((resolve, reject) => {
    axios
      .get(url, attributes)
      .then(res => {
        if (res.status === 200) {
          resolve(res.data)
        } else {
          reject({error: true})
        }
      })
      .catch(e => {
        reject(e)
      })
  })
}

export function deleteUtil(url, data, attributes) {
  return new Promise((resolve, reject) => {
    axios
      .delete(url, attributes)
      .then(res => {
        if (res.status === 200) {
          resolve(res.data)
        } else {
          reject({error: true})
        }
      })
      .catch(e => reject(e))
  })
}

export const getFetch = wrapHeaderToken(getUtil)
export const postFetch = wrapHeaderToken(postUtil)
export const putFetch = wrapHeaderToken(putUtil)
export const deleteFetch = wrapHeaderToken(deleteUtil)

export {getHeaders}
