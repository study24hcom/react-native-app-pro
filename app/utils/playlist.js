const GIFT = {
  yellow: {
    background: '#FFFFF7',
    color: '#EC9900',
    image: require('../images/playlist-background/gift.png')
  },
  pink: {
    background: '#FFF9FA',
    color: '#FF5F5F',
    image: require('../images/playlist-background/gift.png')
  },
  blue: {
    background: '#F8FCFF',
    color: '#007EE5',
    image: require('../images/playlist-background/gift.png')
  }
}

const GEOMETRY = {
  yellow: {
    background: '#FFFFF7',
    color: '#EC9900',
    image: require('../images/playlist-background/geometry.png')
  },
  pink: {
    background: '#FFF9FA',
    color: '#FF5F5F',
    image: require('../images/playlist-background/geometry.png')
  },
  blue: {
    background: '#F8FCFF',
    color: '#007EE5',
    image: require('../images/playlist-background/geometry.png')
  }
}

const TRIAGLE = {
  yellow: {
    background: '#65809B',
    color: '#ffffff',
    image: require('../images/playlist-background/triangle.png')
  },
  pink: {
    background: '#FFB5B2',
    color: '#ffffff',
    image: require('../images/playlist-background/triangle.png')
  },
  blue: {
    background: '#F8FCFF',
    color: '#007EE5',
    image: require('../images/playlist-background/triangle.png')
  },
  green: {
    background: '#86CC38',
    color: '#ffffff',
    image: require('../images/playlist-background/triangle.png')
  }
}
const SHAPE_BACKGROUND = [
  GIFT.yellow,
  TRIAGLE.yellow,
  GIFT.blue,
  GEOMETRY.yellow,
  GIFT.pink,
  TRIAGLE.pink,
  GEOMETRY.pink,
  TRIAGLE.blue,
  GEOMETRY.green,
  GEOMETRY.blue
]

export function getBackgroundType (text, charCodeAt = 1) {
  var number = 0
  if (text) {
    number = text.charCodeAt(charCodeAt)
  } else number = 2
  if (number > 10) {
    number = number % 10
  }
  if (number > 100) {
    number = number % 100
  }
  return SHAPE_BACKGROUND[number]
    ? SHAPE_BACKGROUND[number]
    : SHAPE_BACKGROUND[2]
}
