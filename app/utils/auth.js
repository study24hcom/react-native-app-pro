import { AsyncStorage } from 'react-native'
const AUTH_TOKEN = 'authToken'

export async function getAuthToken (hasJWTString = false) {
  const authToken = await AsyncStorage.getItem(AUTH_TOKEN)
  return hasJWTString ? `JWT ${authToken}` : authToken
}

export async function setAuthToken (token) {
  await AsyncStorage.setItem(AUTH_TOKEN, token)
  return token
}

export function resetAuthToken () {
  AsyncStorage.removeItem(AUTH_TOKEN)
}

export default {
  setAuthToken,
  resetAuthToken,
  getAuthToken
}
