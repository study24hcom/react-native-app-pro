import {Dimensions} from 'react-native'

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window')

function wp (percentage) {
  const value = (percentage * viewportWidth) / 100
  return Math.round(value)
}

const slideWidth = wp(80)
export const slideHeight = viewportHeight * 0.4
export const itemHorizontalMargin = wp(2)
export const sliderWidth = viewportWidth
export const itemWidth = slideWidth + itemHorizontalMargin * 2
