export function validateUsername (username = '') {
  var matches = username.match(/^[A-z][A-z0-9\_]*/g)
  if (username.length < 5) {
    return 'Tên đăng nhập ít nhất 5 ký tự'
  } else if (username.length > 15) {
    return 'Tên đăng nhập không được lớn hơn 15 ký tự'
  } else if (matches && matches.length > 0) {
    if (matches[0] !== username) return 'Tên đăng nhập không chứa khoảng trống, các ký tự đặc biệt, không chứa dấu tiếng việt'
  } else if (!matches) {
    return 'Tên đăng nhập không chứa khoảng trống, các ký tự đặc biệt, không chứa dấu tiếng việt'
  }
  return true
}
