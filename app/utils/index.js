import validateJs from 'validate.js'
import {Platform} from 'react-native'
import { SHAPE, TEXT } from 'constants/color'

export function isIos () {
  return Platform.OS === 'ios'
}

export function isAndroid () {
  return Platform.OS === 'android'
}

export function getDeepObject (object, defaultValue, ...keysDeep) {
  let cloneObject = object
  let value = defaultValue

  for (let i = 0; i < keysDeep.length; i++) {
    const keyDeep = keysDeep[i]
    if (i > 0) {
      if (cloneObject[keyDeep]) {
        value = cloneObject[keyDeep]
        cloneObject = cloneObject[keyDeep]
      } else {
        value = defaultValue
        break
      }
    } else {
      if (object && object[keyDeep]) {
        value = object[keyDeep]
        cloneObject = object[keyDeep]
      } else {
        break
      }
    }
  }
  return value
}

export function randomNumber (maxNumber) {
  return Math.floor(Math.random() * maxNumber)
}

export function getIsNumberPx (number, unit = 'px') {
  if (isNaN(number)) return number
  else return number + unit
}

// Only get zero
export function validateObject (object, constraints) {
  const errors = validateJs(object, constraints)
  if (typeof errors === 'undefined') return {}
  Object.keys(errors).map(key => {
    errors[key] = errors[key][0]
  })
  return errors
}

export function objectToArray (objectData) {
  return Object.keys(objectData).map(key => ({
    ...objectData[key]
  }))
}

export function getColorFromString (color) {
  switch (color) {
    case 'red':
      return SHAPE.RED
    case 'yellow':
      return SHAPE.YELLOW
    case 'green':
      return SHAPE.GREEN
    case 'primary':
      return SHAPE.PRIMARY
    case 'primaryBold':
      return SHAPE.PRIMARYBOLD
    case 'orange':
      return SHAPE.ORANGE
    case 'purple':
      return SHAPE.PURPLE
    case 'pink':
      return SHAPE.PINK
    case 'black':
      return SHAPE.BLACK
    case 'graylight':
      return SHAPE.GRAYLIGHT
    case 'graymedium':
      return SHAPE.GRAYMEDIUM
    case 'graybold':
      return SHAPE.GRAYBOLD
    case 'graytext':
      return SHAPE.GRAYTEXT
    case 'gray':
      return TEXT.GRAY
    case 'white':
      return '#ffffff'
    default:
      return SHAPE.PRIMARYBOLD
  }
}

export function shuffleArray (array) {
  let currentIndex = array.length
  let temporaryValue
  let randomIndex

  // While there remain elements to shuffle...
  while (currentIndex !== 0) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1

    // And swap it with the current element.
    temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  }

  return array
}

export function convertNumberToAlpha (number) {
  return String.fromCharCode(97 + number).toUpperCase()
}
