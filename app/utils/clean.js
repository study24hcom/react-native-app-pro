import {API_RES_AUTH, API_QUIZ_LATEX} from 'config'
import activityType from 'constants/activityType'
import uuid from 'uuid'
import Notification from 'constants/notificationType'

export function cleanUser(user) {
  if (typeof user !== 'object') return {}
  if (user === null) return null
  return {
    ...user,
    id: user._id,
    fullname: user.fullname,
    description: user.biography ? user.biography : null,
    birthday: user.birthday,
    totalFollower: user.totalFollowedUser ? user.totalFollowedUser : 0,
    totalQuizlist: user.totalQuizLists ? user.totalQuizLists : 0,
    totalPlaylist: user.totalPlayLists ? user.totalPlayLists : 0,
    avatar: user.avatar_url
      ? user.avatar_url
      : user.avatar ? `${API_RES_AUTH}/uploads/avatars/${user.avatar}` : '',
    balance: user.balance ? user.balance : 0
  }
}

export function cleanUsers(users) {
  return users.map(user => cleanUser(user))
}

export function cleanQuizAnswers(answers) {
  return answers.map(answer => ({
    id: answer.id,
    _id: answer.id,
    answer: answer.answer
  }))
}

export function cleanQuizzes(quizzes, autoId = false) {
  return quizzes.map(quiz => {
    let quizId = autoId ? uuid.v4() : quiz._id
    return {
      id: quizId,
      _id: quizId,
      isSuggestAnswer: !!quiz.has_suggest_answer,
      suggestAnswer: quiz.suggest_answer ? quiz.suggest_answer : '',
      chooseAnswerIndex: null,
      correctAnswerIndex: quiz.correct_answer !== null &&
      quiz.correct_answer !== 'undefined'
        ? quiz.correct_answer
        : '',
      question: quiz.question,
      answers: cleanQuizAnswers(quiz.answers)
    }
  })
}

export function cleanQuizListCustomField(customField = {}) {
  return {
    ...customField,
    isCustomTime: !!customField.custom_time,
    openTime: customField.start_at,
    endTime: customField.end_at,
    isRandom: customField.isRandom
  }
}

export function cleanQuizList(quizList) {
  if (typeof quizList === 'string') return {}
  if (typeof quizList === 'null') return {}
  if (typeof quizList === 'undefined') return {}
  if (!quizList) return {}
  return {
    _id: quizList.id,
    id: quizList._id,
    name: quizList.title,
    description: quizList.description,
    slug: quizList.slug,
    timeLength: quizList.time,
    pdfFile: quizList.pdf_file,
    customField: cleanQuizListCustomField(quizList.custom_field),
    imageLatex: API_QUIZ_LATEX +
    '/quiz-list/' +
    quizList.slug +
    '/' +
    quizList.slug +
    '.png',
    totalAccess: quizList.access_count,
    totalQuestion: quizList.total_questions,
    totalRatings: quizList.total_ratings ? quizList.total_ratings : 0,
    usersPlayed: quizList.users_played ? quizList.users_played : [],
    ratingAvg: quizList.rating_avg ? quizList.rating_avg : 0,
    user: typeof quizList.user_id === 'object'
      ? cleanUser(quizList.user_id)
      : {},
    categoryId: typeof quizList.category_id === 'object'
      ? quizList.category_id._id
      : quizList.category_id,
    tags: quizList.tags ? cleanTags(quizList.tags) : [],
    createdAt: quizList.created_at,
    updatedAt: quizList.updated_at
  }
}

export function cleanQuizLists(quizLists) {
  if (!quizLists) return []
  return quizLists.map(quizList => cleanQuizList(quizList))
}

export function cleanPagination(pagination) {
  return {
    itemPerPage: pagination.item_per_page ? pagination.item_per_page : pagination.itemPerPage,
    page: pagination.page,
    totalItem: pagination.total_item ? pagination.total_item : pagination.totalItem
  }
}

export function cleanRating(rating) {
  return {
    id: rating.id,
    _id: rating._id,
    content: rating.content,
    user: cleanUser(rating.user_id),
    rating: rating.rating,
    comment: rating.comment
      ? {
        user: cleanUser(rating.comment.user_id),
        content: rating.comment.content,
        isComment: !!rating.comment.content
      }
      : {
        isComment: false
      }
  }
}

export function cleanRatings(ratings) {
  return ratings.map(rating => cleanRating(rating))
}

export function cleanHistoryToScore(history) {
  return {
    id: history._id,
    _id: history._id,
    index: typeof history.index !== 'undefined' ? history.index + 1 : ' ',
    score: history.score,
    histories: history.histories,
    totalQuestion: history.questions.length,
    user: cleanUser(history.user_id)
  }
}

export function cleanPlaylist(playlist) {
  return {
    _id: playlist._id,
    id: playlist._id,
    info: {
      id: playlist._id,
      _id: playlist._id,
      name: playlist.title,
      slug: playlist.slug,
      description: playlist.description,
      createdAt: playlist.created_at,
      updatedAt: playlist.updated_at,
      categoryId: playlist.category_id,
      image: playlist.featuredImage,
      user: cleanUser(playlist.user_id),
      isDisplayInProfile: playlist.displayInMyProfile,
      quizListsLength: playlist.quiz_lists ? playlist.quiz_lists.length : 0
    },
    quizLists: cleanQuizLists(playlist.quiz_lists)
  }
}

export function cleanPlaylists(playlists) {
  return playlists.map(playlist => cleanPlaylist(playlist))
}

export function cleanActivity(activity) {
  let content = ''
  switch (activity.type) {
    case activityType.RATE_QUIZ_LIST:
      content = 'vừa đánh giá đề thi'
      break
    case activityType.DO_QUIZ_LIST:
      content = 'vừa làm đề thi'
      break
    default:
  }
  return {
    _id: activity._id,
    id: activity._id,
    lastUser: cleanUser(activity.last_user_id),
    otherUsers: cleanUsers(activity.user_ids),
    quizList: cleanQuizList(activity.quiz_list_id),
    content: content,
    lastDate: activity.last_updated_date
  }
}

export function cleanActivities(activities) {
  return activities.map(activity => cleanActivity(activity))
}

export function cleanTag(tag) {
  return {
    id: tag._id,
    name: tag.tag_name,
    slug: tag.slug,
    createdAt: tag.created_at,
    updatedAt: tag.updated_at
  }
}

export function cleanTagGroup(tag) {
  return {
    id: tag._id,
    name: tag.tag_name,
    slug: tag.slug,
    groupBy: tag.group_by ? tag.group_by : '',
    createdAt: tag.created_at,
    updatedAt: tag.updated_at,
    quizListsLength: tag.quiz_lists.length,
    totalUserFolowed: tag.followed_users.length,
    groupBy: tag.group_by,
    isFeatured: tag.is_featured
  }
}

export function cleanNotification(notification) {
  const data = notification.data ? notification.data : {}
  let user = {}
  switch (notification.action) {
    case Notification.action.RATE_QUIZ_LIST:
      user = {
        username: data.user,
        id: data.user_id
      }
      break
    default:
      user = {
        id: data.last_access_user_id,
        username: data.last_access_user_name
      }
  }
  return {
    _id: notification._id,
    id: notification._id,
    type: notification.type,
    action: notification.action,
    status: notification.status,
    quizList: {
      name: data.quiz_list_title,
      slug: data.quiz_list_slug,
      tags: data.tag ? data.tag : []
    },
    user,
    data,
    createdAt: notification.created_at
  }
}

export function cleanNotifications(notifications) {
  return notifications.map(notification => {
    return cleanNotification(notification)
  })
}

export function cleanTags(tags) {
  return tags.map(tag => cleanTag(tag))
}

export function cleanTagsGroup(tags) {
  return tags.map(tag => cleanTagGroup(tag))
}

export function cleanGallerySlider(image) {
  return {
    name: image.name,
    id: image.name,
    url: JSON.parse(image.url),
    image: image.image,
    description: image.description
  }
}

export function cleanGallerySliders(images) {
  return images.map(image => cleanGallerySlider(image))
}

export function cleanInfoPlaylist(playlist) {
  return {
    id: playlist._id,
    _id: playlist._id,
    name: playlist.title,
    slug: playlist.slug,
    createdAt: playlist.created_at,
    updatedAt: playlist.updated_at,
    description: playlist.description,
    categoryId: playlist.category_id,
    quizListsLength: playlist.quiz_lists.length,
    user: cleanUser(playlist.user_id)
  }
}

export function cleanKeyWords(keywords) {
  if (keywords === null) return []
  return keywords.map(keyword => cleanKeyWord(keyword))
}

export function cleanKeyWord(keyword) {
  return {
    name: keyword
  }
}

export function cleanHistories(histories) {
  return histories.map(history => cleanHistory(history))
}

export function cleanHistory(history) {
  return {
    name: history
  }
}
