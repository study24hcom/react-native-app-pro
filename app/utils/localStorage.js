import {AsyncStorage} from 'react-native'

const HISTORY_SEARCH = 'historySearch'

export async function loadState () {
  try {
    const historySearch = await AsyncStorage.getItem(HISTORY_SEARCH)

    if (historySearch === null) {
      return undefined
    }

    return JSON.parse(historySearch)
  } catch (err) {
    return undefined
  }
}

export function saveState (state) {
  AsyncStorage.getItem(HISTORY_SEARCH)
    .then((history) => {
      const c = history ? [state, ...JSON.parse(history).filter(h => h !== state)] : [state]
      AsyncStorage.setItem(HISTORY_SEARCH, JSON.stringify(c))
    })
}

export function deleteState () {
  AsyncStorage.removeItem(HISTORY_SEARCH)
}
