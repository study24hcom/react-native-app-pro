import update from 'react-addons-update'

export function cleanProps (cleanKeyProps, props) {
  let newProps = { ...props }
  cleanKeyProps.map(key => {
    delete newProps[key]
    return {}
  })
  return newProps
}

export function getPropsKeyForm (
  context,
  key,
  callback = () => {},
  onChangeEventName = 'onChange'
) {
  return {
    [onChangeEventName]: e => {
      context.setState(
        update(context.state, {
          form: {
            [key]: {
              $set: e.target ? e.target.value : e
            }
          }
        }),
        () => {
          if (typeof callback === 'function') callback(e)
        }
      )
    },
    value: context.state.form[key]
  }
}
