var fs = require('fs')

var lineReader = require('readline').createInterface({
  input: require('fs').createReadStream('./android/settings.gradle')
})

lineReader.on('line', function (line) {
  var rePattern = new RegExp(/node_modules\/(.*)\/android/)

  var arrMatches = line.match(rePattern)
  if (arrMatches !== null) {
    var file = 'node_modules/' + arrMatches[1] + '/android/build.gradle'
    fs.readFile(file, 'utf8', function (err, data) {
      if (err) {
        return console.log(err)
      }
      var version = process.argv.slice(2)
      var result = data.replace(/buildToolsVersion "(.*)"/g, 'buildToolsVersion "' + version + '"')

      fs.writeFile(file, result, 'utf8', function (err) {
        if (err) return console.log(err)
      })
    })
  }
})
