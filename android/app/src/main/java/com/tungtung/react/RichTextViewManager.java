package com.tungtung.react;

import android.support.annotation.Nullable;
import android.util.Log;

import com.daquexian.flexiblerichtextview.FlexibleRichTextView;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

/**
 * This file is created by Tien Nguyen on 12/17/17.
 */

public class RichTextViewManager extends SimpleViewManager<FlexibleRichTextView> {
    public static final String REACT_CLASS = "RCTFlexibleRichTextView";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    public FlexibleRichTextView createViewInstance(ThemedReactContext context) {
        return new FlexibleRichTextView(context);
    }

    @ReactProp(name = "text")
    public void setText(FlexibleRichTextView view, @Nullable String text) {
        Log.d(REACT_CLASS, "setAnswer " + text);
        view.setText(text);
    }

}
