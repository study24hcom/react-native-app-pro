package com.tungtung.react;

import android.content.Intent;
import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.tungtung.player.PlayerActivity;
import com.tungtung.player.utils.Common;

/**
 * This file is created by Tien Nguyen on 2/1/18.
 */

public class ActivityStarterModule extends ReactContextBaseJavaModule {
    ActivityStarterModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "ActivityStarter";
    }

    @ReactMethod
    void navigateToPlayer(String slug, String token, boolean viewResult) {
        ReactApplicationContext context = getReactApplicationContext();
        Log.d(getName(), "TOKEN " + token);
        Common.saveToken(context, token);
        Intent intent = new Intent(context, PlayerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(PlayerActivity.SLUG_EXTRA, slug);
        intent.putExtra(PlayerActivity.VIEW_RESULT_MODE, viewResult);
        context.startActivity(intent);
    }

    @ReactMethod
    void cleanToken() {
        ReactApplicationContext context = getReactApplicationContext();
        Common.saveToken(context, "");
    }
}
