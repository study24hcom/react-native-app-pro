package com.tungtung;

import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.BV.LinearGradient.LinearGradientPackage;
import com.airbnb.android.react.lottie.LottiePackage;
import com.crashlytics.android.Crashlytics;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.react.ReactApplication;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.facebook.soloader.SoLoader;
import com.github.wumke.RNImmediatePhoneCall.RNImmediatePhoneCallPackage;
import com.imagepicker.ImagePickerPackage;
import com.lugg.ReactNativeConfig.ReactNativeConfigPackage;
import com.microsoft.codepush.react.CodePush;
import com.oblador.vectoricons.VectorIconsPackage;
import com.smixx.fabric.FabricPackage;
import com.tungtung.player.PlayerApplication;
import com.tungtung.player.utils.FontsOverride;
import com.tungtung.react.AppReactPackage;

import org.devio.rn.splashscreen.SplashScreenReactPackage;
import org.scilab.forge.jlatexmath.core.AjLatexMath;

import java.util.Arrays;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import io.invertase.firebase.RNFirebasePackage;

public class MainApplication extends MultiDexApplication implements ReactApplication {
    private static CallbackManager mCallbackManager = CallbackManager.Factory.create();

    public static CallbackManager getCallbackManager() {
        return mCallbackManager;
    }

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

        @Override
        protected String getJSBundleFile() {
            return CodePush.getJSBundleFile();
        }

        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
            new SplashScreenReactPackage(),
            new RNImmediatePhoneCallPackage(),
            new LottiePackage(),
                    new RNFirebasePackage(),
                    new LinearGradientPackage(),
                    new ReactNativeConfigPackage(),
                    new FBSDKPackage(mCallbackManager),
                    new ImagePickerPackage(),
                    new CodePush(getResources().getString(R.string.reactNativeCodePush_androidDeploymentKey), getApplicationContext(), BuildConfig.DEBUG),
                    new FabricPackage(),
                    new VectorIconsPackage(),
                    new AppReactPackage()
            );
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        Fabric.with(this, new Crashlytics());
        SoLoader.init(this, /* native exopackage */ false);
        AjLatexMath.init(this); // init library: load fonts, create paint, etc.
        FacebookSdk.sdkInitialize(getApplicationContext());
        PlayerApplication.init(this);
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/Bookerly-Regular.ttf");
    }
}
