package com.tungtung;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.facebook.react.ReactActivity;

import org.devio.rn.splashscreen.SplashScreen;

public class MainActivity extends ReactActivity {

    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SplashScreen.show(this, true, R.style.SplashScreenTheme);  // here
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        handler = new Handler();
        handler.postDelayed(hideTask, 1000);
    }

    Runnable hideTask = new Runnable() {
        @Override
        public void run() {
            SplashScreen.hide(MainActivity.this);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handler != null) {
            handler.removeCallbacks(hideTask);
        }
    }

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "TungTung";
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MainApplication.getCallbackManager().onActivityResult(requestCode, resultCode, data);
    }
}
