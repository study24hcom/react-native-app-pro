package com.tungtung.player.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.tungtung.player.R;
import com.tungtung.player.dialogs.LoadingDialog;
import com.tungtung.player.network.ICountRound;
import com.tungtung.player.network.NetworkUtils;
import com.tungtung.player.utils.IConstants;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * This file is created by Tien Nguyen on 2/9/18.
 */

public class RatingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);

        View rlRightText = findViewById(R.id.rlRightText);
        View rlLeftText = findViewById(R.id.rlLeftText);


        rlRightText.setOnClickListener(view -> submitRating());
        rlLeftText.setOnClickListener(view -> {
            setResult(Activity.RESULT_CANCELED);
            finish();
        });
    }

    private void submitRating() {
        EditText editText = findViewById(R.id.etFeedback);
        MaterialRatingBar ratingBar = findViewById(R.id.mrbRating);
        String slug = getIntent().getStringExtra(IConstants.SLUG_EXTRA);
        String feedback = editText.getText().toString().trim();
        int rating = (int) ratingBar.getRating();

        if (rating == 0 || rating > 5 || TextUtils.isEmpty(feedback)) {
            Toast.makeText(RatingActivity.this, getString(R.string.need_information), Toast.LENGTH_SHORT).show();
            return;
        }

        LoadingDialog dialog = new LoadingDialog(this, "spirit_geek.json", R.string.thank_you);
        dialog.show();

        NetworkUtils.rating(slug, feedback, rating, new ICountRound() {

            @Override
            public void onSuccess(String responseStr) {
                dialog.dismiss();
                safeFinish();
            }

            @Override
            public void onFailed(String message) {
                dialog.dismiss();
                Toast.makeText(RatingActivity.this, message, Toast.LENGTH_SHORT).show();
                safeFinish();
            }
        });
    }

    private void safeFinish() {
        (new Handler()).postDelayed(() -> {
            setResult(Activity.RESULT_OK);
            finish();
        }, 500);
    }
}
