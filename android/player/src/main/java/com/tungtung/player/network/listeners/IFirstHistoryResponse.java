package com.tungtung.player.network.listeners;

import com.tungtung.player.models.History;

/**
 * This file is created by Tien Nguyen on 2/7/18.
 */

public interface IFirstHistoryResponse {
    void onSuccess(History correctAnswers);
    void onFailed(String message);
}
