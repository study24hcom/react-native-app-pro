package com.tungtung.player.models;


import com.tungtung.player.models.core.Model;

/**
 * This file is created by Tien Nguyen on 4/6/17.
 */
public class History extends Model {
    private String _id;
    private boolean first_history;
    private HistoryItem[] histories;
    private String[] questions;
    private int score;
    private int index;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public boolean isFirst_history() {
        return first_history;
    }

    public void setFirst_history(boolean first_history) {
        this.first_history = first_history;
    }

    public HistoryItem[] getHistories() {
        return histories;
    }

    public void setHistories(HistoryItem[] histories) {
        this.histories = histories;
    }

    public String[] getQuestions() {
        return questions;
    }

    public void setQuestions(String[] questions) {
        this.questions = questions;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
