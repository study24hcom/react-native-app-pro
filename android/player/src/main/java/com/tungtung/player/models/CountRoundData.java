package com.tungtung.player.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * This file is created by Tien Nguyen on 3/4/18.
 */

public class CountRoundData implements Serializable {
    @SerializedName("quiz_list_id")
    String quiz_list_id;
    @SerializedName("id")
    String id;

    public String getId() {
        return id;
    }
}
