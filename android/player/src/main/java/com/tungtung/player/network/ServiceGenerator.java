package com.tungtung.player.network;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.GsonBuilder;
import com.readystatesoftware.chuck.ChuckInterceptor;
import com.tungtung.player.PlayerApplication;
import com.tungtung.player.network.retrofit.AuthenticationInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * This file is created by Tien Nguyen on 7/17/2017.
 */

public class ServiceGenerator {
    private static final String BASE_URL = "https://quiz-api.tungtung.vn";
    private static final String BASE_AUTH_URL = "https://auth-api.tungtung.vn";

    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = createRetrofit(BASE_URL);
        return retrofit.create(serviceClass);
    }

    public static <S> S createAuthService(Class<S> serviceClass) {
        Retrofit retrofit = createRetrofit(BASE_AUTH_URL);
        return retrofit.create(serviceClass);
    }

    @NonNull
    private static Retrofit createRetrofit(String endpoint) {
        if (!TextUtils.isEmpty(endpoint) && !endpoint.endsWith("/")) {
            endpoint += "/";
        }

        // Add Request time out
        OkHttpClient.Builder okBuilder = new okhttp3.OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS);
        okBuilder.addInterceptor(new ChuckInterceptor(PlayerApplication.getInstance().getApplicationContext()));
        okBuilder.addInterceptor(new AuthenticationInterceptor());
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.client(okBuilder.build());
        builder.baseUrl(endpoint);
        builder.addCallAdapterFactory(RxJavaCallAdapterFactory.create());
        builder.addConverterFactory(ScalarsConverterFactory.create());
        builder.addConverterFactory(GsonConverterFactory.create(new GsonBuilder().serializeNulls().create()));


        return builder.build();
    }

}
