package com.tungtung.player.utils;

/**
 * This file is created by Tien Nguyen on 1/29/18.
 */

public class IConstants {
    public static final long LOADING_BAR_INCREASE_DELAY = 500;
    public static final String TIME_EXTRA = "TIME_EXTRA";
    public static final long SELECT_DELAY = 600;
    public static final String SLUG_EXTRA = "SLUG_EXTRA";
    public static final int RATING_REQUEST_CODE = 10001;
    public static final int CLOCK_ACTIVITY_REQUEST_CODE = 10002;
    public static final String END_AT = "END_AT";
}
