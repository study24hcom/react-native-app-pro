package com.tungtung.player.models;


import com.tungtung.player.models.core.Model;

/**
 * This file is created by Tien Nguyen on 4/10/17.
 */
public class HistoryItem extends Model {
    private String id;
    private String choose_answer;
    private String correct_answer;

    public String getCorrect_check() {
        return correct_check;
    }

    private String correct_check;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getChoose_answer() {
        try {
            return Integer.valueOf(choose_answer);
        } catch (Exception e) {
            return -1;
        }
    }

    public void setChoose_answer(String choose_answer) {
        this.choose_answer = choose_answer;
    }

    public int getCorrect_answer() {
        try {
            return Integer.valueOf(correct_answer);
        } catch (Exception e) {
            return -1;
        }
    }

    public void setCorrect_answer(String correct_answer) {
        this.correct_answer = correct_answer;
    }

    public String isCorrect_check() {
        return correct_check;
    }

    public void setCorrect_check(String correct_check) {
        this.correct_check = correct_check;
    }
}
