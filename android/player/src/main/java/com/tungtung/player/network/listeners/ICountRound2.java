package com.tungtung.player.network.listeners;

import com.tungtung.player.models.CountRoundData;

/**
 * This file is created by Tien Nguyen on 3/4/18.
 */

public interface ICountRound2 {
    void onSuccess(CountRoundData responseStr);

    void onFailed(String message);
}
