package com.tungtung.player.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daquexian.flexiblerichtextview.FlexibleRichTextView;
import com.tungtung.player.R;
import com.tungtung.player.listeners.ICheckedChangedListener;
import com.tungtung.player.models.Quiz;
import com.tungtung.player.utils.FontsOverride;


/**
 * This file is created by Tien Nguyen on 4/9/17.
 */
public class MyRadioButton extends TableRow implements View.OnClickListener {
    private static final String TAG = MyRadioButton.class.getName();
    private FlexibleRichTextView textViewMain;
    private TextView radioButton;

    ICheckedChangedListener listener;
    private LinearLayout imageInAnswer;

    public MyRadioButton(Context context) {
        super(context);
        initView();
    }

    private void initView() {
        View view = inflate(getContext(), R.layout.custom_radio_button, null);
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        addView(view, layoutParams);
        textViewMain = findViewById(R.id.richTextViewContent);
        radioButton = findViewById(R.id.radioButtonMain);
        imageInAnswer = findViewById(R.id.imageInAnswer);
        radioButton.setTypeface(FontsOverride.getBookerlyTypeface(getContext()));
        radioButton.setOnClickListener(this);
    }

    public MyRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }


    public void setAnswer(Quiz.Answer answer, String letter) {
        this.textViewMain.setText(formatAnswer(answer.getAnswer()));
        this.radioButton.setText(letter);

        for (String url : answer.getImages()) {
            ImageView imageView = new ImageView(getContext());
//            imageView.setPadding(0, 0, 0, 10);
            Glide.with(getContext()).load(url).into(imageView);
            imageInAnswer.addView(imageView);
        }
        imageInAnswer.setVisibility(answer.getImages().size() == 0 ? GONE : VISIBLE);
    }

    String formatAnswer(String answer) {
        return String.format("%s", answer != null ? answer : "");
    }

    public void setOnCheckedChangeListener(ICheckedChangedListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            dispatchCheckedChange();
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP &&
                (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER || event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
            dispatchCheckedChange();
        }
        return super.dispatchKeyEvent(event);
    }

    public void setChecked(MyRadioButtonState checked) {
        switch (checked) {
            case NORMAL:
                radioButton.setBackgroundResource(android.R.color.transparent);
                radioButton.setTextColor(getResources().getColor(R.color.text_color_main));
                break;
            case WRONG:
                radioButton.setBackgroundResource(R.drawable.circle_wrong);
                radioButton.setTextColor(getResources().getColor(R.color.text_color_white));
                break;
            case CHECKED:
                radioButton.setBackgroundResource(R.drawable.circle);
                radioButton.setTextColor(getResources().getColor(R.color.text_color_white));
                break;
            case CORRECT:
                radioButton.setBackgroundResource(R.drawable.circle_correct);
                radioButton.setTextColor(getResources().getColor(R.color.text_color_white));
                break;
            case NOT_CHOSEN:
                radioButton.setBackgroundResource(R.drawable.circle_not_choosen);
                radioButton.setTextColor(getResources().getColor(R.color.text_color_main));
                break;
            default:
                setBackgroundResource(android.R.color.transparent);
                radioButton.setTextColor(getResources().getColor(R.color.text_color_main));
        }
    }

    @Override
    public void onClick(View view) {
        dispatchCheckedChange();
    }

    private void dispatchCheckedChange() {
        Log.d(TAG, "isEnabled" + isEnabled());
        if (listener != null && isEnabled()) {
            listener.onCheckedChanged(this, true);
        }
    }
}
