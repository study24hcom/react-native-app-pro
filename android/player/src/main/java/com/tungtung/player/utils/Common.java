package com.tungtung.player.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;

import com.tungtung.player.R;
import com.tungtung.player.dialogs.TestReportDialog;
import com.tungtung.player.models.CustomField;
import com.tungtung.player.models.Quiz;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;


/**
 * This file is created by Tien Nguyen on 1/29/18.
 */

public class Common {

    public static LinearLayoutManager decorateRecycleView(RecyclerView recyclerView, Context context, int direction, int layoutDirection) {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context, layoutDirection, false) {
            @Override
            public boolean canScrollVertically() {
                return super.canScrollVertically();
            }
        };
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(context, direction);
        dividerItemDecoration.setDrawable(context.getResources().getDrawable(R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        return layoutManager;
    }

    public static String toTimeString(long millis) {
        return String.format(Locale.ENGLISH, "%02d : %02d : %02d",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }

    public static String getLetter(int index, boolean withDot) {
        return Character.toString((char) (65 + index)) + (withDot ? "." : "");
    }

    public static Quiz[] shuffleArray(Quiz[] ar) {
        // If running on Java 6 or older, use `new Random()` on RHS here
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            Quiz a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
        return ar;
    }

    public static Spanned formatScore(Context context, TestReportDialog.LabelType labelType, int score, int totalQuestion) {
        final String scoreString = String.format("<font color=\"%s\">%s</font><font color=\"#389BFF\">/%s</font>",
                getColorString(context, labelType), String.valueOf(score), String.valueOf(totalQuestion));
        Spanned htmlString;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            htmlString = Html.fromHtml(scoreString, Html.FROM_HTML_MODE_COMPACT);
        } else {
            htmlString = Html.fromHtml(scoreString);
        }
        return htmlString;
    }

    public static TestReportDialog.LabelType getLabelByPercent(float percent) {
        if (percent >= 0 && percent <= 30) {
            return TestReportDialog.LabelType.BAD;
        } else if (percent > 30 && percent <= 60) {
            return TestReportDialog.LabelType.MEDIUM;
        } else if (percent > 60 && percent < 80) {
            return TestReportDialog.LabelType.GOOD;
        } else {
            return TestReportDialog.LabelType.EXCELLENCE;
        }
    }

    public static int getColor(Context context, TestReportDialog.LabelType labelType) {
        final Resources resources = context.getResources();
        switch (labelType) {
            case BAD:
                return resources.getColor(R.color.score_very_bad);
            case MEDIUM:
                return resources.getColor(R.color.score_medium);
            case GOOD:
                return resources.getColor(R.color.score_good);
            case EXCELLENCE:
                return resources.getColor(R.color.score_excellence);
            default:
                return resources.getColor(R.color.score_very_bad);
        }
    }


    private static String getColorString(Context context, TestReportDialog.LabelType labelType) {
        final Resources resources = context.getResources();
        switch (labelType) {
            case BAD:
                return resources.getString(R.string.score_very_bad);
            case MEDIUM:
                return resources.getString(R.string.score_medium);
            case GOOD:
                return resources.getString(R.string.score_good);
            case EXCELLENCE:
                return resources.getString(R.string.score_excellence);
            default:
                return resources.getString(R.string.score_very_bad);
        }
    }

    private final static String TOKEN_KEY = "TOKEN_KEY";

    public static void saveToken(Context context, String token) {
        SharedPreferences sharedPref = context.getSharedPreferences(TOKEN_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(TOKEN_KEY, token);
        editor.apply();
    }

    public static String getToken(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(TOKEN_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(TOKEN_KEY, "");
    }

    public static Quiz[] shuffleArrayAdvance(Quiz[] quizzes, CustomField.GroupInfo[] groupsRange) {
        ArrayList<Quiz> quizzesRes = new ArrayList<>();
        for (CustomField.GroupInfo group : groupsRange) {
            if (group.getStart() != -1 && group.getStart() != -1) {
                if (group.isRandom()) {
                    Quiz startGroup = quizzes[group.getStart() - 1];
                    startGroup.replaceGroupInfo(group.getStart(), group.getEnd());
                    quizzesRes.add(startGroup);
                    quizzesRes.addAll(Arrays.asList(Common.shuffleArray(Arrays.copyOfRange(quizzes, group.getStart(), group.getEnd()))));
                } else {
                    quizzes[group.getStart() - 1].replaceGroupInfo(group.getStart(), group.getEnd());
                    quizzesRes.addAll(Arrays.asList(Arrays.copyOfRange(quizzes, group.getStart() - 1, group.getEnd())));
                }
            }
        }
        Quiz[] array = new Quiz[quizzes.length];
        return quizzesRes.toArray(array);
    }
}
