package com.tungtung.player.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.tungtung.player.utils.IConstants;


/**
 * This file is created by Tien Nguyen on 7/30/16.
 */
public class TimerService extends Service {

    private static final String TAG = TimerService.class.getSimpleName();
    public static final String ACTION_BROADCAST_TIMER = "Timer";
    public static final String DATA_TIMER = "DATA_TIMER";
    private Context context;
    private long time;
    private long endTime;

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        context = this;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return super.onStartCommand(null, flags, startId);
        }
        Log.i(TAG, "Current Time = " + time);
        time = intent.getLongExtra(IConstants.TIME_EXTRA, 0);

        endTime = System.currentTimeMillis() + time;

        Log.i(TAG, "Time = " + time);
        if (time > 0) {
            handler.removeCallbacks(stopRunnable);
            handler.postDelayed(stopRunnable, time);
            handler.removeCallbacks(reportStatus);
            handler.postDelayed(reportStatus, 1000);
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private Runnable reportStatus = new Runnable() {
        @Override
        public void run() {
            long now = System.currentTimeMillis();

            if (now < endTime) {
                Intent mIntent = new Intent(getPackageName() + ACTION_BROADCAST_TIMER);
                long remainTime = (endTime - now);
                Log.d(TAG, "Remain time = " + String.valueOf(remainTime));
                mIntent.putExtra(DATA_TIMER, remainTime);
                sendBroadcast(mIntent);
                handler.postDelayed(reportStatus, 1000);
            } else {
                Intent mIntent = new Intent(getPackageName() + ACTION_BROADCAST_TIMER);
                mIntent.putExtra(DATA_TIMER, 0);
                sendBroadcast(mIntent);
            }
        }
    };

    private Runnable stopRunnable = new Runnable() {
        @Override
        public void run() {
            time = 0;
            Intent mIntent = new Intent(getPackageName() + ACTION_BROADCAST_TIMER);
            mIntent.putExtra(DATA_TIMER, time);
            sendBroadcast(mIntent);
            handler.removeCallbacks(reportStatus);
            Log.i(TAG, "Stop service ...");
        }
    };


    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(stopRunnable);
        handler.removeCallbacks(reportStatus);
        Log.i(TAG, "Service Stopped ...");
    }

    Handler handler = new Handler();
}
