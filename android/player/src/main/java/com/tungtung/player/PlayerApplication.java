package com.tungtung.player;

import android.content.Context;

/**
 * This file is created by Tien Nguyen on 1/25/18.
 */

public class PlayerApplication {

    private static Context instance;

    public static void init(Context context) {
        instance = context;
    }

    public static Context getInstance() {
        return instance;
    }
}
