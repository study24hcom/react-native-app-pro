package com.tungtung.player.network.listeners;


import com.tungtung.player.models.QuizList;

/**
 * This file is created by Tien Nguyen on 1/26/18.
 */

public interface IOnlyDescriptionResponse {
    void onSuccess(QuizList quizList);
    void onFailed(String message);
}
