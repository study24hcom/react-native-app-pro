package com.tungtung.player.network.listeners;


import com.tungtung.player.models.Quiz;

/**
 * This file is created by Tien Nguyen on 1/26/18.
 */

public interface IOnlyViewResponse {
    void onSuccess(Quiz[] quizzes);

    void onFailed(String message);
}
