package com.tungtung.player.models;

/**
 * This file is created by Tien Nguyen on 1/27/18.
 */

public class MessageEvent {
    public String id;
    public int answer;

    public MessageEvent(String id, int answer) {
        this.id = id;
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "MessageEvent{" +
                "id=" + id +
                ", answer=" + answer +
                '}';
    }
}
