package com.tungtung.player.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * This file is created by Tien Nguyen on 3/4/18.
 */

public class BaseRespond<T> implements Serializable {
    @SerializedName("success")
    boolean success;
    @SerializedName("data")
    T data;

    public T getData() {
        return data;
    }
}
