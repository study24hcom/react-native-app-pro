package com.tungtung.player.network.retrofit;

import android.util.Log;

import com.tungtung.player.PlayerApplication;
import com.tungtung.player.utils.Common;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * This file is created by Tien Nguyen on 7/21/2017.
 */

public class AuthenticationInterceptor implements Interceptor {

    private static final String TAG = AuthenticationInterceptor.class.getSimpleName();

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
        Log.d(TAG, "TOKEN = " + Common.getToken(PlayerApplication.getInstance()));
        builder.addHeader("Authorization", "JWT " + Common.getToken(PlayerApplication.getInstance()));
//        if (BuildConfig.DEBUG) {
//            builder.addHeader("Authorization", "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfZG9jIjp7ImlkIjoiNTdkNmI1Y2Y1OWYxOTkxOGY3Y2NjYWYyIiwiX2lkIjoiNTdkNmI1Y2Y1OWYxOTkxOGY3Y2NjYWYyIn0sImlkIjoiNTdkNmI1Y2Y1OWYxOTkxOGY3Y2NjYWYyIiwiX2lkIjoiNTdkNmI1Y2Y1OWYxOTkxOGY3Y2NjYWYyIiwiaWF0IjoxNTE2OTQ3NDE1LCJleHAiOjE1MTk1Mzk0MTV9.R3R8QJ4DCfrXhj4_6JBUeuHmZd4oYNNkxjvNUwbuUmE");
//        } else {
//            builder.addHeader("Authorization", "JWT " + Common.getToken(PlayerApplication.getInstance()));
//        }
        return chain.proceed(builder.build());
    }
}
