package com.tungtung.player.network.services;

import com.google.gson.JsonObject;
import com.tungtung.player.models.BaseRespond;
import com.tungtung.player.models.CountRoundData;
import com.tungtung.player.models.History;
import com.tungtung.player.models.LoginRespond;
import com.tungtung.player.models.QuizList;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Ngo Van Vang on 7/17/2017.
 */

public interface BaseService {

    @GET("quiz-lists/{slug}/only-description")
    Observable<QuizList> getOnlyDescription(@Path("slug") String slug);

    @GET("quiz-lists/{slug}/only-view")
    Observable<String> getOnlyView(@Path("slug") String slug);

    @GET("quiz-lists/{slug}/correct-answers")
    Observable<String> getCorrectAnswer(@Path("slug") String slug);

    @GET("quiz-lists/{slug}/count-round")
    Observable<BaseRespond<CountRoundData>> countRound(@Path("slug") String slug);

    @GET("histories/{slug}/first-history")
    Observable<History> firstHistory(@Path("slug") String slug);

    @POST("histories")
    Observable<String> submit(@Body JsonObject params);

    @POST("ratings")
    Observable<String> rating(@Body JsonObject jsonObject);

    @POST("auth/login")
    Observable<LoginRespond> login(@Body JsonObject params);
}
