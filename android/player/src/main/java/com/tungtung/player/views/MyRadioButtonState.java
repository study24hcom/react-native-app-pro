package com.tungtung.player.views;

/**
 * This file is created by Tien Nguyen on 4/10/17.
 */
public enum MyRadioButtonState {
    NORMAL,
    CHECKED,
    WRONG,
    CORRECT,
    NOT_CHOSEN
}
