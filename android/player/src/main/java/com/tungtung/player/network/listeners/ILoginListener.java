package com.tungtung.player.network.listeners;

import com.tungtung.player.models.LoginRespond;

/**
 * This file is created by Tien Nguyen on 4/27/18.
 */

public interface ILoginListener {
    void onSuccess(LoginRespond correctAnswers);

    void onFailed(String message);
}
