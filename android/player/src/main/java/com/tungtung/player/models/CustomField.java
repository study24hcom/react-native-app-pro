package com.tungtung.player.models;

import com.tungtung.player.models.core.Model;

import java.util.Date;

/**
 * This file is created by Tien Nguyen on 1/26/18.
 * <p>
 * <p>
 * import java.util.Date;
 * <p>
 * import tungtung.com.quiz.models.core.Model;
 * import tungtung.com.quiz.utils.Common;
 * <p>
 * <p>
 * /**
 * This file is created by Tien Nguyen on 5/10/17.
 */

public class CustomField extends Model {
    private static final String TAG = CustomField.class.getSimpleName();
    private boolean custom_time;
    private String start_at;
    private Date end_at;

    public GroupInfo[] getGroupsRange() {
        return groupsRange;
    }

    private GroupInfo[] groupsRange;

    public boolean isRandom() {
        return isRandom;
    }

    private boolean isRandom;

    public boolean isCustom_time() {
        return custom_time;
    }

    public void setCustom_time(boolean custom_time) {
        this.custom_time = custom_time;
    }

    public Date getStart_at() {
        return new Date();
    }

    public void setStart_at(String start_at) {
        this.start_at = start_at;
    }

    public Date getEnd_at() {
        return end_at;
    }

    public boolean canAccess(Date currentTime) {
        return !custom_time || currentTime.after(new Date());
    }

    public boolean canViewResult(Date currentTime) {
        return !custom_time || currentTime.after(new Date());
    }


    public long getDelay(Date currentTime) {
        return getStart_at().getTime() - currentTime.getTime();
    }

    public long getViewResultDelay(Date currentTime) {
        return custom_time ? getEnd_at().getTime() - currentTime.getTime() : 0;
    }

    public class GroupInfo {
        public String getId() {
            return id;
        }

        public int getStart() {
            try {
                return Integer.parseInt(start);
            } catch (NumberFormatException e) {
                return -1;
            }
        }

        public int getEnd() {
            try {
                return Integer.parseInt(end);
            } catch (NumberFormatException e) {
                return -1;
            }
        }

        public boolean isRandom() {
            return isRandom;
        }

        private String id;
        private String start;
        private String end;
        private boolean isRandom;
    }
}
