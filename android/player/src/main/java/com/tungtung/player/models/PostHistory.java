package com.tungtung.player.models;

import android.util.Log;
import android.util.SparseArray;

import com.tungtung.player.models.core.Model;

import java.util.HashMap;


/**
 * This file is created by Tien Nguyen on 4/11/17.
 */
public class PostHistory extends Model {
    private static final String TAG = PostHistory.class.getSimpleName();
    private HistoryItem[] histories;
    private int score;
    private String[] questions;
    private String quiz_list_id;
    private String access_log_id;

    private PostHistory() {
    }

    public HistoryItem[] getHistories() {
        return histories;
    }

    public void setHistories(HistoryItem[] histories) {
        this.histories = histories;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String[] getQuestions() {
        return questions;
    }

    public void setQuestions(String[] questions) {
        this.questions = questions;
    }

    public String getQuiz_list_id() {
        return quiz_list_id;
    }

    public void setQuiz_list_id(String quiz_list_id) {
        this.quiz_list_id = quiz_list_id;
    }

    private void setQuizList(SparseArray<String> array) {
        questions = new String[array.size()];
        for (int i = 0; i < array.size(); i++) {
            questions[i] = array.get(i);
        }
    }

    private void setHistories(SparseArray<String> array, HashMap<String, Integer> selectedAnswer,
                              HashMap<String, CorrectAnswer> correctAnswers) {
        histories = new HistoryItem[array.size()];
        for (int i = 0; i < array.size(); i++) {
            String key = array.get(i);
            HistoryItem item = new HistoryItem();
            item.setId(key);
            CorrectAnswer answer = correctAnswers.get(key);
            int selected = selectedAnswer.get(key);
            item.setChoose_answer(String.valueOf(selected));
            if (answer != null) {
                item.setCorrect_answer(String.valueOf(answer.getCorrect_answer()));
                String correctCheck = selected == -1 ? "empty" : answer.getCorrect_answer() == selected ? "true" : "false";
                item.setCorrect_check(correctCheck);
                Log.e(TAG, "key = " + key + " selected = " + selected + " answer = " + answer.getCorrect_answer() + " correctCheck = " + correctCheck);
            }
            histories[i] = item;

        }
    }

    public static PostHistory buildPostHistory(String slug, SparseArray<String> array, HashMap<String, Integer> selectedAnswer,
                                               HashMap<String, CorrectAnswer> correctAnswers, int score, String access_log_id) {
        PostHistory postHistory = new PostHistory();
        postHistory.setQuiz_list_id(slug);
        postHistory.setQuizList(array);
        postHistory.setScore(score);
        postHistory.setHistories(array, selectedAnswer, correctAnswers);
        postHistory.setAccess_log_id(access_log_id);

        return postHistory;
    }

    public void setAccess_log_id(String access_log_id) {
        this.access_log_id = access_log_id;
    }
}
