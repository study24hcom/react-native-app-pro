package com.tungtung.player.network;


import android.util.Log;

import com.google.gson.JsonParser;
import com.tungtung.player.models.BaseRespond;
import com.tungtung.player.models.CorrectAnswer;
import com.tungtung.player.models.CountRoundData;
import com.tungtung.player.models.History;
import com.tungtung.player.models.LoginRespond;
import com.tungtung.player.models.PostHistory;
import com.tungtung.player.models.Quiz;
import com.tungtung.player.models.QuizList;
import com.tungtung.player.models.core.Model;
import com.tungtung.player.models.json.JsonArray;
import com.tungtung.player.models.json.JsonObject;
import com.tungtung.player.network.listeners.ICorrectAnswerResponse;
import com.tungtung.player.network.listeners.ICountRound2;
import com.tungtung.player.network.listeners.IFirstHistoryResponse;
import com.tungtung.player.network.listeners.ILoginListener;
import com.tungtung.player.network.listeners.IOnlyDescriptionResponse;
import com.tungtung.player.network.listeners.IOnlyViewResponse;
import com.tungtung.player.network.services.BaseService;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * This file is created by Tien Nguyen on 1/26/18.
 */

public class NetworkUtils {
    private static final String TAG = NetworkUtils.class.getSimpleName();

    public static void getQuizListDescription(String slug, final IOnlyDescriptionResponse listener) {
        CompositeSubscription subscriptions = new CompositeSubscription();
        BaseService baseService = ServiceGenerator.createService(BaseService.class);
        Observable<QuizList> observable = baseService.getOnlyDescription(slug);
        Subscription subscription = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listener::onSuccess, throwable -> {
                    listener.onFailed(throwable.getMessage());
                });
        subscriptions.add(subscription);
    }

    public static void getQuizListOnlyView(String slug, final IOnlyViewResponse listener) {
        CompositeSubscription subscriptions = new CompositeSubscription();
        BaseService baseService = ServiceGenerator.createService(BaseService.class);
        Observable<String> observable = baseService.getOnlyView(slug);
        Subscription subscription = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((String responseStr) -> {
                    JsonArray json = new JsonArray(responseStr);
                    Quiz[] quizzes = new Quiz[json.size()];
                    for (int i = 0; i < json.size(); i++) {
                        Quiz quiz = Quiz.parse(json.get(i));
                        quizzes[i] = quiz;
                    }
                    listener.onSuccess(quizzes);
                }, throwable -> {
                    listener.onFailed(throwable.getMessage());
                });
        subscriptions.add(subscription);
    }

    public static void getQuizListCorrectAnswer(String slug, final ICorrectAnswerResponse listener) {
        CompositeSubscription subscriptions = new CompositeSubscription();
        BaseService baseService = ServiceGenerator.createService(BaseService.class);
        Observable<String> observable = baseService.getCorrectAnswer(slug);
        Subscription subscription = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((responseStr) -> {
                    Log.d(TAG, responseStr);
                    JsonArray json = new JsonArray(responseStr);
                    CorrectAnswer[] correctAnswers = new CorrectAnswer[json.size()];
                    for (int i = 0; i < json.size(); i++) {
                        final JsonObject jsonObj = json.get(i);
                        if (jsonObj != null) {
                            CorrectAnswer quiz = (CorrectAnswer) jsonObj.getObject(CorrectAnswer.class);
                            quiz.setSuggestAnswer(Model.getText(jsonObj, "suggest_answer"));
                            quiz.setImages(Model.getImage(jsonObj, "suggest_answer"));
                            correctAnswers[i] = quiz;
                        }
                    }
                    listener.onSuccess(correctAnswers);
                }, throwable -> {
                    listener.onFailed(throwable.getMessage());
                });
        subscriptions.add(subscription);
    }

    public static void countRound(String slug, final ICountRound2 listener) {
        CompositeSubscription subscriptions = new CompositeSubscription();
        BaseService baseService = ServiceGenerator.createService(BaseService.class);
        Observable<BaseRespond<CountRoundData>> observable = baseService.countRound(slug);
        Subscription subscription = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((data) -> {
                    listener.onSuccess(data.getData());
                }, throwable -> {
                    listener.onFailed(throwable.getMessage());
                });
        subscriptions.add(subscription);
    }

    public static void submit(PostHistory history, final ICountRound listener) {
        CompositeSubscription subscriptions = new CompositeSubscription();
        BaseService baseService = ServiceGenerator.createService(BaseService.class);
        Observable<String> observable = baseService.submit(new JsonParser().parse(history.toString()).getAsJsonObject());
        Subscription subscription = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((responseStr) -> listener.onSuccess("success"),
                        throwable -> listener.onFailed(throwable.getMessage()));
        subscriptions.add(subscription);
    }

    public static void getFirstHistory(String slug, final IFirstHistoryResponse listener) {
        CompositeSubscription subscriptions = new CompositeSubscription();
        BaseService baseService = ServiceGenerator.createService(BaseService.class);
        Observable<History> observable = baseService.firstHistory(slug);
        Subscription subscription = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listener::onSuccess, throwable -> {
                    listener.onFailed(throwable.getMessage());
                });
        subscriptions.add(subscription);
    }

    public static void rating(String slug, String feedback, int rating, ICountRound listener) {
        CompositeSubscription subscriptions = new CompositeSubscription();
        BaseService baseService = ServiceGenerator.createService(BaseService.class);
        com.google.gson.JsonObject jsonObject = new com.google.gson.JsonObject();
        jsonObject.addProperty("content", feedback);
        jsonObject.addProperty("rating", rating);
        jsonObject.addProperty("quiz_list_id", slug);
        Observable<String> observable = baseService.rating(jsonObject);
        Subscription subscription = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((responseStr) -> {
                            JsonObject json = new JsonObject(responseStr);
                            Boolean success = json.getBoolean("success", true);
                            if (success) {
                                listener.onSuccess("success");
                            } else {
                                listener.onFailed("Bạn đã đánh giá!");
                            }
                        },
                        throwable -> listener.onFailed(throwable.getMessage()));
        subscriptions.add(subscription);
    }


    public static void login(String email, String password, ILoginListener listener) {
        CompositeSubscription subscriptions = new CompositeSubscription();
        BaseService baseService = ServiceGenerator.createAuthService(BaseService.class);
        com.google.gson.JsonObject params = new com.google.gson.JsonObject();
        params.addProperty("email", email);
        params.addProperty("password", password);
        Observable<LoginRespond> observable = baseService.login(params);
        Subscription subscription = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listener::onSuccess,
                        throwable -> listener.onFailed(throwable.getMessage()));
        subscriptions.add(subscription);
    }
}
