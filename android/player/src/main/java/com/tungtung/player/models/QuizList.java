package com.tungtung.player.models;

import android.util.Log;

import com.tungtung.player.models.core.Model;

import java.util.Date;

/**
 * This file is created by Tien Nguyen on 1/26/18.
 */


public class QuizList extends Model {
    private static final String PURCHASE_AFTER_PLAY = "QUIZLIST/purchase-after-play";
    private static final String FREE = "QUIZLIST/free";
    private static final String PURCHASE_BEFORE_PLAY = "QUIZLIST/free";
    private static final String STATUS_NEW = "new";
    private static final String STATUS_OLD = "old";
    private static final String TAG = QuizList.class.getSimpleName();
    private String title;
    private String description;
    private String _id;
    private String slug;
    private String price;
    private String sale_price;
    private String type;
    private boolean purchased;
    //    private Tag[] tags;
    private int time;
    private int access_count;
    private int total_questions;
    private String status_type;
    private CustomField custom_field;
    private Date currentTime;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isPurchased() {
        return purchased;
    }

    public void setPurchased(boolean purchased) {
        this.purchased = purchased;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getAccess_count() {
        return access_count;
    }

    public int getTotal_questions() {
        return total_questions;
    }

    public void setAccess_count(int access_count) {
        this.access_count = access_count;
    }

    public void setTotal_questions(int total_questions) {
        this.total_questions = total_questions;
    }

    public boolean askForPurchase() {
        Log.d(TAG, "type = " + type + "; purchased = " + purchased);
        return !type.equals(FREE) && type.equals(PURCHASE_BEFORE_PLAY) && !purchased;
    }

    public String getStatus_type() {
        return status_type;
    }

    public void setStatus_type(String status_type) {
        this.status_type = status_type;
    }

    public boolean isNew() {
        Log.d(TAG, "status_type = " + status_type);
        return status_type.equals(STATUS_NEW);
    }

    public CustomField getCustom_field() {
        return custom_field;
    }

    public void setCustom_field(CustomField custom_field) {
        this.custom_field = custom_field;
    }

    public boolean canAccess(Date currentTime) {
        return custom_field == null || custom_field.canAccess(currentTime);
    }

    public boolean canViewResult(Date currentTime) {
        return custom_field == null || custom_field.canViewResult(currentTime);
    }

    public long getDelay(Date currentTime) {
        return custom_field == null ? 0 : custom_field.getDelay(currentTime);
    }

    public long getViewResultDelay(Date time) {
        return custom_field == null ? 0 : custom_field.getViewResultDelay(time);
    }

    public Date getCurrentTime() {
        return currentTime;
    }

    public long newCanViewResult() {
        return custom_field == null ? 0 : custom_field.getViewResultDelay(this.currentTime);
    }
}

