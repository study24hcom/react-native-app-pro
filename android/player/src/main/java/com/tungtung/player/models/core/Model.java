package com.tungtung.player.models.core;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.tungtung.player.models.json.JsonArray;
import com.tungtung.player.models.json.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * This file is created by Tien Nguyen on 12/14/15.
 */
public class Model implements Serializable {
    private static final String TAG = Model.class.getSimpleName();

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }

    @NonNull
    public static String getText(JsonObject anObj, String key) {
        String answer = anObj.getString(key, "");
        if (answer.equals("")) {
            StringBuilder buffer = new StringBuilder();
            JsonObject question = anObj.get(key);
            JsonArray blocks = question.getJsonArray("blocks");
            for (int i = 0; i < blocks.size(); i++) {
                JsonObject block = blocks.get(i);
                if (block != null) {
                    String text = getStyles(block.getString("text", ""), block);
                    buffer.append(text).append("\n");
                }
            }
            Log.d(TAG, buffer.toString());
            answer = buffer.toString();
        }
        answer = answer.replaceAll("~", " ");
        return answer.trim();
    }

    private static String getStyles(String text, JsonObject block) {
        if (block == null) {
            return "";
        }
        // Get styles block
        JsonArray array = block.getJsonArray("inlineStyleRanges");
        Style[] styles = new Style[array.size()];
        List<String> strings = new ArrayList<>();
        for (int i = 0; i < array.size(); i++) {
            final JsonObject obj = array.get(i);
            if (obj != null) {
                styles[i] = (Style) obj.getObject(Style.class);
                strings.add(text.substring(styles[i].offset, styles[i].offset + styles[i].length));
            }
        }

        for (int i = 0; i < styles.length; i++) {
            if (styles[i] != null) {
                final String formattedText = formatText(strings.get(i), styles[i].style);
                Log.d(TAG, "text " + strings.get(i) + "; formattedText= " + formattedText + " style = " + styles[i].style);
                text = text.replace(strings.get(i), formattedText);
            }
        }
        return text;
    }

    private static String formatText(String s, String style) {
        if (TextUtils.isEmpty(style)) {
            return s;
        }
        StringBuilder res = new StringBuilder();
        if (style.equals("BOLD")) {
            res.append("[b]").append(s).append("[/b]");
        }
        if (style.equals("UNDERLINE")) {
            res.append("[u]").append(s).append("[/u]");
        }
        if (style.equals("ITALIC")) {
            res.append("[i]").append(s).append("[/i]");
        }
        return res.toString();
    }

    @NonNull
    public static List<String> getImage(JsonObject anObj, String from) {
        //"question.entityMap.0.data.src"
        List<String> imageUrls = new ArrayList<>();
        for (int i = 0; i < anObj.get(from)
                .get("entityMap").size(); i++) {
            imageUrls.add(anObj.get(from)
                    .get("entityMap")
                    .get(String.valueOf(i))
                    .get("data")
                    .getString("imageUrl", ""));
        }
        return imageUrls;
    }

    private static class Style extends Model {
        String style;
        int length;
        int offset;
    }
}
