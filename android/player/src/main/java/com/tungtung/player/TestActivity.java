package com.tungtung.player;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.tungtung.player.models.LoginRespond;
import com.tungtung.player.network.NetworkUtils;
import com.tungtung.player.network.listeners.ILoginListener;
import com.tungtung.player.utils.Common;

public class TestActivity extends AppCompatActivity {

    EditText etSlug;

    EditText etEmail;

    EditText etPassword;

    Button btStartTest;

    Button btStartView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etSlug = findViewById(R.id.etSlug);
        btStartTest = findViewById(R.id.btStartTest);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btStartView = findViewById(R.id.btStartView);
        final String slug = "De-thi-thu-mon-Tieng-Anh-lan-1-RAojm";

        btStartTest.setOnClickListener(view -> {

            NetworkUtils.login("toantien.vn91@gmail.com", "happy2code", new ILoginListener() {
                @Override
                public void onSuccess(LoginRespond correctAnswers) {
                    Common.saveToken(TestActivity.this, correctAnswers.getToken());
                    Intent intent = new Intent(TestActivity.this, PlayerActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(PlayerActivity.SLUG_EXTRA, slug);
                    intent.putExtra(PlayerActivity.VIEW_RESULT_MODE, false);
                    startActivity(intent);
                }

                @Override
                public void onFailed(String message) {

                }
            });
        });

        btStartView.setOnClickListener(view -> {
            NetworkUtils.login("toantien.vn91@gmail.com", "happy2code", new ILoginListener() {
                @Override
                public void onSuccess(LoginRespond correctAnswers) {
                    Common.saveToken(TestActivity.this, correctAnswers.getToken());
                    Intent intent = new Intent(TestActivity.this, PlayerActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(PlayerActivity.SLUG_EXTRA, slug);
                    intent.putExtra(PlayerActivity.VIEW_RESULT_MODE, true);
                    startActivity(intent);
                }

                @Override
                public void onFailed(String message) {

                }
            });
        });
    }
}
