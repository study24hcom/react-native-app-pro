package com.tungtung.player.models.player;

import android.util.Log;
import android.util.SparseArray;

import com.tungtung.player.models.CorrectAnswer;
import com.tungtung.player.models.HistoryItem;
import com.tungtung.player.models.PostHistory;
import com.tungtung.player.models.Quiz;
import com.tungtung.player.models.QuizList;
import com.tungtung.player.utils.Common;

import java.util.HashMap;
import java.util.Locale;


/**
 * This file is created by Tien Nguyen on 2/1/18.
 */

public class QuizKeeper {
    private static final String TAG = QuizKeeper.class.getSimpleName();
    private final String slug;
    private SparseArray<String> keyKeeper = new SparseArray<>();
    private Quiz[] quizzes = new Quiz[0];
    private HashMap<String, CorrectAnswer> correctAnswerHashMap = new HashMap<>();
    private HashMap<String, Integer> selectedAnswer = new HashMap<>();

    public QuizKeeper(String slug) {
        this.slug = slug;
    }

    public void setQuizzes(Quiz[] quizzes, QuizList isRandom) {
        this.quizzes = isRandom.getCustom_field().getGroupsRange() != null ? Common.shuffleArrayAdvance(quizzes, isRandom.getCustom_field().getGroupsRange())
                : isRandom.getCustom_field().isRandom() ? Common.shuffleArray(quizzes) : quizzes;
        int i = 0;
        for (Quiz quiz : this.quizzes) {
            selectedAnswer.put(quiz.get_id(), -1);
            keyKeeper.put(i, quiz.get_id());
            i++;
        }
    }

    public void setCorrectAnswer(CorrectAnswer[] correctAnswers) {
        for (CorrectAnswer item : correctAnswers) {
            correctAnswerHashMap.put(item.get_id(), item);
        }
    }

    public void updateSelectedAnswer(String[] questions, HistoryItem[] historyItems) {
        int i = 0;
        for (String questionId : questions) {
            selectedAnswer.put(questionId, historyItems[i].getChoose_answer());
            Log.e(TAG, "key = " + questionId + " selected = " + historyItems[i].getChoose_answer()
                    + " answer = " + historyItems[i].getCorrect_answer() + " correct_check = " + historyItems[i].getCorrect_check());
            i++;
        }
    }

    public int countScore() {
        int score = 0;
        for (int i = 0; i < keyKeeper.size(); i++) {
            String key = keyKeeper.get(i);
            score += (correctAnswerHashMap.get(key).getCorrect_answer() == selectedAnswer.get(key)) ? 1 : 0;
        }
        return score;
    }

    public String getScoreString() {
        return String.format(Locale.getDefault(), "%d/%d", countScore(), quizzes.length);
    }

    public Quiz getQuizAt(int position) {
        return quizzes[position];
    }

    public CorrectAnswer getCorrectAnswerAt(int position) {
        String key = keyKeeper.get(position);
        return correctAnswerHashMap.get(key);
    }

    public Integer getSelectedAnswerAt(int position) {
        String key = keyKeeper.get(position);
        return selectedAnswer.get(key);
    }

    public int totalQuestion() {
        return quizzes.length;
    }

    public PostHistory getPostHistory(String accessLogId) {
        return PostHistory.buildPostHistory(slug, keyKeeper,
                selectedAnswer, correctAnswerHashMap, countScore(), accessLogId);
    }

    public void updateSelectedAnswer(String quizId, Integer answer) {
        selectedAnswer.put(quizId, answer);
    }
}
