package com.tungtung.player;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.tungtung.player.activities.ClockActivity;
import com.tungtung.player.activities.RatingActivity;
import com.tungtung.player.adapters.QuestionStateRecycleAdapter;
import com.tungtung.player.dialogs.ConfirmationDialog;
import com.tungtung.player.dialogs.LoadingDialog;
import com.tungtung.player.dialogs.TestReportDialog;
import com.tungtung.player.models.CorrectAnswer;
import com.tungtung.player.models.CountRoundData;
import com.tungtung.player.models.History;
import com.tungtung.player.models.MessageEvent;
import com.tungtung.player.models.Quiz;
import com.tungtung.player.models.QuizList;
import com.tungtung.player.models.player.QuizKeeper;
import com.tungtung.player.network.ICountRound;
import com.tungtung.player.network.NetworkUtils;
import com.tungtung.player.network.listeners.ICorrectAnswerResponse;
import com.tungtung.player.network.listeners.ICountRound2;
import com.tungtung.player.network.listeners.IFirstHistoryResponse;
import com.tungtung.player.network.listeners.IOnlyDescriptionResponse;
import com.tungtung.player.network.listeners.IOnlyViewResponse;
import com.tungtung.player.services.TimerService;
import com.tungtung.player.transformers.ZoomOutPage;
import com.tungtung.player.utils.Common;
import com.tungtung.player.utils.IConstants;
import com.tungtung.player.views.ViewPagerCustomDuration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PlayerActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private static final String TAG = PlayerActivity.class.getSimpleName();
    public static final String SLUG_EXTRA = "SLUG_EXTRA";
    public static final String VIEW_RESULT_MODE = "VIEW_RESULT_MODE";
    private boolean viewResultMode = false;

    RecyclerView rvQuestionList;
    protected HashMap<String, Integer> selectedAnswer = new HashMap<>();

    private QuestionStateRecycleAdapter adapter;
    private LinearLayoutManager layoutManager2;
    TextView mCvCountdownView;
    private CountDownReceiver countDownReceiver;
    private String slug;
    private MyPagerAdapter adapterViewPager;
    private ViewPagerCustomDuration vpPager;

    QuizKeeper quizKeeper;
    private LoadingDialog waitingDialog;
    private String roundId;

    QuizList quizList = null;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.mipmap.menu);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_bar);
        slug = getIntent().getStringExtra(SLUG_EXTRA);
        quizKeeper = new QuizKeeper(slug);
        viewResultMode = getIntent().getBooleanExtra(VIEW_RESULT_MODE, false);


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        rvQuestionList = findViewById(R.id.rvQuestionList);
        mCvCountdownView = findViewById(R.id.mCvCountdownView);
        View btBottomRight = findViewById(R.id.btBottomRight);
        View btBottomLeft = findViewById(R.id.btBottomLeft);

        View tvBottomButton = findViewById(R.id.tvBottomButton);

        TextView txSubmit = findViewById(R.id.txSubmit);
        txSubmit.setText(viewResultMode ? R.string.finish : R.string.submit);

        vpPager = findViewById(R.id.vpQuizzes);
        vpPager.setPageTransformer(true, new ZoomOutPage());

        adapterViewPager = new MyPagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);
        vpPager.setScrollDuration(500);

        vpPager.addOnPageChangeListener(this);

        adapter = new QuestionStateRecycleAdapter(this, vpPager, drawer);
        rvQuestionList.setAdapter(adapter);
        layoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvQuestionList.setLayoutManager(layoutManager2);
        rvQuestionList.setItemAnimator(new DefaultItemAnimator());


        btBottomRight.setOnClickListener(view -> {
            int currentIndex = vpPager.getCurrentItem();
            if (currentIndex < adapterViewPager.getCount() - 1) {
                vpPager.setCurrentItem(currentIndex + 1);
            }
        });

        tvBottomButton.setOnClickListener(view -> finish());

        btBottomLeft.setOnClickListener(view -> {
            if (vpPager.getCurrentItem() > 0) {
                vpPager.setCurrentItem(vpPager.getCurrentItem() - 1);
            }
        });

        txSubmit.setOnClickListener(view -> {
            if (viewResultMode) {
                openRatingActivity();
            } else {
                submitClick(view);
            }
        });

        waitingDialog = new LoadingDialog(this, "spirit_geek.json", R.string.waiting);
        waitingDialog.show();


        NetworkUtils.getQuizListDescription(slug, new IOnlyDescriptionResponse() {
            @Override
            public void onSuccess(QuizList quizList) {
                PlayerActivity.this.quizList = quizList;

                NetworkUtils.getQuizListOnlyView(slug, new IOnlyViewResponse() {
                    @Override
                    public void onSuccess(Quiz[] quizzes) {
                        quizKeeper.setQuizzes(quizzes, quizList);
                        adapterViewPager.notifyDataSetChanged();
                        adapter.updateQuizKeeper(quizKeeper);
                        adapter.notifyDataSetChanged();
                        if (viewResultMode) {
                            NetworkUtils.getQuizListCorrectAnswer(slug, new ICorrectAnswerResponse() {
                                @Override
                                public void onSuccess(CorrectAnswer[] correctAnswers) {
                                    dismissProgressDialog();
                                    quizKeeper.setCorrectAnswer(correctAnswers);
                                    adapterViewPager.notifyDataSetChanged();
                                    adapter.updateQuizKeeper(quizKeeper);
                                    adapter.notifyDataSetChanged();

                                }

                                @Override
                                public void onFailed(String message) {
                                    dismissProgressDialog();
                                    Log.e(TAG, message);
                                    Toast.makeText(PlayerActivity.this, "Có vấn đề về kết nối trong quá trình lấy thông tin đề thi. Vui lòng thử lại", Toast.LENGTH_LONG).show();
                                }
                            });
                            NetworkUtils.getFirstHistory(slug, new IFirstHistoryResponse() {
                                @Override
                                public void onSuccess(History correctAnswers) {
                                    quizKeeper.updateSelectedAnswer(correctAnswers.getQuestions(), correctAnswers.getHistories());
                                    adapterViewPager.notifyDataSetChanged();
                                    adapter.updateQuizKeeper(quizKeeper);
                                    adapter.notifyDataSetChanged();
                                    final int score = correctAnswers.getScore();
                                    final int totalQuestion = correctAnswers.getQuestions().length;

                                    showScore(score, totalQuestion);
                                }

                                @Override
                                public void onFailed(String message) {
                                    Log.e(TAG, message);
                                    Toast.makeText(PlayerActivity.this, "Có vấn đề về kết nối trong quá trình lấy thông tin đề thi. Vui lòng thử lại", Toast.LENGTH_LONG).show();
                                }
                            });
                        } else {
                            dismissProgressDialog();
                        }
                        if (!viewResultMode) {
                            final long time = quizList.getTime() * 60 * 1000;
                            mCvCountdownView.setText(Common.toTimeString(time));
                            startTimerService(time);
                            registerPlayerBroadCastReceiver();
                        }
                    }

                    @Override
                    public void onFailed(String message) {
                        dismissProgressDialog();
//                        Log.e(TAG, message);
                        Toast.makeText(PlayerActivity.this, "Có vấn đề về kết nối trong quá trình lấy thông tin đề thi. Vui lòng thử lại", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onFailed(String message) {
                dismissProgressDialog();
                Log.e(TAG, message);

                Toast.makeText(PlayerActivity.this, "Có vấn đề về kết nối trong quá trình lấy thông tin đề thi. Vui lòng thử lại", Toast.LENGTH_LONG).show();
            }
        });

        if (!viewResultMode) {
            NetworkUtils.countRound(slug, new ICountRound2() {

                @Override
                public void onSuccess(CountRoundData roundData) {
//                Toast.makeText(PlayerActivity.this, roundData.getId(), Toast.LENGTH_LONG).show();
                    roundId = roundData.getId();
                }

                @Override
                public void onFailed(String message) {
                    Log.e(TAG, message);
                }
            });
        }
    }

    private void dismissProgressDialog() {
        if (waitingDialog != null && waitingDialog.isShowing()) {
            waitingDialog.dismiss();
        }
    }


    private void openRatingActivity() {
        Intent intent = new Intent(this, RatingActivity.class);
        intent.putExtra(IConstants.SLUG_EXTRA, slug);
        startActivityForResult(intent, IConstants.RATING_REQUEST_CODE);
    }

    private void showScore(int score, int totalQuestion) {
        findViewById(R.id.mCvScore).setVisibility(View.VISIBLE);
        mCvCountdownView.setVisibility(View.GONE);
        final float percent = totalQuestion != 0 ? score * 100 / totalQuestion : 0;
        final TestReportDialog.LabelType labelType = Common.getLabelByPercent(percent);
        ((TextView) findViewById(R.id.mCvScore)).setText(Common.formatScore(PlayerActivity.this, labelType, score, totalQuestion));
    }

    private void showScore() {

        NetworkUtils.getQuizListDescription(slug, new IOnlyDescriptionResponse() {
            @Override
            public void onSuccess(QuizList quizList) {
                long timeToOpen = quizList.newCanViewResult();
                Log.d(TAG, "timeToOpen = " + timeToOpen + " quizList = " + quizList.toString());
                if (timeToOpen > 0) {
                    openClockActivity(timeToOpen);
                } else {

                    showScore(quizKeeper.countScore(), quizKeeper.totalQuestion());

                    TextView txSubmit = findViewById(R.id.txSubmit);
                    txSubmit.setText(R.string.finish);

                    (new TestReportDialog(PlayerActivity.this, quizKeeper.countScore(), quizKeeper.totalQuestion()) {

                        @Override
                        protected void onShareLink() {

                        }

                        @Override
                        protected void onShareAction(Bitmap bitmap) {

                        }

                        @Override
                        public void onPositiveAction() {

                        }
                    }).show();
                }
            }

            @Override
            public void onFailed(String message) {

            }
        });

    }

    private void openClockActivity(long end_at) {
        Intent intent = new Intent(this, ClockActivity.class);
        intent.putExtra(IConstants.TIME_EXTRA, end_at);
        startActivityForResult(intent, IConstants.CLOCK_ACTIVITY_REQUEST_CODE);
    }

    public void submitClick(View view) {
        if (viewResultMode) {
            return;
        }
        new ConfirmationDialog(this) {
            @Override
            protected void onPositiveAction() {
                stopTimerService();
                submit();
                dismiss();
            }

            @Override
            protected void onNegativeAction() {
                dismiss();
            }

            @Override
            protected String okButton() {
                return getString(R.string.sure);
            }

            @Override
            protected String getDescription() {
                return getString(R.string.make_sure);
            }

            @Override
            protected String cancelButton() {
                return getString(R.string.cancel);
            }
        }.show();

    }

    private void submit() {
        waitingDialog = new LoadingDialog(this, "stopwatch.json", R.string.waiting_submit);
        waitingDialog.show();
        NetworkUtils.getQuizListCorrectAnswer(slug, new ICorrectAnswerResponse() {
            @Override
            public void onSuccess(CorrectAnswer[] correctAnswers) {
                Log.d(TAG, "TIENTN onSuccess ==============");
                viewResultMode = true;
                quizKeeper.setCorrectAnswer(correctAnswers);
                adapterViewPager.notifyDataSetChanged();
                adapter.updateQuizKeeper(quizKeeper);
                adapter.notifyDataSetChanged();
                NetworkUtils.submit(quizKeeper.getPostHistory(roundId), new ICountRound() {
                    @Override
                    public void onSuccess(String responseStr) {
                        Log.d(TAG, responseStr);
                        dismissProgressDialog();
                        showScore();
                    }

                    @Override
                    public void onFailed(String message) {
                        dismissProgressDialog();
                        Log.e(TAG, message);
                    }
                });

            }

            @Override
            public void onFailed(String message) {
                dismissProgressDialog();
                Toast.makeText(PlayerActivity.this, message, Toast.LENGTH_SHORT).show();
                viewResultMode = true;
                Log.e(TAG, message);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        layoutManager2.scrollToPosition(position);
        adapter.setCurrent(position);
        current = position;

    }

    private void startTimerService(long time) {
        Intent serviceIntent = new Intent(this, TimerService.class);
        serviceIntent.putExtra(IConstants.TIME_EXTRA, time);
        startService(serviceIntent);
    }

    public void stopTimerService() {
        Intent serviceIntent = new Intent(this, TimerService.class);
        stopService(serviceIntent);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter {

        MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return quizKeeper.totalQuestion();
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            CorrectAnswer correctAnswer = quizKeeper.getCorrectAnswerAt(position);
            int selected = quizKeeper.getSelectedAnswerAt(position);
            Quiz quiz = quizKeeper.getQuizAt(position);
            return QuizFragment.newInstance(position + 1, quiz, correctAnswer, selected, viewResultMode);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissProgressDialog();
        if (countDownReceiver != null) {
            unregisterReceiver(countDownReceiver);
            countDownReceiver = null;
        }
    }


    public void nextQuiz() {
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, IConstants.SELECT_DELAY);
    }

    private int current;
    private Runnable runnable = () -> vpPager.setCurrentItem(++current);

    private Handler handler = new Handler();

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        Log.d(TAG, event.toString());
        selectedAnswer.put(event.id, event.answer);
        quizKeeper.updateSelectedAnswer(event.id, event.answer);
        adapterViewPager.notifyDataSetChanged();
        adapter.updateQuizKeeper(quizKeeper);
        adapter.notifyDataSetChanged();
        nextQuiz();
    }

    public void registerPlayerBroadCastReceiver() {
        if (countDownReceiver != null) {
            return;
        }
        Log.d(TAG, "registerPlayerBroadCastReceiver");
        countDownReceiver = new CountDownReceiver();
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(getPackageName() + TimerService.ACTION_BROADCAST_TIMER);
        registerReceiver(countDownReceiver, mIntentFilter);
    }

    private class CountDownReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive");
            if (intent != null) {
                long time = intent.getLongExtra(TimerService.DATA_TIMER, 0);
                Log.d(TAG, "Received time = " + time);
                if (time > 0) {
                    Log.d(TAG, "formatted time: " + Common.toTimeString(time));
                    mCvCountdownView.setText(Common.toTimeString(time));
                } else {
                    submit();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IConstants.RATING_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            finish();
        }
        if (requestCode == IConstants.CLOCK_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            finish();
        }
    }
}
