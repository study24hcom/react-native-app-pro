package com.tungtung.player.network;

/**
 * This file is created by Tien Nguyen on 1/26/18.
 */

public interface ICountRound {
    void onSuccess(String responseStr);

    void onFailed(String message);
}
