package com.tungtung.player.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tungtung.player.R;
import com.tungtung.player.utils.Common;


/**
 * This file is created by Tien Nguyen on 1/29/18.
 */

public class QuizStateView extends LinearLayout {

    TextView tvQuestion;
    private TextView tvQuizState;
    private View clMainQuizStateView;

    public void setState(State state, boolean current, Integer selected) {
        switch (state) {
            case WRONG:
                tvQuizState.setText(getString(R.string.wrong));
                tvQuizState.setTextColor(getResources().getColor(R.color.red_color));
                break;
            case NORMAL:
                tvQuizState.setText(getString(R.string.normal));
                tvQuizState.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
            case CHECKED:
                tvQuizState.setText(String.format(getString(R.string.checked), Common.getLetter(selected, false)));
                tvQuizState.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
            case CORRECT:
                tvQuizState.setText(getString(R.string.correct));
                tvQuizState.setTextColor(getResources().getColor(R.color.correct_answer));
                break;
            case NOT_CHOSEN:
                tvQuizState.setText(getString(R.string.not_chosen));
                tvQuizState.setTextColor(getResources().getColor(R.color.user_info_orange));
                break;
            default:
                break;
        }
        setChosen(current);
    }

    private void setChosen(boolean current) {
        clMainQuizStateView.setBackgroundColor(getResources().getColor(current ? R.color.line_light : android.R.color.transparent));
    }

    public String getString(int resId) {
        return getContext().getString(resId);
    }

    public void setText(int text) {
        tvQuestion.setText(String.format(getContext().getString(R.string.question), text));
    }

    public enum State {
        NORMAL,
        CHECKED,
        WRONG,
        CORRECT,
        NOT_CHOSEN
    }

    public QuizStateView(Context context) {
        super(context);
        init();
    }

    public QuizStateView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    void init() {
        View view = inflate(getContext(), R.layout.quiz_state_view, null);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        addView(view, layoutParams);
        tvQuestion = findViewById(R.id.tvQuestion);
        tvQuizState = findViewById(R.id.tvQuizState);
        clMainQuizStateView = findViewById(R.id.clMainQuizStateView);
    }
}
