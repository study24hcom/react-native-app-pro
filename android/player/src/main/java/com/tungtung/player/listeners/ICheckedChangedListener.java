package com.tungtung.player.listeners;

import com.tungtung.player.views.MyRadioButton;

/**
 * This file is created by Tien Nguyen on 4/10/17.
 */
public interface ICheckedChangedListener {
    void onCheckedChanged(MyRadioButton button, boolean checked);
}
