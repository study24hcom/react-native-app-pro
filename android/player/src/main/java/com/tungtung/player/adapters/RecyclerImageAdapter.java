package com.tungtung.player.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tungtung.player.PlayerApplication;

import java.util.List;

/**
 * This file is created by Tien Nguyen on 2/8/18.
 */

public class RecyclerImageAdapter extends RecyclerView.Adapter<RecyclerImageAdapter.ViewHolder> {
    public RecyclerImageAdapter(Context context) {
        this.context = context;
    }

    private Context context;

    public List<String> getData() {
        return data;
    }

    private List<String> data;

    @Override
    public RecyclerImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(new ImageView(context));
    }

    @Override
    public void onBindViewHolder(RecyclerImageAdapter.ViewHolder holder, int position) {
        Glide.with(PlayerApplication.getInstance()).load(data.get(position)).into(holder.imgViewIcon);
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgViewIcon;

        public ViewHolder(ImageView itemLayoutView) {
            super(itemLayoutView);
            imgViewIcon = itemLayoutView;
        }
    }
}
