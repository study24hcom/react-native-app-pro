package com.tungtung.player.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.tungtung.player.R;
import com.tungtung.player.utils.Common;
import com.zzhoujay.markdown.MarkDown;

import java.util.Random;


public abstract class TestReportDialog extends Dialog {
    private static final String TAG = TestReportDialog.class.getSimpleName();
    private final int score;
    private final int totalQuestion;

    public TestReportDialog(Context context, int score, int totalQuestion) {
        super(context);
        this.score = score;
        this.totalQuestion = totalQuestion;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_test_report);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final float percent = score * 100 / totalQuestion;
        Log.d(TAG, "score = " + score + " total = " + totalQuestion + "percent = " + percent);
        final LabelType labelType = Common.getLabelByPercent(percent);
        int color = Common.getColor(getContext(), labelType);

        RoundCornerProgressBar progressBar = findViewById(R.id.progressStatus);
        progressBar.setMax(100.0f);
        progressBar.setProgress(percent);
        progressBar.setProgressColor(color);

        TextView textViewLabel = findViewById(R.id.textViewScoreLabel);
        textViewLabel.setText(getLabel(labelType));
        textViewLabel.setTextColor(color);

        TextView textViewScore = findViewById(R.id.textViewScore);
        final Spanned htmlString = Common.formatScore(getContext(), labelType, score, totalQuestion);
        textViewScore.setText(htmlString);

        findViewById(R.id.textViewMessage).post(() -> {
//            Spanned spanned = MarkDown.fromMarkdown(getMoreInfo(labelType), source -> null, findViewById(R.id.textViewMessage));
            ((TextView) findViewById(R.id.textViewMessage)).setText(getMoreInfo(labelType));
        });
//        findViewById(R.id.buttonConfirm).setOnClickListener(v -> {
//            dismiss();
//            onPositiveAction();
//        });
//
//        findViewById(R.id.buttonShare).setOnClickListener(v -> {
//            View targetView = findViewById(R.id.scoreView);
//            Bitmap bitmap = Bitmap.createBitmap(targetView.getWidth(),
//                    targetView.getHeight(),
//                    Bitmap.Config.ARGB_8888);
//            Canvas bitmapCanvas = new Canvas(bitmap);
//            targetView.draw(bitmapCanvas);
//            BitmapDrawable d = new BitmapDrawable(getContext().getResources(), bitmap);
//            dismiss();
//            onShareAction(d.getBitmap());
//        });
//
//        findViewById(R.id.buttonShareLink).setOnClickListener(v -> onShareLink());
    }

    protected abstract void onShareLink();

    protected abstract void onShareAction(Bitmap bitmap);


    private String getLabel(LabelType type) {
        switch (type) {
            case BAD:
                return getContext().getString(R.string.very_bad);
            case MEDIUM:
                return getContext().getString(R.string.medium);
            case GOOD:
                return getContext().getString(R.string.good);
            case EXCELLENCE:
                return getContext().getString(R.string.excellence);
            default:
                return getContext().getString(R.string.very_bad);
        }
    }

    private String getMoreInfo(LabelType labelType) {
        switch (labelType) {
            case BAD:
                return getRandomString(R.array.bad_array);
            case MEDIUM:
                return getRandomString(R.array.medium_array);
            case GOOD:
                return getRandomString(R.array.good_array);
            case EXCELLENCE:
                return getRandomString(R.array.excellence_array);
            default:
                return getRandomString(R.array.bad_array);
        }
    }


    private String getRandomString(int resourceId) {
        final String[] badArray = getContext().getResources().getStringArray(resourceId);
        int random = (new Random()).nextInt(badArray.length);
        return badArray[random];
    }


    public enum LabelType {
        BAD,
        MEDIUM,
        GOOD,
        EXCELLENCE
    }

    public abstract void onPositiveAction();

    public static Spanned formatScore(Context context, TestReportDialog.LabelType labelType, int score, int totalQuestion) {
        final String scoreString = String.format("<font color=\"%s\">%s</font><font color=\"#8f949a\">/%s</font>",
                getColorString(context, labelType), String.valueOf(score), String.valueOf(totalQuestion));
        Spanned htmlString;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            htmlString = Html.fromHtml(scoreString, Html.FROM_HTML_MODE_COMPACT);
        } else {
            htmlString = Html.fromHtml(scoreString);
        }
        return htmlString;
    }

    private static String getColorString(Context context, TestReportDialog.LabelType labelType) {
        final Resources resources = context.getResources();
        switch (labelType) {
            case BAD:
                return resources.getString(R.string.score_very_bad);
            case MEDIUM:
                return resources.getString(R.string.score_medium);
            case GOOD:
                return resources.getString(R.string.score_good);
            case EXCELLENCE:
                return resources.getString(R.string.score_excellence);
            default:
                return resources.getString(R.string.score_very_bad);
        }
    }

}
