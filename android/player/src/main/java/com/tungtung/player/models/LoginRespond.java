package com.tungtung.player.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * This file is created by Tien Nguyen on 4/27/18.
 */

public class LoginRespond implements Serializable {

    @SerializedName("success")
    boolean success;

    @SerializedName("message")
    String message;

    public String getToken() {
        return token;
    }

    @SerializedName("token")
    String token;
}
