package com.tungtung.player.adapters;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.ViewGroup;

import com.tungtung.player.models.CorrectAnswer;
import com.tungtung.player.models.player.QuizKeeper;
import com.tungtung.player.views.QuizStateView;

/**
 * This file is created by Tien Nguyen on 4/12/17.
 */
public class QuestionStateRecycleAdapter extends RecyclerView.Adapter<QuestionStateRecycleAdapter.MyViewHolder> {

    private static final String TAG = QuestionStateRecycleAdapter.class.getSimpleName();
    private final Context context;
    private ViewPager mPager;
    private final DrawerLayout drawerLayout;
    private int current = -1;
    private QuizKeeper quizKeeper;

    public QuestionStateRecycleAdapter(final Context context, ViewPager pager, DrawerLayout drawer) {
        this.context = context;
        mPager = pager;
        drawerLayout = drawer;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        QuizStateView itemView = new QuizStateView(context);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        itemView.setLayoutParams(layoutParams);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Integer selected = quizKeeper.getSelectedAnswerAt(position);

        holder.questionLabel.setText(position + 1);
        if (selected.equals(-1)) {
            holder.questionLabel.setState(QuizStateView.State.NOT_CHOSEN, current == position, selected);
        } else {
            final CorrectAnswer answer = quizKeeper.getCorrectAnswerAt(position);
            if (answer != null) {
                if (selected.equals(answer.getCorrect_answer())) {
                    holder.questionLabel.setState(QuizStateView.State.CORRECT, current == position, selected);
                } else {
                    holder.questionLabel.setState(QuizStateView.State.WRONG, current == position, selected);
                }
            } else {
                holder.questionLabel.setState(QuizStateView.State.CHECKED, current == position, selected);
            }
        }
        holder.questionLabel.setOnClickListener(view -> {
            drawerLayout.closeDrawer(Gravity.START);
            mPager.setCurrentItem(position, true);
        });
    }

    @Override
    public int getItemCount() {
        return quizKeeper != null ? quizKeeper.totalQuestion() : 0;
    }



    public void setCurrent(int current) {
        final int savedCurrent = this.current;
        this.current = current;
        notifyItemChanged(current);
        notifyItemChanged(savedCurrent);
    }

    public void updateQuizKeeper(QuizKeeper quizKeeper) {
        this.quizKeeper = quizKeeper;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        QuizStateView questionLabel;

        private MyViewHolder(QuizStateView itemView) {
            super(itemView);
            this.questionLabel = itemView;
        }

    }

}
