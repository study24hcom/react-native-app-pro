package com.tungtung.player.models;


import com.tungtung.player.models.core.Model;

import java.util.List;

/**
 * This file is created by Tien Nguyen on 4/10/17.
 */
public class CorrectAnswer extends Model {
    @Override
    public String toString() {
        return "CorrectAnswer{" +
                "_id='" + _id + '\'' +
                ", correct_answer=" + correct_answer +
                ", has_suggest_answer=" + has_suggest_answer +
                ", suggestAnswer='" + suggestAnswer + '\'' +
                '}';
    }

    private String _id;
    private int correct_answer;
    private boolean has_suggest_answer;
    private String suggestAnswer;

    private List<String> images;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }


    public boolean getHas_suggest_answer() {
        return has_suggest_answer;
    }

    public void setHas_suggest_answer(boolean has_suggest_answer) {
        this.has_suggest_answer = has_suggest_answer;
    }


    public int getCorrect_answer() {
        return correct_answer;
    }

    public void setCorrect_answer(int correct_answer) {
        this.correct_answer = correct_answer;
    }

    public String getSuggestAnswer() {
        return suggestAnswer.trim();
    }

    public void setSuggestAnswer(String suggestAnswer) {
        this.suggestAnswer = suggestAnswer;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public List<String> getImages() {
        return images;
    }
}
