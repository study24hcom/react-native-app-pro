package com.tungtung.player;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daquexian.flexiblerichtextview.FlexibleRichTextView;
import com.tungtung.player.adapters.RecyclerImageAdapter;
import com.tungtung.player.listeners.ICheckedChangedListener;
import com.tungtung.player.models.CorrectAnswer;
import com.tungtung.player.models.MessageEvent;
import com.tungtung.player.models.Quiz;
import com.tungtung.player.utils.Common;
import com.tungtung.player.views.MyRadioButton;
import com.tungtung.player.views.MyRadioButtonState;

import org.greenrobot.eventbus.EventBus;

/**
 * This file is created by Tien Nguyen on 1/25/18.
 */

public class QuizFragment extends Fragment implements ICheckedChangedListener {
    private static final String TAG = QuizFragment.class.getSimpleName();
    // Store instance variables
    private int index;
    private Quiz quiz;

    TextView textViewQuestion;
    private SparseArray<MyRadioButton> radioButtons = new SparseArray<>();
    private int selectedAnswer;
    private CorrectAnswer correctAnswer;
    private boolean viewResultMode;
    private RecyclerImageAdapter adapter;

    // newInstance constructor for creating fragment with arguments
    public static QuizFragment newInstance(int page, Quiz quiz, CorrectAnswer correctAnswer, int selected, boolean viewResultMode) {
        QuizFragment fragmentFirst = new QuizFragment();
        Bundle args = new Bundle();
        args.putInt("QUIZ_INDEX", page);
        args.putSerializable("QUIZ_DATA", quiz);
        args.putSerializable("CORRECT_ANSWER_DATA", correctAnswer);
        args.putSerializable("SELECTED_ANSWER", selected);
        args.putSerializable("VIEW_RESULT_MODE", viewResultMode);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_quiz, container, false);
        textViewQuestion = rootView.findViewById(R.id.textViewQuestion);
        FlexibleRichTextView richTextResult = rootView.findViewById(R.id.richTextResult);
        FlexibleRichTextView quizContent = rootView.findViewById(R.id.richTextView);
        LinearLayout imageInQuiz = rootView.findViewById(R.id.imageInQuiz);
        LinearLayout imageInSuggestAnswer = rootView.findViewById(R.id.imageInSuggestAnswer);
        View resultForm = rootView.findViewById(R.id.resultForm);
        RadioGroup radioGroup = rootView.findViewById(R.id.radioGroupQuiz);

        index = getArguments().getInt("QUIZ_INDEX");
        quiz = (Quiz) getArguments().getSerializable("QUIZ_DATA");
        correctAnswer = (CorrectAnswer) getArguments().getSerializable("CORRECT_ANSWER_DATA");
        selectedAnswer = getArguments().getInt("SELECTED_ANSWER");
        viewResultMode = getArguments().getBoolean("VIEW_RESULT_MODE", false);

        Log.d(TAG, String.valueOf(quiz.getImages()));
        textViewQuestion.setText(String.format(getString(R.string.question), index));

        for (String url : quiz.getImages()) {
            ImageView imageView = new ImageView(getContext());
//            imageView.setPadding(0, 0, 0, 10);
            Glide.with(getContext()).load(url).into(imageView);
            imageInQuiz.addView(imageView);
        }
        imageInQuiz.setVisibility(quiz.getImages().size() == 0 ? View.GONE : View.VISIBLE);

        if (correctAnswer != null) {
            for (String url : correctAnswer.getImages()) {
                ImageView imageView = new ImageView(getContext());
//                imageView.setPadding(0, 0, 0, 10);
                Glide.with(getContext()).load(url).into(imageView);
                imageInSuggestAnswer.addView(imageView);
            }
            imageInSuggestAnswer.setVisibility(correctAnswer.getImages().size() == 0 ? View.GONE : View.VISIBLE);

        }
//        Common.decorateRecycleView(imageInQuiz, getContext(), DividerItemDecoration.HORIZONTAL, DividerItemDecoration.HORIZONTAL);

        quizContent.setText(quiz.getQuestion());

        int i = 0;
        for (Quiz.Answer answer : quiz.getAnswer()) {
            MyRadioButton radioButton = new MyRadioButton(getContext());
            radioButton.setEnabled(!viewResultMode);
            radioButton.setOnCheckedChangeListener(this);
            radioButton.setAnswer(answer, Common.getLetter(i, true));
            radioButtons.put(i, radioButton);
            radioGroup.addView(radioButton, new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            i++;
        }

        if (selectedAnswer != -1 && correctAnswer != null && correctAnswer.getCorrect_answer() == selectedAnswer) {
            radioButtons.get(selectedAnswer).setChecked(MyRadioButtonState.CORRECT);
        } else if (selectedAnswer != -1 && correctAnswer != null && correctAnswer.getCorrect_answer() != selectedAnswer) {
            radioButtons.get(selectedAnswer).setChecked(MyRadioButtonState.WRONG);
        } else if (selectedAnswer != -1) {
            radioButtons.get(selectedAnswer).setChecked(MyRadioButtonState.CHECKED);
        }

        Log.d(TAG, String.valueOf(correctAnswer));
        if (correctAnswer != null && !TextUtils.isEmpty(correctAnswer.getSuggestAnswer()) && correctAnswer.getHas_suggest_answer()) {
            richTextResult.setText(correctAnswer.getSuggestAnswer());
            resultForm.setVisibility(View.VISIBLE);
        }

        if (correctAnswer != null && radioButtons.get(correctAnswer.getCorrect_answer()) != null) {
            radioButtons.get(correctAnswer.getCorrect_answer())
                    .setChecked(selectedAnswer != -1 ? MyRadioButtonState.CORRECT : MyRadioButtonState.NOT_CHOSEN);
        }

        if (selectedAnswer == -1 && correctAnswer != null && radioButtons.get(correctAnswer.getCorrect_answer()) != null) {
            radioButtons.get(correctAnswer.getCorrect_answer()).setChecked(MyRadioButtonState.NOT_CHOSEN);
        }
        return rootView;
    }


    @Override
    public void onCheckedChanged(MyRadioButton button, boolean checked) {
        if (checked) {
            for (int i = 0; i < radioButtons.size(); i++) {
                radioButtons.get(i).setChecked(MyRadioButtonState.NORMAL);
            }
            button.setChecked(MyRadioButtonState.CHECKED);
            runAnimation(button);
            selectedAnswer = radioButtons.indexOfValue(button);
        }
        EventBus.getDefault().post(new MessageEvent(quiz.get_id(), selectedAnswer));
    }

    private void runAnimation(View button) {
        Animation a = AnimationUtils.loadAnimation(this.getContext(), R.anim.quiz_view_fade_in);
        a.reset();
        button.clearAnimation();
        button.startAnimation(a);
    }
}
