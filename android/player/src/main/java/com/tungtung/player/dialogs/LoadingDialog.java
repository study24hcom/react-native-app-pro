package com.tungtung.player.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.Window;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.tungtung.player.R;
import com.tungtung.player.utils.IConstants;


/**
 * This file is created by Tien Nguyen on 4/17/17.
 */

public class LoadingDialog extends Dialog {
    private static final String TAG = LoadingDialog.class.getSimpleName();
    private final String animationPath;

    private TextView loadingInfo;
    private LottieAnimationView animationView;

    private Handler handler = new Handler();
    private int step = 1;

    public LoadingDialog(@NonNull Context context, String animationPath, int stringId) {
        super(context);
        this.animationPath = animationPath;
        this.stringId = stringId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_loading_tung_tung_style);
        setCancelable(false);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        loadingInfo = findViewById(R.id.tvLoadingInfo);
        animationView = findViewById(R.id.animationView);

        setAnimationImage(animationPath);

        handler.postDelayed(loading, IConstants.LOADING_BAR_INCREASE_DELAY);
    }

    private int stringId;
    private Runnable loading = new Runnable() {
        @Override
        public void run() {
            loadingInfo.setText(String.format(getContext().getString(stringId), step == 3 ? "..." : step == 2 ? ".." : "."));
            if (step < 3) {
                step++;
            } else {
                step = 1;
            }
            handler.removeCallbacks(loading);
            handler.postDelayed(loading, IConstants.LOADING_BAR_INCREASE_DELAY);
        }
    };

    @Override
    public void dismiss() {
        super.dismiss();
        handler.removeCallbacks(loading);
    }

    private void setAnimationImage(String image) {
        animationView.setAnimation(image);
//        animationView.startActionMode(new ActionMode.Callback() {
//            @Override
//            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
//                return false;
//            }
//
//            @Override
//            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
//                return false;
//            }
//
//            @Override
//            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
//                return false;
//            }
//
//            @Override
//            public void onDestroyActionMode(ActionMode actionMode) {
//
//            }
//        });
    }
}
