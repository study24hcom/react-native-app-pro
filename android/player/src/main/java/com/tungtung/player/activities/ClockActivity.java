package com.tungtung.player.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.tungtung.player.R;
import com.tungtung.player.utils.IConstants;

import cn.iwgang.countdownview.CountdownView;

/**
 * This file is created by Tien Nguyen on 5/6/18.
 */

public class ClockActivity extends AppCompatActivity {

    View llBackButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clock);
        llBackButton = findViewById(R.id.llBackButton);
        llBackButton.setOnClickListener(view -> {
            onBackPressed();
        });

        long timeToOpen = getIntent().getLongExtra(IConstants.TIME_EXTRA, 0);

        CountdownView mCvCountdownViewTest2 = findViewById(R.id.countDownView);
        mCvCountdownViewTest2.start(timeToOpen);

    }


    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }
}
