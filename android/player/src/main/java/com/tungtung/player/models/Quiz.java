package com.tungtung.player.models;

/**
 * This file is created by Tien Nguyen on 1/26/18.
 */

import android.util.Log;

import com.tungtung.player.models.core.Model;
import com.tungtung.player.models.json.JsonArray;
import com.tungtung.player.models.json.JsonObject;

import java.util.ArrayList;
import java.util.List;


/**
 * This file is created by Cash on 3/29/17.
 */
public class Quiz extends Model {
    private List<String> images;

    public Quiz() {
    }

    private static final String TAG = Quiz.class.getName();
    private String _id;
    private String question;
    private List<Answer> answer;

    public static class Answer extends Model {

        public Answer() {

        }

        String answer;

        public String getAnswer() {
            return answer;
        }

        public List<String> getImages() {
            return images;
        }

        List<String> images;

        public void setAnswer(String answer) {
            this.answer = answer;
        }

        public void setImages(List<String> images) {
            this.images = images;
        }
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Answer> getAnswer() {
        return answer;
    }

    public void setAnswer(List<Answer> answer) {
        this.answer = answer;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public static Quiz parse(JsonObject obj) {
        Quiz quiz = new Quiz();
        if (obj != null) {
            String id = obj.getString("_id", "");
            quiz.set_id(id);
            String questionText = getText(obj, "question");
            quiz.setQuestion(questionText);
            List<String> image = getImage(obj, "question");
            Log.d(TAG, "getImage =======" + String.valueOf(image));
            quiz.setImages(image);

            JsonArray jsonArray = obj.getJsonArray("answers");
            List<Answer> answers = new ArrayList<>();
            for (int i = 0; i < jsonArray.size(); i++) {
                JsonObject anObj = jsonArray.get(i);
                Answer answer = new Answer();
                answer.setAnswer(getText(anObj, "answer"));
                answer.setImages(getImage(anObj, "answer"));
                answers.add(answer);
            }
            quiz.setAnswer(answers);
        }
        return quiz;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public List<String> getImages() {
        return images;
    }

    public void replaceGroupInfo(int start, int end) {
        question = question.replaceAll("[{][@][\\d]+[}] - [{][@][\\d]+[}]",
                String.valueOf(start) + " - " + String.valueOf(end));
    }
}
