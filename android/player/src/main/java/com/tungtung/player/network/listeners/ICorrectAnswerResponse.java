package com.tungtung.player.network.listeners;


import com.tungtung.player.models.CorrectAnswer;

/**
 * This file is created by Tien Nguyen on 1/26/18.
 */

public interface ICorrectAnswerResponse {
    void onSuccess(CorrectAnswer[] correctAnswers);
    void onFailed(String message);
}
