package com.tungtung.player.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.tungtung.player.R;


public class ConfirmationDialog extends Dialog {
    public ConfirmationDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        setContentView(R.layout.dialog_tung_tung_style);
        ((TextView) findViewById(R.id.tvDescription)).setText(getDescription());

        ((TextView) findViewById(R.id.okView)).setText(okButton());
        ((TextView) findViewById(R.id.cancelView)).setText(cancelButton());
        findViewById(R.id.okView).setOnClickListener(v -> onPositiveAction());

        findViewById(R.id.cancelView).setOnClickListener(view -> onNegativeAction());
    }

    protected void onPositiveAction() {

    }

    protected void onNegativeAction() {

    }

    protected String getDescription() {
        return "";
    }

    protected String okButton() {
        return getContext().getString(R.string.view_result);
    }

    protected String cancelButton() {
        return getContext().getString(R.string.done);
    }

}
